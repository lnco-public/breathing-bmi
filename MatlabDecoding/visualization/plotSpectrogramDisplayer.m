
function plotSpectrogramDisplayer(data,time,freq,label,line2Plot,currentFigure)
%PLOTSPECTROGRAMDISPLAYER Summary of this function goes here
%   Detailed explanation goes here


ch = [ 0 0 1 0 0; ...
    2  3 4 5 6; ...
    7 8 9 10 11; ...
    12 13 14 15 16];


if nargin > 5
    figure()
end

imagesc('XData',time,'YData',freq,'CData',data);
colormap('jet');
xlabel('Time[s]'); ylabel('Frequency[Hz]');
axis([time(1) time(end) freq(1) freq(end)]);
title(label)
hold all


if ~isempty(line2Plot)
    for iPosition = 1:length(line2Plot)
        thisTime = line2Plot(iPosition);
        line([thisTime thisTime],[freq(1) freq(end)],'color','k','LineWidth',4);
    end
end
if nargin > 5
    figure(currentFigure)
end

end
