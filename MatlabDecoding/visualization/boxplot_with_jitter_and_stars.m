function boxplot_with_jitter_and_stars(data,star)
%BOXPLOT_WITH_JITTER_AND_STARS Summary of this function goes here

if nargin == 2
    star = true;
end


boxplot(data,'notch','on');

set(findobj(gca,'type','line'),'linew',2)
hold on

if star
    % Significant results to show
    if size(data,2) > 2
        [~,~,stats] =   kruskalwallis(data,[],'off');
        c = multcompare(stats,'Display','off');
    else
        c = ranksum(data(:,1),data(:,2));
    end
    index_significant = find(c(:,end)< 0.05);
    
    for ii = 1:length(index_significant)
        thisTest = index_significant(ii);
        if size(data,2) > 2
            sigstar({c(thisTest,1:2)},c(thisTest,end))
        else
            sigstar({[1,2]},[c])
        end
    end
end
nVar = size(data,2);

for iVar = 1:nVar
    
    theseData = data(:,iVar);
    x = rand(1,length(theseData))/4+iVar-0.5/4;
    plot(x,theseData,'.',...
        'LineWidth',5,...
        'MarkerSize',10,...
        'MarkerFaceColor',[0.4941    0.4941    0.4941],...
        'MarkerEdgeColor',[0.4941    0.4941    0.4941])
    hold on;
end

end

