function plot_trials_for_one_channel(epochs,strChannel,interval)

nEpoch = size(epochs.data,1);
ystart = zeros(nEpoch,1);
iChannel = epochs.map_ch(strChannel);


for iEpoch = 1:nEpoch
    thisEpoch = squeeze(epochs.data(iEpoch,iChannel,:));
    plot(epochs.time, thisEpoch -(iEpoch-1)*interval);
    ystart(iEpoch) = nanmean( thisEpoch-(iEpoch-1)*interval);
    hold on;
end
hold off;
xlabel('Time(sec)'); ylabel('#Trials')
title(['Trials for channel ',strChannel])
[ystart,~]= sort(ystart);

set(gca,'YTick',ystart(1:5:end),'YTickLabel',1:5:nEpoch)

end


