function [theseEpochs,timeFinal,matrix] = plot_singleTrial(epochs,channelID,varargin)

% TO DO
% - event = 999 legend
% - comment + code

inputs = parse_my_inputs(epochs,channelID,varargin{:});
if ndims(epochs.data) == 2 
theseEpochs = epochs.data; 
else
theseEpochs = squeeze(epochs.data(:,inputs.channelID,:));
end
color = {'g*','b*','r*','k*','c*','m*','y*','w*'};

[nEpoch,nTime] = size(theseEpochs);
ttime = 1:nTime;
if ~isempty(inputs.showEvent)
    for iEvent = 1:length(inputs.showEvent)
        timeFinal{iEvent} = [];
        matrix{iEvent}= [];
        for iEpoch = 1:nEpoch
            index = epochs.event(iEpoch).name == inputs.showEvent(iEvent);
            iPos = epochs.event(iEpoch).position(index);
            ii = epochs.event(iEpoch).name == epochs.alignEvent;
            pos0 = epochs.event(iEpoch).position(ii);
            %             matrix{iEvent} = [matrix{iEvent} iEpoch*ones(1,length(iPos))];
            matrix{iEvent} = [matrix{iEvent} iEpoch];
            
            if length(ttime(iPos)) > 1
                %                 newTime = ttime(iPos)';
                newTime = ttime(iPos(1));
                
            elseif length(ttime(iPos)) == 1
                newTime = ttime(iPos);
            else
                newTime = NaN;
                matrix{iEvent} = [matrix{iEvent} NaN];
            end
            timeFinal{iEvent} = [timeFinal{iEvent};newTime];
        end
    end
end


% sorting based on difference of time between selected event and origin
% event
if ~isempty(inputs.sortLength)
    [timeFinal{inputs.sortLength},index] = sort(timeFinal{inputs.sortLength});
    array = setdiff(2:length(inputs.showEvent),inputs.sortLength);
    for iEvent = array
        timeFinal{iEvent} = timeFinal{iEvent}(index);
    end
    theseEpochs = theseEpochs(index,:);
  
end



% normalization
if inputs.normalize
    theseEpochs = zscore(theseEpochs')';
end

% smoothing
if ~isempty(inputs.smoothing)
    for iTime = 1:nTime
        theseEpochs(:,iTime) =  sgolayfilt(theseEpochs(:,iTime),1,inputs.smoothing);
    end
end


%% Single Trial
if inputs.showAverage
    subplot(10,1,1:9)
end
imagesc(theseEpochs);
colormap('jet');
% colorbar
if ~isempty(inputs.caxis)
    caxis(inputs.caxis)
end
ylabel('#Trial');xlabel('Time(s)');

try
str = strcat('Single trial ',epochs.label(channelID));
catch
    fprintf('Raw time not available')
end

if ~isempty(inputs.sortLength)
    title([ str '- sorted with event ' num2str(inputs.showEvent(inputs.sortLength))]);
    title(str);
else

end
try 
set(gca,'xTick',1:epochs.rate/2:size(theseEpochs,2),'XTickLabel',epochs.time(1:epochs.rate/2:end));
catch
    fprintf('Raw time not available')
end

for iEvent = 1:length(inputs.showEvent)
    hold on;
    matrix_new = matrix{iEvent};
    matrix_new(isnan(matrix_new)) = [];
    plot(timeFinal{iEvent},matrix_new,color{iEvent},'LineWidth',1);
%     legend(num2str(inputs.showEvent(1:iEvent)'));
end

%% Average of trials
if inputs.showAverage
    
    if inputs.showAverage
        set(gca,'xTick',1:128:size(theseEpochs,2),'XTickLabel',[]);
        subplot(10,1,10)
        imagesc(nanmean(theseEpochs,1));
        for iEvent = 1:length(inputs.showEvent)
            hold on;
            plot(nanmean(timeFinal{iEvent}),1,color{iEvent},'LineWidth',2);
        end
        set(gca,'xTick',1:epochs.rate/2:size(theseEpochs,2),'XTickLabel',epochs.time(1:epochs.rate/2:end));
        set(gca,'yTick',1,'yTickLabel',[]);
        colorbar
        if ~isempty(inputs.caxis)
            caxis(inputs.caxis)
        end
        xlabel('Time(s)');
    end

end
end



%% Parser
function funcInputs = parse_my_inputs(epochs,channelID,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'epochs',@isstruct);
addRequired(funcInputs,'channelID',@isnumeric);

addParameter(funcInputs,'showAverage',true,@islogical);
addParameter(funcInputs,'showEvent',[],@isnumeric);
addParameter(funcInputs,'time',[],@isnumeric);
addParameter(funcInputs,'sortLength',[],@isnumeric);
addParameter(funcInputs,'normalize',false,@islogical);
addParameter(funcInputs,'caxis',[],@isnumeric);
addParameter(funcInputs,'smoothing',[],@isnumeric);
parse(funcInputs, epochs,channelID, varargin{:});
funcInputs = funcInputs.Results;


end
