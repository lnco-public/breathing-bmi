function plot_FPR(p,time,varargin)

inputs = parse_my_inputs(p,time,varargin{:});

if iscell(p)
    prob_output = cell2mat(p);
end

fpr = prob_output > 0.5; 

sumFPR = sum(fpr);
nTrial = size(fpr,1);
rate_mean = sumFPR/nTrial;

plot(time,rate_mean,'Color',inputs.color)


xlim([min(time) max(time)])
ylim([0 1])
title('FPR over time')
xlabel('Time(s)')
ylabel('FPR')

end



function funcInputs = parse_my_inputs(p,time,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'prob_output',@(x) iscell(x) || isnumeric(x));
addRequired(funcInputs,'time', @isnumeric);
addParameter(funcInputs, 'color',[1 1 1], @(x) isnumeric(x) && length(x) == 3);

addParameter(funcInputs, 'beta', 0.96, @(x) isnumeric(x) && x <= 1);


parse(funcInputs, p,time, varargin{:});
funcInputs = funcInputs.Results;

end
