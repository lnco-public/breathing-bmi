function plot_topo_features(matrix,labelUsed,labelTemplate,elocFile,freqgrid,strBand)

AlphaFreqRange = 8:0.1:15;
[~,iFreqA] = intersect(freqgrid,AlphaFreqRange);
BetaFreqRange = 16:0.1:28;
[~,iFreqB] = intersect(freqgrid,BetaFreqRange);

nChannel = length(labelTemplate);
index = ismember(labelTemplate,labelUsed) ;

matrix_mu = zeros(nChannel,1);
matrix_mu(index) = mean(matrix(:,iFreqA),2);

matrix_beta = zeros(nChannel,1);
matrix_beta(index) = mean(matrix(:,iFreqB),2);

matrix_all = zeros(nChannel,1);
matrix_all(index) = mean(matrix,2);
% Visualization
colormap jet;

if strcmp(strBand,'mu')
    topoplot(matrix_mu,elocFile);
    title('Mu [8-14] Hz')
elseif strcmp(strBand,'beta')
    topoplot(matrix_beta,elocFile);
    title('Beta[16-28] Hz')
else strcmp(strBand,'all')
    topoplot(matrix_all,elocFile);
    %     title('Features selected')
    
end


% 
% 
% 
% AlphaFreqRange = 8:0.1:12;
% [~,iFreqA] = intersect(freq,AlphaFreqRange);
% Beta1FreqRange = 12.5:0.1:16;
% [~,iFreqB] = intersect(freq,Beta1FreqRange);
% Beta1FreqRange2 = 16.5:0.1:20;
% [~,iFreqB2] = intersect(freq,Beta1FreqRange2);
% Beta1FreqRange3 = 21:0.1:28;
% [~,iFreqB3] = intersect(freq,Beta1FreqRange3);
% artefact = 28:0.1:40;
% [~,artefactB] = intersect(freq,artefact);
% 
% h3 = figure();
% subplot(3,5,1)
% matrix_featSpectral_All = squeeze(mean(matrix_feat_good_subject(:,iFreqA),2));
% topoplot(matrix_featSpectral_All,runProcessedData{1}.eloc,'style','map');
% title('Mu')
% %     caxis([0 0.2])
% subplot(3,5,2)
% matrix_featSpectral_All = squeeze(mean(matrix_feat_good_subject(:,iFreqB),2));
% topoplot(matrix_featSpectral_All,runProcessedData{1}.eloc,'style','map');
% title('Low Beta')
% %     caxis([0 0.2])
% subplot(3,5,3)
% matrix_featSpectral_All = squeeze(mean(matrix_feat_good_subject(:,iFreqB2),2));
% topoplot(matrix_featSpectral_All,runProcessedData{1}.eloc,'style','map');
% title('Mid-Beta')
% %     caxis([0 0.2])
% subplot(3,5,4)
% matrix_featSpectral_All = squeeze(mean(matrix_feat_good_subject(:,iFreqB3),2));
% topoplot(matrix_featSpectral_All,runProcessedData{1}.eloc,'style','map');
% title('High-Beta')
% %     caxis([0 0.2])
% subplot(3,5,5)
% matrix_featSpectral_All = squeeze(mean(matrix_feat_good_subject(:,artefactB),2));
% topoplot(matrix_featSpectral_All,runProcessedData{1}.eloc,'style','map');
% %     caxis([0 0.2])
% title('Artefact?')

% Visualization
% subplot(3,5,[6:15])
% imagesc(matrix_feat_good_subject);
% c = colorbar;
% c.Label.String = 'Fisher score';
% ylabel('Channels');xlabel('Frequency(Hz)')
% set(gca,'xTick',1:intervalS:size(matrix_featSpectral_GA_mean,2),'XTickLabel',freq(1:intervalS:end),'XTickLabelRotation',90);
% set(gca,'yTick',1:size(matrix_featSpectral_GA_mean,1),'YTickLabel',label);
% title('Offset features')
%     caxis([0 0.2])

