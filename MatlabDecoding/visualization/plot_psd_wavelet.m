function m = plot_psd_wavelet(epochs,varargin)

%PLOT_PSD Summary of this function goes here
%   Detailed explanation goes here

inputs = parse_my_inputs(epochs,varargin{:});

chanOfInterest = ismember(epochs.label,inputs.channel);
theseEpochs = squeeze(epochs.data(:,chanOfInterest,:));
nFreq = length(inputs.freq);
k = length(epochs.time);
nEpoch = size(theseEpochs,1);
P = zeros(nEpoch,nFreq,k);
R = zeros(nEpoch,nFreq,k);


range_cycles = [5 10];

% other wavelet parameters
nCycles = logspace(log10(range_cycles(1)),log10(range_cycles(end)),nFreq);

for iFreq = 1:nFreq
    freq = inputs.freq(iFreq);
    
    % create a Morlet wavelet for a specific frequency
    cmw = create_morelet_wavelet(freq,epochs.rate,inputs.win,nCycles(iFreq));
    for iEpoch = 1:nEpoch
        clf
        thisEpoch = theseEpochs(iEpoch,:);
        
        % calculation power with wavelet transform
        p = calculate_fft(thisEpoch,cmw);
        P(iEpoch,iFreq,:) =  p;
        
    end
    
end
meaP = mean(P,1);
baseidx = [find(epochs.time == -2.5) find(epochs.time == -1)];
% db conversion
m = 10*log10( bsxfun(@rdivide, squeeze(meaP), mean(meaP(:,baseidx(1):baseidx(2)),2)'));
  

% 
% if strcmp(inputs.normalization,'log')
%     X = 10*log10(mean(P,1)./mean(R,1));
% else
%     X = 100*(mean(P,1)-mean(R,1))./mean(R,1);
% end


colormap('jet');
c = colorbar
imagesc('XData',epochs.time,'YData',inputs.freq,'CData',m)
label = inputs.channel;
title(format_text_for_plot(['ERD percentage for ',label]))
xlabel('Time(sec)'); ylabel('Frequency(Hz)')

if strcmp(inputs.normalization,'linear')
    caxis([-100 100])
    c.Label.String = 'ERD/ERS - Power';
else
    c.Label.String = 'ERD/ERS - dB';
end

end


function cmw = create_morelet_wavelet(freq,srate,win_half,nCycle)

time  = -win_half:1/srate:win_half; % best practice is to have time=0 at the center of the wavelet
% create complex sine wave
sine_wave = exp( 1i*2*pi*freq.*time );

% create Gaussian window
% 7 is the number of cycles
s = nCycle / (2*pi*freq); % this is the standard deviation of the gaussian
gaus_win  = exp( (-time.^2) ./ (2*s^2) );

% now create Morlet wavelet
cmw = sine_wave .* gaus_win;

end

function power = calculate_fft(data,cmw)

% convolution parameters
nData = length(data);
nKern = length(cmw);
nConv = nData + nKern -1;
half_wav = floor(length(cmw)/2) + 1;


% perform FFT on wavelet
cmwX = fft(cmw,nConv);
cmwX = cmwX./max(cmwX);

% perform FFT on data
dataX = fft(data,nConv);
dataX = dataX./max(dataX);
% convolution
fft_data = ifft(dataX .* cmwX);

% cut 1/2 of the length of the wavelet from the beginning and from the end
fft_data = fft_data(half_wav-1:end-half_wav);

power = abs(fft_data).^2;
phase = angle(fft_data);
end




function funcInputs = parse_my_inputs(epochs,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addParameter(funcInputs,'win',[],@isnumeric);
addParameter(funcInputs,'channel',[],@iscellstr);
addParameter(funcInputs,'noverlap',[],@isnumeric);
addParameter(funcInputs,'freq',7:25,@isnumeric);
expectedType = {'log','linear'};
addParameter(funcInputs, 'normalization',{'log'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,epochs,varargin{:});
funcInputs = funcInputs.Results;


end