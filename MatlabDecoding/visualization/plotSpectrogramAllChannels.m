
function  plotSpectrogramAllChannels(M,time,freq,label,line2Plot)


ch = [ 0 0 1 0 0; ...
    2  3 4 5 6; ...
    7 8 9 10 11; ...
    12 13 14 15 16];

for iChannel  = 1:size(M,1)
    
    % Plotting
    ii = find(ch' == iChannel);
    ax = subplot(4,5,ii);
    AveragePower4ThisChannel = squeeze(M(iChannel,:,:,:));
    plotSpectrogramDisplayer(AveragePower4ThisChannel,time,freq,label(iChannel),line2Plot);
    set(gca,'XLabel',[])
    set(gca,'YLabel',[])
    
    
    currentPlot = gca;
    currentFigure = gcf;
    set(currentPlot,'ButtonDownFcn',@(~,~)plotSpectrogramDisplayer(AveragePower4ThisChannel,time,freq,label(iChannel),line2Plot,currentFigure));
    
end


hp4 = get(subplot(4,5,20),'Position');
c = colorbar('Position', [hp4(1)+hp4(3)+0.01  hp4(2)  0.02  hp4(2)+hp4(3)*5.7]);
c.Label.String = 'ERD/ERS - dB';
caxis([-5 5])
end
