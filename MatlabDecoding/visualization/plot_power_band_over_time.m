function  plot_power_band_over_time(PowerPerTrial,time,varargin)

inputs = parse_my_inputs(PowerPerTrial,time,varargin{:});


mea = squeeze(nanmean(PowerPerTrial,1));
if strcmp(inputs.showDeviation,'stdError')
    dev = squeeze(nanstd(PowerPerTrial,[],1))/sqrt(size(PowerPerTrial,1));
elseif strcmp(inputs.showDeviation,'std')
    dev = squeeze(nanstd(PowerPerTrial,[],1));
else
    dev = 0;
end



hold all;
if ~strcmp(inputs.showDeviation,'none') && ~strcmp(inputs.showDeviation{1},'alllines')
    h1 = patch([time fliplr(time)],[mea+dev fliplr(mea-dev)],inputs.ColorPatch,'EdgeColor','None','HandleVisibility','off','FaceAlpha',inputs.FaceAlpha);
end


if strcmp(inputs.showDeviation,'alllines')
    h1 = plot(time,PowerPerTrial','Color',inputs.color,'LineWidth',0.05);
end

h2 = plot(time,mea,'Color',inputs.color,'LineWidth',2);


hold off;
axis tight
xlabel('Time[s]');ylabel('Band Power[dB]')


%% Parser
function funcInputs = parse_my_inputs(PowerPerTrial,time,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'PowerPerTrial',@isnumeric);
addRequired(funcInputs,'time', @isnumeric);
addParameter(funcInputs, 'color',[1 1 1], @(x) isnumeric(x) && length(x) == 3);
addParameter(funcInputs, 'ColorPatch',[0.9 0.9 0.9], @(x) isnumeric(x) && length(x) == 3);
expectedType = {'std','stdError','none','alllines'};
addParameter(funcInputs,'showDeviation',{'std'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'FaceAlpha',0.7,@isnumeric);

parse(funcInputs, PowerPerTrial,time, varargin{:});
funcInputs = funcInputs.Results;

end

end

