function [result,accuracyTrial,latency4EachTrial,ratio,detectionAll] = plot_single_trial_detection(prob,varargin)


inputs = parse_my_inputs(prob,varargin{:});


Posterior_prob = cell2mat(prob.value);
Posterior_prob = Posterior_prob(:,2:2:end);
nTrial = size(Posterior_prob,1);
result = zeros(size(Posterior_prob));
rate = (prob.time(end) - prob.time(1))/length(prob.time);
detectionAll = [];
switch inputs.detectionMethod{1}
    
    case 'accumulateDecision'
        detectionMatrix = Posterior_prob>inputs.threshold;
        
        for iTrial = 1:nTrial
            thisTrialDetection = find(detectionMatrix(iTrial,:) == 1);
            detectionIndex = find_consecutive_detection(thisTrialDetection,inputs.parameter);
            detectionAll = [detectionAll;detectionIndex(1)];
            if ~isnan(detectionIndex)
                result(iTrial,detectionIndex(1)) = 1;
            end
        end
        
    case 'evidenceAccumulation'
        detectionMatrix = evidence_accumulation(Posterior_prob,inputs.smoothing_parameter);
        
    case 'movingAverage'
%         detectionMatrix =  movingmean(Posterior_prob,inputs.parameter,2);
         detectionMatrix =  calculate_moving_average(Posterior_prob,inputs.parameter);
    case 'findPeaks'
        detectionMatrix = double(Posterior_prob>inputs.threshold);
        for  iTrial = 1:nTrial
           
          [~,loc] = findpeaks(detectionMatrix(iTrial,:),1:size(detectionMatrix,2),'SortStr','descend');  
          if ~isnan(loc)
           result(iTrial,loc(1)) = 1; 
          end
        end
end

if ~strcmp(inputs.detectionMethod,'accumulateDecision')
    for iTrial = 1:nTrial
        thisTrialDetection = detectionMatrix(iTrial,:);
        detectionIndex = find(thisTrialDetection>=inputs.threshold,1,'first');
        if isempty(detectionIndex)
           detectionIndex = nan; 
        end
        
        detectionAll = [detectionAll;detectionIndex(1)];
        if ~isnan(detectionIndex)
            result(iTrial,detectionIndex) = 1;
        end
    end
end

% Calculate latency
iTime = find(prob.time  == inputs.latencyTime2Compare);
latency4EachTrial = zeros(1,nTrial);
for iTrial = 1:nTrial
    thisDetectionIndex = find(result(iTrial,:) == 1);
    try
    latency4EachTrial(iTrial) = (thisDetectionIndex - iTime)*rate;
    catch 
     latency4EachTrial(iTrial) = NaN;   
    end
end


if inputs.showCorrectTrials && inputs.showPlot
 subplot(1,10,[1 9]);    
end
if inputs.showPlot
    % Visualization detection time
    plot_singleTrial_classification(result,prob.time);
    title(['Single Trial Detection with ' inputs.detectionMethod{1}])
    caxis([0 1])
end
correctTrialsIndex = find_correct_trials_detection(result,prob.time,inputs.windowForCorrectTrial);
accuracyTrial = 100*sum(correctTrialsIndex)/size(result,1);

if inputs.showPlot
    map = [
        0.2081    0.1663    0.5292
        1 0 0
        0.6000    1.0000    0.6000];
    resultCorrect = result;
    resultCorrect(~correctTrialsIndex,:) = 0.5*resultCorrect(~correctTrialsIndex,:);
    hold on;
    imagesc('XData',prob.time,'YData',1:size(detectionMatrix,1),'CData',resultCorrect);
    colormap(gca,map);
    
    for i = 1:length(inputs.windowForCorrectTrial)
        hold on;
        timePosition = inputs.windowForCorrectTrial(i);
        line([timePosition timePosition],[1 size(result,1)],'color','k','LineWidth',2);
    end
    hold on;
end



if inputs.showCorrectTrials && inputs.showPlot
    ax = subplot(1,10,10);
        map = [
        1 0 0
        0.6000    1.0000    0.6000];
    imagesc('YData',1:size(detectionMatrix,1),'CData',double(correctTrialsIndex));
    ylim([1 size(detectionMatrix,1)]);
    colormap(ax,map);
    caxis([0 1]);
   set(gca,'xtick',[]);
%     set(gca,'ytick',[])
    ylabel(['Trial Accuracy Detection: ' num2str(round(accuracyTrial,2)) '%']);

end

correctTrialsIndex = find_correct_trials_detection(result,prob.time,inputs.windowForCorrectTrial);
ratio = accuracyTrial * abs(nanmean(latency4EachTrial));







end

function proSmoothed = calculate_moving_average(prob_values,smooth_parameter)

nTrial = size(prob_values,1);
prob_values_temp = [ prob_values(:,1)*ones(1,smooth_parameter) prob_values];

for i = 1:nTrial
    [~,p] = movavg(prob_values_temp(i,:),1,smooth_parameter,1);
    proSmoothed(i,:) = p(smooth_parameter+1:end);
end


end


function detectionIndex = find_consecutive_detection(thisTrialDetection,n)

k = [true;diff(thisTrialDetection(:))~=1 ];
s = cumsum(k);
x =  histc(s,1:s(end));
idx = find(k);

try
 detectionIndex = thisTrialDetection(idx(x>=n));
 
catch
    detectionIndex = nan;
    
end
if isempty(detectionIndex)
    detectionIndex = nan;
else
    detectionIndex = detectionIndex+ n;
end

end


function correctTrialsIndex = find_correct_trials_detection(detectionMatrix,time,windowOfCorrectness)

try
tLeft = find(time >= windowOfCorrectness(1),1,'first');
tRight = find(time < windowOfCorrectness(2),1,'last');
catch
disp('the time window doesnt correspond to the probabilities')    
end

nTrial  = size(detectionMatrix,1);
correctTrialsIndex = false(nTrial,1);
for iTrial = 1:nTrial
    detectionIndex = find(detectionMatrix(iTrial,:) == 1);
    correctTrialsIndex(iTrial) = any(ismember(tLeft:tRight,detectionIndex));
end
end


%% Parser
function funcInputs = parse_my_inputs(prob,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'prob',@isstruct);
expectedType = {'accumulateDecision','evidenceAccumulation','movingAverage','findPeaks'};
addParameter(funcInputs, 'detectionMethod', {'accumulateDecision'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'parameter',5,@isnumeric);
addParameter(funcInputs,'smoothing_parameter',0.96,@isnumeric);
addParameter(funcInputs,'showCorrectTrials',false,@islogical);
addParameter(funcInputs,'windowForCorrectTrial',[0 3],@(x) isnumeric(x) && length(x) == 2);
addParameter(funcInputs,'latencyTime2Compare',0,@(x) isnumeric(x) && length(x) == 1);
addParameter(funcInputs,'threshold',0.8,@isnumeric);
addParameter(funcInputs,'showPlot',true,@islogical);
parse(funcInputs,prob, varargin{:});

funcInputs = funcInputs.Results;

end