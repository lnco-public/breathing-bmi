
if verbose 
    fprintf('\nPlotting Process for classification results...\n')
end

%% Parameters for plotting
intervalS = 1;
channelOfInterestERD = 1:16;
labelSpectral = runProcessedData{1}.label(channelOfInterestERD);
nChannelSpectral = length(labelSpectral);
freq = params.params_featExtraction.freq;
nFeaturePerChanSpectral = length(freq);
%% Prepare the data for plotting

BS = classifier.Beta;
matrix_featSpectral = create_features_selection_matrix(BS,nFeaturePerChanSpectral,nChannelSpectral);
chan_tot = 16;
%% Visualization
h1 = figure();

%% Feature selection
if onePlotOnly
subplot(3,4,[1 2 5 6])
end

imagesc(matrix_featSpectral);
ylabel('Channels');
xlabel('Frequency(Hz)')
title('Features Maping');
set(gca,'xTick',1:intervalS:size(matrix_featSpectral,2),'XTickLabel',freq(1:intervalS:end),'XTickLabelRotation',90)
set(gca,'yTick',1:size(matrix_featSpectral,1),'YTickLabel',labelSpectral)

%% Topoplot
if onePlotOnly
subplot(3,4,9)
else
    h2 = figure();
    subplot(2,1,1)
end

plot_topo_features(matrix_featSpectral,labelSpectral,runProcessedData{1}.label(1:chan_tot),runProcessedData{1}.eloc,freq,'mu')

if onePlotOnly
subplot(3,4,10)
else
    subplot(2,1,2)
end
plot_topo_features(matrix_featSpectral,labelSpectral,runProcessedData{1}.label(1:chan_tot),runProcessedData{1}.eloc,freq,'beta')
%% Draw the discriminant hyperplane and features
if onePlotOnly
    subplot(3,4,[3 4 7 8 11 12])
else
    h3 = figure();
end
h3 = gscatter(classifier.DC(:,1),classifier.DC(:,2),classifier.Y);

[~,~,~,~,coeffLDA] = classify(classifier.DC,classifier.DC,classifier.Y);
K = coeffLDA(1,2).const;
L = coeffLDA(1,2).linear;
f = @(x,y) K+L(1)*x + L(2)*y;
hold on;

class = unique(classifier.Y);
nClass = length(class);
color = {'r','c','g'};
for iClass = 1:nClass
ii = find(classifier.Y == class(iClass));
xx = fit_ellipse(classifier.DC(ii,1),classifier.DC(ii,2));
ellipse(xx.a,xx.b,xx.phi,xx.X0,xx.Y0,color{iClass});
plot(xx.X0,xx.Y0,'kx','MarkerSize',20,'LineWidth',4);
end
h2 = ezplot(f,[min(classifier.DC(:,1)) max(classifier.DC(:,1)) min(classifier.DC(:,2)) max(classifier.DC(:,2)) ]);
set(h2,'Color','m','LineWidth',2);
xlabel('PC1');ylabel('PC2');
title('CLassification with LDA Training Data')