global verbose 

onePlotOnly = true

if verbose
fprintf('\nPlotting Process for classification results...\n')
end
%% Parameters for plotting

intervalS = 1;
labelSpectral = params.params_featExtraction.channelOfInterestERD_name;
nChannelSpectral = length(labelSpectral);
time = timeInfo.testing.timeInterval(1) + params.params_featExtraction.ws:...
timeInfo.testing.noverlap:timeInfo.testing.timeInterval(2);
nFeaturePerChanSpectral = training.features.Dspec;
freq = params.params_featExtraction.freq;

if ~classifier.regularization
    BetaS = classifier.Pfeature;
else
    BetaS = classifier.Beta;
end
if classifier.applyPCA
    nFold = size(BetaS,2);
    for iFold = 1:nFold
        BetaS{iFold} =  classifier.coeff{iFold}*BetaS{iFold};
    end
end
% if iscell(BetaS)
    BS = abs(cell2mat(BetaS));
    BS = BS./(ones(size(BS,1),1)*max(BS,[],1));
%     nF = size(BetaS,2);
% end

BS = abs(cell2mat(BetaS));
matrix_featSpectral = create_features_selection_matrix(BS,nFeaturePerChanSpectral,nChannelSpectral);

%% Spectral features selection

h1 = figure();

if onePlotOnly
subplot(2,3,1)
end
imagesc(matrix_featSpectral);
colorbar;
ylabel('Channels');
xlabel('Frequency(Hz)')
title('Feature selection on Spectral features');
set(gca,'xTick',1:intervalS:size(matrix_featSpectral,2),'XTickLabel',freq(1:intervalS:end),'XTickLabelRotation',90);
set(gca,'yTick',1:size(matrix_featSpectral,1),'YTickLabel',labelSpectral);

%% ROC curve

if onePlotOnly
    subplot(2,3,2)
else
    h2 = figure();
end
class_perf = plot_performance_classifier(output,'showLegend',true,'showTraining',true,'showTesting',true,'showRandom',false);
title('ROC classification')

%% Single Trial Classification

if onePlotOnly
    subplot(2,3,3)
else
    h3 = figure()
end
    plot_singleTrial_classification(prob.value,prob.time);
    caxis([0 1])

%% Average over trials Performance
if onePlotOnly
    subplot(2,3,4)
else
    h4 = figure();
end

 plot_CR_over_time(prob.value,prob.time,'color',[0 0 0],'ColorPatch',[0.8    0.8    0.8],'showDeviation',{'std'});


 if onePlotOnly
     subplot(2,3,5)
 else
     h5 = figure()
 end

Y = [class_perf.TPR.train.mean*100,class_perf.TPR.test.mean*100];
for i = 1:2
    h=bar(Y(i));
    if i ==1
        set(h,'FaceColor',[ 0    0.4471    0.7412]);
    elseif i==2
        set(h,'FaceColor',[0.8510    0.3255    0.0980]);
    end
    hold on;
end
hold off
ylim([0 100])
xlim([0 3])
legend(['Training ' num2str(class_perf.TPR.train.mean*100)],['Testing ' num2str(class_perf.TPR.test.mean*100)])
set(gca,'xtick',[]);
title('Classification accuracy')
 if onePlotOnly
     subplot(2,3,6)
 else
     h6 = figure()
 end
probAll = reshape(cell2mat(prob.value),[],1);
histogram(probAll,20)

title('Distribution posterior probabilities')

