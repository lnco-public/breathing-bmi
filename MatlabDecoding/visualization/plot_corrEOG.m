function plot_corrEOG(dataEEG,vEOG,hEOG,eloc,clim)

subplot(1,2,1)
R_v = corr(dataEEG',vEOG');
topoplot(R_v,eloc);
title('Vertical EOG')
colorbar;
if nargin >4
    caxis(clim);
end
subplot(1,2,2)
R_h = corr(dataEEG',hEOG');
topoplot(R_h,eloc);
colorbar;
if nargin >4
    caxis(clim);
end
title('Horizontal EOG')
end

