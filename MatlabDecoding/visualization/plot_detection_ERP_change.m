function [timeIndex,timeOfDetection] = plot_detection_ERP_change(epochs,nConsecutive,sign)



if nargin == 1 
   nConsecutive = 10; 
end



if nargin == 2  
   sign = 'positive'; 
end

theseEpochs = squeeze(epochs.data);
for iTime = 1:size(theseEpochs,2)
    theseEpochs(:,iTime) =  sgolayfilt(theseEpochs(:,iTime),1,5);
end

timeStart = find(epochs.time == 0);
[matrix,timeIndex] = detect_ERP_change(theseEpochs,nConsecutive,timeStart,sign);
timeOfDetection = (timeIndex-timeStart)/epochs.rate;

subplot(1,2,1)
imagesc('YData',1:epochs.nTrial,'XData',epochs.time,'CData',theseEpochs)
axis tight
caxis([-2 2])

%% Detect ERS
subplot(1,2,2)
imagesc('XData',epochs.time,'YData',1:epochs.nTrial,'CData',matrix);
hold on;
plot(timeOfDetection,1:epochs.nTrial,'g*');
line([0 0],[1 epochs.nTrial],'Color','k')
axis tight


end

