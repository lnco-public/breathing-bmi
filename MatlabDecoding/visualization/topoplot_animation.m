function topoplot_animation(band,band_title,pow,t,f,session)
%% Compute spectrogramm
% [ figure_topo,movie_topo ] = new_get_topoplot_animation([8 12],'alpha',power_offset,time_off,freqfrid,spectrogramData{1})

idx1=f(:)>band(1);
idx2=f(:)<band(2);
idx=idx1 & idx2;

for i = 1:16
    cut_spec(i,:,:)= pow{i}(idx,:);
end
chan_pow=mean(cut_spec,2);% sum over the samples

eloc = session.eloc;

chan_pow_t=squeeze(chan_pow); %keep the time dimension
figure()
[~,n_frame]=size(chan_pow_t);

for frame=1:n_frame
    topoplot(chan_pow_t(:,frame),eloc,'style','map');
    caxis([-4 4])
    title([band_title ' time: ' num2str(round(t(frame),2)) '[s]']);
    colorbar();
    movie_topo(frame)=getframe(gcf);
end

end

