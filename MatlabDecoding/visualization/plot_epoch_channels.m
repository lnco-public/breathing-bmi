function  plot_epoch_channels(epochs,iEpoch,label,interval)
%PLOT_EPOCH_CHANNELS Summary of this function goes here
%   Detailed explanation goes here

nChannel = size(epochs.data,2);
ystart = zeros(nChannel,1);
for iChannel = 1:nChannel
    theseEpochs = squeeze(epochs.data(iEpoch,iChannel,:));
    plot(epochs.time,theseEpochs-(iChannel-1)*interval);
    ystart(iChannel) = nanmean(theseEpochs-(iChannel-1)*interval);
    hold on;
end
hold off;
xlabel('Time(sec)'); ylabel('Channel')
title(['Epoch ',num2str(iEpoch)])
[ystart,~]= sort(ystart);

set(gca,'YTick',ystart,'YTickLabel',label(end:-1:1))


end

