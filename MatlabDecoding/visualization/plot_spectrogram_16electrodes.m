
function  [M,time,freq,PR,P,R] = plot_spectrogram_16electrodes(epochs,baselines,varargin)

inputs = parse_my_inputs(epochs,baselines,varargin{:});

M = [];
time = epochs.time;
freq = epochs.freq;

global verbose

if verbose && inputs.plotting
    fprintf('Spectrogram Plotting in Process...\n')
end

if strcmp(inputs.channelOfInterest{1},'all')
    channelOfInterest = epochs.label; 
else
    channelOfInterest = inputs.channelOfInterest;
end
  
for iChannel = 1:length(channelOfInterest)
    thisChannel = epochs.map_ch(channelOfInterest{iChannel});
    theseEpochs = squeeze(epochs.data(:,thisChannel,:,:));
    theseBaselines = squeeze(baselines.data(:,thisChannel,:,:));
    [P(iChannel,:,:,:),R(iChannel,:,:,:)] = extract_PSD_trials(theseEpochs,theseBaselines);
      [P(iChannel,:,:,:),R(iChannel,:,:,:)] = extract_PSD_trials(theseEpochs,theseBaselines);
%     [M(iChannel,:,:),PR(iChannel,:,:,:)] = normalize_power(squeeze(P(iChannel,:,:,:,:)),squeeze(R(iChannel,:,:,:,:)));  
        [M(iChannel,:,:,:),PR(iChannel,:,:,:,:)] = normalize_power(squeeze(P(iChannel,:,:,:,:)),squeeze(R(iChannel,:,:,:,:)));  
end
   
if inputs.plotting
    switch inputs.channelOfInterest{1}
        case 'all'
            plotSpectrogramAllChannels(M,time,freq,channelOfInterest,inputs.line2Plot)
        otherwise
            nChannel = length(channelOfInterest);
            for iChannel = 1:nChannel
                subplot((nChannel/2),(nChannel/2),iChannel)
                AveragePower4ThisChannel =squeeze(M(iChannel,:,:)); 
%                 AveragePower4ThisChannel =squeeze(M(iChannel,:,:,:));
                plotSpectrogramDisplayer(AveragePower4ThisChannel,time,freq,channelOfInterest(iChannel),inputs.line2Plot);
            end
    end
end

if verbose
    fprintf('\n')
end

end

function [P,R] = extract_PSD_trials(theseEpochs,theseBaselines)

nEpoch = size(theseEpochs,1);
nBaseline = size(theseBaselines,1);
for iEpoch = 1:nEpoch
    
    P(iEpoch,:,:) = squeeze(theseEpochs(iEpoch,:,:));
end

for iBaseline = 1:nBaseline
    
    basPower = squeeze(theseBaselines(iBaseline,:,:));
    R(iBaseline,:,:) = mean(basPower,2)*ones(1,size(P,3));  
end




end

function [X,PR] = normalize_power(P,R)


if  size(P,1) > size(R,1)
    X = bsxfun(@minus,mean(P,1),mean(R,1));
    PR = X;
else
    PR = bsxfun(@minus,P,R);
    X = 10*mean(PR,1);
end
X = squeeze(X);


end

%% Parser
function funcInputs = parse_my_inputs(epochs,baselines,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baselines',@isstruct);
addParameter(funcInputs,'freq',5:0.1:35,@isnumeric);
addParameter(funcInputs, 'channelOfInterest',{},@iscellstr);
addParameter(funcInputs, 'plotting',true,@islogical);
addParameter(funcInputs, 'line2Plot',0,@isnumeric);
parse(funcInputs, epochs,baselines, varargin{:});

funcInputs = funcInputs.Results;

if isempty(funcInputs.channelOfInterest)
    funcInputs.channelOfInterest = {'all'};
end

end


