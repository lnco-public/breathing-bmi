function plot_singleTrial_classification(prob,time,showProb,smoothingPoint)

if nargin > 3
    smoothingToDo = true;
else
    smoothingToDo = false;
end

if nargin <3
    showProb = 2;
end

if iscell(prob)
    nClasses = size(cell2mat(prob(1,1)),2);
else
    nClasses = 2;
end

if iscell(prob)
    prob = cell2mat(prob);
end


if size(prob,2) > size(time,2)
    prob = prob(:,showProb:nClasses:end);
end

[nEpoch,nTime] = size(prob);


if smoothingToDo
    for iTime = 1:nTime
        
        prob(:,iTime) =  sgolayfilt(prob(:,iTime),1,smoothingPoint);
    end
end

imagesc('XData',time,'YData',1:nEpoch,'CData',prob)
title('Single Trial Classification')
xlim([time(1) time(end)]);
ylim([1 nEpoch])
caxis([0 1])
ylabel('#Trials');xlabel('Time[s]')

end