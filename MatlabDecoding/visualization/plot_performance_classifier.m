

function [perf,threshold] = plot_performance_classifier(output,varargin)

global verbose
inputs = parse_my_inputs(output,varargin{:});

% %% Performance classifier
threshold = [];

posterior_train =  cat(1,output.training(:).posterior);
target_train = cat(1,output.training(:).target);

posterior_test = cat(1,output.testing(:).posterior);
target_test = cat(1,output.testing(:).target);

posterior_random=  cat(1,output.random(:).posterior);
target_random = cat(1,output.random(:).target);

%% Classification Accuracy
FPR_train  = [];TPR_train  = [];
FPR_test  = [];TPR_test  = [];
FPR_random  = [];TPR_random = [];
for i = 1:size(output.training,2)
    index = output.testing(i).target == 1;
    FPR_train = [FPR_train sum(output.training(i).class ~= output.training(i).target)/length(output.training(i).target)];
    TPR_train = [TPR_train sum(output.training(i).class == output.training(i).target)/length(output.training(i).target)];
   
    FPR_test = [FPR_test sum(output.testing(i).class ~= output.testing(i).target)/length(output.testing(i).target)];
    
    if inputs.accuracy
        TPR_test = [TPR_test sum(output.testing(i).class == output.testing(i).target)/length(output.testing(i).target)];
    else
        TPR_test = [TPR_test sum(output.testing(i).class(index) == output.testing(i).target(index))/length(output.testing(i).target(index))];
    end
    
    FPR_random = [FPR_random sum(output.random(i).class ~= output.random(i).target)/length(output.random(i).target)];
    TPR_random = [TPR_random sum(output.random(i).class == output.random(i).target)/length(output.random(i).target)];

end

stdFPR_train  = std(FPR_train);
stdTPR_train  = std(TPR_train);
stdFPR_test  = std(FPR_test);
stdTPR_test  = std(TPR_test);
stdFPR_random  = std(FPR_random);
stdTPR_random  = std(TPR_random);

meaFPR_train  = mean(FPR_train);
meaTPR_train  = mean(TPR_train);
meaFPR_test  = mean(FPR_test);
meaTPR_test  = mean(TPR_test);
meaFPR_random  = mean(FPR_random);
meaTPR_random  = mean(TPR_random);

%% ROC
[XT,YT] = perfcurve(target_train,posterior_train(:,2),1);
[Xt,Yt,~,~,threshold] =  perfcurve(target_test,posterior_test(:,2),1);
 [Xrt,Yrt] =  perfcurve(target_random,posterior_random(:,2),1);

aucTrainFold =[];
aucTestFold = [];
aucRandomFold = [];
for iFold = 1:numel(output.training)
    
    [~,~,~,auc_Train,~] = perfcurve(output.training(iFold).target,output.training(iFold).posterior(:,2),1);
    [~,~,~,auc_Test,~] = perfcurve(output.testing(iFold).target,output.testing(iFold).posterior(:,2),1);
    [~,~,~,auc_Random,~] = perfcurve(output.random(iFold).target,output.random(iFold).posterior(:,2),1);
    
    aucTrainFold = [aucTrainFold; auc_Train];
    aucTestFold = [aucTestFold ;auc_Test];
    aucRandomFold  = [aucRandomFold ;auc_Random];

end

meaAUC_Train = mean(aucTrainFold);
stdAUC_Train = std(aucTrainFold,[],1);

meaAUC_Test = mean(aucTestFold);
stdAUC_Test = std(aucTestFold,[],1);

meaAUC_Random = mean(aucRandomFold);
stdAUC_Random = std(aucRandomFold,[],1);


%% Plotting 

if inputs.showTraining
    plot(XT,YT,'LineWidth',4);
end

if inputs.showTesting
    hold on;plot(Xt,Yt,'LineWidth',4);
    if inputs.showThreshold
        plot(threshold(1),threshold(2),'ro')
    end
    
    xlabel('False positive rate')
    ylabel('True positive rate')
    title('ROC Curve for Classification')
    grid on;
end

if inputs.showRandom
    plot(Xrt,Yrt,'k--','LineWidth',4);
end


if inputs.showLegend
    lgd =  legend(['Training - AUC' num2str(round(meaAUC_Train,2))],...
        ['Testing - AUC' num2str(round(meaAUC_Test,2))]);
    
    
    if inputs.showRandom
        lgd =  legend(['Training - AUC' num2str(meaAUC_Train) '+/-' num2str(stdAUC_Train)],...
            ['Testing - AUC' num2str(meaAUC_Test) '+/-' num2str(stdAUC_Test)],...
            ['Random - AUC' num2str(meaAUC_Random) '+/-' num2str(stdAUC_Random)]);
        
    end
    lgd.Location = 'southeast';
    
end

%% Compute statistical chance threshold
nsamples = length(output.testing(1).class);
IC = 0.95;
stChanceLevel = binoinv(IC,nsamples,0.5)*100/nsamples;
stChanceLevel = round(stChanceLevel,2);



%% Verbose
if verbose
fprintf('************************** \n')
fprintf('Classification performance : Accuracy = %.2f +/- %.2f, AUC = %.2f +/- %.2f\n',meaTPR_test*100,stdTPR_test*100, meaAUC_Test*100,stdAUC_Test*100);

end

%% Save performances
TPR.train.mean = meaTPR_train;
TPR.train.dev = stdTPR_train;
FPR.train.mean = meaFPR_train;
FPR.train.dev = stdFPR_train;
TPR.train.values = TPR_train;
FPR.train.values = FPR_train;

TPR.test.mean = meaTPR_test;
TPR.test.dev = stdTPR_test;
FPR.test.mean = meaFPR_test;
FPR.test.dev = stdFPR_test;
TPR.test.values = TPR_test;
FPR.test.values = FPR_test;

TPR.random.mean = meaTPR_random;
TPR.random.dev = stdTPR_random;
FPR.random.mean = meaFPR_random;
FPR.random.dev = stdFPR_random;

AUC.train.mean = meaAUC_Train;
AUC.train.dev = stdAUC_Train;
AUC.train.values = aucTrainFold;

AUC.test.mean = meaAUC_Test;
AUC.test.dev = stdAUC_Test;
AUC.test.values = aucTestFold;

AUC.random.mean = meaAUC_Random;
AUC.random.dev = stdAUC_Random;

perf.TPR = TPR;
perf.FPR = FPR;
perf.AUC = AUC;

end






function funcInputs = parse_my_inputs(output,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'output',@isstruct);
addParameter(funcInputs,'accuracy',true,@islogical);
addParameter(funcInputs, 'showTraining',false,@islogical);
addParameter(funcInputs, 'showTesting',false,@islogical);
addParameter(funcInputs, 'showRandom',false,@islogical);
addParameter(funcInputs,'showLegend',false,@islogical);
addParameter(funcInputs,'showThreshold',false,@islogical);

parse(funcInputs,output, varargin{:});
funcInputs = funcInputs.Results;

end







