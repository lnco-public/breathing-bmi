
function plot_periodogram_16electrodes(epochs,baselines,varargin)
% Compute Periodogram
global verbose 

inputs = parse_my_inputs(epochs,baselines,varargin{:});


ch = [ 0 0 1 0 0; ...
    2  3 4 5 6; ...
    7 8 9 10 11; ...
    12 13 14 15 16];

FaceAlpha = 0.6;

fprintf('Periodogram Plotting in Process...\n')    


% Plot spectrogram for 16 electrodes
[nEpoch,nChannel,~] = size(epochs.data);
if strcmp(inputs.type,'trial')
    fprintf('\nEpoch #')
    for iEpoch =  1:nEpoch
        clf
        for j=0:log10(iEpoch-1)
            fprintf('\b');
        end
        
        fprintf('%d', iEpoch);
        for iChannel = 1:nChannel

            label = epochs.label{iChannel};
            chan = epochs.map_ch(label);
            theseEpochs = squeeze(epochs.data(iEpoch,chan,:))';
            theseBaselines= squeeze(baselines.data(iEpoch,chan,:))';
            ii = find(ch' == iChannel);
            subplot(4,5,1)
            title([ 'Trial ' num2str(iEpoch) '/' num2str(nEpoch)])
           
            subplot(4,5,ii)
            [meanPower,w] = get_psd_epochs(theseEpochs,epochs.rate);
            plot(w,20*log10(meanPower),'r');
            [meanPowerBaseline,w] = get_psd_epochs(theseBaselines,baselines.rate);
            hold on; plot(w,20*log10(meanPowerBaseline),'b');
            xlim([inputs.freq(1) inputs.freq(end)]);
            ylim([-20 20])
            
            title(epochs.label(iChannel))
            MeanPowerEpochAll(iChannel,:) = meanPower;
            MeanPowerBaselineAll(iChannel,:) = meanPowerBaseline;
            
        end
        waitforbuttonpress
        subplot(4,5,1)
        mea = 10*log10(mean(MeanPowerEpochAll,1));
        dev = 10*log10(std(MeanPowerEpochAll,1));
        subplot(4,5,1)
        hold on; patch([w' fliplr(w')],[mea+dev fliplr(mea-dev)],[1 0.8 0.8],'EdgeColor','None','HandleVisibility','off','FaceAlpha',0.5);
        hold on;plot(w,mea,'r');
        mea = 10*log10(mean(MeanPowerBaselineAll,1));
        dev = 10*log10(std(MeanPowerBaselineAll,1));
        hold on; patch([w' fliplr(w')],[mea+dev fliplr(mea-dev)],[0.8 0.8 1],'EdgeColor','None','HandleVisibility','off','FaceAlpha',0.5);
        hold on;plot(w,mea,'b');
        ylim([-20 20])
        xlim([inputs.freq(1) inputs.freq(end)]);
        title('Average')
    end


elseif strcmp(inputs.type,'average')
    fprintf('\nChannel #')
    for iChannel = 1:nChannel
 
        for j=0:log10(iChannel-1)
            fprintf('\b');
        end
        fprintf('%d', iChannel);
        label = epochs.label{iChannel};
        chan = epochs.map_ch(label);
        theseEpochs = squeeze(epochs.data(:,chan,:));
        theseBaselines= squeeze(baselines.data(:,chan,:));
        ii = find(ch' == iChannel);
        
        subplot(4,5,ii)
        
        [meanPower,w] = get_psd_epochs(theseEpochs,epochs.rate);
        plot(w,10*log10(meanPower),'r');
        [meanPowerBaseline,w] = get_psd_epochs(theseBaselines,baselines.rate);
        hold on;   plot(w,10*log10(meanPowerBaseline),'b');
        ylim([-20 20])
        xlim([inputs.freq(1) inputs.freq(end)]);
        title(epochs.label(iChannel))
        
        MeanPowerEpochAll(iChannel,:) = meanPower;
        MeanPowerBaselineAll(iChannel,:) = meanPowerBaseline;
        
    end
end

subplot(4,5,1)
mea = 10*log10(mean(MeanPowerEpochAll,1));
dev = 10*log10(std(MeanPowerEpochAll,1));
subplot(4,5,1)
hold on; patch([w' fliplr(w')],[mea+dev fliplr(mea-dev)],[1 0.8 0.8],'EdgeColor','None','HandleVisibility','off','FaceAlpha',0.5);
hold on;plot(w,mea,'r');
mea = 10*log10(mean(MeanPowerBaselineAll,1));
dev = 10*log10(std(MeanPowerBaselineAll,1));
hold on; patch([w' fliplr(w')],[mea+dev fliplr(mea-dev)],[0.8 0.8 1],'EdgeColor','None','HandleVisibility','off','FaceAlpha',0.5);
hold on;plot(w,mea,'b');
ylim([-20 20])
xlim([inputs.freq(1) inputs.freq(end)]);
title('Average')




ta = annotation('textbox', [0.85, 0.95, 0 0], 'string', 'Epoch');
ta.FontSize = 15;
ta.Color = 'red';
ta =annotation('textbox', [0.85, 0.9, 0, 0], 'string', 'Baseline');
ta.FontSize = 15;
ta.Color = 'blue';

fprintf('\n')
end


%% Parser
function funcInputs = parse_my_inputs(epochs,baselines,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baselines',@isstruct);
addParameter(funcInputs,'win',[],@isnumeric);
addParameter(funcInputs,'noverlap',[],@isnumeric);
expectedType = {'average','trial'};
addParameter(funcInputs,'type',{'average'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'freq',5:0.05:40,@isnumeric);
expectedType = {'log','linear'};
addParameter(funcInputs, 'normalization',{'log'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'event2show',[],@isnumeric);

parse(funcInputs, epochs,baselines, varargin{:});

funcInputs = funcInputs.Results;

if isempty(funcInputs.win)
   funcInputs.win = epochs.rate; 
end

if isempty(funcInputs.noverlap)
   funcInputs.noverlap = epochs.rate-16; 
end

end
