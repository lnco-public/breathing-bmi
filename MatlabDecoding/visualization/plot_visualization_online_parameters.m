global verbose

if verbose
    fprintf('\nPlotting Process for online parameters...\n')
end
h = figure()
subplot(2,4,1)
plot_singleTrial_classification(prob.value,prob.time,3);
set(gca,'title',[])
subplot(2,4,2)
result = plot_single_trial_detection(prob, 'detectionMethod', {'accumulateDecision'},'parameter',5);
set(gca,'title',[])
subplot(2,4,5)
plot_CR_over_time(prob.value,prob.time,'color',[0 0 0],'ColorPatch',[0.8    0.8    0.8],'showDeviation',{'std'});
set(gca,'title',[])
subplot(2,4,6)
bar(prob.time,mean(result,1))
axis([prob.time(1) prob.time(end) 0 0.1])
subplot(2,4,3)
result = plot_single_trial_detection(prob, 'detectionMethod', {'evidenceAccumulation'},'smoothing_parameter',0.8,'threshold',0.7);
set(gca,'title',[])
subplot(2,4,7)
bar(prob.time,mean(result,1))
axis([prob.time(1) prob.time(end) 0 0.1])
subplot(2,4,4)
result = plot_single_trial_detection(prob, 'detectionMethod', {'movingAverage'},'parameter',16,'threshold',0.7);
set(gca,'title',[])
subplot(2,4,8)
bar(prob.time,mean(result,1))
axis([prob.time(1) prob.time(end) 0 0.1])



figure()
result = plot_single_trial_detection(prob, 'detectionMethod', {'accumulateDecision'},...
    'parameter',5,'showCorrectTrials',true,...
    'windowForCorrectTrial',[0.5 1.5]);
