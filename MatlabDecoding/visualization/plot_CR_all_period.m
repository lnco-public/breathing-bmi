function plot_CR_all_period(prob_output,time,varargin)


inputs = parse_my_inputs(prob_output,time,varargin{:});

if iscell(prob_output)
   p = cell2mat(prob_output);
end



if strcmp(inputs.smoothing,'acumulative')
    % smoothing - exponential accumulative function
    for iTime = 1:size(prob_output,2)
        if iTime == 1
            p(:,iTime) = inputs.beta*0.5 + (1-inputs.beta) * p(:,iTime);  
        else
            p(:,iTime) = inputs.beta*p(:,iTime-1) + (1-inputs.beta) * p(:,iTime);
        end
    end
end

p = reshape(p',1,[]);  

plot(p','Color',inputs.color)


title('Classification rate')
ylim([0 1]);
xlabel('Time(s)')
ylabel('Probability Onset')

end

%% Parser
function funcInputs = parse_my_inputs(prob_output,time,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'prob_output',@(x) iscell(x) || isnumeric(x));
addRequired(funcInputs,'time', @isnumeric);
addParameter(funcInputs, 'color',[1 1 1], @(x) isnumeric(x) && length(x) == 3);
expectedType = {'acumulative','none'};
addParameter(funcInputs,'smoothing',{},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'beta', 0.9, @(x) isnumeric(x) && x <= 1);


parse(funcInputs, prob_output,time, varargin{:});
funcInputs = funcInputs.Results;

end
