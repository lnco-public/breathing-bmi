function [ICs_raws, matrix, W]  = plot_ICA_decomposition(runData,alignEvent,timeInterval)
%PLOT_ICA_DECOMPOSITION Summary of this function goes here
%   Detailed explanation goes here

timeBeforeEvent = 5;
timeAfterEvent = 5;

epochs = extract_epochs_runs(runData,...
    alignEvent,...
    timeBeforeEvent,...
    timeAfterEvent);

[nEpoch,nChannel,nTime] = size(epochs.data);

timeOfInterest = epochs.time >= timeInterval(1) & epochs.time <= timeInterval(2);

data = matrix2raw(epochs.data(:,:,timeOfInterest));

[ICs, matrix, W] = fastica(data,'verbose','off', 'lastEig', 16,'numOfIC', 16);
ICs_epochs = W* matrix2raw(epochs.data);
nICs = size(ICs,1);
% percent variance explained by IC component
pvaf = zeros(1,nICs);
fprintf('\nComputation of variance explained by each IC component \n')
for iIC = 1:nICs
    back_proj = matrix(:,iIC)*ICs(iIC,:);
    pvaf(iIC) = 100-100*mean(var(data - back_proj))/mean(var(data));
end

[pvaf,index] = sort(pvaf,'descend');

matrix = matrix(:,index);
W = W(index,:);
eegIC.data = ICs_epochs(index,:);
eegIC.label = 1:nICs;

for iRun = 1:length(runData)
    ICs_raws{iRun}= W* runData{iRun}.data(1:16,:);
end





%% Sorting 



for iIC = 1:nICs
     disp(['IC_' num2str(iIC) ':  ' num2str(pvaf(iIC))])
end

epochs_ICs = raw2matrix(ICs_epochs,nTime,nEpoch);


fprintf('\Plotting independant component \n')


time = timeInterval(1):1/epochs.rate:timeInterval(2);

for iIC = 1:nICs
    h1 = figure();
    subplot(2,2,1)
    theseEpochs = squeeze(epochs_ICs(iIC,:,:));
    [meanPower,w] = get_psd_epochs(theseEpochs,512);
    plot(w,10*log10(meanPower),'r');
    xlim([0 40])
     
    subplot(2,2,2)
    plot_channel(eegIC,'channelID',1:nICs,'interval',20);
    title('IC components')
    
    subplot(2,2,3)
    topoplot(squeeze(matrix(:,iIC)),epochs.eloc,'style','map');
    
    subplot(2,2,4)
    theseEpochs = raw2matrix(ICs_epochs,nTime,nEpoch);
    theseEpochs = squeeze(theseEpochs(:,iIC,:));
    imagesc('XData',time,'YData',1:nEpoch,'CData',theseEpochs);
    axis tight;
    line([0 0],[1 nEpoch],'Color','k')
end

