function plot_features_mapping(matrix_featSpectral,freq,channels,intervalS)


if nargin == 3 
   intervalS = 1;
end

imagesc(matrix_featSpectral);
ylabel('Channels');
xlabel('Frequency(Hz)')
title('Feature selection');
set(gca,'xTick',1:intervalS:size(matrix_featSpectral,2),'XTickLabel',freq(1:intervalS:end),'XTickLabelRotation',90);
set(gca,'yTick',1:size(matrix_featSpectral,1),'YTickLabel',channels);
% caxis([0 1])
end

