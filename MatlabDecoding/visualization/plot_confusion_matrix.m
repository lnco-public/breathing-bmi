function C = plot_confusion_matrix(output_classification)



targets = cat(1,output_classification.testing(:).target)';
targets = targets + 1;
outputs = cat(1,output_classification.testing(:).class)';
outputs = outputs+1;

nClasses = length(unique(targets));
nSample = length(outputs);

targetsVector = zeros(nClasses,nSample);
outputsVector = zeros(nClasses,nSample);

targetsIdx = sub2ind(size(targetsVector), targets, 1:nSample);
outputsIdx = sub2ind(size(outputsVector), outputs, 1:nSample);

targetsVector(targetsIdx) = 1;
outputsVector(outputsIdx) = 1;

C = confusionmat(outputs,targets);

plotconfusion(targetsVector,outputsVector)
end
