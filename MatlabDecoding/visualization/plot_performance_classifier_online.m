

function perf = plot_performance_classifier_online(output,varargin)

global verbose
inputs = parse_my_inputs(output,varargin{:});

% %% Performance classifier

posterior_test = cat(1,output.posterior);
output_test = cat(1,output.class);
target_test = cat(1,output.target);

FPR_test  = [];TPR_test  = [];
aucTestFold = [];


FPR_test =  sum(output.class ~= output.target)/length(output.target);
TPR_test = sum(output.class == output.target)/length(output.target);


stdFPR_test  = std(FPR_test);
stdTPR_test  = std(TPR_test);


meaFPR_test  = mean(FPR_test);
meaTPR_test  = mean(TPR_test);

% ROC curve
[Xt,Yt,~,aucTest,optrocpt] =  perfcurve(target_test,posterior_test(:,2),1);


posterior_test = cat(1,output.posterior);
target_test = cat(1,output.target);

[~,~,~,auc_Test,~] = perfcurve(output.target,output.posterior(:,2),1);

aucTestFold = [aucTestFold ;auc_Test];


meaAUC_Test = mean(aucTestFold);
stdAUC_Test = std(aucTestFold,[],1);

if inputs.showTesting
    hold on;plot(Xt,Yt,'LineWidth',4);
    if inputs.showThreshold
        plot(optrocptt(1),optrocptt(2),'ro')
    end
    
    xlabel('False positive rate')
    ylabel('True positive rate')
    title('ROC Curve for Classification')
    grid on;
end

if inputs.showLegend
    legend(['Testing - AUC' num2str(meaAUC_Test) '+/-' num2str(stdAUC_Test)])
end

%% Verbose
if verbose
    fprintf('************************** \n')
    fprintf('Classification performance - Testing Set \nTPR = %.2f +/- %.2f,FPR = %.2f  +/- %.2f\n',meaTPR_test*100,stdTPR_test*100,meaFPR_test*100,stdFPR_test*100);
    fprintf('ROC - Testing Set AUC = %.2f  \n',aucTest*100);
end

%% Save performances

TPR.test.mean = meaTPR_test;
TPR.test.dev = stdTPR_test;

FPR.test.mean = meaFPR_test;
FPR.test.dev = stdFPR_test;

AUC.test.mean = meaAUC_Test;
AUC.test.dev = stdAUC_Test;

perf.TPR = TPR;
perf.FPR = FPR;
perf.AUC = AUC;

end

function funcInputs = parse_my_inputs(output,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'output',@isstruct);
addParameter(funcInputs, 'showTesting',false,@islogical);
addParameter(funcInputs,'showLegend',false,@islogical);
addParameter(funcInputs,'showThreshold',false,@islogical);

parse(funcInputs,output, varargin{:});
funcInputs = funcInputs.Results;

end







