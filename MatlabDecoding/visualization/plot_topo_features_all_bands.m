function [matrix_data,eloc] = plot_topo_features_all_bands(matrix,freq,eloc,varargin)


inputs = parse_my_inputs(matrix,freq,eloc,varargin{:});

if inputs.topo64
    processFolder = 'C:\Users\bastien\Documents\internship\Miw\hiamp64';
    eloc64 = importdata([processFolder filesep 'chanlocs.mat']);
    meanPower=  zeros(60,size(matrix,2));
    index60 = ismember(upper({eloc64.labels}),upper({eloc{1}.labels}));
    eloc = eloc64;
    meanPower(index60,:) = matrix;
    matrix = meanPower;
else
    eloc = eloc{1};
end


matrix_data = {};
nFreqRange = length(inputs.freqRangeList);
for iFreqRange = 1:nFreqRange
   thisFreqRange =  inputs.freqRangeList{iFreqRange};
   thisFreqRange = thisFreqRange{1}:0.1:thisFreqRange{2};
   [~,iFreq] = intersect(freq,thisFreqRange);
   matrix_data{end+1} = squeeze(mean(matrix(:,iFreq),2));
end

if inputs.plotting
    style = 'map';
    nMatrix = length(matrix_data);
    for iMatrix = 1:nMatrix
        subplot(1,nMatrix,iMatrix)
        topoplot(matrix_data{iMatrix},eloc,'style',style);
        title(inputs.freqRangeList{iMatrix}{3});
        caxis(inputs.caxis);
    end
end
end

%% Parser

function funcInputs = parse_my_inputs(matrix,freq,eloc,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'matrix', @isnumeric);
addRequired(funcInputs,'freq', @isnumeric);
addRequired(funcInputs,'eloc');
addParameter(funcInputs, 'caxis',[0 1], @(x) isnumeric(x) && length(x) == 2);
addParameter(funcInputs, 'topo64',true, @islogical);
addParameter(funcInputs, 'freqRangeList',[], @iscell);
addParameter(funcInputs, 'plotting',true, @islogical);

parse(funcInputs, matrix,freq,eloc, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.freqRangeList)
    freqRangeList = {};
    freqRangeList{1} ={-100,100,'All'};
    freqRangeList{2} ={8,12,'Mu band'};
    freqRangeList{3} ={12.5,30,'Beta band'};
    freqRangeList{4} ={12.5,15,'Low-Beta band'};
    freqRangeList{5} ={15.5,22,'Mid-Beta band'};
    freqRangeList{6} ={22.5,30,'High-Beta band'};
%     
%     
%     freqRangeList = {};
%     freqRangeList{1} ={-100,100,'All'};
%     freqRangeList{2} ={8,12,'Mu band'};
%     freqRangeList{3} ={12.5,30,'Beta band'};
%     freqRangeList{4} ={12.5,16,'Low-Beta band'};
%     freqRangeList{5} ={16.5,24,'Mid-Beta band'};
%     freqRangeList{6} ={24.5,30,'High-Beta band'};
%     
    
    funcInputs.freqRangeList = freqRangeList;
end

end

