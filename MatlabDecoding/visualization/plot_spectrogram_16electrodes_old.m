
function  [M,time,freq] = plot_spectrogram_16electrodes_old(epochs,baselines,varargin)

inputs = parse_my_inputs(epochs,baselines,varargin{:});

ch = [ 0 0 1 0 0; ...
    2  3 4 5 6; ...
    7 8 9 10 11; ...
    12 13 14 15 16];

[~,nChannel,~,~] = size(epochs.data);

M = {};
time = epochs.time;
freq = epochs.freq;

global verbose

if verbose
    fprintf('Spectrogram Plotting in Process...\n')
end

switch inputs.channelOfInterest{1}
    
    case 'all'
        
        for iChannel  = 1:nChannel
            
            theseEpochs = squeeze(epochs.data(:,iChannel,:,:));
            theseBaselines = squeeze(baselines.data(:,iChannel,:,:));
            [P,R] = extract_PSD_trials(theseEpochs,theseBaselines);
            
            M{iChannel} = normalize_power(P,R);
            
            if inputs.plotting
                % Plotting
                ii = find(ch' == iChannel);
                ax = subplot(4,5,ii);
                plotSpectrogramDisplayer(time,freq,M{iChannel},epochs.label(iChannel));
                
                currentPlot = gca;
                currentFigure = gcf;
                set(currentPlot,'ButtonDownFcn',@(~,~)plotSpectrogramDisplayer(time,freq,M{iChannel},epochs.label(iChannel),currentFigure)); 
            end       
        end
        if inputs.plotting
            hp4 = get(subplot(4,5,20),'Position');
            colorbar('Position', [hp4(1)+hp4(3)+0.01  hp4(2)  0.02  hp4(2)+hp4(3)*5.7])
        end
        
    otherwise
        
        for iChannel = 1:length(inputs.channelOfInterest)
            thisChannel = epochs.map_ch(inputs.channelOfInterest{iChannel});
            theseEpochs = squeeze(epochs.data(:,thisChannel,:,:));
            theseBaselines = squeeze(baselines.data(:,thisChannel,:,:));
            [P,R] = extract_PSD_trials(theseEpochs,theseBaselines);
            M{iChannel} = normalize_power(P,R);
            
           
            if inputs.plotting
                % Plotting
                figure()
                plotSpectrogramDisplayer(time,freq,M{iChannel},epochs.label(iChannel));
            end
        end
        
end

if verbose
    fprintf('\n')
end
end



function [P,R] = extract_PSD_trials(theseEpochs,theseBaselines)

nEpoch = size(theseEpochs,1);

for iEpoch = 1:nEpoch
    
    P(iEpoch,:,:) = squeeze(theseEpochs(iEpoch,:,:));
    basPower = squeeze(theseBaselines(iEpoch,:,:));
    R(iEpoch,:,:) = mean(basPower,2)*ones(1,size(P,3));
end

end

function X = normalize_power(P,R)

X = bsxfun(@minus,P,R);
X = 10*mean(X,1);
X = squeeze(X);

end


%% Parser
function funcInputs = parse_my_inputs(epochs,baselines,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baselines',@isstruct);
addParameter(funcInputs,'freq',5:0.1:35,@isnumeric);
addParameter(funcInputs, 'channelOfInterest',{},@iscellstr);
addParameter(funcInputs, 'plotting',true,@islogical);
parse(funcInputs, epochs,baselines, varargin{:});

funcInputs = funcInputs.Results;

if isempty(funcInputs.channelOfInterest)
    funcInputs.channelOfInterest = {'all'};
end

if ndims(epochs.data) > 3
    funcInputs.fastPwelchUsed = true;
end


end
