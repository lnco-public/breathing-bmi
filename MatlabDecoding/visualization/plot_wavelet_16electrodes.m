function [M,time,freq,P,R] = plot_wavelet_16electrodes(epochs,baselines,varargin)

inputs = parse_my_inputs(epochs,baselines,varargin{:});

M = [];
time = epochs.time;
freq = inputs.freq;

global verbose

if verbose
    fprintf('Spectrogram Plotting in Process...\n')
end

if strcmp(inputs.channelOfInterest{1},'all')
    channelOfInterest = epochs.label; 
else
    channelOfInterest = inputs.channelOfInterest;
end
  
for iChannel = 1:length(channelOfInterest)
    thisChannel = epochs.map_ch(channelOfInterest{iChannel});
    theseEpochs = squeeze(epochs.data(:,thisChannel,:,:));
    theseBaselines = squeeze(baselines.data(:,thisChannel,:,:));
    [P(iChannel,:,:),R(iChannel,:,:)] = extract_PSD_wth_wavelet(theseEpochs,theseBaselines,inputs);
    M(iChannel,:,:) = 10*log10( bsxfun(@rdivide, squeeze(P(iChannel,:,:)), mean(squeeze(R(iChannel,:,:)),2)));
end
   
if inputs.plotting
    line2Plot = 0;
    switch inputs.channelOfInterest{1}
        case 'all'
            plotSpectrogramAllChannels(M,time,freq,channelOfInterest,line2Plot)
        otherwise
            for iChannel = 1:length(channelOfInterest)
%                 figure()
                AveragePower4ThisChannel =squeeze(M(iChannel,:,:,:));
                plotSpectrogramDisplayer(AveragePower4ThisChannel,time,freq,channelOfInterest,line2Plot);
                xlim([time(1)+0.15 time(end)-0.15]);
                c= colorbar;
                c.Label.String = 'ERD/ERS - dB';
               
            end
    end
end

if verbose
    fprintf('\n')
end

end

function [tf_epoch,tf_baseline] = extract_PSD_wth_wavelet(theseEpochs,theseBaselines,inputs)

nFreq = length(inputs.freq);
[nEpoch,nPointsEpoch] = size(theseEpochs);
[nBaseline,nPointsBaseline] = size(theseBaselines);
time  = -inputs.win:1/inputs.epochs.rate:inputs.win;
half_wave = (length(time)-1)/2;

% other wavelet parameters
cycRange = [4 15];
nCycles  = logspace(log10(cycRange(1)),log10(cycRange(end)),nFreq);

% FFT parameters Epochs
nWave = length(time);
nData = nPointsEpoch*nEpoch;
nConv = nWave+nData-1;

% FFT parameters Baseline
nData = nPointsBaseline*nBaseline;
nConvB = nWave+nData-1;

% initialize output time-frequency data
tf_epoch = zeros(nFreq,nPointsEpoch);
tf_baseline = zeros(nFreq,nPointsBaseline);

% spectra of data
data_epoch = fft( reshape(theseEpochs',1,[]) ,nConv);
data_baseline = fft( reshape(theseBaselines',1,[]) ,nConvB);


for fi=1:nFreq
    
    % create wavelet and get its FFT
    s = nCycles(fi)/(2*pi*inputs.freq(fi));
    cmw = exp(2*1i*pi*inputs.freq(fi).*time) .* exp(-time.^2./(2*s^2));
    
    tempX = fft(cmw,nConv);
    cmwX = tempX ./ max(tempX);
    
    % run convolution for epochs
    asE = ifft(cmwX.*data_epoch,nConv);
    asE = asE(half_wave+1:end-half_wave);
    asE = reshape(asE,nPointsEpoch,nEpoch);
    
    tempX = fft(cmw,nConvB);
    cmwX = tempX ./ max(tempX);
    
    % run convolution for epochs
    asB = ifft(cmwX.*data_baseline,nConvB);
    asB = asB(half_wave+1:end-half_wave);
    asB = reshape(asB,nPointsBaseline,nBaseline);
    
    % put power data into big matrix
    tf_epoch(fi,:) = mean(abs(asE).^2,2);
    tf_baseline(fi,:) = mean(abs(asB).^2,2);
end

end

%% Parser
function funcInputs = parse_my_inputs(epochs,baselines,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baselines',@isstruct);
addParameter(funcInputs,'freq',4:1:40,@isnumeric);
addParameter(funcInputs,'win',2,@isnumeric);

addParameter(funcInputs, 'channelOfInterest',{},@iscellstr);
addParameter(funcInputs, 'plotting',true,@islogical);
parse(funcInputs, epochs,baselines, varargin{:});

funcInputs = funcInputs.Results;

if isempty(funcInputs.channelOfInterest)
    funcInputs.channelOfInterest = {'all'};
end

if ndims(epochs.data) > 3
    funcInputs.fastPwelchUsed = true;
end


end


