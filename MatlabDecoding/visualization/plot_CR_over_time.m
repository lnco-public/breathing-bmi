function [meanOverFold,devOVerFold,timeDetection] = plot_CR_over_time(prob_output,time,varargin)


inputs = parse_my_inputs(prob_output,time,varargin{:});

nFold = size(prob_output,1);
meanFold =[];
meanEvidence_Fold = [];
devOVerFold = [];
if iscell(prob_output)
    nClasses = size(cell2mat(prob_output(1,1)),2);
    
    if nClasses > 2 && isempty(inputs.threshold)
   inputs.threshold =  1/nClasses;
    end

end

for iFold = 1:nFold
    if iscell(prob_output)
        outputFold = cell2mat(prob_output(iFold,:));
    else
        outputFold = prob_output(iFold,:);
    end
    
    if size(prob_output,2) < size(outputFold,2) 
      outputFold = outputFold(:,inputs.showProb:nClasses:end); 
    end
    meanFold = [meanFold; mean(outputFold,1)];
    
end

% Detection over time - posterior probability
meanOverFold = mean(meanFold,1);

if strcmp(inputs.showDeviation,'std')
    devOVerFold = std(meanFold); 
elseif strcmp(inputs.showDeviation,'stdError')
    devOVerFold = std(meanFold)/sqrt(nFold);
end

hold all

if strcmp(inputs.showDeviation,'std') || strcmp(inputs.showDeviation,'stdError')
    h1 = patch([time fliplr(time)],[meanOverFold+devOVerFold fliplr(meanOverFold-devOVerFold)],inputs.ColorPatch,'EdgeColor','None','HandleVisibility','off','FaceAlpha',inputs.FaceAlpha);
end

if strcmp(inputs.showDeviation,'lines')
    for i = 1:size(prob_output,1)
        plot(inputs.time,prob_output(i,:),'Color',inputs.ColorPatch,'LineWidth',0.75)
    end
end



h2 = plot(inputs.time,meanOverFold,'Color',inputs.color,'LineWidth',2);


% align to 0
line([0 0],[0 1],'Color',[0.85 0.33 0.1],'LineWidth',2,'HandleVisibility','off');


if strcmp(inputs.smoothing{1},'accumulative')
    meanOverFold = mean(meanEvidence_Fold,1);
    if strcmp(inputs.showDeviation,'none')
        devOVerFold = std(meanEvidence_Fold);
        h4 = patch([time fliplr(time)],[meanOverFold+devOVerFold fliplr(meanOverFold-devOVerFold)],inputs.ColorPatch,'EdgeColor','None');
    end
    h5 = plot(inputs.time,meanOverFold,'Color',[0 0 1],'LineWidth',2);
end

% Find time when above threshold
if ~isempty(inputs.threshold)
try
    iOrigin = find(time == 0) ;
    
     dataUptoThreshold = find(meanOverFold(iOrigin:end) >= inputs.threshold,1,'first');
%        dataUptoThreshold = find(meanOverFold >= inputs.threshold,1,'first');
      detectionIndex = dataUptoThreshold;
    
    threshold = iOrigin + detectionIndex(1);
%       threshold = detectionIndex(1);
    timeDetection = time(threshold);
    if ~isempty(threshold)
        line([timeDetection timeDetection],[0 1],'Color',inputs.color,'LineStyle','--','LineWidth',1,'HandleVisibility','off');
    end
catch
   disp('no detection') 
   timeDetection = nan;
end
end


ylim([0 1])
xlim([time(1) time(end)])
xlabel('Time[s]');ylabel('Decoding Likelihood')
grid on;
box off;

%Get text
if inputs.checkLegend
    hLegend = findobj(gcf, 'Type', 'Legend');
end


end

%% Parser
function funcInputs = parse_my_inputs(prob_output,time,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'prob_output',@(x) iscell(x) || isnumeric(x));
addRequired(funcInputs,'time', @isnumeric);
addParameter(funcInputs, 'color',[0 0 0], @(x) isnumeric(x) && length(x) == 3);
addParameter(funcInputs, 'dash',false, @islogical);
addParameter(funcInputs, 'checkLegend',false, @islogical);
expectedType = {'acumulative','none'};
addParameter(funcInputs,'smoothing',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'alpha', 0.96, @(x) isnumeric(x) && x <= 1);
expectedType = {'std','stdError','lines','none'};
addParameter(funcInputs,'showDeviation',{'std'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'ColorPatch',[0.9 0.9 0.9], @(x) isnumeric(x) && length(x) == 3);
addParameter(funcInputs, 'threshold',0.5, @(x) isnumeric(x));
addParameter(funcInputs, 'FaceAlpha',0.8, @(x) isnumeric(x));
addParameter(funcInputs, 'showProb',2, @isnumeric);
parse(funcInputs, prob_output,time, varargin{:});
funcInputs = funcInputs.Results;




end
