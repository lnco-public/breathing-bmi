function  plot_epoch_for_rejection(epochs,interval)
% Create a figure and axes
f = figure('Visible','off','KeyPressfcn',@KeyPressCallback);

% initialize figure
nEpoch = size(epochs.data,1);
iEpoch = 1;
label = epochs.label;
plot_epoch(epochs,iEpoch,label,interval)

epoch2Remove = zeros(1,nEpoch);

% Create push button
btn1 = uicontrol('Style', 'pushbutton', 'String', '<<Previous',...
    'Position', [80 5 100 20],...
    'Callback', @previousfunction);

% Create push button
btn2 = uicontrol('Style', 'pushbutton', 'String', 'Reject',...
    'Position', [240 5 100 20],...
    'Callback', @rejectfunction);


btn3 = uicontrol('Style', 'pushbutton', 'String', 'Next>>',...
    'Position', [380 5 100 20],...
    'Callback', @nextfunction);

set(btn1, 'Enable', 'off');
set(f, 'DeleteFcn', @myhandle) 


% Make figure visble after adding all components
f.Visible = 'on';
% This code uses dot notation to set properties.
% Dot notation runs in R2014b and later.
% For R2014a and earlier: set(f,'Visible','on');


    function nextfunction(source,event)
        
       
        if iEpoch ~= nEpoch
            iEpoch = iEpoch + 1;
        end
        
        if iEpoch == nEpoch
            set(btn3, 'Enable', 'off');
        else
            set(btn3, 'Enable', 'on');
        end;
        
        if iEpoch <= 1
            set(btn1, 'Enable', 'off');
        else
            set(btn1, 'Enable', 'on');
        end
        
        
        plot_epoch(epochs,iEpoch,label,interval)
      
        
    end

    function previousfunction(source,event)
        
        
        if iEpoch > 1
            iEpoch = iEpoch - 1;
       
            
        end
  
        if iEpoch == nEpoch
            set(btn3, 'Enable', 'off');
        else
            set(btn3, 'Enable', 'on');
        end;
        
        if iEpoch <= 1
            set(btn1, 'Enable', 'off');
        else
            set(btn1, 'Enable', 'on');
        end
        
        
        
        plot_epoch(epochs,iEpoch,label,interval)

    end


 function epoch2Remove  = rejectfunction(source,event)
        
       
       epoch2Remove(1,iEpoch) = 1; 
       save('epoch2Remove.mat','epoch2Remove')
        
    end

 
    function  KeyPressCallback(source,evnt)
       
        switch evnt.Key
            case 'leftarrow'
                previousfunction(source,evnt);
            case 'rightarrow'
                nextfunction(source,evnt);
            case 'escape'
                save('epoch2Remove.mat','epoch2Remove')
                myhandle;
        end
    end






  function  myhandle
       
        
         save('epoch2Remove.mat','epoch2Remove')
                close(f)
  end


end


