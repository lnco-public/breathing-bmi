function plot_channel(eeg,varargin)

inputs = parse_my_inputs(eeg,varargin{:});

%% Channel visualization
% disp( 'Plotting raw data...' );
title('EEG Recording')

nChannel = length(inputs.channelID);
ystart = zeros(nChannel,1);

% if ~isempty(eeg.event)
%     eventIndex = strcmp(eeg.event.name,inputs.alignedEvent);
%     eventPosition = eeg.event.position(eventIndex);
% else
%     eventPosition = [];
% end
for iChannel = 1:nChannel
    
   thisChannel = inputs.channelID(iChannel);
    plot(eeg.data(thisChannel,:)-((iChannel-1)*inputs.interval) );
    ystart(iChannel) = nanmean(eeg.data(thisChannel,:)-((iChannel-1)*inputs.interval));
    hold all;
end

[ystart,index]= sort(ystart);
if ~isempty(eeg.label)
    label = eeg.label(inputs.channelID);
set(gca,'YTick',ystart,'YTickLabel',label(index))
end

ylim([min(ystart)-inputs.interval max(ystart)+inputs.interval])
hold off;

end


function funcInputs = parse_my_inputs(eeg, varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'eeg',@isstruct);
addParameter(funcInputs,'channelID',[],@isnumeric);
addParameter(funcInputs,'interval',1000,@isnumeric);
addParameter(funcInputs,'alignedEvent',[],@iscellstr)
parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;


if isempty(funcInputs.channelID)
    funcInputs.channel = 1:size(eeg.data,1);
end

end

