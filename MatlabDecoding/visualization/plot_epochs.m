function  ystart = plot_epochs(theseEpochs,time,interval)

disp( 'Plotting epoch..' );
title('Amplitude of epochs over time')

nEpoch= size(theseEpochs,1);
ystart = zeros(nEpoch,1);
for iEpoch = 1:nEpoch
 
    plot(time,theseEpochs(iEpoch,:)-(iEpoch-1)*interval);
    ystart(iEpoch) = nanmean(theseEpochs(iEpoch,:)-(iEpoch-1)*interval);
    hold on;
end
xlabel('Time(sec)'); ylabel('Trials')


 [ystart,index]= sort(ystart);
 label = 1:nEpoch;
 label = label(index);
 index = mod(label,10);
set(gca,'YTick',ystart(index==0),'YTickLabel', num2str(label(index==0)'))

ylim([ystart(1)-interval ystart(end)+interval])


end


    
