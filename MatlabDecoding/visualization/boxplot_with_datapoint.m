function boxplot_with_datapoint(data,grp,label,Data)
%BOXPLOT_WITH_JITTER_AND_STARS Summary of this function goes here


if nargin == 1
    %% Latency for all subject + all runs
    if ndims(data) == 1
        nSubject = 1;
    else
        nSubject = size(data,1);
    end
    boxplot(data,'notch','on');
    set(findobj(gca,'type','line'),'linew',2);
    grid on
    hold all
    ylabel('Latency [s]')
    box off
    
    
    if ndims(data) == 1
        x = rand(1,length(data))/4+1-0.5/4;
        plot(x,data,'o',...
            'LineWidth',1,...
            'MarkerSize',5,'MarkerEdgeColor','k',...
            'MarkerFaceColor',[0.8 0.8 0.8])
        
        hold on;
    else
        for iSubject = 1:nSubject
            dataPoint = data(iSubject,:);
            x = rand(1,length(dataPoint))/4+iSubject-0.5/4;
            plot(x,dataPoint,'o',...
                'LineWidth',1,...
                'MarkerSize',5,'MarkerEdgeColor','k',...
                'MarkerFaceColor',[0.8 0.8 0.8])
            
            hold on;
        end
    end
else
    
    
    %% Latency for all subject + all runs
    nSubject = max(grp);
    grp_GA = [grp (nSubject+1)*ones(1,length(data))];
    dataGA = [data ;data];
    
    
    boxplot(dataGA,grp_GA,'notch','on');
    set(findobj(gca,'type','line'),'linew',2);
    set(gca,'xTick',1:nSubject+1,'XTickLabel',label);
    grid on
    hold all
    ylabel('Latency [s]')
    
    box off
    for iSubject = 1:nSubject
%         dataPoint = Data{iSubject};
        
        dataPoint = Data{iSubject}.time_latency;
        x = rand(1,length(dataPoint))/4+iSubject-0.5/4;
        plot(x,dataPoint,'o',...
            'LineWidth',1,...
            'MarkerSize',5,'MarkerEdgeColor','k',...
            'MarkerFaceColor',[0.8 0.8 0.8])
        hold on;
    end
end



end