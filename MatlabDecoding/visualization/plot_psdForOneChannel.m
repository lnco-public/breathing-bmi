function plot_psdForOneChannel(epochs,baseline,varargin)

%PLOT_PSD Summary of this function goes here
%   Detailed explanation goes here

inputs = parse_my_inputs(epochs,baseline,varargin{:});

chanOfInterest = ismember(epochs.label,inputs.channel);
theseEpochs = squeeze(epochs.data(:,chanOfInterest,:));
theseBaselines = squeeze(baseline.data(:,chanOfInterest,:));


[X,time] = compute_psd(theseEpochs,theseBaselines,...
    'win',inputs.win,...
    'noverlap',noverlap,...
    'rate',epochs.rate,...
    'freq',inputs.freq,...
    'normalization',inputs.normalization);

if inputs.doPlot
    colormap('jet');
    c = colorbar;
    imagesc('XData',time + epochs.time(1),'YData',f,'CData',m)
    label = inputs.channel;
    title(['ERD percentage for ',label])
    xlabel('Time(sec)'); ylabel('Frequency(Hz)')
    
    if strcmp(inputs.normalization,'linear')
        caxis([-100 100])
        c.Label.String = 'ERD/ERS - Power';
    else
        c.Label.String = 'ERD/ERS - dB';
    end
end

end

function funcInputs = parse_my_inputs(epochs,baseline,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baseline',@isstruct);
addParameter(funcInputs,'win',[],@isnumeric);
addParameter(funcInputs,'channel',[],@iscellstr);
addParameter(funcInputs,'noverlap',[],@isnumeric);
addParameter(funcInputs,'freq',7:25,@isnumeric);
addParameter(funcInputs,'doPlot',true,@islogical);
expectedType = {'spectrogram','periodogram'};
addParameter(funcInputs,'plotType',{'spectrogram'},@(x) any(validatestring(x{:}, expectedType)));

expectedType = {'log','linear'};
addParameter(funcInputs, 'normalization',{'log'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,epochs,baseline,varargin{:});
funcInputs = funcInputs.Results;


end