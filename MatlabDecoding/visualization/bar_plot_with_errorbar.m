function stat_pvalues = bar_plot_with_errorbar(mean_data,dev_data,varargin)
%BAR_PLOT_WITH_ERRORBAR Summary of this function goes here


inputs = parse_my_inputs(mean_data,dev_data,varargin{:});


[nBar,nGroups] = size(mean_data);
color = [0.8 0.8 0.8];
face_color =   [0    0.4471    0.7412];
% Finding the number of groups and the number of bars in each group
% Calculating the width for each bar group
groupwidth = min(0.8, nGroups/(nGroups + 1.5));
% Set the position of each error bar in the centre of the main bar

if inputs.horizontalPlot
    barh(mean_data,'FaceColor',face_color);
else
    bar(mean_data,'FaceColor',face_color);
end

stat_pvalues = [];

center_1 = (1:nBar) - groupwidth/2 + (2-1) * groupwidth / (2*nGroups);
center_2 = (1:nBar) - groupwidth/2 + (2*2-1) * groupwidth / (2*nGroups);

center = [];

for iGroups = 1:nGroups
    % Calculate center of each bar
    center(iGroups,:) = (1:nBar) - groupwidth/2 + (2*iGroups-1) * groupwidth / (2*nGroups);
end    
    

if nGroups > 1 && inputs.statDiff && ~isempty(inputs.Data)
    data_all = cell2mat(inputs.Data);
    nVar = size(inputs.Data{1},2);
    nClass = length(inputs.Data);
    for iBar = 1:nBar
        
        if iBar == nBar
            thisBarData = mean_data(1:end-1,:);
        else
            thisBarData = data_all(iBar,:);
            thisBarData = reshape(thisBarData,nVar,nClass);
        end
        
        try
            
            %             [p,tbl,stats] = anova1(thisBarData,[],'off');
            %             stat_pvalues_temp = multcompare(stats,[],'off');
            [h, p,ci, stats] = ttest(thisBarData(:,1),thisBarData(:,2));
            stat_pvalues = [stat_pvalues;p];
            
            left= center(1,iBar);
            right = center(2,iBar);
            if p < 0.05
                sigstar({[left right]},p)
            end
        catch
            disp('cannot do the stat')
        end
    end
end


% if nGroups > 1 && inputs.statDiff && ~isempty(inputs.Data)
%     data_all = cell2mat(inputs.Data);
%     nVar = size(inputs.Data{1},2);
%     nClass = length(inputs.Data);
%     for iBar = 1:nBar
%         
%         if iBar == nBar 
%             thisBarData = mean_data(1:end-1,:);
%         else 
%             thisBarData = data_all(iBar,:);
%             thisBarData = reshape(thisBarData,nVar,nClass);
%         end
% %          c = ranksum(thisBarData(:,1),thisBarData(:,2));
% %          disp(c)
% %          index_significant = find(c(:,end)< 0.05);
% %          stat_pvalues(iBar) = c(:,end);
%         
%         try
%          stat_pvalues(iBar) = mattest(thisBarData(:,1)',thisBarData(:,2)');
%          index_significant = stat_pvalues(iBar)< 0.05;
%         
%         for ii = 1:length(index_significant)
%             thisTest = index_significant(ii);
%             if size(inputs.Data,2) > 2
%                 sigstar({stat_pvalues(thisTest,1:2)},stat_pvalues(thisTest,end))
%             else
%                 if stat_pvalues(iBar)<0.05
%                     sigstar({[center_1(iBar) center_2(iBar)]},stat_pvalues(iBar))
%                 end
%             end
%         end
%         catch
%             disp('cannot do the stat')
%         end
%         
%     end
% end

hold on;

for iGroups = 1:nGroups
    % Calculate center of each bar
    center = (1:nBar) - groupwidth/2 + (2*iGroups-1) * groupwidth / (2*nGroups);
    
    %     errorbar([],x,mean_data(:,iBar), dev_data(:,iBar),'k.');
    if inputs.horizontalPlot
        errorbarxy(mean_data(:,iGroups),center, dev_data(:,iGroups),[],{'k.', 'k', 'k'})
    else
        errorbarxy(center,mean_data(:,iGroups),[],dev_data(:,iGroups),{'k.', 'k', 'k'})
    end
    hold all
    for iBar = 1:nBar
        if iBar == nBar
            
            dataPoint = mean_data(1:end-1,iGroups);
            x = rand(1,length(dataPoint))/6+center(iBar)-groupwidth/12;
        else
            if ~isempty(inputs.Data)
                dataPoint =inputs.Data{iGroups}(iBar,:)';
                x = rand(1,length(dataPoint))/6+center(iBar)-groupwidth/12;
            else
                x = [];
            end
        end
        if isempty(inputs.Color)
            colorTemp = color;
            if iGroups < 3 
                colorTemp(iGroups) = 1;
            end
        end
        if ~isempty(x)
%             try
            if inputs.horizontalPlot
                plot(dataPoint,x,'o',...
                    'LineWidth',1,...
                    'MarkerSize',5,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor',inputs.Color{iGroups})
            else
                plot(x',dataPoint,'o',...
                    'LineWidth',1,...
                    'MarkerSize',5,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor',inputs.Color{iGroups})
            end
%             catch
%             end
        end
        hold on;
    end
    
    
   
end

ylabel('Accuracy');

if nGroups == 1
    title(['GA accuracy: ' num2str(round(mean_data(end),3)) ' -/+ ' num2str(round(dev_data(end)/2,3))])
end

grid on

end


%% Parser
function funcInputs = parse_my_inputs(mean_data,dev_data,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'mean_data',@isnumeric);
addRequired(funcInputs,'dev_data',@isnumeric);
addParameter(funcInputs, 'horizontalPlot',false, @islogical);
addParameter(funcInputs, 'statDiff',true, @islogical);
addParameter(funcInputs,'Data',[],@iscell);
addParameter(funcInputs,'Color',{ [0    0.4471    0.7412],...
    [0    0.4471    0.7412],...
    [0    0.4471    0.7412]},@iscell);

parse(funcInputs,mean_data,dev_data, varargin{:});
funcInputs = funcInputs.Results;




end

