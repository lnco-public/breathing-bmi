function epochs = remove_trials(epochs,trial2Remove)

if ndims(epochs.data) == 3
    epochs.data(trial2Remove,:,:) = [];
else
    epochs.data(trial2Remove,:,:,:) = [];
end

epochs.event(trial2Remove) = [];
epochs.nTrial = epochs.nTrial - length(trial2Remove);
    

end

