function epochs_cropped = crop_epochs(epochs,timeInterval)
%CROP_EPOCHS Summary of this function goes here
%   Detailed explanation goes here



tLeft = find(epochs.time >= timeInterval(1),1,'first');

tRight = find(epochs.time >= timeInterval(2),1,'last');



epochs_cropped = epochs;
if ndims == 4
    epochs_cropped.data = epochs.data(:,:,:,tLeft:tRight);
elseif ndims == 3
    epochs_cropped.data = epochs.data(:,:,tLeft:tRight);
end
epochs_cropped.time  = epochs.time(tLeft:tRight);

end

