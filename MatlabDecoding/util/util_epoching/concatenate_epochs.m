function epochs = concatenate_epochs(epochsCellFormat)

epochs = epochsCellFormat{1};
epochs.data = [];
epochs.event = [];
epochs.nTrial = 0;

for iCell = 1:length(epochsCellFormat)
    if epochsCellFormat{iCell}.nTrial ~= 0 
        epochs.data = cat(1,epochs.data ,epochsCellFormat{iCell}.data);
        epochs.event = [epochs.event,epochsCellFormat{iCell}.event];
        epochs.nTrial = epochs.nTrial  + epochsCellFormat{iCell}.nTrial;
    end
end

end

