function epochs = concatenate_epochs_over_time(epochsCellFormat)

epochs = epochsCellFormat{1};

for iCell = 2:length(epochsCellFormat)
    if epochsCellFormat{iCell}.nTrial ~= 0 
        epochs.data = cat(4,epochs.data ,epochsCellFormat{iCell}.data);
         epochs.time = [epochs.time epochs.time(end)  + epochsCellFormat{iCell}.time];
        epochs.event = add_events_on_epoch_struct(epochs.event,epochsCellFormat{iCell}.event);
    end
end

end

function new_event = add_events_on_epoch_struct(event,eventOfCell)

try 
    new_event.name = [[event.name];[eventOfCell.name]];
    new_event.position = [[event.position]; [eventOfCell.position]];
    new_event.time = [[event.time] ;[eventOfCell.time] ];
    new_event.raw_position = [[event.raw_position] ;[eventOfCell.raw_position]];
catch
   disp('issue')
   new_event = event;
end


end