function latency = get_latency_for_epochs(epochs,event1,event2)
%GET_LATENCY_FOR_EPOCHS Summary of this function goes here
%   Detailed explanation goes here


pos1 = zeros(1,epochs.nTrial);
pos2 = zeros(1,epochs.nTrial);

for iTrial = 1 :epochs.nTrial;
    thesePosition = epochs.event(iTrial).time;
    theseName = epochs.event(iTrial).name;
    
    try
    pos1(iTrial) = thesePosition(theseName == event1);
    catch
    pos1(iTrial) = nan;
    end
    
    try
    pos2(iTrial) = thesePosition(theseName == event2);
    catch
    pos2(iTrial)= nan;
    end
    
    
    
end

latency = pos2-pos1;
end

