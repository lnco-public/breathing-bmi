
function epochs_PSD = get_psd_as_epochs_fast(epochs,baselines,freq,channel_str)

epochs_PSD  = epochs;
epochs_PSD.data = [];
if nargin == 3
    channelID = [];
else
    channelID = find(ismember(epochs.label,channel_str) == 1); 
    epochs_PSD.label  = epochs.label(channelID);
end

iFreq = ismember(epochs.freq,freq);

theseEpochs = squeeze(epochs.data(:,channelID,iFreq,:));
theseBaselines = squeeze(baselines.data(:,channelID,iFreq,:));
basPower = mean(theseBaselines,3);

nEpoch = size(theseEpochs,1);

for iEpoch = 1:nEpoch
    R(iEpoch,:,:) = basPower(iEpoch,:)'*ones(1,size(theseEpochs,3));
end  

PR = bsxfun(@minus,theseEpochs,R);

epochs_PSD.data(:,1,:) = 10*mean(PR,2);
epochs_PSD.baseline(:,1) = 10*mean(basPower,2);


end