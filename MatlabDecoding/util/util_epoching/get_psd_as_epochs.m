
function epochs_PSD = get_psd_as_epochs(epochs,baselines,freq,channel_str)

epochs_PSD  = epochs;

if nargin == 3
    channelID = [];
else
    channelID = find(ismember(epochs.label,channel_str) == 1); 
    epochs_PSD.label  = epochs.label(channelID);
end


win = epochs.rate;
noverlap = epochs.rate-1;
rate = epochs.rate;
theseEpochs = squeeze(epochs.data(:,channelID,:));
theseBaselines = squeeze(baselines.data(:,channelID,:));
[p_beta,time,f] = compute_psd(theseEpochs, theseBaselines,...
    'freq',freq,...
    'win',win,...
    'noverlap',noverlap,...
    'averagePowerEpoch',false);

p_beta = squeeze(mean(p_beta,3));


% 
if length(channelID) == 1
    epochs_PSD.data = [];
    leftmatrix = zeros(size(p_beta,1),win/2);
    rightmatrix = zeros(size(p_beta,1),win/2 - 1);
    epochs_PSD.data(:,1,:) = cat(2,leftmatrix,p_beta,rightmatrix);

else
    epochs_PSD.data = [];
    leftmatrix = zeros(size(p_beta,1),size(p_beta,2),win/2);
    rightmatrix = zeros(size(p_beta,1),size(p_beta,2),win/2 - 1);
    epochs_PSD.data = cat(3,leftmatrix,p_beta,rightmatrix);
  
end

end