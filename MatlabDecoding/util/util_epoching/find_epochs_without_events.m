function epochList = find_epochs_without_events(epochs)

epochList = [];


for iEpoch = 1:length(epochs.event)
    
    thisEpoch =  epochs.event(iEpoch).name;
    
    if isempty(thisEpoch)
        
        epochList = [epochList iEpoch];
        
        
    end
    
    
end


end

