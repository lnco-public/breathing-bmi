function epochs= timing_epochs(epochs,timeBeforeEvent,timeAfterEvent)
%TIMING_EPOCHS Summary of this function goes here
%   Detailed explanation goes here


alignEventTime = find(epochs.time == 0);
posLeft = alignEventTime - floor(epochs.rate*timeBeforeEvent);
posRight = alignEventTime + floor(epochs.rate*timeAfterEvent);
epochs.data = epochs.data(:,:,posLeft+1:posRight);
epochs.time = epochs.time(posLeft+1:posRight);

%% TOO DO 
% event to change on the epochs

end

