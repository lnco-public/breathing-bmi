function s = correct_mistakes_order(s,eventOfInterestON,eventOfInterestOFF)


stop = false;
iteration = 0;
while ~stop
    indexON = find(s.event.name == eventOfInterestON);
    indexOFF = find(s.event.name == eventOfInterestOFF);
    iteration = iteration + 1;
    onset = length(indexON);
    offset = length(indexOFF);
    
        indexNon =  ~ismember(indexON + 1,indexOFF);
        indexNoff =  ~ismember(indexOFF - 1,indexON);
        
        s.event.name(indexON(indexNon)) = [];
        s.event.name(indexOFF(indexNoff)) = [];
        
        s.event.position(indexON(indexNon)) = [];
        s.event.position(indexOFF(indexNoff)) = [];
        
    if (sum(indexNon) + sum(indexNoff)) == 0 && length(indexON) == length(indexOFF)
       stop = true; 
    end
    
    fprintf('iteration %d: (%d,%d)trials \n',iteration,onset,offset)
    
end




end