function [mean_power,w,std_power] = get_psd_epochs(theseEpochs,fs)
%GET_PSD_EPOCHS Summary of this function goes here
%   Detailed explanation goes here


win = fs;
noverlap = fs/2;
% rate can be calculated
[nEpoch,nTime] = size(theseEpochs);
power = [];

for iEpoch = 1:nEpoch
    
    thisEpoch = theseEpochs(iEpoch,:);
    [P,w] = pwelch(thisEpoch, fs,noverlap ,[], fs,'power');
    power = [power P];
   
end


mean_power = mean(power,2);
std_power = std(power,[],2);

end

