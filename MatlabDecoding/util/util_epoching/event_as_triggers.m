function eeg = event_as_triggers(eeg,eventOfInterest,varargin)

inputs = parse_my_inputs(eeg,eventOfInterest,varargin{:});

index = find(eeg.event.name == eventOfInterest);
trigEvent = eeg.event.position(index);
tt = trigEvent;
trig  = trigEvent;
indexTrig = [];

if strcmp(inputs.strOrder,'both')
    str = {'first','last'};
    
    for i = 1:2
        inputs.strOrder = str{i};
        indexTrig{i} = check_event_to_remove(eeg,trig,trigEvent,inputs);
    end
    
else
    indexTrig = check_event_to_remove(eeg,trig,trigEvent,inputs);
end

% remove useless event of the structure event
if iscell(indexTrig)
    
    ii = intersect(indexTrig{1},indexTrig{2});
    indexTrig = ii;
end
    
trig(indexTrig) = [];
event2Remove = index(indexTrig);
eeg.event.position(event2Remove) = [];
eeg.event.name(event2Remove) = [];

fprintf('Found %d trials',length(trig))

if inputs.verbose
    figure()
    plot(tt,ones(1,length(tt)),'b*')
    hold on;
    plot(trig,1.2*ones(1,length(trig)),'r*')
    ylim([0 2])
    
end

end

function index = check_event_to_remove(eeg,trig,trigEvent,inputs)


for iTrig = 1:length(trigEvent)
    if strcmp(inputs.strOrder,'last')
        win = trig(iTrig)+ 1:trig(iTrig) + eeg.rate*inputs.nSecOfInterest;
    elseif strcmp(inputs.strOrder,'first')
        win = trig(iTrig)-eeg.rate*inputs.nSecOfInterest:trig(iTrig)-1;
    end
    su = sum(ismember(win,trig));
    if su ~= 0 || win(1)<0
        trigEvent(iTrig) = NaN;
    end
end

index = find(isnan(trigEvent) == 1);

end

%% Parser
function funcInputs = parse_my_inputs(eeg,eventOfInterest,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);
addRequired(funcInputs,'eventOfInterest',@isnumeric);
addParameter(funcInputs,'nSecOfInterest',0,@isnumeric);
expectedType = {'first','last','both'};
addParameter(funcInputs,'strOrder',{'first'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'verbose',true,@islogical);
parse(funcInputs, eeg,eventOfInterest, varargin{:});
funcInputs = funcInputs.Results;


end
