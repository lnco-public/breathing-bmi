function theseEpochs = get_epochs_from_chan(epochs,chan)
%GET_EPOCHS_FROM_CHAN Summary of this function goes here
%   Detailed explanation goes here

if isstr(chan)
    iChan = ismember(epochs.label,chan);
else
    iChan = chan;
end
theseEpochs = squeeze(epochs.data(:,iChan,:));

end

