function index = flip_channels(label)

labelPosition = zeros(length(label),1);
% find central channels 
expression = 'z';
index_centralChannel = regexpi(label,expression);
index_centralChannel = ~cellfun(@isempty,index_centralChannel);
labelPosition(index_centralChannel) = 0;

% find left channels 
expression = '[1-9]';
[index_LeftChannel] = strsplit(label,expression);
index_LeftChannel = ~cellfun(@isempty,index_LeftChannel);
labelPosition(index_LeftChannel) = 1;



end

