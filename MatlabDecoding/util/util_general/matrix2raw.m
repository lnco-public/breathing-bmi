function raw = matrix2raw(matrix)
%MATRIX2RAW Summary of this function goes here
%   Detailed explanation goes here


nEpoch = size(matrix,1);
epoch_size = size(matrix,3);
nChannel = size(matrix,2);
raw = zeros(nChannel, nEpoch*epoch_size);

for iChannel = 1:nChannel
    t = squeeze(matrix(:,iChannel,:));
    raw(iChannel,:) = reshape(t',1,[]);  
end

end

