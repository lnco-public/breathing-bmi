function eeg = add_channel(eeg,data2Add,str)

% label data container
nChannel = size(data2Add,1);

if any(ismember(str,eeg.label))
   warning('channel already existing') 
    
else
    for iChannel = 1:nChannel
        eeg.data(end+1,:) = data2Add(iChannel,:);
        eeg.label(end+1,:) = str(iChannel);
    end
    
    % put triggers data at the end
    trig = eeg.data(eeg.map_ch('TRIG'),:);
    eeg = remove_channels(eeg,{'TRIG'});
    eeg.data(end+1,:) = trig;
    eeg.label{end+1} = 'TRIG';
    
    eeg.map_ch = containers.Map(eeg.label, 1:length(eeg.label));
end
end

