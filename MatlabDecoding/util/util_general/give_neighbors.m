function [label,neighbors] = give_neighbors

neighbors = {};


%% Order label

label = {'FP1','FPz','FP2','AF7','AF3','AF4','AF8','F7','F5','F3','F1','Fz','F2',...
         'F4','F6','F8','FT7','FC5','FC3','FC1','FCz','FC2','FC4','FC6','FT8',...
         'T7','C5','C3','C1','Cz','C2','C4','C6','T8','TP7','CP5','CP3','CP1','CPz',...
         'CP2','CP4','CP6','TP8','P7','P5','P3','P1','Pz','P2','P4','P6','P8','PO7',...
         'PO3','POz','PO4','PO8','O1','Oz','O2',};

%% Enter the neighboring channels for each channel
% 'FP1'
neighbors{1} = {'AF7' 'FPz' 'AF3'};

% 'FPz'
neighbors{2} = {'FP1' 'FP2' 'AF3' 'AF4'};

% 'FP2'
neighbors{3} = {'FPz' 'AF8' 'AF4'};

% 'AF7'
neighbors{4} = {'FP1' 'F7' 'AF3' 'F5'};

% 'AF3'
neighbors{5} = {'F3' 'AF7' 'FP1' 'F5' 'F1'};

% 'AF4'
neighbors{6} = {'F4' 'F2' 'F6' 'AF8' 'FP2'};

% 'AF8'
neighbors{7} = {'F8' 'FP2' 'F6' 'AF4'};

% 'F7'
neighbors{8} = {'AF7' 'FT7' 'FC5' 'F5' 'F10'};

% 'F5'
neighbors{9} = {'F7' 'F3' 'FC5' 'FT7' 'FC3' 'AF3' 'AF7'};

% 'F3'
neighbors{10} = {'F5' 'F1' 'FC3' 'AF3' 'FC5' 'FC1'};

% 'F1'
neighbors{11} = {'F3' 'Fz' 'FC1' 'AF3' 'FC3' 'FCz'};

% 'Fz'
neighbors{12} = {'F1' 'F2' 'FCz' 'FC1' 'FC2'};

% 'F2'
neighbors{13} = {'F4' 'Fz' 'FC2' 'AF4'};

% 'F4'
neighbors{14} = {'AF4' 'F2' 'F6' 'FC4' 'FC2' 'FC6'};

% 'F6'
neighbors{15} = {'F4' 'F8' 'FC6' 'FC4' 'FT8'};

% 'F8'
neighbors{16} = {'AF8' 'FT8' 'F6' 'FC6' 'F9'};

% 'FT7'
neighbors{17} = {'F7' 'T7' 'FC5' 'C5' 'F5' 'F10'};

% 'FC5'
neighbors{18} = {'FT7' 'F5' 'C5' 'FC3' 'F7' 'F3' 'C3' 'T7'};

% 'FC3'
neighbors{19} = {'F3' 'FC5' 'FC1' 'C3' 'C1' 'C5' 'F5' 'F1'};

% 'FC1'
neighbors{20} = {'F1' 'FC3' 'FCz' 'C1' 'F3' 'Fz' 'C3' 'Cz'};

% 'FCz'
neighbors{21} = {'FC1' 'FC2' 'Cz' 'Fz' 'F1' 'F2' 'C1' 'C2'};

% 'FC2'
neighbors{22} = {'FCz' 'FC4' 'F2' 'C2' 'Fz' 'F4' 'Cz' 'C4'};

% 'FC4'
neighbors{23} = {'F4' 'C4' 'FC6' 'FC2' 'F6' 'F2' 'C2' 'C6'};

% 'FC6'
neighbors{24} = {'FC4' 'FT8' 'C6' 'F6' 'F4' 'F8' 'C4' 'T8'};

% 'FT8'
neighbors{25} = {'F8' 'T8' 'FC6' 'F9' 'F6' 'C6'};

% 'T7'
neighbors{26} = {'C5' 'FT7' 'TP7' 'FC5' 'CP5'};

% 'C5'
neighbors{27} = {'T7' 'C3' 'FC5' 'CP5' 'FT7' 'FC1' 'TP7' 'CP3'};

% 'C3'
neighbors{28} = {'C5' 'C1' 'FC3' 'CP3' 'FC5' 'FC1' 'CP5' 'CP1'};

% 'C1'
neighbors{29} = {'C3' 'FC1' 'Cz' 'CP1' 'FC3' 'FCz' 'CP3' 'CPz'};

% 'Cz'
neighbors{30} = {'FCz' 'CPz' 'C1' 'C2' 'FC1' 'FC2' 'CP1' 'CP2'};

% 'C2'
neighbors{31} = {'CP2' 'FC2' 'Cz' 'C4' 'FCz' 'FC4' 'CPz' 'CP4'};

% 'C4'
neighbors{32} = {'C2' 'C6' 'FC4' 'CP4' 'FC2' 'FC6' 'CP2' 'CP6' };

% 'C6'
neighbors{33} = {'C4' 'T8' 'FC6' 'CP6' 'FC4' 'FT8' 'CP4' 'TP8'};

% 'T8'
neighbors{34} = {'C6' 'FT8' 'TP8' 'FC6' 'CP6'};

% 'TP7'
neighbors{35} = {'CP5' 'T7' 'P7' 'C5' 'P5'};

% 'CP5'
neighbors{36} = {'TP7' 'P5' 'C5' 'CP3' 'T7' 'C3' 'P7' 'P3'};

% 'CP3'
neighbors{37} = {'CP5' 'P3' 'C3' 'CP1' 'C5' 'C1' 'P5' 'P1'};

% 'CP1'
neighbors{38} = {'CP3' 'P1' 'C1' 'CPz' 'C3' 'Cz' 'P3' 'Pz'};

% 'CPz'
neighbors{39} = {'CP1' 'Pz' 'Cz' 'CP2' 'C1' 'C2' 'P1' 'P2'};

% 'CP2'
neighbors{40} = {'CPz' 'P2' 'C2' 'CP4' 'Cz' 'C4' 'Pz' 'P4' };

% 'CP4'
neighbors{41} = {'CP2' 'P4' 'C4' 'CP6' 'C2' 'C6' 'P2' 'P6'};

% 'CP6'
neighbors{42} = {'CP4' 'P6' 'C6' 'TP8' 'C4' 'T8' 'P4' 'P8'};

% 'TP8'
neighbors{43} = {'P8' 'T8' 'CP6' 'C6' 'P6'};

% 'P7'
neighbors{44} = {'P5' 'PO7' 'TP7' 'CP5'};

% 'P5'
neighbors{45} = {'P7' 'P3' 'CP5' 'CP3' 'TP7' 'PO7' 'PO3'};

% 'P3'
neighbors{46} = {'P5' 'P1' 'PO3' 'CP3' 'CP5' 'CP1'};

% 'P1'
neighbors{47} = {'Pz' 'P3' 'CP1' 'CP3' 'CPz' 'PO3' 'POz'};

% 'Pz'
neighbors{48} = {'P2' 'P1' 'POz' 'CPz' 'CP1' 'CP2' 'PO3' 'PO4'};

% 'P2'
neighbors{49} = {'P4' 'Pz' 'CP2' 'CPz' 'CP4' 'POz' 'PO4'};

% 'P4'
neighbors{50} = {'P2' 'P6' 'PO4' 'CP4' 'CP2' 'CP6'};

% 'P6'
neighbors{51} = {'P4' 'P8' 'CP6' 'PO8' 'CP4' 'TP8'};

% 'P8'
neighbors{52} = {'P6' 'PO8' 'TP8' 'CP6'};

% 'PO7'
neighbors{53} = {'P7' 'O1' 'PO3' 'P5'};

% 'PO3'
neighbors{54} = {'P3' 'PO7' 'O1' 'POz' 'P1'};

% 'POz'
neighbors{55} = {'Pz' 'Oz' 'P1' 'P2' 'PO3' 'PO4' 'O1' 'O2'};

% 'PO4'
neighbors{56} = {'P4' 'POz' 'O2' 'P2' 'P6' 'PO8' 'Oz'};

% 'PO8'
neighbors{57} = {'O2' 'P8' 'PO4' 'P6'};

% 'O1'
neighbors{58} = {'PO7' 'Oz' 'PO3' 'POz'};

% 'Oz'
neighbors{59} = {'O1' 'O2' 'POz' 'PO3' 'PO4'};

% 'O2'
neighbors{60} = {'PO8' 'Oz' 'POz' 'PO4' };
end

