function matrix = raw2matrix(raw,epoch_size,nEpoch)
%RAW2MATRIX Summary of this function goes here
%   Detailed explanation goes here
nRow = size(raw,1);

matrix = zeros(nEpoch,nRow,epoch_size);

for iRow = 1: nRow
        matrix(:,iRow,:)= transpose(reshape(raw(iRow,:),epoch_size,[]));
end

