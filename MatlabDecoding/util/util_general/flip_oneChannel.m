function epochs = flip_oneChannel(epochs,str1,str2)
%FLIP_ONECHANNEL Summary of this function goes here
%   Detailed explanation goes here

iChannel1 = epochs.map_ch(str1); 
iChannel2 = epochs.map_ch(str2);
tmp = epochs.data(:,iChannel1,:);
epochs.data(:,iChannel1,:) = epochs.data(:,iChannel2,:);
epochs.data(:,iChannel2,:) = tmp;

epochs.label{iChannel1} = str2;
epochs.label{iChannel2} = str1;

epochs.map_ch = containers.Map(epochs.label, 1:length(epochs.label));

end

