function template_parser(theseEpochs,varargin)


inputs = parse_my_inputs(eeg,varargin{:});



end

%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'showEvent',@isstruct);
addParameter(funcInputs,'alignEvent',[], @iscell);
addParameter(funcInputs, 'timeBeforeEvent', 1, @(x) isnumeric(x));
addParameter(funcInputs, 'timeAfterEvent', 1, @(x) isnumeric(x));
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'channelID',[],@(x) isnumeric(x));
addParameter(funcInputs,'removeBaseline',false,@islogical)
addParameter(funcInputs,'intervalBaseline',[],@isnumeric)

parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1) ;
end

end
