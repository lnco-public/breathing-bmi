function arrayOfChannel= convert_string_channel_to_numeric(map,chString)
  
arrayOfChannel = [];

for i = 1:length(chString)
    try
        chan = map(chString{i});
    catch
        continue;
    end
    arrayOfChannel = [arrayOfChannel chan ];
end


end