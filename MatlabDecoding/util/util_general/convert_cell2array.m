function ArrayData = convert_cell2array(CellData)
%CONVERT_CELL2ARRAY convert cell data into array


nCell = length(CellData);
n_Dimension = ndims(CellData);
dim_matrix = size(CellData{1});
if n_Dimension == 1
    ArrayData = zeros(nCell,dim_matrix(:));
elseif n_Dimension == 2
    ArrayData = zeros(nCell,dim_matrix(1),dim_matrix(2));
elseif n_Dimension == 3
    ArrayData = zeros(nCell,dim_matrix(1),dim_matrix(2),dim_matrix(3));
end
for iCell = 1:nCell
    if n_Dimension == 1
        ArrayData(iCell,:) = CellData{iCell};
    elseif n_Dimension == 2
        ArrayData(iCell,:,:) = CellData{iCell};
    elseif n_Dimension == 3
        ArrayData(iCell,:,:,:) = CellData{iCell};
    end
    
end
end

