function runData = add_events_to_raw(runData,listEvent)
%ADD_EVENTS Summary of this function goes here
%   Detailed explanation goes here

if iscell(runData)
    nRun = length(runData);
    for iRun = 1:nRun
        thisRun = runData{iRun};
        thisList = listEvent{iRun};
        runData{iRun} = add_event_to_run(thisRun,thisList);
    end
else
     runData = add_event_to_run(runData,listEvent);  
end
end

function runData = add_event_to_run(runData,listEvent)

    name = runData.event.name;
    position = runData.event.position;
    
    name = [name ;listEvent.name];
    [position,index] = sort([position; listEvent.position]);
    name = name(index);
    
    runData.event.name = name;
    runData.event.position = position;
        
end
