function myText = format_text_for_plot(myText)
% FORMAT_TEXT_FOR_PLOT ensures special characters are displayed correctly
% on plots
% 
%   F = FORMAT_TEXT_FOR_PLOT(T) takes either a char or cellstr T as the
%   input text, and performs the following operations for special
%   characters to ensure the resulting F to be displayed correctly on
%   figures:
%     (1) repalce '_' to '\_'
    assert(ischar(myText) || iscellstr(myText));
    if iscellstr(myText)
        for i=1:numel(myText)
            myText{i} = format_text(myText{i});
        end
    elseif ischar(myText)
        myText = format_text(myText);
    else
        % do nothing
    end
end
function fText = format_text(oriText)
    fText = strrep(oriText, '_', '\_');
end