function saving_figure_format(h,str,fileSave,array)


set(h,'PaperOrientation','landscape');
  
if nargin == 3
    array = [1 2 3];
end

if length(fileSave) > 1 
    try
        fileSave = fileSave(1) ;
    catch
        disp('char type')
    end
end


if any(ismember(array,1))
    saveas(h,[fileSave{1} filesep str '.fig']);
end

if any(ismember(array,2))
    saveas(h,[fileSave{1} filesep str '.pdf']);
end

if any(ismember(array,3))
    saveas(h,[fileSave{1} filesep str '.png'])
end

end

