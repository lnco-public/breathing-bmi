function concat_runData_cell = concatenate_runs(runDataCell)
%CONCATENATE_RUNS Summary of this function goes here
%   Detailed explanation goes here


concat_runData = runDataCell{1};

nChannel = size(runDataCell{1}.data,1);
nPoint = 500;

for iRun = 2:length(runDataCell)
    iRun
    position = runDataCell{iRun}.event.position + size(concat_runData.data,2)+nPoint;
    concat_runData.data = [concat_runData.data zeros(nChannel,nPoint) runDataCell{iRun}.data];
    concat_runData.event.name = [concat_runData.event.name;runDataCell{iRun}.event.name];
    concat_runData.event.position = [concat_runData.event.position;position];
end

concat_runData_cell{1} = concat_runData;