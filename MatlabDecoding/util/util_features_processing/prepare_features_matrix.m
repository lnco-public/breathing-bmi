function [matrix_featSpectral,matrix_featSpectral_conc] = prepare_features_matrix(classifier,nChannel,nFeaturePerChan,options,normalization)
%PREPARE_FEATURES_MATRIX Summary of this function goes here
%   Detailed explanation goes here

if nargin == 2 
    normalization = true;
end


nSubject = numel(classifier);
nCV = numel(classifier{1}.Beta);
% nFeature =numel(classifier{1}.Beta{1});
matrix_featSpectral = zeros(nSubject,nCV,nChannel,nFeaturePerChan);

matrix_featSpectral_conc = [];

for iSubject = 1:nSubject
    if strcmp(options,'Beta')
        BetaS = classifier{iSubject}.Beta;
    elseif strcmp(options,'Power')
        BetaS = classifier{iSubject}.Pfeature;
    end
    if iscell(BetaS)
        BS = cell2mat(BetaS);
    end
    
    if normalization
        ma_min = ones(size(BS,1),1)*min(BS);
        ma_max = ones(size(BS,1),1)*max(BS);
        BS = (BS -ma_min)./(ma_max-ma_min);
    end
    
    
    for i = 1:10
        matrix_featSpectral(iSubject,i,:,:) = reshape(BS(:,i),[nFeaturePerChan nChannel])';
    end
    
    matrix_featSpectral_conc = [matrix_featSpectral_conc; BS'];
    
    
end

end


