
function F = fisher_score(d1, d2)
% d2   [samples x dimensions]
% d2   [samples x dimensions]
% 
m1 = mean(d1, 1);
m2 = mean(d2, 1);

s1 = std(d1, [], 1);
s2 = std(d2, [], 1);

F = abs(m2 - m1) ./ sqrt(s1.^2 + s2.^2);

end