function [x_Train,bestFeatures,P] = features_selection(training,trainingLabel,varargin)


inputs = parse_my_inputs(training,trainingLabel,varargin{:});

if strcmp(inputs.method,'rankfeatures')
    if ~isempty(inputs.nfeature2Select)
        [bestFeatures,P]= rankfeatures(training',trainingLabel,'Criterion','bhattacharyya','NumberOfIndices',inputs.nfeature2Select);
        
    else
        bestFeatures = 1:size(training,2);
        
    end
    
else
    [orderedInd, orderedPower] = rankfeat(training, trainingLabel,inputs.method{1});
    bestFeatures =orderedInd(1:inputs.nfeature2Select);
    [~,orderedInd] = sort(orderedInd,'ascend');
    P = orderedPower(orderedInd);
    P = P';
end

bestFeatures = ismember(1:size(training,2),bestFeatures)';

% training and test dataset modified for only selected channel
x_Train = training(:,bestFeatures);


end

%% Parser
function funcInputs = parse_my_inputs(training,trainingLabel, varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@(x) isnumeric(x));
addRequired(funcInputs, 'trainingLabel',@(x) isnumeric(x));
addParameter(funcInputs, 'testing',[],@(x) isnumeric(x));
addParameter(funcInputs,'nfeature2Select',[],@(x) isnumeric(x));
expectedType = {'rankfeatures','corr','fisher','relief','infgain'};
addParameter(funcInputs, 'method',{'fisher'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs, training,trainingLabel, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.nfeature2Select)
funcInputs.nfeature2Select = size(training,2);
end

end
