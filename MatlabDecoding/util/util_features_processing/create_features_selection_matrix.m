function matrix = create_features_selection_matrix(Beta,nFeaturePerChan,nChannel,no_mean)

if nargin ==3
    no_mean = false;
end


matrix= zeros(nChannel,nFeaturePerChan);

Bb = Beta;

if ~no_mean
    Bb = nanmean(Bb,2);    
    matrix = reshape(Bb,[nFeaturePerChan nChannel])';
else
    
    nCV = size(Bb,2);
    matrix = zeros(nChannel,nFeaturePerChan,nCV);
    for iCV = 1:nCV
        m = reshape(Bb(:,iCV),[nFeaturePerChan nChannel])';
        matrix(:,:,iCV) = m;
    end
end
