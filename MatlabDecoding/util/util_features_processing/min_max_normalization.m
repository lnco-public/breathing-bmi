function features = min_max_normalization(features)


ma_min = ones(size(features,1),1)*min(features);
ma_max = ones(size(features,1),1)*max(features);
features = (features -ma_min)./(ma_max-ma_min);

end

