clear;close all

% Folder of Interest
folderName = 'G:\My Drive\BCI course 2019\DataSet\session3\offline';

% File Processing
[subSubjectFolders,nFolder] = find_subject_folder(folderName);

%% Loop Subject

fprintf('Starting Main Script \n');
for iSubject = 1:nFolder
  
    fprintf('*******************************\n');
    fprintf(['Subject %d - ' subSubjectFolders(iSubject).name '\n'],iSubject-2);
    subjectFolder = fullfile(folderName,subSubjectFolders(iSubject).name);
    
    try
    convertgdf2mat(subjectFolder)
    catch 
       disp('eror with subject') 
    end
end
