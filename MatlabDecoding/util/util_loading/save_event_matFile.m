function  save_event_matFile(folderName,eventStartTrial)
%SAVE_EVENT_MATFILE Summary of this function goes here
%   Detailed explanation goes here
if nargin == 1
   eventStartTrial = 100;
end



% File Processing
subSessionFolders = dir([folderName filesep '*.gdf']);
nFile = numel(subSessionFolders);

[~,idx] = sort([subSessionFolders.datenum]);
subSessionFolders = subSessionFolders(idx);

for iFile = 1:nFile
    
    disp('****************************')
    fprintf('******* Run %d/%d ******* \n',iFile,nFile)
    
    fileName = fullfile(folderName,subSessionFolders(iFile).name);
    
    [signal,header] = sload(fileName);
    
    ev = zeros(length(header.EVENT.POS),3);
    ev(:,1) = header.EVENT.POS;
    ev(:,3) = header.EVENT.TYP;
    
    position = header.EVENT.POS;
    nTimePts = size(signal,1);
    disp(nTimePts)
    iEvent2Remove = find(position > nTimePts);
    
    if ~isempty(iEvent2Remove)
        
        trial = find(header.EVENT.TYP == eventStartTrial);
        if iEvent2Remove(1) ~= eventStartTrial
            iTrial = find(trial <= iEvent2Remove(1),1,'last');
            iEvent2Remove = [trial(iTrial):1:iEvent2Remove(1) iEvent2Remove'];
            
            trial2Remove = find(header.EVENT.TYP(iEvent2Remove) == eventStartTrial);
            
            ev(iEvent2Remove,:) = [];
            
        end
        
        fprintf('Discarded trials: %d \n',length(trial2Remove))
        
    end
    eventStruct{iFile} = ev;
    
end


 save([folderName filesep 'eventStruct.mat'],'eventStruct');