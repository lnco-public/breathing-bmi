function import_classifier_online_for_analysis(folderRecordings,folderResults,str)

% File Processing
[subSubjectFolders,nFolder] = find_subject_folder(folderRecordings);

%% Loop Subject

fprintf('Starting Main Script \n');
for iSubject = 3:nFolder
    
    fprintf('*******************************\n');
    fprintf(['Subject %d - ' subSubjectFolders(iSubject).name '\n'],iSubject-2);
    subjectRecordingsFolder = fullfile(folderRecordings,subSubjectFolders(iSubject).name);
    subjectResultsFolder = fullfile(folderResults,subSubjectFolders(iSubject).name);
    
    try
        var = importdata([subjectRecordingsFolder filesep str]);
        save([subjectResultsFolder filesep str],'var');
    catch
        disp('cannot load this variable for this subject')
    end
    
end


end

