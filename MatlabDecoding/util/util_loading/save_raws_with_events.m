function  convertgdf2mat( folderName,eventStartTrial)
%CONVERTGDF2MAT Summary of this function goes here
%   Detailed explanation goes here

if nargin == 1
   eventStartTrial = 100;
end

% File Processing
subSessionFolders = dir([folderName filesep '*.gdf']);
nFile = numel(subSessionFolders);

[~,idx] = sort([subSessionFolders.datenum]);
subSessionFolders = subSessionFolders(idx);

for iFile = 1:nFile
    
    disp('****************************')
    fprintf('******* Run %d/%d ******* \n',iFile,nFile)
    
    fileName = fullfile(folderName,subSessionFolders(iFile).name);
    
    [signal,header] = sload(fileName);
    
    event = zeros(length(header.EVENT.POS),3);
    event(:,1) = header.EVENT.POS;
    event(:,3) = header.EVENT.TYP;
    
    position = header.EVENT.POS;
    nTimePts = size(signal,1);
    disp(nTimePts)
    iEvent2Remove = find(position > nTimePts);
    
    if ~isempty(iEvent2Remove)
        
        trial = find(header.EVENT.TYP == eventStartTrial);
        if iEvent2Remove(1) ~= eventStartTrial
            iTrial = find(trial <= iEvent2Remove(1),1,'last');
            iEvent2Remove = [trial(iTrial):1:iEvent2Remove(1) iEvent2Remove'];
            
            trial2Remove = find(header.EVENT.TYP(iEvent2Remove) == eventStartTrial);
            
            event(iEvent2Remove,:) = [];
            
        end
        
        fprintf('Discarded trials: %d \n',length(trial2Remove))
        
    end
    
    event(:,1) =  event(:,1)-1; % index python begins at 0
    fileSave = strsplit(subSessionFolders(iFile).name,'.');
    fileSave = [fileSave{1} '.mat'];
    fileSave = fullfile(folderName,fileSave);
    name = subSessionFolders(iFile).name;
    save(fileSave, 'signal', 'event','name');
    
end


end

