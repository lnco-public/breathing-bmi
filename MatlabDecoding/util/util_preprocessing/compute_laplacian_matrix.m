function laplacian_matrix = compute_laplacian_matrix(label)

%% Create the matrix
a = ones(64,1);
laplacian_matrix = diag(a);
map_ch = containers.Map(label, 1:length(label));

[~,neighbors] = give_neighbors;


%% Laplacian matrix coefficient 

for iChan = 1:size(neighbors,2)
    theseNeighbors = neighbors{iChan};
    nNeighbors = length(theseNeighbors);
    for iNeighbors = 1:nNeighbors
        ch = theseNeighbors{iNeighbors};
     laplacian_matrix(iChan,map_ch(ch)) =  -1;
    end
    
end


end











