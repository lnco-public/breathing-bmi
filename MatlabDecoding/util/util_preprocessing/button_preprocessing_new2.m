function eeg = button_preprocessing_new2(eeg,varargin)

inputs = parse_my_inputs(eeg,varargin{:});


iChannel =  eeg.map_ch('TRIG');
dataButton = eeg.data(iChannel,:);
dataButton(dataButton < 0.5) = max(dataButton);
dataButton = double(bitget(int8(dataButton),inputs.nBytes));
[~,trig]= findpeaks(dataButton,'MinPeakDistance',eeg.rate*inputs.nSecOfInterest);
tt_unpress = trig;

eventNamePress = 889;
eventNameUnPress = 888;


if strcmp(inputs.interest,'unpress') || strcmp(inputs.interest,'both')
    eeg = create_event_structure(eeg,tt_unpress,eventNameUnPress);
end
% 
% if strcmp(inputs.interest,'press') || strcmp(inputs.interest,'both')
%     eeg = create_event_structure(eeg,tt_press,eventNamePress);
% end
% 

if inputs.verbose
    nTrial = length(tt_unpress);
    fprintf('Found %d trials \n',nTrial);
    figure(20)
    dataButton = eeg.data(iChannel,:);
    plot(dataButton')
    hold on;
    
    if strcmp(inputs.interest,'press') || strcmp(inputs.interest,'both')
        plot(tt_press,max(dataButton)*ones(1,length(tt_press)),'r*')
        hold on;
    end
    
 
    if strcmp(inputs.interest,'unpress') || strcmp(inputs.interest,'both')
        plot(tt_unpress,max(dataButton)*ones(1,length(tt_unpress)),'c*')
    end
    
    ylim([round(min(dataButton)-1) round(max(dataButton)+1)])
end

end
 
function eeg = create_event_structure(eeg,trig,eventName)

if ~isempty(eeg.event.position)
    position = eeg.event.position;
    name = eeg.event.name;
else 
    position = [];
    name=  [];
end
    
for iTrig = 1:length(trig)
    position(end+1) = trig(iTrig);
    name(end+1) = eventName;
end

[pos,index] = sort(position,'ascend');
position = position(index);
name = name(index);

eeg.event = struct('name',name,'position',position);

end


%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);
addParameter(funcInputs,'nSecOfInterest',0,@isnumeric);
addParameter(funcInputs,'nBytes',2,@isnumeric);
expectedType = {'press','unpress','both'};
addParameter(funcInputs,'interest',{'tPress'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'verbose',true,@islogical);
parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;


end


