function eeg= resampling(eeg,fraction)

warning('change this function')


eeg.data  = downsample(eeg.data',fraction)';
eeg.rate = eeg.rate/fraction;

% resampling of event structure
trig = eeg.event;
trig = cellfun(@(x) floor(x./fraction), {trig.position},'UniformOutput',false);
eeg.event = struct('name',eeg.event.name,'position',trig);

end

