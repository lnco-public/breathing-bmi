function [ iEEG ] = filterEOG( iEEG, b )

    % We create 3 EOG channels following the paper:
    % "[Croft 2000] EOG correction of blinks with caccade coefficients: a test and
    % revision of the aligned-artefact average solution"
    
    
    HEOG = iEEG.signal(:,iEEG.numChannelsEEG+5)-iEEG.signal(:,iEEG.numChannelsEEG+6);
    VEOG = ((iEEG.signal(:,iEEG.numChannelsEEG+1) + iEEG.signal(:,iEEG.numChannelsEEG+2)) - (iEEG.signal(:,iEEG.numChannelsEEG+3) + iEEG.signal(:,iEEG.numChannelsEEG+4))) / 2;
    REOG = (iEEG.signal(:,iEEG.numChannelsEEG+1) + iEEG.signal(:,iEEG.numChannelsEEG+2) + iEEG.signal(:,iEEG.numChannelsEEG+3) + iEEG.signal(:,iEEG.numChannelsEEG+4)) / 4;
    
    
    % We perform regression following the paper:
    % "[Sch�gl 2007] A fully automated correction method of EOG artifact in
    % EEG recordings"
    % Y = S + U*b
    % Y: recorded EEG signal
    % S: clean EEG signal   
    % U: EOG channels
    % b: EOG coefficients
    
    Y = iEEG.signal(:,1:iEEG.numChannelsEEG);
    U = [HEOG VEOG REOG];
    
    if (nargin == 1)
        covariance = cov([U Y]);
        autoCovariance_EOG = covariance(1:size(U,2), 1:size(U,2));
        crossCovariance_EOG_EEG = covariance(1:size(U,2), size(U,2)+1:end);
        b = autoCovariance_EOG \ crossCovariance_EOG_EEG;
        disp('EOG: computing b');
    else
        disp('EOG: b previously computed');
    end
    
    iEEG.filters.EOG.b = b;
    iEEG.signal(:,1:iEEG.numChannelsEEG) = Y - U * b;
    
    
end

