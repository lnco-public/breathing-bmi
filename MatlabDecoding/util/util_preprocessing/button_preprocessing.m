
function eeg = button_preprocessing(eeg,nSecInterest,strOrder,verbose)


iChannel =  eeg.map_ch('TRIG');
dataButton = eeg.data(iChannel,:);
% dataButton(dataButton < 0.5) = 3;
% dataButton(dataButton < 16) = 16;
% dataButton(dataButton< 16 )= 32;
% trigButton = find(dataButton >=2);
trigButton = find(dataButton == 32);
begin_experiment = find(dataButton == 16,1,'first');
trigButton (trigButton <= begin_experiment) = [];
trig = trigButton;
for iTrig = 1:length(trigButton)
    if strcmp(strOrder,'end')
        win = trig(iTrig)+ 1:trig(iTrig) + eeg.rate*nSecInterest;
    elseif strcmp(strOrder,'begin')
        win = trig(iTrig)-eeg.rate*nSecInterest:trig(iTrig)-1;
        
    end
    
     su = sum(ismember(win,trig));
    
    if su ~= 0 || win(1)<0 
        trigButton(iTrig) = NaN;
    end

end

trig(isnan(trigButton)) = [];
nTrial = length(trig);
fprintf('Found %d trials \n',nTrial)
if verbose
    plot(dataButton')
    hold on;
    plot(trig,32*ones(1,length(trig)),'r*') 
    ylim([min(dataButton)-1 max(dataButton)+1])
end


eeg = create_event_structure(eeg,trig);


end

function eeg = create_event_structure(eeg,trig)

if ~isempty(eeg.event.position)
   position = eeg.event.position;
   name = eeg.event.name;
else
position = [];
name = [];
end

for iTrig = 1:length(trig)
    position(end+1) = trig(iTrig);
    name(end+1) = 998;
end

[position,index] = sort(position,'ascend');
name = name(index);

eeg.event = struct('name',name,'position',position);

end