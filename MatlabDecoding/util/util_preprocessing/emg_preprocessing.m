
function s = emg_preprocessing(s,name)

if strcmp(name,'left')
    iChannel = s.map_ch('EXG1');
    s.label{iChannel} = 'EXG1';
    iChannel = s.map_ch('EXG3');
    s.data(iChannel,:) = [];
    s.label(iChannel) = [];
elseif strcmp(name,'right')
    iChannel = s.map_ch('EXG3');
    s.label{iChannel} = 'EXG1';
    iChannel = s.map_ch('EXG1');
    s.data(iChannel,:) = [];
    s.label(iChannel) = [];
end

s.map_ch = containers.Map(s.label, 1:length(s.label));

iChannel = s.map_ch('EXG1');

% filtering between 30 and 50 Hz
[s,~] = temporal_filtering(s,'channelID',iChannel,'cutoff',[30 50],'order',2,'type',{'bandpass'},'backward',true);

% compute the triggers with TK (better)
tk = compute_TK(s.data(iChannel,:));
s.data(iChannel,1:length(tk))= tk;

