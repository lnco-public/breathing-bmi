function [eeg,spatial_matrix] = spatial_interpolation(eeg,channelID,badChannel,spatial_matrix)

[label,neighbors] = give_neighbors;


%% Laplacian matrix coefficient 

for iChan = 1:length(channelID)
    
    thisChannel = channelID{iChan};
    try
        theseNeighbors = neighbors{strcmp(label,thisChannel)};
        theseNeighbors = theseNeighbors(~ismember(theseNeighbors,badChannel));
        neighborsIndex = convert_string_channel_to_numeric(eeg.map_ch,theseNeighbors);
        spatial_matrix(eeg.map_ch(thisChannel),eeg.map_ch(thisChannel)) = 0;
        spatial_matrix(eeg.map_ch(thisChannel),neighborsIndex) = 1/length(neighborsIndex);
        spatial_matrix(neighborsIndex,eeg.map_ch(thisChannel)) = 1/length(neighborsIndex);
        eeg.data(eeg.map_ch(thisChannel),:) = mean(eeg.data(neighborsIndex,:));
    catch
        warning('this channel cannot be interpolated')
    end
    
end




end

