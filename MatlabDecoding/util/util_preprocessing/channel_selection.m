function [x_Train,bestChannels] = channel_selection(training,trainingLabel,varargin)


inputs = parse_my_inputs(training,trainingLabel,varargin{:});

% features onset and rest class
index = ismember(trainingLabel,1);
f_onset = squeeze(training(index,:));
f_base = squeeze(training(~index,:));
P = [];
nTime = size(f_onset,2);
indexChannel = 1:size(f_onset,2);
indexChannel = ceil(indexChannel./inputs.nFeaturePerChannel);

if strcmp(inputs.method,'fisherScore')
    
    
    for iTime = 1:nTime
        onset = f_onset(:,iTime);
        base = f_base(:,iTime);
        P = [P fisher_score(onset,base)];
    end
    

    % select the channels based on Fisher score
    [~,orderedInd] = sort(P,'descend');
    bestFeatures =orderedInd(1:inputs.nfeature2Select);
    
elseif strcmp(inputs.method,'rankFeatures')
    if ~isempty(inputs.nchan2Select)
        [bestFeatures,P]= rankfeatures(training',trainingLabel,'Criterion','bhattacharyya');
        bestChannels = ceil(bestFeatures./inputs.nFeaturePerChannel);
        [~,orderedInd] = sort(bestChannels,'ascend');
        P = P(orderedInd);
        P = reshape(P,inputs.nFeaturePerChannel,max(bestChannels));
        P = sum(P);
        [~,orderChannel] = sort(P,'descend');
         bestChannels = orderChannel(1:inputs.nchan2Select);
    else 
        bestChannels = 1:size(training,2)/inputs.nFeaturePerChannel;
    
    end
end

bestChannels = ismember(indexChannel,bestChannels)';

% training and test dataset modified for only selected channel
x_Train = training(:,bestChannels);



end

%% Parser
function funcInputs = parse_my_inputs(training,trainingLabel, varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@(x) isnumeric(x));
addRequired(funcInputs, 'trainingLabel',@(x) isnumeric(x));
addParameter(funcInputs, 'testing',[],@(x) isnumeric(x));
addParameter(funcInputs,'nchan2Select',[],@(x) isnumeric(x));
addParameter(funcInputs,'nFeaturePerChannel',64,@(x) isnumeric(x));
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'method',{'rankFeatures'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs, training,trainingLabel, varargin{:});
funcInputs = funcInputs.Results;

end
