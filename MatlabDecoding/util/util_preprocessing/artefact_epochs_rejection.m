
function [epochs,epochs2reject] = artefact_epochs_rejection(epochs,threshold)


nEpoch = size(epochs.data,1);
nChannel = size(epochs.data,2);

epochs2reject = [];

% amplitude range
peak2peak = zeros(nChannel,nEpoch);
for iChannel = 1: nChannel
    for iEpoch = 1:nEpoch
        thisEpoch = squeeze(epochs.data(iEpoch,iChannel,:));
        peak2peak(iChannel,iEpoch) = abs(max(thisEpoch) - min(thisEpoch));
           
    end
    peak2peak(iChannel,:) = zscore(peak2peak(iChannel,:));

epochs2reject = [epochs2reject find(peak2peak(iChannel,:) > threshold)]; 

end

epochs2reject  = unique(epochs2reject);
epochs.data(epochs2reject,:,:) = [];

end