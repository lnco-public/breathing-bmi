function eeg = apply_sgolayfilter(eeg,order,framelen,channelID)

data = eeg.data(channelID,:);

dataFiltered = sgolayfilt(data',order,framelen);

eeg.data(channelID,:) = dataFiltered';


end

