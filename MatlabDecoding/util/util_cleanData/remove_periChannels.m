function s = remove_periChannels(s)

%% Remove electrode in the scalp periphery
peripheryChannels  = {'FP1','FPz','FP2','AF7','AF8','F7',...
                        'F8','FT7','FT8','T7','T8','TP7','TP8'...
                         'P7','P8','PO7','PO8','O1','Oz','O2','F9','F10','AF4','AF3','PO3','POz','PO4'};

s = remove_channels(s,peripheryChannels);
end

