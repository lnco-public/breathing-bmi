function  [eeg,structICA] = perform_ICA(eeg,varargin)


inputs = parse_my_inputs(eeg,varargin{:});

%% Epoch extraction
timeBeforeEvent = 1;
timeAfterEvent = 1;
epochs  = extract_epochs(eeg,'alignEvent',{'ST_MOV'},'timeBeforeEvent',1,...
                         'timeAfterEvent',1,'channelID',inputs.channelID);

epoch_size = size(epochs.data,3);
nEpoch = size(epochs.data,1);

conc_epoch = matrix2raw(epochs.data);
data = eeg.data(inputs.channelID,:);

%% Run ica

[weight,sphere] = runica(conc_epoch,'extended',1,'stop',1e-7,'pca',inputs.nICs,'verbose','off');
W = weight * sphere;
matrix = pinv(W) ;
ICs = W * data;
ICs_epochOnly = W * conc_epoch;
epochIC.data = raw2matrix(ICs_epochOnly,epoch_size,nEpoch);
epochIC.time = -timeBeforeEvent:1/eeg.rate:timeAfterEvent;
% percent variance explained by IC component
pvaf = zeros(1,inputs.nICs);
fprintf('\nComputation of variance explained by each IC component \n')
for iIC = 1:inputs.nICs
    back_proj = matrix(:,iIC)*ICs(iIC,:);
    pvaf(iIC) = 100-100*mean(var(data - back_proj))/mean(var(data));
    disp(['IC_' num2str(iIC) ':  ' num2str(pvaf(iIC))])
end

%% Visualization
if strcmp(inputs.choiceBased,'manual')
    
    plot_ICA_decomposition(eeg,inputs.channelID,ICs,epochIC,matrix,eeg.eloc, inputs.frequency);
   
    answer  = inputdlg({'Artifact ICs'},'Components to remove',[1 inputs.nICs]);
    badIC = str2num(answer{1});
    disp(['Suppressed components: ' answer{1}]);
else
    badIC = find(pvaf >= inputs.threshRejection);
    disp(['Suppressed components: ' answer{1}]);
   
    
    
end

%% Backpropagation in channel domain
matrix(:,badIC) = 0;
ICs(badIC,:) = 0;
eeg.data(inputs.channelID,:) = matrix*ICs;

structICA = struct('matrix',matrix,'weight',W);
end

%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);

addParameter(funcInputs,'channelID',[],@(x) isnumeric(x))
addParameter(funcInputs,'nICs',10,@(x) isnumeric(x));
expectedType = {'auto','manual'};
addParameter(funcInputs,'choiceBased',{'manual'},@(x) any(validatestring(x{:}, expectedType)))
addParameter(funcInputs,'threshRejection',4,@isnumeric)
addParameter(funcInputs,'frequency',[0.1 30],@isnumeric)
parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1) ;
end

end


