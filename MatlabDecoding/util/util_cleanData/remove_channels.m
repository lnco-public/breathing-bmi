function s = remove_channels(s,rmChannel)
%REMOVE_CHANNELS update the eeg structure by removing the channel unwanted.


if all(cellfun(@ischar,rmChannel))
    
    iChannel = [];
    
    for i = 1:length(rmChannel)
        try
            chan = s.map_ch(rmChannel{i});
        catch
            continue;
        end
        iChannel = [iChannel chan ];
    end
    
    rmChannel = iChannel;
end

if iscell(rmChannel)
    
   rmChannel = cell2mat(rmChannel);
    
end



s.label(rmChannel) = [];
s.data(rmChannel,:) = [];
s.map_ch = containers.Map(s.label, 1:length(s.label));

rmChannel = rmChannel(rmChannel <=size(s.eloc,2));
s.eloc(rmChannel) = [];

end
