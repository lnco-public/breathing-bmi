function runData = remove_channels_runs(runData,rmChannel)
%REMOVE_CHANNELS_RUNS Summary of this function goes here
%   Detailed explanation goes here

nRun = length(runData);

for iRun = 1:nRun
    runData{iRun} = remove_channels(runData{iRun},rmChannel);
end
end

