function [epochs,epoch2Remove] = eog_artefact_rejection(epochs,timeInterval,threshold,rmChannel,ichannel)

posLeft = find(epochs.time == timeInterval(1));
posRight = find(epochs.time == timeInterval(2));
epoch2Remove = [];
nEpoch = size(epochs.data,1);

if nargin > 3 && ~isempty(ichannel)
    theseEpochs = squeeze(epochs.data(:,ichannel,:));
    for iEpoch = 1:nEpoch
        thisEpoch = theseEpochs(iEpoch,posLeft:posRight);
        ii = find(abs(thisEpoch) > threshold);
        if ~isempty(ii)
            epoch2Remove = [epoch2Remove iEpoch];
        end
    end
    epoch2Remove = unique(epoch2Remove);
else
    for iChannel = 1:length(epochs.label)
        theseEpochs = squeeze(epochs.data(:,iChannel,:));
        for iEpoch = 1:nEpoch
            
            thisEpoch = theseEpochs(iEpoch,posLeft:posRight);
            ii = find(abs(thisEpoch) > threshold);
            if ~isempty(ii)
                epoch2Remove = [epoch2Remove iEpoch];
            end
        end
    end
    
    tab = tabulate(epoch2Remove);
    try
        index = tab(:,2) > 6;
        epoch2Remove = tab(index,1);
    catch
        disp('nothing to remove')
    end
end


nEpoch2Remove = length(epoch2Remove);
fprintf('Discard of %d epoch(s) because of eye movement \n',nEpoch2Remove);



if rmChannel
    epochs.data(epoch2Remove,:,:) = [];
else
    figure()
    for iEpoch2Remove = 1:nEpoch2Remove
        plot_epoch(epochs,epoch2Remove(iEpoch2Remove),10)
        waitforbuttonpress
    end
end


end

function plot_epoch(epochs,iEpoch,interval)

nChannel = size(epochs.data,2);
ystart = zeros(nChannel,1);
for iChannel = 1:nChannel
    theseEpochs = squeeze(epochs.data(iEpoch,iChannel,:));
    plot(epochs.time,theseEpochs-(iChannel-1)*interval);
    ystart(iChannel) = nanmean(theseEpochs-(iChannel-1)*interval);
    hold on;
end
hold off;
xlabel('Time(sec)'); ylabel('Channel')
title(['Epoch ',num2str(iEpoch)])
[ystart,~]= sort(ystart);

set(gca,'YTick',ystart,'YTickLabel',epochs.label(end:-1:1))



end
