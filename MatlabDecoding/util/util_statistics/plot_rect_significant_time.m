function plot_rect_significant_time(pvalues,time,minima,maxima)
%PLOT_RECT_SIGNIFICANT_TIME Summary of this function goes here
%   Detailed explanation goes here

if nargin == 2
   minima = 0.001;
   maxima = 0.999;
    
end

timePointSignificant05 = double(pvalues <= 0.05);
index = find(timePointSignificant05 == 1);
hold all;
face_color = [0.8314    0.8157    0.843];

for iRect = 1:length(index)-1
    posi_rect = [time(index(iRect)) minima  time(index(iRect)+1)-time(index(iRect)) maxima-minima];
    rectangle('Position',posi_rect,'FaceColor',face_color,'EdgeColor','none')
end

end

