function tab = summary_table_performance_online(latency,grpSubject,indexRun,timeInterval,nTrial)


tab = compute_percentage_correct_detection(latency,grpSubject,timeInterval,nTrial);

nRun = max(indexRun);
nTrialPerRun = nTrial/nRun;

tab_res_run = [];
for iRun = 1:nRun
    theselatencies = latency(indexRun == iRun);
    theseGroup = grpSubject(indexRun == iRun);
    tab_run = compute_percentage_correct_detection(theselatencies,theseGroup,timeInterval,nTrialPerRun);
    tab_res_run = [tab_res_run  tab_run(:,5)];
end

disp('SubjectID nStopDetected GoodTr Percentage PercentAll   Run1      Run2      Run3     Run4')
tab(:,end+1:end+nRun) = tab_res_run;
tab(end+1,:) = nanmean(tab);
tab(end,1) = 99;
disp(tab)

end


function tab = compute_percentage_correct_detection(latency,grpSubject,timeInterval,nTrial)
%COMPUTE_PERCENTAGE_CORRECT_DETECTION Summary of this function goes here

if nargin == 2
   timeInterval = [];
end

latency_temp = latency;
grpSubject_temp = grpSubject;

% Number of time - event
index = isnan(latency_temp);
latency_temp(index) = [];
grpSubject_temp(index) = [];
tab = tabulate(grpSubject_temp);



%%

if ~isempty(timeInterval)
    
    index = latency > timeInterval(1) & latency < timeInterval(2);
    latency(~index) = [];
    grpSubject(~index) = [];
    t = tabulate(grpSubject);
    tab(:,end:end+2) = [t(:,2) t(:,2)./tab(:,2) t(:,2)];
    
end

end

