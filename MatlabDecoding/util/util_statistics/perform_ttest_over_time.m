function [h,p] = perform_ttest_over_time(X,Y,method)


pval = 0.05;

if nargin == 2
    method = 'signrank';
end

if strcmp(method,'ttest')
    p =  mattest(X',Y');
elseif strcmp(method,'signrank')
    nTime = size(X,2);
    p = zeros(1,nTime);
    for iTime = 1:nTime
        p(iTime) =  signrank(X(:,iTime),Y(:,iTime));
%          p(iTime) =  signrank(X(:,iTime),Y(:,iTime),'tail','right');
    end
elseif strcmp(method,'ttest2')
    nTime = size(X,2);
    p = zeros(1,nTime);
    for iTime = 1:nTime
        p(iTime) =  ttest2(X(:,iTime),Y(:,iTime));
    end    
    
    
end

% if  strcmp(method,'ttest') || strcmp(method,'signrank')
h = mafdr(p,'BHFDR',true);
h = double(h <pval);
h(h == 0) = nan;

% end

end


