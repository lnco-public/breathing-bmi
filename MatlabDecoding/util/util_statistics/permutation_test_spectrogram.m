function permmaps = permutation_test_spectrogram(powerA,powerB,n_permutes)
%% statistics via permutation testing

[ntrials,num_frex,times2save] = size(powerA);

% initialize null hypothesis maps
permmaps = zeros(n_permutes,num_frex,times2save);

% for convenience, tf power maps are concatenated
%   in this matrix, trials 1:ntrials are from channel "1"
%   and trials ntrials+1:end are from channel "2"
% tf3d = cat(3,squeeze(tf(1,:,:,:)),squeeze(tf(2,:,:,:)));

tf3d = cat(1,powerA,powerB);

% generate maps under the null hypothesis
for permi = 1:n_permutes
    
    % randomize trials, which also randomly assigns trials to channels
    randorder = randperm(size(tf3d,1));
    temp_tf3d = tf3d(randorder,:,:);
    
    % compute the "difference" map
    % what is the difference under the null hypothesis?
    permmaps(permi,:,:) = squeeze( mean(temp_tf3d(1:ntrials,:,:),1) - mean(temp_tf3d(ntrials+1:end,:,:),1) );
end

end