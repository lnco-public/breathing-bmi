function [Z,thresh] = multiple_comparison_correction(Power,map,n_permutes,pval,str)

%% corrections for multiple comparisons
zval = abs(norminv(pval));
mean_h0 = squeeze(mean(map));
std_h0  = squeeze(std(map));
% initialize matrices for cluster-based correction
max_cluster_sizes = zeros(1,n_permutes);
% ... and for maximum-pixel based correction
max_val = zeros(n_permutes,2); % "2" for min/max
zmap = (Power-mean_h0) ./ std_h0;
zmap(abs(zmap)<zval) = 0;

% loop through permutations
for permi = 1:n_permutes
    
    % take each permutation map, and transform to Z
    threshimg = squeeze(map(permi,:,:));
    threshimg = (threshimg-mean_h0)./std_h0;
    % threshold image at p-value
    threshimg(abs(threshimg)<zval) = 0;
    
    % find clusters (need image processing toolbox for this!)
    islands = bwconncomp(threshimg);
    if numel(islands.PixelIdxList)>0
        
        % count sizes of clusters
        tempclustsizes = cellfun(@length,islands.PixelIdxList);
        
        % store size of biggest cluster
        max_cluster_sizes(permi) = max(tempclustsizes);
    end
    
    
    % get extreme values (smallest and largest)
    temp = sort(reshape(map(permi,:,:),1,[] ));
    max_val(permi,:) = [ min(temp) max(temp) ];
    
end

% h = figure()
% hist(max_cluster_sizes,20);
% title('Expected cluster size under the null hypothesis')
% xlabel('Maximum cluster sizes');ylabel('Number of observation')
% 
% find cluster threshold (need image processing toolbox for this!)
% based on p-value and null hypothesis distribution


if strcmp(str,'max-pixel-based clustering')
    
    thresh = prctile(max_cluster_sizes,100-(100*pval));
    
    islands = bwconncomp(zmap);
    for i=1:islands.NumObjects
        % if real clusters are too small, remove them by setting to zero!
        if numel(islands.PixelIdxList{i}==i)<thresh
            zmap(islands.PixelIdxList{i})=0;
        end
    end
    
elseif strcmp(str,'max-pixel-based thresholding')
    % find the threshold for lower and upper values
    thresh_lo = prctile(max_val(:,1),    100*pval); % what is the
    thresh_hi = prctile(max_val(:,2),100-100*pval); % true p-value?
    % note about the above code: a 2-tailed test actually requires pval/2 on each tail;
    % thus, the above is actually testing for p<.1 !
    
    % threshold real data
    zmap = Power;
    zmap(zmap>thresh_lo & zmap<thresh_hi) = 0;
    thresh = [thresh_lo thresh_hi];
end



% threshold real data
Z = zmap;

end