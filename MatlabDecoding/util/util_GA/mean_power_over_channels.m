function GA_mea = mean_power_over_channels(GA_epochs)
%MEAN_POWER_OVER_CHANNELS Summary of this function goes here
%   Detailed explanation goes here

    % Mean over channels
    nChannel = length(GA_epochs);
    [nTrial,nTime] = size(GA_epochs{1});
    
    GA_all = nan(nChannel,nTrial,nTime);
    
    for iChannel = 1:nChannel
        GA_all(iChannel,:,:) = GA_epochs{iChannel};
    end
  
    %% Visualization all channels
    GA_mea = squeeze(nanmean(GA_all,1));
    
end

