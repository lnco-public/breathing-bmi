function [acc_mean_GA,acc_CV_subject,perf_classifier] = performance_GA(output)
%PERFORMANCE_GA Summary of this function goes here
%   Detailed explanation goes here


nSubject = length(output);
perf_classifier = [];
for iSubject = 1:nSubject
    try
    perf_classifier{iSubject} = plot_performance_classifier(output{iSubject});
    acc_CV_subject{iSubject} = perf_classifier{iSubject}.TPR.test.values;
    mea(iSubject) = mean(acc_CV_subject{iSubject});
    dev(iSubject) = std(acc_CV_subject{iSubject});
    catch
        keyboard
    end
end


% mea = mean(acc_CV_subject,2);
% dev = std(acc_CV_subject,[],2);

acc_mean_GA = struct('mean',[mea mean(mea)],...
    'dev',[dev std(mea)]);

fprintf('Accuracy: %d +/- %d \n',round(acc_mean_GA.mean(end)*100,2),round(acc_mean_GA.dev(end)*100,2))
   
end

