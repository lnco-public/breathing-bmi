function [p_GA,p_trial_GA] = pseudo_online_GA(posterior_prob,oneclass)
%PSEUDO_ONLINE_GA Summary of this function goes here
%   Detailed explanation goes here

if nargin == 1
    oneclass = false;
end

nSubject = length(posterior_prob);
%% Detection over time for the three sessions
p_GA = [];
p_trial_GA = [];
for iSubject = 1:nSubject
    if ~isempty(posterior_prob{iSubject})
        prob = cell2mat(posterior_prob{iSubject}.value);
        p_trial_GA = [p_trial_GA; prob];
        p_GA = [p_GA; mean(prob)];
    end
end

if oneclass
    p_GA = p_GA(:,2:2:end);
end

end

