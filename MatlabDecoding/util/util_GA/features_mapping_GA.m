function matrix_featSpectral = features_mapping_GA(classifier,nFeature,nChannel,options,normalization,concatFeature,mean_cv)
%FEATURES_MAPPINGG_GA Summary of this function goes here
%   Detailed explanation goes here

if nargin == 5
    
    concatFeature = false;
    mean_cv = false;
end
if nargin == 6
    mean_cv = false;
end

nSubject = numel(classifier);
if concatFeature
    matrix_featSpectral = zeros(nSubject,nFeature*nChannel);
else
    if mean_cv
    matrix_featSpectral = zeros(nChannel,nFeature,nSubject);
    else
    matrix_featSpectral = zeros(nChannel,nFeature,nSubject,10);    
    end
end

for iSubject = 1:nSubject
    
%     try
        if concatFeature
            matrix_featSpectral(iSubject,:) = features_mapping_subject(classifier{iSubject},nFeature,nChannel,options,normalization,concatFeature,mean_cv);
        else
            if mean_cv
                matrix_featSpectral(:,:,iSubject) = features_mapping_subject(classifier{iSubject},nFeature,nChannel,options,normalization,concatFeature,mean_cv);
            else
                matrix_featSpectral(:,:,iSubject,:) = features_mapping_subject(classifier{iSubject},nFeature,nChannel,options,normalization,concatFeature,mean_cv);
            end
        end
        
%     catch
%         if concatFeature
%             matrix_featSpectral(iSubject,:) = nan(1,nFeature*nChannel);
%         else
%             matrix_featSpectral(:,:,iSubject) = nan(nChannel,nFeature);
%         end
%     end
end

end


function matrix_featSpectral = features_mapping_subject(classifier,nFeature,nChannel,options,normalization,concatFeature,mean_cv)

if strcmp(options,'Beta')
    BS = classifier.Beta;
elseif strcmp(options,'Power')
    BS = classifier.Pfeature;
end
if iscell(BS)
    BS = cell2mat(BS);
end

if normalization
    BS = min_max_normalization(BS);
end
if concatFeature
    matrix_featSpectral = nanmean(BS,2);
else
    if mean_cv
        matrix_featSpectral = create_features_selection_matrix(BS,nFeature,nChannel);
    else
        matrix_featSpectral = create_features_selection_matrix(BS,nFeature,nChannel,true);
    end
    
end

end

