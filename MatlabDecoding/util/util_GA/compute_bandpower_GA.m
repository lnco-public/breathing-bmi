function [meaGA_epochs_Beta,meaGA_epochs_Alpha,GA_epochs_Beta,GA_baseline_Beta,GA_epochs_Alpha,GA_baseline_Alpha] = compute_bandpower_GA(epochs_GA,epochs_baseline_GA,channelList)
%COMPUTE_BANDPOWER_GA


GA_epochs_Beta = {};GA_epochs_Alpha = {};
GA_baseline_Beta= {};GA_baseline_Alpha= {};
BetaRange = 20:0.1:30;
AlphaRange = 10:0.1:14;


if strcmp(channelList{1},'all')
    channelList =  epochs_GA{1}.label;
end


nChannel = length(channelList);
nSubject = length(epochs_GA);
meaGA_epochs_Beta = [];
meaGA_epochs_Alpha = [];
for iChannel = 1:nChannel
    GA_epochs_Beta_chan = [];GA_epochs_Alpha_chan = [];
    GA_baseline_Alpha_chan = [];GA_baseline_Beta_chan = [];
    
    for iSubject = 1:nSubject
        try
            
            thisSubject_epochs = epochs_GA{iSubject};
            thisSubject_baseline = epochs_baseline_GA{iSubject};
            thisSubject_baseline.data = thisSubject_baseline.data(1:size(thisSubject_epochs.data,1),:,:,:);
            channelPosition = thisSubject_epochs.map_ch(channelList{iChannel});
            
            [Alpha_bandpowerPerTrial,~,theseBaselines_Alpha,theseEpochs_Alpha]  = calculate_bandpower_for_channels(thisSubject_epochs,thisSubject_baseline,channelPosition,AlphaRange);
            [Beta_bandpowerPerTrial,~,theseBaselines_Beta,theseEpochs_Beta]  = calculate_bandpower_for_channels(thisSubject_epochs,thisSubject_baseline,channelPosition,BetaRange);
            
            GA_epochs_Beta_chan = [GA_epochs_Beta_chan ; mean(theseEpochs_Beta,1)];
            GA_epochs_Alpha_chan = [GA_epochs_Alpha_chan ; mean(theseEpochs_Alpha,1)];
            
            GA_baseline_Beta_chan = [GA_baseline_Beta_chan ; mean(theseBaselines_Beta,1)];
            GA_baseline_Alpha_chan = [GA_baseline_Alpha_chan ; mean(theseBaselines_Alpha,1)];
            
            meaGA_epochs_Alpha(iSubject,iChannel,:) = Alpha_bandpowerPerTrial;
            meaGA_epochs_Beta(iSubject,iChannel,:) = Beta_bandpowerPerTrial;
            
        catch
            fprintf('issue with subject %d\n',iSubject);
        end
    end
    
    GA_epochs_Beta{iChannel} = GA_epochs_Beta_chan;
    GA_baseline_Beta{iChannel} = GA_baseline_Beta_chan;
    
    GA_epochs_Alpha{iChannel} = GA_epochs_Alpha_chan;
    GA_baseline_Alpha{iChannel} = GA_baseline_Alpha_chan;
    
end
end

