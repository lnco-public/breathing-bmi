function [latency,grpSubject,indexRun,target,dataLatency] = anticipation_struct_GA(Data_Anticipation)
%ANTICIPATION_STRUCT_GA Summary of this function goes here
%   Detailed explanation goes here

 
    grpSubject = [];latency = [];indexRun=[];target=[];
    dataLatency = struct('mean',[],'median',[],'dev',[]);
    
    nSubject = length(Data_Anticipation);
    for iSubject = 1:nSubject
        theselatencies = Data_Anticipation{iSubject}.time_latency;
        latency= [latency;theselatencies];
        grpSubject= [grpSubject iSubject*ones(1,length(theselatencies))];
        
        dataLatency(iSubject).mean = nanmean(theselatencies);
        dataLatency(iSubject).median = nanmedian(theselatencies);
        dataLatency(iSubject).dev = nanstd(theselatencies);
        dataLatency(iSubject).iqr = iqr(theselatencies);
        
        indexRun = [indexRun Data_Anticipation{iSubject}.indexRun];
        target = [target Data_Anticipation{iSubject}.listEvent];
    end
    
    
end

