function [erd,time,P,R] = compute_ERD(epochs,channelEEG,rate,intervalBaseline,freq,win,noverlap)
%COMPUTE_ERD Summary of this function goes here
%   Detailed explanation goes here

erd = [];
nChannel = length(channelEEG);

for iChannel = 1:nChannel
    
    theseEpochs = epochs.data(:,channelEEG(iChannel),:);
    for iTrial = 1:size(theseEpochs,1)
        
        thisEpoch = theseEpochs(iTrial,:);
        [~,~,time,p] = spectrogram(thisEpoch,win/2,[],freq,rate,'power');
        time = time + epochs.time(1);
        ii = time <= intervalBaseline(2);
        R(iTrial,:,:) = nanmean(p(:,ii),2)*ones(1,size(p,2));
        P(iTrial,:,:) =  p;
        
    end
    
    X = 100*(nanmean(P)- nanmean(R))./nanmean(R);
    X = 10*log10(nanmean(P)./nanmean(R));
    erd = [erd;transpose(squeeze(nanmean(X,2)))];
end


end

