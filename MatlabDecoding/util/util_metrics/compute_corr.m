function R = compute_corr(eeg1,eeg2)


nChan = size(eeg1.data,1);
R = zeros(1,nChan);
for iChan = 1: nChan
    
   r = corrcoef(eeg1.data(iChan,:),eeg2.data(iChan,:));
   R(iChan) = r(1,2);
    
    
end




end