function plv = compute_PLV(epochs,channelEEG)
%COMPUTE_PLV Summary of this function goes here
%   Detailed explanation goes here

plv = zeros(length(channelEEG),size(epochs,3));

for iChannel = 1:length(channelEEG);
    theseEpochs = squeeze(epochs(:,channelEEG(iChannel),:));
    
    for iEpoch = 1:size(theseEpochs,1)
        thisEpoch = hilbert(theseEpochs(iEpoch,:));
        plv(iChannel,:) = plv(iChannel,:) + thisEpoch./abs(thisEpoch);
    end
    plv(iChannel,:) = abs(plv(iChannel,:))./size(theseEpochs,1);

end


end

