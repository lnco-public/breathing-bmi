function [psd,time,PR] = compute_psd_wavelet(epochs,baseline,varargin)
%COMPUTE_ Summary of this function goes here
%   Detailed explanation goes here

inputs = parse_my_inputs(epochs,baseline,varargin{:});


% define convolution parameters


psd = [];
nChannel = length(inputs.channelID);
for iChannel = 1:nChannel
    theseEpochs = epochs.data(:,inputs.channelID(iChannel),:);
    theseBaselines = baseline.data(:,inputs.channelID(iChannel),:);
    for iFreq = 1:length(inputs.freq)
        freq = inputs.freq(iFreq);
        
        % create a Morlet wavelet for a specific frequency
        cmw = create_morelet_wavelet(freq,srate,1);
        
        for iTrial = 1:size(theseEpochs,1)
            thisEpoch = theseEpochs(iTrial,:);
            thisBaseline = theseBaselines(iTrial,:);
            
            % calculation power with wavelet transform
            p = calculate_fft(thisEpoch,cmw);
            b = calculate_fft(thisBaseline,cmw);
            
            
            P(iTrial,:,:) =  p; 
            R(iTrial,:,:) = nanmean(b,1)*ones(1,size(p,2));
            PR(iTrial,iChannel,:,:) = 10*log(P(iTrial,:,:)./R(iTrial,:,:));
        end
        
        if strcmp(inputs.normalization,'log')
            X = 10*log10(mean(P,1)./mean(R,1));
        else
            X = 100*(mean(P,1)-mean(R,1))./mean(R,1);
        end
        psd = [psd;transpose(squeeze(nanmean(X,2)))];
    end
    
    
end


end

function cmw = create_morelet_wavelet(freq,srate,win_half)

time  = -win_half:1/srate:win_half; % best practice is to have time=0 at the center of the wavelet
% create complex sine wave
sine_wave = exp( 1i*2*pi*freq.*time );

% create Gaussian window
% 7 is the number of cycles
s = 7 / (2*pi*freq); % this is the standard deviation of the gaussian
gaus_win  = exp( (-time.^2) ./ (2*s^2) );

% now create Morlet wavelet
cmw = sine_wave .* gaus_win;

end

function [power,phase] = calculate_fft(data,cmw)

% convolution parameters
nData = length(data);
nKern = length(cmw);
nConv = nData + nKern -1;
half_wav = floor(length(cmw)/2) + 1;


% perform FFT on wavelet
cmwX = fft(cmw,nConv);
cmwX = cmwX./max(cmwX);

% perform FFT on data
dataX = fft(data,nConv);

% convolution
fft_data = ifft(dataX .* cmwX);

% cut 1/2 of the length of the wavelet from the beginning and from the end
fft_data = fft_data(half_wav-1:end-half_wav);

power = abs(fft_data).^2;
phase = angle(fft_data);
end




function funcInputs = parse_my_inputs(epochs,baseline,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@isstruct);
addRequired(funcInputs, 'baseline',@isstruct);
addParameter(funcInputs,'win',[],@isnumeric);
addParameter(funcInputs,'channelID',[],@isnumeric);
addParameter(funcInputs,'noverlap',[],@isnumeric);
addParameter(funcInputs,'freq',7:25,@isnumeric);
expectedType = {'log','linear'};
addParameter(funcInputs, 'normalization',{'log'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,epochs,baseline,varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:length(epochs.label);
end

end