function [distance, coefficient] = It_bhattacharyya(mean1, mean2, cov1, cov2)

cov1 = (cov1);
cov2 = (cov2); 


covP = (cov1 + cov2) / 2;
% coefNeperiano1 = det(covP/chol(cov1*cov2));
coefNeperiano2 = det(covP) / sqrt(det(cov1)*det(cov2));
distance = (1/8) * ((mean1-mean2)*pinv(covP))*(mean1-mean2)' + (1/2) * log(coefNeperiano2);

coefficient = exp(-distance);


end