function ce = compute_binary_cross_entropy(predicted,target)
%COMPUTE_BINARY_CROSS_ENTROPY Summary of this function goes here
%   Detailed explanation goes here

ce = -sum(sum(target.*log(predicted)+ (1-target).*log(1-predicted)))/numel(target);



end

