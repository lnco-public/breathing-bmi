function rate_mean = compute_mean(yPredicted,beta)

    
% performance over time
tt= [];
for iC = 1:size(yPredicted,1)
    for jC = 1:size(yPredicted,2)
        tt(iC,jC) = mean(yPredicted{iC,jC});
    end
end

yPredicted = tt;

rate_mean = mean(yPredicted);



if nargin >1
    
    for iTime = 2: length(rate_mean)
        rate_mean(iTime) = beta*rate_mean(iTime) + (1-beta)* rate_mean(iTime-1);
    end
    
end
    

end

