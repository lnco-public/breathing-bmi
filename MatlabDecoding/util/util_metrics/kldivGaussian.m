function kl = kldivGaussian(mean1, cov1, mean2, cov2)

% KLDIVGAUSSIAN Give the KL divergence between two Gaussians.
%
%	Description:
%
%	KLDIVGAUSSIAN(MEAN1, COV1, MEAN2, COV2) returns the Kullback-Leibler
%	divergence between two Gaussians with given means and covariances.
%	 Arguments:
%	  MEAN1 - mean of the first Gaussian.
%	  COV1 - covariance of the first Gaussian.
%	  MEAN2 - mean of the second Gaussian.
%	  COV2 - covariance of the second Gaussian.
%	
%
%	See also
%	LOGDET, PDINV


%	Copyright (c) 2005 Neil D. Lawrence
% 	kldivGaussian.m CVS version 1.3
% 	kldivGaussian.m SVN version 22
% 	last update 2007-11-03T14:25:20.000000Z

[invCov2, U] = pdinv(cov2);
logDet2 = logdet(cov2, U);
logDet1 = logdet(cov1);
N = size(cov1, 1);
meanDiff = mean1 - mean2;
if size(meanDiff, 1) == 1
  meanDiff = meanDiff';
end

kl = -0.5*(logDet1 - logDet2 - trace(cov1*invCov2) ...
           + N - meanDiff'*invCov2*meanDiff);

end

function [Ainv, UC, jitter] = pdinv(A, UC)

% PDINV Invert a positive definite matrix.
% FORMAT
% DESC inverts a positive definite matrix. If the matrix isn't
% quite positive definite the function adds 'jitter' to make it
% positive definite and gives out a warning message (this is done
% through JITCHOL).
% ARG A : the input positive definite matrix to be inverted.
% RETURN Ainv : the inverse of A computed using Cholesky
% decomposition.
% RETURN U : the Cholesky decomposition of A.
%
% FORMAT
% DESC inverts a positive definite matrix given the Cholesky
% decomposition of A.
% ARG A : the input positive definite matrix to be inverted.
% ARG U : the Cholesky decomposition of A.
% RETURN Ainv : the inverse of A computed using Cholesky
% decomposition.
% RETURN U : the Cholesky decomposition of A.
%
% FORMAT
% DESC inverts a positive definite matrix given the Cholesky
% decomposition of A. If jitter is used then the
% amount of jitter used is returned. 
% ARG A : the input positive definite matrix to be inverted.
% ARG U : the Cholesky decomposition of A.
% RETURN Ainv : the inverse of A computed using Cholesky
% decomposition.
% RETURN U : the Cholesky decomposition of A.
% RETURN jitter : the amount of jitter added.
%
% SEEALSO : jitChol, logdet, chol
%
% COPYRIGHT : Neil D. Lawrence, 2003, 2004, 2005, 2006


% NDLUTIL

if nargin < 2
  UC=[];
end

% Obtain a Cholesky decomposition.
if isempty(UC)
  if nargout > 2
    [UC, jitter] = jitChol(A);
  else
    UC = jitChol(A);
  end
end

invU = UC\eye(size(A, 1));
%invU = eye(size(A, 1))/UC;
Ainv = invU*invU'; 

end


function [ld, UC] = logdet(A, UC)

% LOGDET The log of the determinant when argument is positive definite.
% FORMAT
% DESC returns the log determinant of a positive definite matrix. If
% the matrix isn't quite positive definite the function adds 'jitter'
% to make it positive definite and gives out a warning message (this
% is done through JITCHOL).
% ARG A : the input positive definite matrix for which the log
% determinant is required.
% RETURN d : the log determinant of A computed using Cholesky
% decomposition.
% RETURN U : the Cholesky decomposition of A.
%
% FORMAT
% DESC returns the log determinant of a positive definite matrix given
% the Cholesky decomposition of A. If jitter is used then the
% amount of jitter used is returned.
% ARG A : the input positive definite matrix for which the log
% determinant is required.
% ARG U : the Cholesky decomposition of A.
% RETURN d : the log determinant of A computed using Cholesky
% decomposition.
% RETURN U : the Cholesky decomposition of A.
% RETURN jitter : the amount of jitter added.
%
% SEEALSO : jitChol, pdinv, chol
%
% COPYRIGHT : Neil D. Lawrence, 2003, 2004, 2005, 2006

% NDLUTIL

if nargin < 2
    UC=[];
end

% Obtain a Cholesky decomposition.
if isempty(UC)
    if nargout > 2
        [UC, jitter] = jitChol(A);
    else
        UC = jitChol(A);
    end
end
ld = 2*sum(log(abs(diag(UC))));
end

function [UC, jitter] = jitChol(A, maxTries)

% JITCHOL Do a Cholesky decomposition with jitter.
% FORMAT
% DESC attempts a Cholesky decomposition on the given matrix, if
% matrix isn't positive definite the function gives a warning, adds
% 'jitter' and tries again. At the first attempt the amount of
% jitter added is 1e-6 times the mean of the diagonal. Thereafter
% the amount of jitter is multiplied by 10 each time it is added
% again. This is continued for a maximum of 10 times.
% ARG A : the matrix for which the Cholesky decomposition is required.
% ARG maxTries : the maximum number of times that jitter is added
% before giving up (default 10).
% RETURN U : the Cholesky decomposition for the matrix.
%
% FORMAT
% DESC attempts a Cholesky decomposition on the given matrix, if
% matrix isn't positive definite the function adds 'jitter' and tries
% again. Thereafter the amount of jitter is multiplied by 10 each time
% it is added again. This is continued for a maximum of 10 times.  The
% amount of jitter added is returned.
% ARG A : the matrix for which the Cholesky decomposition is required.
% ARG maxTries : the maximum number of times that jitter is added
% before giving up (default 10).
% RETURN U : the Cholesky decomposition for the matrix.
% RETURN jitter : the amount of jitter that was added to the
% matrix.
%
% SEEALSO : chol, pdinv, logdet
%
% COPYRIGHT : Neil D. Lawrence, 2005, 2006

% NDLUTIL

if nargin < 2
  maxTries = 10;
end
jitter = 0;
for i = 1:maxTries
  try
    % Try --- need to check A is positive definite
    if jitter == 0;
      jitter = abs(mean(diag(A)))*1e-6;
      UC = chol(A);
      break
    else
      if nargout < 2
        warning(['Matrix is not positive definite in jitChol, adding ' num2str(jitter) ' jitter.'])
      end
      UC = chol(real(A+jitter*eye(size(A, 1))));
      break
    end
  catch
    % Was the error due to not positive definite?
    nonPosDef = 0;
    verString = version;
    if str2double(verString(1:3)) > 6.1
      [void, errid] = lasterr;
      if strcmp(errid, 'MATLAB:posdef')
        nonPosDef = 1;
      end
    else
      errMsg = lasterr;
      if findstr(errMsg, 'positive definite')
        nonPosDef = 1;
      end
    end
  end
  if nonPosDef
    jitter = jitter*10;
    if i==maxTries
      error(['Matrix is non positive definite tried ' num2str(i) ...
             ' times adding jitter, but failed with jitter ' ...
             'of ' num2str(jitter) '. Increase max tries'])
    end
  else
    error(lasterr)
  end
end
end


