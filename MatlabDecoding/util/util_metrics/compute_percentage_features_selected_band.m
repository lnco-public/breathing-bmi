function percentage =  compute_percentage_features_selected_band(matrix,freq,channelOfInterest,plotting)
%CO Summary of this function goes here
%   Detailed explanation goes here


if nargin == 3
    plotting = false;
end




AlphaFreqRange = 8:0.1:12;
[~,iFreqA] = intersect(freq,AlphaFreqRange);
BetaFreqRange = 20:0.1:26;
[~,iFreqB] = intersect(freq,BetaFreqRange);

if  ndims(matrix) == 3
    nSubject = size(matrix,3);
else
    nSubject = size(matrix,1);
end

for iSubject = 1:nSubject
    disp(iSubject)
    if ndims(matrix) == 3 
        thisMatrix = squeeze(matrix(:,:,iSubject));
        
        alphaBand =reshape(thisMatrix(channelOfInterest,iFreqA),[],1);
        betaBand = reshape(thisMatrix(channelOfInterest,iFreqB),[],1);
        alphaBand(alphaBand == 0) = nan;
        betaBand(betaBand == 0) = nan;
        percentage_freqAlphaBand(iSubject) =nanmean(alphaBand);
        percentage_freqBetaBand(iSubject)=  nanmean(betaBand);
    elseif ndims(matrix) == 4
        for iCV = 1:10
            thisMatrix = squeeze(matrix(iSubject,iCV,:,:));
            percentage_freqAlphaBand(iSubject,iCV) =mean(reshape(thisMatrix(channelOfInterest,iFreqA),[],1));
            percentage_freqBetaBand(iSubject,iCV)=  mean(reshape(thisMatrix(channelOfInterest,iFreqB),[],1));
        end
    end
end

percentage = struct('mu',percentage_freqAlphaBand,'beta',percentage_freqBetaBand);
disp(percentage)

if plotting  && ndims(matrix)==3
    
    h = figure()
    bar([percentage_freqAlphaBand' percentage_freqBetaBand'])
    
end
end

