function grand_average = compute_GA(data,varargin)
%CUMPUTE_GA_ICA compute the grand average of IC components



inputs = parse_my_inputs(data,varargin{:});

grand_average = zeros(length(inputs.channel),size(data,3));

for iChannel = 1: length(inputs.channel)
    theseEpochs= squeeze(data(:,inputs.channel(iChannel),:));
    if inputs.zscore
    theseEpochs = zscore_across_trials(theseEpochs);
    end
    mean_trials = nanmean(theseEpochs,1);
    grand_average(iChannel,:) = mean_trials;
end
 
end


%% Parser
function funcInputs = parse_my_inputs(data, varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'data',@(x) isnumeric(x) & length(size(x)) == 3);
addParameter(funcInputs,'zscore',false, @islogical);
addParameter(funcInputs,'channel',[],@isnumeric)
parse(funcInputs, data, varargin{:});
funcInputs = funcInputs.Results;


if isempty(funcInputs.channel)
funcInputs.channel = 1: size(data,2);
end

end
