function TK = compute_TK(dataEMG)
%% calculate the operator

TK(1) = 0;
for i=2:length(dataEMG)-2
TK(i) = dataEMG(i)*dataEMG(i) - dataEMG(i-1)*dataEMG(i+1);
end

