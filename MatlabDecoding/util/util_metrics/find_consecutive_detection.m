
function detectionIndex = find_consecutive_detection(thisTrialDetection,n)

k = [true;diff(thisTrialDetection(:))~=1 ];
s = cumsum(k);
x =  histc(s,1:s(end));
idx = find(k);
detectionIndex = thisTrialDetection(idx(x>=n));

if isempty(detectionIndex)
   detectionIndex = nan; 
end

end

