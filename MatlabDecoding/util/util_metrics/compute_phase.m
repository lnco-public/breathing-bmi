function phase = compute_phase(epochs,ichannel)

nEpoch = size(epochs,1);
phase = zeros(nEpoch,size(epochs,3));
theseEpochs = squeeze(epochs(:,ichannel,:));

for iEpoch = 1: nEpoch
    phase(iEpoch,:) = angle(hilbert(theseEpochs(iEpoch,:)));
    
end

end

