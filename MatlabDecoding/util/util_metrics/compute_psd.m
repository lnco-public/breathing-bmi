function [X,t,f,P,R,PR] = compute_psd(theseEpochs,theseBaselines,varargin)
%COMPUTE_ Summary of this function goes here
%   Detailed explanation goes here

inputs = parse_my_inputs(theseEpochs,theseBaselines,varargin{:});

nEpoch = size(theseEpochs,1);

for iEpoch = 1:nEpoch
    if ndims(theseEpochs) == 3
        P(iEpoch,:,:) = squeeze(theseEpochs(iEpoch,:,:));
        basPower = squeeze(theseBaselines(iEpoch,:,:));
        f = [];
        t = [];
        
    else
        thisEpoch = theseEpochs(iEpoch,:);
        thisBaseline = theseBaselines(iEpoch,:);
        
        [~,~,~,basPower] = spectrogram(thisBaseline,inputs.win,inputs.noverlap,inputs.freq,inputs.rate,'power');
        [~,f,t,power] = spectrogram(thisEpoch,inputs.win,inputs.noverlap,inputs.freq,inputs.rate,'power');
        
        P(iEpoch,:,:) =  log(power);
        basPower = log(basPower);
    end
    
    
    R(iEpoch,:,:) = mean(basPower,2)*ones(1,size(P,3));
    
end

 [X,PR] = normalize_power(P,R,inputs);

end


function [X,PR] = normalize_power(P,R,inputs)


if ndims(inputs.theseEpochs) == 3
     PR = bsxfun(@minus,P,R);
     X = squeeze(10*mean(PR,1));
else
    if strcmp(inputs.normalization,'log')
        
        PR = bsxfun(@minus,P,R);
        X = squeeze(10*mean(PR,1));
    else
        PR = bsxfun(@minus,P,R);
        PR= bsxfun(@rdivide,  PR, R);
        X = 100*squeeze(mean(PR,1));
    end
       
end

  

end

function funcInputs = parse_my_inputs(theseEpochs,theseBaselines,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'theseEpochs',@isnumeric);
addRequired(funcInputs, 'theseBaselines',@isnumeric);
addParameter(funcInputs,'win',512,@isnumeric);
addParameter(funcInputs,'rate',512,@isnumeric);
addParameter(funcInputs,'noverlap',256,@isnumeric);
addParameter(funcInputs,'freq',5:0.1:35,@isnumeric);
addParameter(funcInputs,'averagePowerEpoch',true,@islogical);
expectedType = {'log','linear'};
addParameter(funcInputs, 'normalization',{'log'},@(x) any(validatestring(x{:}, expectedType)));


parse(funcInputs,theseEpochs,theseBaselines,varargin{:});
funcInputs = funcInputs.Results;

end