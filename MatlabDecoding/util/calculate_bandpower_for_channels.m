function [bandpowerAverage,bandpowerPerTrial,theseBaselines,theseEpochs] = calculate_bandpower_for_channels(epochs,baselines,channelOfInterest,freqInterval)
%CALCULATE_BANDPOWER_FOR_CHANNEL Summary of this function goes here
%   Detailed explanation goes here

freqInterval = freqInterval(1):0.01:freqInterval(2);
[~,iFreq] = intersect(epochs.freq,freqInterval);

nChannel = length(channelOfInterest);
[nEpoch,~,~,nTime] = size(epochs.data);
bandpowerPerTrial = nan(nChannel,nEpoch,nTime);
bandpowerAverage = nan(nChannel,nTime);
for iChannel = 1 :length(channelOfInterest)  
    thisChannel = channelOfInterest(iChannel);
    theseEpochs = squeeze(nansum(epochs.data(:,thisChannel,iFreq,:),3));
    theseBaselines = squeeze(sum(nanmean(baselines.data(:,thisChannel,iFreq,:),4),3))*ones(1,size(theseEpochs,2));
    bandpowerPerTrial(iChannel,:,:) = 10*bsxfun(@minus, theseEpochs,theseBaselines);
    bandpowerAverage(iChannel,:) = nanmean(squeeze(bandpowerPerTrial(iChannel,:,:)),1); 
    
end

end
