function [matrix,eloc] = matrix16to64channels(matrix,processFolder,eloc)
%MATRIX16TO64CHANNELS Summary of this function goes here
%   Detailed explanation goes here
eloc64 = importdata([processFolder filesep 'chanlocs.mat']);
meanPower=  zeros(60,size(matrix,2));
index60 = ismember(upper({eloc64.labels}),upper({eloc.labels}));
eloc = eloc64;
meanPower(index60,:) = matrix;
matrix = meanPower;


end

