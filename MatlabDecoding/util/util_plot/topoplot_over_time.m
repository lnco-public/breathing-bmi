function topoplot_over_time(power,time,freqgrid,timeInterval,eloc,rate,eloc64,alltogether)
%TOPOPLOT_OVER_TIME Summary of this function goes here
%   Detailed explanation goes here

if nargin <8
    alltogether = false;
end


P = mean(power,1);
iLeft = find(time == timeInterval(1));
iRight = find(time == timeInterval(2));
noverlap = 0.25*rate;
timeOfInterest = iLeft:noverlap:iRight;
nTime = length(timeOfInterest);


if ~alltogether
    AlphaFreqRange = 8:0.1:12;
    [~,iFreqA] = intersect(freqgrid,AlphaFreqRange);
    Beta1FreqRange = 12.5:0.1:16;
    [~,iFreqB1] = intersect(freqgrid,Beta1FreqRange);
    Beta2FreqRange = 16.5:0.1:20;
    [~,iFreqB2] = intersect(freqgrid,Beta2FreqRange);
    Beta3FreqRange = 20.5:0.1:28;
    [~,iFreqB3] = intersect(freqgrid,Beta3FreqRange);
    
    meanPower4Band4Channel =[];
    for iChannel = 1:size(power,1)
        thisPower = squeeze(power(iChannel,:,:));
        meanPower4Band4Channel(1,iChannel,:) = mean(thisPower(iFreqA,:),1);
        meanPower4Band4Channel(2,iChannel,:) = mean(thisPower(iFreqB1,:),1);
        meanPower4Band4Channel(3,iChannel,:) = mean(thisPower(iFreqB2,:),1);
        meanPower4Band4Channel(4,iChannel,:) = mean(thisPower(iFreqB3,:),1);
    end
    
    
    %
    % meanPower=  zeros(size(meanPower4Band4Channel,1),60,size(meanPower4Band4Channel,3));
    
    % if topo64
    %     index60 = ismember(upper({eloc64.labels}),upper({eloc.labels}));
    %     eloc = eloc64;
    %     meanPower(:,index60,:) = meanPower4Band4Channel;
    %     meanPower4Band4Channel = [];
    %     meanPower4Band4Channel = meanPower;
    % end
    
    
    iLeft = find(time == timeInterval(1));
    iRight = find(time == timeInterval(2));
    noverlap = 0.25*rate;
    timeOfInterest = iLeft:noverlap:iRight;
    
    nBand = size(meanPower4Band4Channel,1);
    nTime = length(timeOfInterest);
    for iBand = 1:nBand
        thismeanPower = squeeze(meanPower4Band4Channel(iBand,:,:));
        for iTime = 1:nTime
            subplot(nBand,nTime,iTime + (iBand-1)*nTime)
            plot_mytopoplot(thismeanPower(:,timeOfInterest(iTime)),eloc64,'style','map');
            colormap jet;
            title(['t = ' num2str(time(timeOfInterest(iTime)))])
            caxis([-5 5])
        end
    end
    
    
else
    for iTime = 1:nTime
        subplot(nBand,nTime,iTime + (iBand-1)*nTime)
        plot_mytopoplot(P(:,timeOfInterest(iTime)),eloc64,'style','map');
        colormap jet;
        title(['t = ' num2str(time(timeOfInterest(iTime)))])
        caxis([-5 5])
    end
    
end





if ~alltogether
    ta = annotation('textbox', [0, 0.95, 1 0], 'string', 'Mu');
    ta.FontSize = 15;
    ta =annotation('textbox', [0, 0.73, 1, 0], 'string', 'Low Beta');
    ta.FontSize = 15;
    ta =annotation('textbox', [0, 0.52, 1, 0], 'string', 'Med Beta');
    ta.FontSize = 15;
    ta =annotation('textbox', [0, 0.3, 1, 0], 'string', 'High Beta');
    ta.FontSize = 15;
end

end

