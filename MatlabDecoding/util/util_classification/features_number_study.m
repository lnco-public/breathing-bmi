function yPredicted = features_number_study(training,testing,varargin)
%FEATURES_NUMBER_STUDY find the optimal number of features  
%   YPREDICTED = FEATURES_NUMBER_STUDY(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = FEATURES_NUMBER_STUDY(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut')
%                              (default LeaveOneOut)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type    
%                             (default 1/10)
%    'channelSelection'        method use for channel selection (defaul 'fisherScore')


%% Parameters

channel2Keep = 6;
inputs = parse_my_inputs(training,testing,varargin{:});
if strcmp(inputs.feature2Use,'temporal')
    nChannelTemporal = size(training.features.temporal,3)/8;
elseif strcmp(inputs.feature2Use,'spectral')
    nChannelSpectral = size(training.features.spectral,3)/19;
elseif strcmp(inputs.feature2Use,'both')||strcmp(inputs.feature2Use,'fusion')
    nChannelTemporal = size(training.features.temporal,3)/8;
    nChannelSpectral = size(training.features.spectral,3)/19;
end
nSample = size(training.label,2);


%sLDA parameters
delta = inputs.delta;
maxiter = 250; % maximum number of iterations
convergenceCriterion = inputs.convCriterion;

% choose the type of cross validation we want
if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(1:nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(1:nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(1:nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
        
    end
    
end

warning('off','all')
Beta = cell(1,CVO.NumTestSets);
iTime = inputs.timeOfInterest;

if  strcmp(inputs.verbose,'on')
    fprintf('\n*************************')
    fprintf('\nClassification Process...\n')
end
%% Cross validation
for iFeature = 1:length(inputs.featureRange)
    stop  = -1*(inputs.featureRange(iFeature)); % request non-zero variables
    if  strcmp(inputs.verbose,'on')
        fprintf('nb features #%d\n', iFeature);
    end
    for iCV = 1:CVO.NumTestSets
        
        % distribution of samples for training and testing set (temporal and spectral)   
        if strcmp(inputs.cvType{1},'LeaveOneRun')
            iTrain = CVO.training{iCV};
            iTest = CVO.test{iCV};
        else
            iTrain = CVO.training(iCV);
            iTest = CVO.test(iCV);
        end
        
        % label for training and testing
        Y = training.label(:,iTrain);
        Y = reshape(Y',[],1);
        
        % prepare label for sLDA use
        yTrain = prepare_label_sLDA(Y);
        
        
        if  ~strcmp(inputs.feature2Use,'spectral')
            xt_Train = training.features.temporal(:,iTrain,:);
            xt_Train = change_structure_period(xt_Train);
            xt_Test = testing.features.temporal(:,iTest,:);
        else
            xt_Train = [];
            xt_Test = [];
            
        end
        
        if ~strcmp(inputs.feature2Use,'temporal')
            xs_Train = training.features.spectral(:,iTrain,:);
            xs_Train = change_structure_period(xs_Train);
            xs_Test = testing.features.spectral(:,iTest,:);
        else
            xs_Train = [];
            xs_Test = [];
        end
       
        if strcmp(inputs.channelSelection{1},'fisherScore')
            
            if ~strcmp(inputs.feature2Use,'spectral')
                
                xt_Train = fisher_channel_selection(xt_Train,Y,'testing',xt_Test,...
                    'nChannel',nChannelTemporal,'nChannel2Select',channel2Keep);
            end
            if ~strcmp(inputs.feature2Use,'temporal')
                
                xs_Train = fisher_channel_selection(xs_Train,Y,'testing',xs_Test,...
                    'nChannel',nChannelSpectral,'nChannel2Select',channel2Keep);
            end
            
        else
            if ~strcmp(inputs.feature2Use,'spectral')
                bestChannels4tF{iCV} = 1:nChannelTemporal;
            end
            if ~strcmp(inputs.feature2Use,'temporal')
                bestChannels4sF{iCV} = 1:nChannelSpectral;
            end
        end
        
    if strcmp(inputs.feature2Use,'fusion')
        
        %normalize features independently 
        [xt_Train,mu_t,sigma_t] = normalize(xt_Train);
        [xs_Train,mu_s,sigma_s] = normalize(xs_Train);
        
        % train sLDA with temporal features
        [BetaT{iCV}, ~] = slda(xt_Train, yTrain,delta,stop,1, maxiter, convergenceCriterion);
        
        % train sLDA with spectral features
        [BetaS{iCV}, ~] = slda(xs_Train, yTrain,delta,stop,1, maxiter, convergenceCriterion);
        
        % project data 
        DC_ttrain = xt_Train*BetaT{iCV};
        DC_strain = xs_Train*BetaS{iCV};
        
        DC_train = [DC_ttrain DC_strain];
    else
        % put together features
        xTrain = [xt_Train xs_Train];
        % normalize features together
        [xTrain,mu,sigma] = normalize(xTrain);
        
        % train SLDA
        Beta{iCV} = slda(xTrain, yTrain, delta, stop,1, maxiter, convergenceCriterion);
        DC_train = xTrain*Beta{iCV};
        
    end
    
     if strcmp(inputs.feature2Use,'both')
         xTest = cat(3,xt_Test, xs_Test);
    elseif strcmp(inputs.feature2Use,'temporal')
        xTest = xt_Test;
    elseif strcmp(inputs.feature2Use,'spectral')
        xTest = xs_Test;
    end
    
    
    
    if strcmp(inputs.feature2Use,'fusion')
        feat_test = squeeze(xt_Test(iTime,:,:));
        n = size(feat_test,1);
        feat_test = (feat_test-ones(n,1)*mu_t)./(ones(n,1)*sigma_t);
        DC_ttest = feat_test*BetaT{iCV};
        
        feat_test = squeeze(xs_Test(iTime,:,:));
        feat_test = (feat_test-ones(n,1)*mu_s)./(ones(n,1)*sigma_s);
        DC_stest = feat_test*BetaS{iCV};
        
        DC_test = [DC_ttest DC_stest];
    else
        feat_test = squeeze(xTest(iTime,:,:));
        n = size(feat_test,1);
        feat_test = (feat_test-ones(n,1)*mu)./(ones(n,1)*sigma);
        DC_test = feat_test*Beta{iCV};
    end
    
    [~,~,POSTERIOR] = classify(DC_test, DC_train, Y, 'linear');
    yPredicted{iFeature,iCV} = POSTERIOR(:,2);
   
    end
end

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function funcInputs = parse_my_inputs(training,testing,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addRequired(funcInputs, 'testing',@isstruct);
expectedType = {'asynchronous','synchronous'};
addParameter(funcInputs, 'condClassification', {'asynchronous'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'percent2Hold',1/10,@(x) isnumeric(x))
expectedType = {'fisherScore','none'};
addParameter(funcInputs, 'channelSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',@isnumeric)
addParameter(funcInputs,'featureRange',@isnumeric)
addParameter(funcInputs,'timeOfInterest',@isnumeric)
addParameter(funcInputs,'delta',1e-3,@isnumeric);
addParameter(funcInputs,'convCriterion',1e-6,@isnumeric);
expectedType = {'spectral','temporal','both','fusion'};
addParameter(funcInputs,'feature2Use',{'both'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'off','on'};
addParameter(funcInputs,'verbose',{'on'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs,training,testing, varargin{:});
funcInputs = funcInputs.Results;


end
