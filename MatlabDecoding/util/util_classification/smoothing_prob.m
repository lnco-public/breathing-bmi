function output = smoothing_prob(prob,alpha)

[nTrial,nPeriod] = size(prob);
 output = prob;
for iTrial = 1:nTrial
    for iPeriod = 2:nPeriod
        output(iTrial,iPeriod) = prob(iTrial,iPeriod)* (1-alpha) + output(iTrial,iPeriod-1)* alpha;
    end
end