function index = create_index_CV(nEpoch,KFold)

nTrial4Fold = floor(nEpoch/KFold);

index = [];

for iFold = 1:KFold-1
    index = [index iFold*ones(1,nTrial4Fold)];    
end

rest = nEpoch - (KFold-1)*nTrial4Fold ; 
index = [index KFold*ones(1,rest)];


end

