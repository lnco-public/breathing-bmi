function yTrain = prepare_label_sLDA(Y)

class = sort(unique(Y)); 
yTrain = zeros(length(Y),length(class));

for iLabel = 1:length(Y)
    
    for iClass = 1:length(class)
        
        if Y(iLabel) == class(iClass)
            yTrain(iLabel,iClass) = 1;
        else
            continue;
            
        end
        
    end
    
    
end

end

