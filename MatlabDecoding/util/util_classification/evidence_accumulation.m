function prob_smoothed = evidence_accumulation(prob,alpha)
%EVIDENCE_ACCUMULATION Summary of this function goes here
%   Detailed explanation goes here

prob_smoothed = prob;
newArray = 0.5*ones(size(prob,1),1);
prob_smoothed = [newArray prob_smoothed];
[nEpoch,nTime] = size(prob);
if size(prob,1) > 1
    
    for i = 2:nTime
            prob_smoothed(:,i) = prob(:,i)* (1-alpha) + prob_smoothed(:,i-1)* alpha;
    end
    
else 
    for i = 2:nTime
        prob_smoothed(i) = prob(i)* (1-alpha) + prob_smoothed(i-1)* alpha;
    end 
end


prob_smoothed(:,1) = [];

end


    


