function data = zscore_across_trials(data)
%ZSCORE_ACROSS_TRIALS Summary of this function goes here
%   Detailed explanation goes here


temp_data = reshape(data',1,[]);
[~,mu,sigma] = zscore(temp_data);

n = size(data,2);
for i = 1: size(data,1) 
   data(i,:) = (data(i,:)-mu*ones(1,n))./(sigma*ones(1,n));
end


end

