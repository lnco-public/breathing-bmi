function [order_trial_test,order_trial_train] = get_order_trial_from_CV(CVO)

nFold = CVO.NumTestSets;

order_trial_train = [];
order_trial_test = [];

for iFold = 1:nFold 
    
    % test
    theseTrials_train = find(CVO.training(iFold) == 1);
    order_trial_train = [order_trial_train; theseTrials_train];
    
    % train
    theseTrials_test = find(CVO.test(iFold) == 1);
    order_trial_test = [order_trial_test; theseTrials_test];
end

