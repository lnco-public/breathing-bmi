function resultOptimizer = GridSearchCV(training,tuningParameter,tuningValue,varargin)
%GRIDSEARCHCV Summary of this function goes here
%   Detailed explanation goes here

inputs = parse_my_inputs(training,tuningParameter,tuningValue,varargin{:});


parfor iTuneValue = 1: length(tuningValue)
    [~,output,~] = apply_classifier_new(training,...
        'cvType',{'KFold'},...
        'nFold',inputs.cv,....
        tuningParameter{1},tuningValue(iTuneValue));
    estimatorResults(:,iTuneValue) = calculate_estimator(output,inputs);
end


% Find bestOptimizer
meanEstimator = mean(estimatorResults,1);
if strcmp(inputs.estimator,'accuracy')
    [bestValueEstimator,indx] = max(meanEstimator);
elseif strcmp(inputs.estimator,'binary_cross_entropy')
    [bestValueEstimator,indx] = min(meanEstimator);
end
  
resultOptimizer = struct('tuningParameter',tuningParameter,...
    'tuningValue',{tuningValue},...
    'estimatorResults',estimatorResults,...
    'bestParameter',tuningValue(indx),...
    'bestEstimator',bestValueEstimator);

plot_results(resultOptimizer,inputs)
end


function plot_results(resultOptimizer,inputs)

mea = mean(resultOptimizer.estimatorResults,1);
dev = std(resultOptimizer.estimatorResults,1);


h = figure();
if strcmp(inputs.plotType,'line')
    errorbar(1:length(resultOptimizer.tuningValue),mea,dev/2)
elseif strcmp(inputs.plotType,'bar')
    bar_plot_with_errorbar(mea',dev','Data',{resultOptimizer.estimatorResults'});
    set(gca,'XTickLabel',resultOptimizer.tuningValue,'XTickLabelRotation',45);
end
ylim([0 1])
title(['Parameter Optimization - ' inputs.tuningParameter{1}])
xlabel('Tuning Values');ylabel(inputs.estimator{1});

end

function estimatorResults = calculate_estimator(output,inputs)

acc_test  = [];
binCrossEntropy = [];
for i = 1:size(output.testing,2)
    if strcmp(inputs.estimator,'accuracy')
        acc_test = [acc_test sum(output.testing(i).class == output.testing(i).target)/length(output.testing(i).target)];
        
    elseif strcmp(inputs.estimator,'binary_cross_entropy')
        
        
        y = output.testing(i).posterior(:,2); 
        t = output.testing(i).target;
        ce = -sum(sum(t.*log(y)+ (1-t).*log(1-y)))/numel(t);
        binCrossEntropy = [binCrossEntropy ce];
    end
    
end

if strcmp(inputs.estimator,'accuracy')
    estimatorResults = acc_test;
elseif strcmp(inputs.estimator,'binary_cross_entropy')
   estimatorResults = binCrossEntropy;
end

end


function funcInputs = parse_my_inputs(training,tuningParameter,tuningValue,varargin)

funcInputs = inputParser;
addRequired(funcInputs, 'training',@isstruct);
addRequired(funcInputs, 'tuningParameter',@iscellstr);
addRequired(funcInputs, 'tuningValue');
expectedType = {'line','bar'};
addParameter(funcInputs, 'plotType',{'line'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'accuracy','binary_cross_entropy'};
addParameter(funcInputs, 'estimator',{'accuracy'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'cv',10,@isnumeric);


parse(funcInputs,training,tuningParameter,tuningValue, varargin{:});
funcInputs = funcInputs.Results;

end
