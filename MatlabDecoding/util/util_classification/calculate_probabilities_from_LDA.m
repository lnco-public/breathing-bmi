function [posterior,test_label] = calculate_probabilities(xTest,model,xTrain)

w = model.Coeffs(2,1).Linear;
mu = model.Coeffs(2,1).Const;
distance = xTrain * w + mu;

p1 = 0.025;
p2 = 1-p1;
bcoeff1=-log((1-p1)/p1)/prctile(distance,100*p1);
bcoeff2=-log((1-p2)/p2)/prctile(distance,100*p2);
b = (bcoeff1+bcoeff2)/2;

model = @(x) 1./(1+exp(-b*(x*w+mu)));

p = model(xTest);
test_label = p > 0.5;

% p = softmax(distance);

posterior = [1-p p];

end

