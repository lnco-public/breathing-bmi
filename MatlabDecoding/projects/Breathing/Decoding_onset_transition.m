%% START DECODING

timeIntervalRest = [-3 -1];
timeIntervalMI = [1 3];
timeInterval_pseudo_online = [-4 5];

params.params_classifier.nbFeature2Select =5;
params.params_classifier.nFold = 5;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.classifierType = {'diaglinear'};
params.params_classifier.cvType = {'LeaveOneRun'};
params.parms_classifier.nestedCV = false;
params.params_featExtraction.freq = 8:1:30;

%% ONSET 
alignEvent = runProcessedData{1}.state.MI_START;
timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalRest,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalMI,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeInterval_pseudo_online,'meanOverWindow',false,'alignEvent',alignEvent);

[training,testing] = extract_features_for_classification(runProcessedData,timeInfo,params);

  nEpoch = size(training.features.spectral,2);
index4LeaveOneRun = create_index_CV(nEpoch,params.params_classifier.nFold);

[prob,output,classifier] = apply_classifier_new(training,...
    'cvType',params.params_classifier.cvType,...
    'index4LeaveOneRun',index4LeaveOneRun,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'featureSelection',params.params_classifier.featureSelection,...
    'pseudoOnline',testing,...
    'nestedCV', params.parms_classifier.nestedCV);

plot_visualization_offline_decoder

prob_onset = prob;
output_onset=output;
classifier_onset = classifier;

if saveProcess
    
   set(h1,'PaperOrientation','landscape');
   saveas(h1,[fileSave  filesep 'Decoder_onset_results']); 
    
end


%% EXHALE
alignEvent = runProcessedData{1}.state.MI_START_EXHALE;
timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalRest,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalMI,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeInterval_pseudo_online,'meanOverWindow',false,'alignEvent',alignEvent);

[training_EXHALE,testing_EXHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

nEpoch = size(training_EXHALE.features.spectral,2);
index4LeaveOneRun = create_index_CV(nEpoch,params.params_classifier.nFold);

[prob,output,classifier] = apply_classifier_new(training_EXHALE,...
    'cvType',params.params_classifier.cvType,...
    'index4LeaveOneRun',index4LeaveOneRun,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'featureSelection',params.params_classifier.featureSelection,...
    'pseudoOnline',testing_EXHALE,...
    'nestedCV', params.parms_classifier.nestedCV);

training = training_EXHALE;
prob_EXHALE = prob;
output_EXHALE=output;
classifier_EXHALE = classifier;

plot_visualization_offline_decoder
matrix_featSpectral_EXHALE = matrix_featSpectral;

if saveProcess
    
   set(h1,'PaperOrientation','landscape');
   saveas(h1,[fileSave  filesep 'Decoder_onset_exhale_results']); 
    
end

%% INHALE
alignEvent = runProcessedData{1}.state.MI_START_INHALE;
timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalRest,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeIntervalMI,'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',timeInterval_pseudo_online,'meanOverWindow',false,'alignEvent',alignEvent);

[training_INHALE,testing_INHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

nEpoch = size(training_EXHALE.features.spectral,2);
index4LeaveOneRun = create_index_CV(nEpoch,params.params_classifier.nFold);


[prob,output,classifier] = apply_classifier_new(training_INHALE,...
    'cvType',params.params_classifier.cvType,...
    'index4LeaveOneRun',index4LeaveOneRun,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'regularization',params.params_classifier.regularization,...
    'featureSelection',params.params_classifier.featureSelection,...
    'pseudoOnline',testing_INHALE,...
    'nestedCV', params.parms_classifier.nestedCV);

training = training_INHALE;
prob_INHALE = prob;
output_INHALE=output;
classifier_INHALE = classifier;

plot_visualization_offline_decoder
matrix_featSpectral_INHALE = matrix_featSpectral;

plot_decoding_comparison_phase
% 
if saveProcess
    save([fileSave filesep 'prob_onset.mat'],'prob_onset');
    save([fileSave filesep 'output_onset.mat'],'output_onset');
    save([fileSave filesep 'classifier_onset.mat'],'classifier_onset');
    
    save([fileSave filesep 'prob_onset_EXHALE.mat'],'prob_EXHALE');
    save([fileSave filesep 'output_onset_EXHALE.mat'],'output_EXHALE');
    save([fileSave filesep 'classifier_onset_EXHALE.mat'],'classifier_EXHALE');
    
    save([fileSave filesep 'prob_onset_INHALE.mat'],'prob_INHALE');
    save([fileSave filesep 'output_onset_INHALE.mat'],'output_INHALE');
    save([fileSave filesep 'classifier_onset_INHALE.mat'],'classifier_INHALE');
    
    str = 'Decoder_onset_inhale_results';
    saving_figure_format(h1,str,{fileSave})
    
    str = 'Comparison_onset_phase_accuracy';
    saving_figure_format(h2,str,{fileSave})
    
end
