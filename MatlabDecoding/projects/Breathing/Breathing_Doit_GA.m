 
% DESK
clear;
% close all
set(0,'DefaultFigureWindowStyle','docked'); % makes figures dock into the same window
set(0,'defaultAxesFontSize',10)
set(0,'defaultLineLineWidth',2)

global verbose saveProcess
verbose = false;
saveProcess = true;

% Folder to select
folderResults = 'D:\results\Results_ANS';
folderName = uipickfiles('FilterSpec',folderResults);
% Task to perform
folder_GA = 'C:\Users\bastien\Documents\internship\Miw\MOD_loop\projects\Breathing';
analysisGA2Perform = get_task_perform(folder_GA);
 
% File Processing
try
    [subSubjectFolders,nFolder,GAexist] = find_subject_folder(folderName{1});
catch
    [subSubjectFolders,nFolder,GAexist] = find_subject_folder(folderName);
end

strSubjectID = {subSubjectFolders(3:end).name};
strSubjectID{end+1} = 'GA';
fileSaveGA = fullfile(folderName(1),'GA');
if ~GAexist
    mkdir(fileSaveGA{1})
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('*******************************\n');
fprintf('Starting Grand Average Script \n');
fprintf('*******************************\n');
if ~isempty(analysisGA2Perform)
    fprintf('GA Analyis to run: \n');
    for iAnalysis = 1:length(analysisGA2Perform)
        fprintf([analysisGA2Perform{iAnalysis} '\n'])
    end
end
j = 1;
for iFolder = j+2:nFolder
    
    fprintf('*******************************\n');
    fprintf(['Subject %d - ' subSubjectFolders(iFolder).name '\n'],j);
    fileSave = fullfile(folderName,subSubjectFolders(iFolder).name);
            try
    for iAnalysis = 1:numel(analysisGA2Perform)
        thisAnalysis = analysisGA2Perform{iAnalysis};
        [~,thisAnalysis,~] = fileparts(thisAnalysis);
        feval(thisAnalysis)
    end
            catch
                fprintf('Problem with this subject \n')
            end
    j = j + 1;
end


fprintf('*******************************\n');
fprintf('Finished script \n');
