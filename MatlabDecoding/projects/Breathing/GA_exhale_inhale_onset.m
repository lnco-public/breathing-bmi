
% 'prob_OnsetOnOffset'
varOfinterest = {'prob_onset_EXHALE','prob_onset_INHALE'};
posterior_prob_1{j} = importdata([fileSave{1} filesep varOfinterest{1} '.mat']);
posterior_prob_2{j} = importdata([fileSave{1} filesep varOfinterest{2} '.mat']);

% 'output_OnsetOnOffset'
varOfinterest = {'output_onset_EXHALE','output_onset_INHALE'};
output_1{j} = importdata([fileSave{1} filesep varOfinterest{1} '.mat']);
output_2{j} = importdata([fileSave{1} filesep varOfinterest{2} '.mat']);

% 'classifier_onset_offline'
varOfinterest = {'classifier_onset_EXHALE','classifier_onset_INHALE'};
classifier_1{j} = importdata([fileSave{1} filesep varOfinterest{1} '.mat']);
classifier_2{j} = importdata([fileSave{1} filesep varOfinterest{2} '.mat']);



if iFolder == nFolder
    keyboard
    strSubjectID{end+1} = 'GA';
    nSubject = length(posterior_prob_1);
    time = posterior_prob_1{1}.time;
    
    %% Accuracy comparison
    nvar = length(varOfinterest);
    tpr_mean = nan(length(output_1)+1,nvar);
    tpr_dev = nan(length(output_1)+1,nvar);
    
    for iSubject = 1:length(output_1)
       
        disp(iSubject)
        perf_classifier_exhale{iSubject} = plot_performance_classifier(output_1{iSubject});
        perf_classifier_inhale{iSubject} = plot_performance_classifier(output_2{iSubject});
        
        % Exhale
        tpr_mean(iSubject,1) = perf_classifier_exhale{iSubject}.TPR.test.mean;
        tpr_dev(iSubject,1)  = perf_classifier_exhale{iSubject}.TPR.test.dev;
        tpr_exhale(iSubject,:)  = perf_classifier_exhale{iSubject}.TPR.test.values;
        
        % Inhale
        tpr_mean(iSubject,2) = perf_classifier_inhale{iSubject}.TPR.test.mean;  
        tpr_dev(iSubject,2)  = perf_classifier_inhale{iSubject}.TPR.test.dev;
        tpr_inhale(iSubject,:)  = perf_classifier_inhale{iSubject}.TPR.test.values;
          
    end
    
    Data = {tpr_exhale,tpr_inhale};
    
    % Exhale
    tpr_mean(end,1) = nanmean(tpr_mean(:,1));    
    tpr_dev(end,1)  = nanmean(tpr_dev(:,1));
    
    % Inhale
    tpr_mean(end,2) = nanmean(tpr_mean(:,2));
    tpr_dev(end,2)  = nanmean(tpr_dev(:,2));
    
    %% Plotting
    nsamples = length([output_1{1}.testing.class]);
    IC = 0.95;
    stChanceLevel = binoinv(IC,nsamples,0.5)/nsamples;
    stChanceLevel = round(stChanceLevel,2);
    h1 = figure();
    subplot(2,1,1)
    color = {[0 0.4471 0.7412],...
        [0.8 0.8 0.8],[0 0 0]};
    stat_pvalues = bar_plot_with_errorbar(tpr_mean,tpr_dev,'Data',Data,'Color',color);
    ylim([0 1])
    set(gca,'xTick',1:size(tpr_mean,1),'XTickLabel',strSubjectID,'XTickLabelRotation',45);
    xlim([0.5 size(tpr_mean,1)+1-0.5])
    line([0.5 size(tpr_mean,1)+1-0.5],[stChanceLevel stChanceLevel],'Color','r','LineStyle','--');

    legend('EXHALE','INHALE')
    ylabel('Accuracy [%]');
    title('Accuracy comparison across subjects')
    grid on
    
    %% Scatter Plotting
    subplot(2,1,2)
    scatter(tpr_mean(:,2),tpr_mean(:,1),500,'b.')
    hold on;
    scatter(tpr_mean(end,2),tpr_mean(end,1),500,'r.')
    xlim([0,1]);ylim([0,1])
    xlabel('Accuracy Inhale [%]');
    ylabel('Accuracy Exhale [%]')
    title('Onset Decoding Performances Comparison between phases')
    line([0,1],[0,1],'LineStyle','--')
    

   %% Detection over time
   [p_GA_1,p_trial_GA1] = pseudo_online_GA(posterior_prob_1,true);
   [p_GA_2,p_trial_GA2] = pseudo_online_GA(posterior_prob_2,true);
 
%     timeDetectionPerSubject_1 = [];
%     timeDetectionPerSubject_2 = [];
% 
%     h5 = figure()
%    for iSubject = 1:nSubject
%        try
%            [~,~,timeDetectionPerSubject_1(iSubject)] = plot_CR_over_time(posterior_prob_1{iSubject}.value,posterior_prob_1{iSubject}.time,'color',[0 0 0],'showDeviation',{'std'},'ColorPatch',[0.8    0.8    0.8],'threshold',stChanceLevel);
%            [~,~,timeDetectionPerSubject_2(iSubject)] = plot_CR_over_time(posterior_prob_2{iSubject}.value,posterior_prob_2{iSubject}.time,'color',[0 0 0],'showDeviation',{'std'},'ColorPatch',[0.8    0.8    0.8],'threshold',stChanceLevel);
%                 catch
%            disp('project with this subject')
%        end
%    end
   
   h2 = figure();
%    fprintf('timeDetection: %d +/- %d \n',round(nanmean(timeDetectionPerSubject_1),2),round(std(timeDetectionPerSubject_1),2));
%    fprintf('timeDetection: %d +/- %d \n',round(nanmean(timeDetectionPerSubject_2),2),round(std(timeDetectionPerSubject_2),2));
   FaceAlpha = 0.3;
   colormap = {[0.8706,0.4902,0],[0,0.4471,0.7412]};
   [~,~,timeDetection_exhale] = plot_CR_over_time(p_GA_1,time,'color',colormap{2},'FaceAlpha',FaceAlpha,'showDeviation',{'stdError'},'ColorPatch',colormap{2},'threshold',stChanceLevel);
   [~,~,timeDetection_inhale] = plot_CR_over_time(p_GA_2,time,'color', colormap{1},'FaceAlpha',FaceAlpha,'showDeviation',{'stdError'},'ColorPatch',colormap{1},'threshold',stChanceLevel);
    line([-4 4],[stChanceLevel stChanceLevel],'Color','r','LineStyle','--');
    legend('CUE EXHALE','CUE INHALE','Chance Level')
    
%     h3 = figure();
%     X = [timeDetectionPerSubject_1 ;timeDetectionPerSubject_2]';
%     [p,tbl,stats] = anova1(X);
%     C = multcompare(stats,[],'off');
%     hold on;
%     timeDetection = {timeDetectionPerSubject_1',timeDetectionPerSubject_2'};
%     for iBox = 1:2
%         dataPoint = timeDetection{iBox};
%         x = rand(1,length(dataPoint))/4+iBox-0.5/4;
%         plot(x,dataPoint,'o',...
%             'LineWidth',1,...
%             'MarkerSize',5,'MarkerEdgeColor','k',...
%             'MarkerFaceColor',[0.8 0.8 0.8])
%         hold on;
%     end
%     
    
    %% Features
    % Preprocessing
    nSubject = numel(classifier_1);
    Breathing_params;
    intervalS = 1;
    channels_name = params.params_featExtraction.channelOfInterestERD_name;
    freq = params.params_featExtraction.freq;
    nChannel = length(channels_name);
    nFeature = length(params.params_featExtraction.freq);
    
    normalization = true;
    cinterval = [0 0.3];
    options = 'Power'; % Power,Beta
    concatFeature = false;
    
    style = 'both';
    
    matrix_featSpectral_1 = features_mapping_GA(classifier_1,nFeature,nChannel,options,normalization,concatFeature,true);
    matrix_featSpectral_2 = features_mapping_GA(classifier_2,nFeature,nChannel,options,normalization,concatFeature,true);

    %% Grand Average
    cinterval = [0 0.3];
    matrix_featSpectral_GA_mean_1 = squeeze(nanmean(matrix_featSpectral_1,3));
    matrix_featSpectral_GA_mean_2 = squeeze(nanmean(matrix_featSpectral_2,3));
      
    % Preprare Data
    matrix_features_topo = {matrix_featSpectral_GA_mean_1,matrix_featSpectral_GA_mean_2};
    nData = length(matrix_features_topo);
    
    % Heatmap
    titlestr = {'CUE EXHALE','CUE INHALE'};
    h3 = figure();
    for iData = 1:nData
        subplot(1,nData,iData)
        plot_features_mapping(matrix_features_topo{iData},freq,channels_name);
        title(titlestr{iData})
         caxis(cinterval)
         colorbar
    end
    
    
    %% Comparison per subjects
    h4 = figure();
    for iSubject = 1:nSubject
        subplot(ceil(nSubject/3),ceil(nSubject/3),iSubject)
        plot_CR_over_time(posterior_prob_1{iSubject}.value,posterior_prob_1{iSubject}.time,'color',colormap{2},'showDeviation',{'std'},'ColorPatch',colormap{2},'threshold',stChanceLevel,'FaceAlpha',0.2);
        plot_CR_over_time(posterior_prob_2{iSubject}.value,posterior_prob_2{iSubject}.time,'color',colormap{1},'showDeviation',{'std'},'ColorPatch',colormap{1},'threshold',stChanceLevel,'FaceAlpha',0.2);
        xlabel('');ylabel('')
        title(strSubjectID{iSubject});
 
    end
    
    %% SavingProcess
%     timeDetectionStruct = struct('exhale',timeDetectionPerSubject_1,'inhale',timeDetectionPerSubject_2);
    if saveProcess
%         DataAccuracy = struct('mu',tpr_mean,'dev',tpr_dev,'id',strSubjectID,'stat_comparison',stat_pvalues);
%         save([fileSaveGA{1} filesep 'DataAccuracy.mat'],'DataAccuracy');
        
        str = 'Accuracy_comparison_exhale_inhale_onset';
        set(h1,'PaperOrientation','landscape');
        saving_figure_format(h1,str,fileSaveGA)
        
        str = 'PseudoOnlineOffset_exhale_inhale_onset';
        set(h2,'PaperOrientation','landscape');
        saving_figure_format(h2,str,fileSaveGA)
        
        str = 'Features selection_exhale_inhale_onset';
        set(h3,'PaperOrientation','landscape');
        saving_figure_format(h3,str,fileSaveGA)
        
        str = 'PseudoOnlineOffset_exhale_inhale_onset_per_subjects';
        set(h4,'PaperOrientation','landscape');
        saving_figure_format(h4,str,fileSaveGA)
        
    end
  
end





    
    
 