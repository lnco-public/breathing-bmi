clear;close all
set(0,'DefaultFigureWindowStyle','docked'); % makes figures dock into the same window
% Recording output
outputFolder = 'Decoding_MI_START_STOP_all_band_nested_C3CP3';
outputPath = ['D:\results\Results_ANS\' outputFolder];

% Folder of Interest
folderName = 'D:\results\Results_ANS\subjectFolderExperiment';

% File Processing
[subSubjectFolders,nFolder] = find_subject_folder(folderName);

% Load parameters
params_scr = 'Breathing_params';
loadStateCode = 'ONOFF_load_state_code';

% Analysis and Classification to Perform
folder_analysis = 'C:\Users\bastien\Documents\internship\Miw\MOD_loop\projects\Breathing';
analysis2Perform = get_task_perform(folder_analysis);

% Save Folder
fileSaveAllSubjects = create_saveFolder_4Subjects(outputPath,subSubjectFolders);
logfile = fullfile(outputPath,'log.txt');

global verbose saveProcess saveVariable
verbose = false;
saveProcess = true;
saveVariable = true;

%% Loop Subject

diary(logfile)
diary on
fprintf('*******************************\n');
fprintf('Starting Main Script \n');
fprintf('Saving the data in %s \n', outputPath)
fprintf('*******************************\n');
if ~isempty(analysis2Perform)
    fprintf('Analyis to run: \n');
    for iAnalysis = 1:length(analysis2Perform)
        fprintf([analysis2Perform{iAnalysis} '\n'])
    end
end

fprintf('*******************************\n');

j =13;
startSubject = 2+j;

varExist = exist('onePlotOnly','var');
if varExist == 0
    onePlotOnly = true;
end

% Load parameters
Breathing_params

channel2Keep_list = {'Fz','FC5','FC3','FC1',...
    'FCz','FC2','FC4','FC6','C5','C3','C1'...
    'Cz','C2','C4','C6','CP5','CP3',...
    'CP1','CP2','CP4','CP6'};

j =1;
startSubject = 2+j;
for iSubject = startSubject:nFolder
    clear runData
    try
        fprintf('*******************************\n');
        fprintf(['Subject %d - ' subSubjectFolders(iSubject).name '\n'],iSubject-2);
        subjectFolder = fullfile(folderName,subSubjectFolders(iSubject).name);
        disp(subjectFolder)
        fileSave = fileSaveAllSubjects{j};
        disp(fileSave)        
        %% Load Python Data
%         file2Import = fullfile(subjectFolder,'recording_run_align.mat');
        file2Import = fullfile(subjectFolder,'recording_run.mat');

        runData = importdata(file2Import);
        %% Put same structure
        runData.eloc = [];
        runData.label =  cellstr(runData.label);
        runData.map_ch = containers.Map(runData.label, 1:length(runData.label));
        runData.bad_channels = {};
        runData.amplifierUsed = 'antneuro';
        
        channel2Keep = convert_string_channel_to_numeric(runData.map_ch,channel2Keep_list);
        channel2Remove = setdiff(1:61,channel2Keep);
        
        runData = remove_channels(runData,{channel2Remove});
        runData.event.name =double(runData.event.name);
        runData.event.position = double(runData.event.position);
        
        %% Preprocessing
        params.params_preprocessing.spatialFilter = {'CAR'};
        [runProcessedData,~] = preprocess_signal_runs({runData},params.params_preprocessing);
        
        
        %% Analysis script
        for iAnalysis = 1:numel(analysis2Perform)
            thisAnalysis = analysis2Perform{iAnalysis};
            [~,thisAnalysis,~] = fileparts(thisAnalysis);
            feval(thisAnalysis)
        end
        
    catch
        disp('Problem with this subject')
    end
    j = j + 1;
end
%

fprintf('*******************************\n');
fprintf('Script finished \n')
fprintf('*******************************\n');
diary off






