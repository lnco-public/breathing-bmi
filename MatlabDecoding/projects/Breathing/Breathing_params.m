

%% Params_plot
params.params_plotting.fontsize = 14;

%% Params_extraction
params.params_loading.channelOfInterestERD = 1:16;
params.params_loading.amplifierType = {'gtec'};
params.params_loading.chan_tot = 16;
% params.params_loading.processFolder= '/home/cnbiuser/bastien/git/Miw/gtec16';
params.params_loading.processFolder= 'C:\Users\bastien\Documents\internship\Miw\gtec16';
params.params_loading.manualSeleCh = {};
params.params_loading.alignEvent = 300;
params.params_loading.eventStartTrial = 100;
%% Params_preprocessing
params.params_preprocessing.ch_last = 'TRIG';
params.params_preprocessing.spatialFilter = {'CAR'};
params.params_preprocessing.correctionArtefact = {'none'}; % 'EOG','ICA','none','addEOG'
params.params_preprocessing.channelEOG = 21:23;
params.params_preprocessing.backward = false;
params.params_preprocessing.cutoff = [];
params.params_preprocessing.type = {'low'};
% params.params_preprocessing.processFolder = '/home/cnbiuser/bastien/git/Miw/gtec16';
params.params_preprocessing.processFolder = 'C:\Users\bastien\Documents\internship\Miw\gtec16';
params.params_preprocessing.removeBadChannels = false;
params.params_preprocessing.channelOfInterestERD = 1:16;
params.params_preprocessing.doSpatialInterpolation = false;
params.params_preprocessing.setChannelToNull = false;
%% Params_epoching
params.params_epoching.channelOfInterestERD = 1:16;
params.params_epoching.timeBeforeEventMI = 4;
params.params_epoching.timeAfterEventMI = 4;
params.params_epoching.timeBeforeEventMIT = 4;
params.params_epoching.timeAfterEventMIT = 4;
params.params_epoching.timeBeforeEventBaseline = 2;
params.params_epoching.timeAfterEventBaseline =0;
params.params_epoching.timeBeforeEventBaselineExo = 2;
params.params_epoching.timeAfterEventBaselineExo = 0;
%% Processing spectrogram
params.params_spectrogram.mlength    = 1;
params.params_spectrogram.wshift     = 0.0625;
params.params_spectrogram.selchans   = 1:16;
params.params_spectrogram.freq = 4:0.1:40;

%% Processing multitaper
params.params_multitaper.wlength    = 0.5;
params.params_multitaper.wshift     = 0.0625;
params.params_multitaper.selchans   = 1:16;
params.params_multitaper.freq = 4:2:40;

%% Processing PSD with Fastwelch
params.params_fastpwelch.mlength    = 1;
params.params_fastpwelch.wlength    = 0.5;
params.params_fastpwelch.pshift     = 0.25;
params.params_fastpwelch.wshift     = 0.0625;
params.params_fastpwelch.selchans   = 1:16;
params.params_fastpwelch.freq = 8:2:40;

%% Params_featExtraction

params.params_featExtraction.timeBeforeEvent = 4;
params.params_featExtraction.timeAfterEvent = 4;
params.params_featExtraction.wshift  = 0.0625;

% Feature Extraction
params.params_featExtraction.featureExtractedDomain = {'spectral'};

params.params_featExtraction.ws = 1;
params.params_featExtraction.freq = 8:1:30;
test = struct('noverlap',params.params_spectrogram.wshift,'timeInterval',[-2 3],'meanOverWindow',false);
offset = struct('noverlap',params.params_spectrogram.wshift,'timeInterval',[0.5 2.5],'meanOverWindow',false);
onset = struct('noverlap',params.params_spectrogram.wshift,'timeInterval',[-2 0],'meanOverWindow',false);
params.params_featExtraction.timeInfo = struct('onset',offset,'rest',onset,'test',test);
params.params_featExtraction.psdMethod = {'multitaper'}; %pwelch,fastPSDUsed,multitaper
params.params_featExtraction.tempMethod = {'amplitude'};
params.params_featExtraction.parallelComputing = true;
% channels of interest
params.params_featExtraction.channelOfInterestERD = 1:15;

params.params_featExtraction.channelOfInterestERD_name = {...
    'Fz','FC3','FC1',...
    'FCz','FC2','FC4','C3','C1'...
    'Cz','C2','C4','CP3',...
    'CP1','CP2','CP4'};

% params.params_featExtraction.channelOfInterestERD_name = {'C3','CP3'};

% params.params_featExtraction.channelOfInterestERD_name = {...
%     'FP1','FPZ','FP2','F7','F3','FZ','F4','F8','FC5','FC1','FC2','FC6','T7','C3','CZ', 'C4', 'T8',...
%     'CP5','CP1','CP2','CP6','P7','P3','PZ','P4','P8','POZ','O1',...
%     'O2','AF7','AF3','AF4','AF8','F5','F1','F2','F6','FC3','FCZ',...
%     'FC4','C5','C1','C2','C6',...
%     'CP3','CP4','P5','P1','P2','P6','PO5','PO3',...
%     'PO4','PO6','FT7','FT8','TP7','TP8','PO7','PO8','OZ'};

%% Params_classifier
params.params_classifier.nbFeature2Select =10;
params.params_classifier.nFold = 10;
params.params_classifier.variance2KeepPC = 0.9;
params.params_classifier.applyPCA = false;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.normalizeFeature = true;
params.params_classifier.classifierType = {'diaglinear'};
params.params_classifier.featureSelection = {'none'};
params.params_classifier.features2Use = {'spectral'};
params.params_classifier.regularization = false;
params.params_classifier.kernelType = {'linear'};
params.params_classifier.trueProb = false;
params.params_classifier.model2Use = [];
params.params_classifier.riemann = false;
params.params_classifier.nestedCV = true;
%% Params_online
params.params_online.detectionMethod ={'evidenceAccumulation'}; %movingAverage,evidenceAccumulation, accumulateDecision
params.params_online.nPoint2DetectOrSmooth = 20;
params.params_online.smoothing = 0.96;
params.params_online.threshold = 0.7;
params.params_online.detectionFrame= [1 3];

