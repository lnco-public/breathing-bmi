
folderData = 'G:\My Drive\BCI course 2019\DataSet';
folderData = uigetdir(folderData);

% Load parameters
Breathing_params


%% Load Python Data
file2Import = fullfile(folderData,'recording_run.mat');
runData = importdata(file2Import);


%% Put same structure
runData.eloc = [];
runData.label =  cellstr(runData.label);
runData.map_ch = containers.Map(runData.label, 1:length(runData.label));
runData.bad_channels = {};
runData.amplifierUsed = 'antneuro';

channel2Keep = {'Fz','FC5','FC3','FC1',...
    'FCz','FC2','FC4','FC6','C5','C3','C1'...
    'Cz','C2','C4','C6','CP5','CP3',...
    'CP1','CP2','CP4','CP6'};

channel2Keep = convert_string_channel_to_numeric(runData.map_ch,channel2Keep);

channel2Remove = setdiff(1:61,channel2Keep);

runData = remove_channels(runData,{channel2Remove});
runData.event.name =double(runData.event.name);
runData.event.position = double(runData.event.position);

%% Preprocessing
params.params_preprocessing.spatialFilter = {'CAR'};
[runProcessedData,~] = preprocess_signal_runs({runData},params.params_preprocessing);

varExist = exist('onePlotOnly','var');
if varExist == 0
     onePlotOnly = true;
end

%% START DECODING

%% EXHALE
alignEvent = runProcessedData{1}.state.MI_START_EXHALE;
params.params_classifier.nbFeature2Select =10;
params.params_classifier.nFold = 10;
params.params_classifier.variance2KeepPC = 0.9;
params.params_classifier.applyPCA = false;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.normalizeFeature = true;
params.params_classifier.classifierType = {'diaglinear'};

timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-2 0],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[0 2],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-4 4],'meanOverWindow',false,'alignEvent',alignEvent);

[training_EXHALE,testing_EXHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

[prob,output,classifier] = apply_classifier_new(training_EXHALE,...
    'cvType',params.params_classifier.cvType,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'regularization',params.params_classifier.regularization,...
    'featureSelection',params.params_classifier.featureSelection,...
    'applyPCA',params.params_classifier.applyPCA,...
    'kernelType',params.params_classifier.kernelType,...
    'pseudoOnline',testing_EXHALE,...
    'trueProb',false,...
    'riemann',params.params_classifier.riemann);

plot_visualization_offline_decoder;

%% INHALE
alignEvent = runProcessedData{1}.state.MI_START_INHALE;
params.params_classifier.nbFeature2Select =10;
params.params_classifier.nFold = 10;
params.params_classifier.variance2KeepPC = 0.9;
params.params_classifier.applyPCA = false;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.normalizeFeature = true;
params.params_classifier.classifierType = {'diaglinear'};

timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-2 0],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[0 2],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-4 4],'meanOverWindow',false,'alignEvent',alignEvent);

[training_INHALE,testing_INHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

[prob,output,classifier] = apply_classifier_new(training_INHALE,...
    'cvType',params.params_classifier.cvType,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'regularization',params.params_classifier.regularization,...
    'featureSelection',params.params_classifier.featureSelection,...
    'applyPCA',params.params_classifier.applyPCA,...
    'kernelType',params.params_classifier.kernelType,...
    'pseudoOnline',testing_INHALE,...
    'trueProb',false,...
    'riemann',params.params_classifier.riemann);

plot_visualization_offline_decoder

%% STOP DECODING

%% EXHALE
alignEvent = runProcessedData{1}.state.MI_STOP_EXHALE;
params.params_classifier.nbFeature2Select =10;
params.params_classifier.nFold = 10;
params.params_classifier.variance2KeepPC = 0.9;
params.params_classifier.applyPCA = false;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.normalizeFeature = true;
params.params_classifier.classifierType = {'diaglinear'};

timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-2 0],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[0.5 2.5],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-4 4],'meanOverWindow',false,'alignEvent',alignEvent);

[training_EXHALE,testing_EXHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

[prob,output,classifier] = apply_classifier_new(training_EXHALE,...
    'cvType',params.params_classifier.cvType,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'regularization',params.params_classifier.regularization,...
    'featureSelection',params.params_classifier.featureSelection,...
    'applyPCA',params.params_classifier.applyPCA,...
    'kernelType',params.params_classifier.kernelType,...
    'pseudoOnline',testing_EXHALE,...
    'trueProb',false,...
    'riemann',params.params_classifier.riemann);

training = training_EXHALE;
plot_visualization_offline_decoder

%% INHALE
alignEvent = runProcessedData{1}.state.MI_STOP_INHALE;
params.params_classifier.nbFeature2Select =10;
params.params_classifier.nFold = 10;
params.params_classifier.variance2KeepPC = 0.9;
params.params_classifier.applyPCA = false;
params.params_classifier.cvType = {'KFold'};
params.params_classifier.normalizeFeature = true;
params.params_classifier.classifierType = {'diaglinear'};

timeInfo.training{1} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-2 0],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.training{2} = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[0.5 2.5],'meanOverWindow',false,'alignEvent',alignEvent);
timeInfo.testing = struct('noverlap',params.params_featExtraction.wshift,'timeInterval',[-4 4],'meanOverWindow',false,'alignEvent',alignEvent);

[training_INHALE,testing_INHALE] = extract_features_for_classification(runProcessedData,timeInfo,params);

[prob,output,classifier] = apply_classifier_new(training_INHALE,...
    'cvType',params.params_classifier.cvType,...
    'nFold',params.params_classifier.nFold,....
    'nbFeature2Select',params.params_classifier.nbFeature2Select,...
    'classifierType',params.params_classifier.classifierType,...
    'regularization',params.params_classifier.regularization,...
    'featureSelection',params.params_classifier.featureSelection,...
    'applyPCA',params.params_classifier.applyPCA,...
    'kernelType',params.params_classifier.kernelType,...
    'pseudoOnline',testing_INHALE,...
    'trueProb',false,...
    'riemann',params.params_classifier.riemann);

training = training_INHALE;
plot_visualization_offline_decoder
