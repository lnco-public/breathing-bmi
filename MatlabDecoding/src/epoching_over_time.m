function epochs_concatenated = epoching_over_time(epochs_Cell,resampling)

if nargin == 1
    resampling = false;
end

epochs_concatenated = epochs_Cell{1};

if resampling
    nn = [];
    for i = 1:length(epochs_Cell)
        nn = [nn length(epochs_Cell{i}.time)];
    end
    nSample = floor(max(nn));
    
    [data,time] = resampling_3d(epochs_Cell{1},nSample);
    
    epochs_concatenated.data = data;
    epochs_concatenated.time = time;
end



nTrial = size(epochs_concatenated,1);

for iCell = 2:length(epochs_Cell)
    
    if resampling
    [data,time] = resampling_3d(epochs_Cell{iCell},nSample);
    else
        data = epochs_Cell{iCell}.data;
        time = epochs_Cell{iCell}.time;
    end
    
    epochs_concatenated.data = cat(3,epochs_concatenated.data,data);
    epochs_concatenated.time = [ epochs_concatenated.time time + epochs_concatenated.time(end)];
    
    for iTrial = 1:nTrial
        epochs_concatenated.event(iTrial).name = [epochs_concatenated.event(iTrial).name; epochs_Cell{iCell}.event(iTrial).name] ;
        epochs_concatenated.event(iTrial).time = [epochs_concatenated.event(iTrial).time ;epochs_Cell{iCell}.event(iTrial).time + epochs_concatenated.event(iTrial).time(end)] ;
        epochs_concatenated.event(iTrial).position  = [epochs_concatenated.event(iTrial).position ;epochs_Cell{iCell}.event(iTrial).position] ;
    end
    
end

end



function [data,time] = resampling_3d(epoch,nSample)

[nEpoch,nChannel,nPoint] = size(epoch.data);
data = zeros(nEpoch,nChannel,nSample);

for iEpoch = 1:nEpoch
    thisEpoch = squeeze(epoch.data(iEpoch,:,:));
    data(iEpoch,:,:) =  resample(thisEpoch',nSample,nPoint)';
end

time =  resample(epoch.time,nSample,nPoint);

end

