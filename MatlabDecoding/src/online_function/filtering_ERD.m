function [data,Zf,initFilter] = filtering_ERD(params,data,initFilter,Zf)

data = data';

dataBis = data;
nChannel = size(data,1);
ZfBis = Zf;
%% Temporal filtering
if ~isempty(params.filter.ERD.temporal)
    if (initFilter)
        initFilter = false;
        Zf = [];
        for iChannel = 1:nChannel
            [data(iChannel,:) ,Zf(iChannel,:)] = ...
                filter(params.filter.ERD.temporal.b,params.filter.ERD.temporal.a,data(iChannel,:));
        end
        
    else
        for iChannel = 1:nChannel
            [data(iChannel,:),Zf(iChannel,:)] = filter(params.filter.ERD.temporal.b,params.filter.ERD.temporal.a,data(iChannel,:),Zf(iChannel,:));
        end
    end
end

% EOG correction
if ~isempty(params.filter.ERD.spatial.bss)
    coeff = params.filter.ERD.spatial.bss.coeff(1:nChannel,:);
    chan_EOG = params.filter.ERD.spatial.bss.chan_EOG;
    
    hEOG = data(chan_EOG(1),:) - data(chan_EOG(2),:);
    vEOG = mean(data(chan_EOG(1:2),:)) - data(chan_EOG(3),:);
    data = data - (coeff(:,1)*hEOG + coeff(:,2)*vEOG);
end


%% Spatial filtering
data = params.filter.ERD.spatial.matrix * data(1:16,:);


end

