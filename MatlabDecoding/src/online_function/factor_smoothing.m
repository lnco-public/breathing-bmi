function prob_smoothed = factor_smoothing(prob,factor,threshold)
%FACTOR_SMOOTHING Summary of this function goes here
%   Detailed explanation goes here

%EVIDENCE_ACCUMULATION Summary of this function goes here
%   Detailed explanation goes here

prob_smoothed = prob;
[nEpoch,nTime] = size(prob);
prob_smoothed = [0.1 prob_smoothed];
for i = 2:nTime
    prob_smoothed(i) = (prob_smoothed(:,i) - threshold)*factor + prob_smoothed(:,i-1);
    prob_smoothed(i) = max(0.1,prob_smoothed(i));
    prob_smoothed(i) =min(1,prob_smoothed(i));
end

prob_smoothed(1) = [];
		


end

