function [data,Zf,initFilter] = filtering_SCP(params,data,initFilter,Zf)


data = data';
nChannel = size(data,1);

%% Temporal filter
if ~params.filter.SCP.sgolay.use
    if (initFilter)
        initFilter = false;
        Zf = [];
        for iChannel = 1:nChannel
            [data(iChannel,:) ,Zf(iChannel,:)] = ...
                filter(params.filter.SCP.temporal.b,params.filter.SCP.temporal.a,data(iChannel,:));
        end
    else
        for iChannel = 1:nChannel
            
            [data(iChannel,:),Zf(iChannel,:)] = filter(params.filter.SCP.temporal.b,params.filter.SCP.temporal.a,data(iChannel,:),Zf(iChannel,:));
            
        end
    end
    
else
    dataFiltered = sgolayfilt(data',params.filter.SCP.sgolay.order,params.filter.SCP.sgolay.framelen);
    
    data = dataFiltered';
      
end

%% EOG correction
if ~isempty(params.filter.SCP.spatial.bss)
    coeff = params.filter.SCP.spatial.bss.coeff(1:nChannel,:);
    chan_EOG = params.filter.SCP.spatial.bss.chan_EOG;
    
    hEOG = data(chan_EOG(1),:) - data(chan_EOG(2),:);
    vEOG = mean(data(chan_EOG(1:2),:)) - data(chan_EOG(3),:);
    data = data - (coeff(:,1)*hEOG + coeff(:,2)*vEOG);
end

%% Spatial filtering
data = params.filter.SCP.spatial.matrix * data;

end

