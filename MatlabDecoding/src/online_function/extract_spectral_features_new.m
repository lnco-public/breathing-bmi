function features = extract_spectral_features_new(params,data)

features = [];
freqRange = params.features2Use;
win = 0.5;
noverlap = 0.5;
channelID = params.channel2Keep.spectral;
fs = 512;

for iChannel = 1: length(channelID)
    ch = channelID(iChannel);
    signal = data(ch,:);
   
    [psd, freqgrid] = pwelch(signal, fs*win,fs*win*noverlap,[],fs);
    
    % Selecting desired frequencies
    [freqs, idfreqs] = intersect(freqgrid,freqRange);
    f = psd(idfreqs)';
    %% extract spectral features
    
    features = [features log(f)];
    
end

end

