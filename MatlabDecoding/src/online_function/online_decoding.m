function prob = online_decoding(features,params)


% normalize features
features = normalize_feature(features,params);

% apply PCA
if params.params_classifier.applyPCA
    features = features * params.classifier.coeff;
end

% regularization sLDA
if params.params_classifier.regularization
    features = features * params.classifier.Beta;
else
    features = features(params.classifier.Beta);
    
end

% %% Classification
% [~,~,POSTERIOR] = classify(DC,params.classifier.DC,params.classifier.Y, params.classifier.classifierType);
%  prob = POSTERIOR(:,2);

[~,POSTERIOR ] = predict(params.classifier.Model,features);
prob  = POSTERIOR(:,2);

end

function features = normalize_feature(features,params)

% features = (features - params.classifier.paramZscore.mu) ./(params.classifier.paramZscore.sigma);

sigma = params.classifier.paramZscore.sigma;
sigma(sigma==0) = 1;
features = bsxfun(@minus,features, params.classifier.paramZscore.mu);
features = bsxfun(@rdivide, features, sigma);
end