function features = extract_temporal_features_new(params,data,covmatrixUse)


features = [];
channelID = params.channel2Keep.temporal;
fs = size(data,2);
Dstep = fs/params.classifier.training.features.Dtemp;

for iChannel = 1: length(channelID)
    ch = channelID(iChannel);
    signal = data(ch,:);

    %% resampling
    
   
    %% extract temporal features
    if covmatrixUse
        features = [features ;signal];
    else
        signal= signal(1:Dstep:end);
        features = [features signal];
    end
end
features = cov(features');
end

