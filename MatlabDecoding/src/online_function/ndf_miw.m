% Simple ndf_*.m example for CNBI loop. It implmements alpha rhythms
% monitoring on channel Cz, when coupled with a gTec gUSBamp.
% Classificiation output is forwarded to a feedback through iC interface.

function ndf_miw()

% Include any required toolboxes
ndf_include();
addpath(genpath('/home/cnbiadmin/Git/Miw/MOD_loop'))

% Prepare and enter main loop
try
    % Initialize loop structure
    cl = cl_new();
    
    % Connect to the CnbiTk loop
    if(cl_connect(cl) == false)
        disp('[ndf_miw] Cannot connect to CNBI Loop, killing matlab');
        exit;
    end
    
    % Prepare NDF srtructure
    ndf.conf  = {};
    ndf.size  = 0;
    ndf.frame = ndf_frame();
    ndf.sink  = ndf_sink('/tmp/cl.pipe.ndf.0'); % Connect to /pipe0
    
    % Dump TiC messages
    IC  = tic_newsetonly(); % Create iC
    icm = icmessage_new(); % Create iC message
    ics = icserializerrapid_new(icm); % Create iC serializer
    % Add classisifer to ic message
    icmessage_addclassifier(icm, 'alpha', 'example',...
        icmessage_getvaluetype('prob'), icmessage_getlabeltype('biosig'));
    % Add a single class to ic message
    icmessage_addclass(icm, 'alpha', '1', 0.50);
    ica = '/ctrl0'; % Specify socket for IC communication
    icmessage_dumpmessage(icm); % Dump message as example
    
    
    % Create a single ID client for sending and receiving events to/from a feedback
    % (or other loop modules). The possibility for separate ID clients for
    % receiving/sendind exists
    ID = tid_new(); % Create ID client
    idm = idmessage_new(); % Create ID message for both sening/receiving
    ids = idserializerrapid_new(idm); % Create ID message serializer
    % Configure ID message
    idmessage_setdescription(idm, 'io');
    idmessage_setfamilytype(idm, idmessage_familytype('biosig'));
    idmessage_dumpmessage(idm);
    ida = '\bus'; % Alias of iD bus
    
    % Pipe opening and NDF configuration
    % - Here the pipe is opened
    % - ... and the NDF ACK frame is received
    disp('[ndf_miw] Receiving ACK...');
    [ndf.conf, ndf.size] = ndf_ack(ndf.sink);
    
    
    % Load the MAT file containing a trained classifier's parameters
    user = load('/home/cnbiadmin/Desktop/recording_MO/online/parameters.mat');
    
    % Create folder for saving data
    formatOut = 'yyyyddmmHHMM';
    today = datestr(now,formatOut);
    folder = ['recording/recordingOnline_' today];
    mkdir(folder);
     save([folder filesep 'ndf.mat'],'ndf')
    fprintf('classifier type -  %s \n',user.parameters.classifier.type)
    
    % Create the data buffer with a single channel, 1 second long buffer
    buffer.scp = ndf_ringbuffer(ndf.conf.sf, ndf.conf.eeg_channels, 1.00);
    buffer.erd = ndf_ringbuffer(ndf.conf.sf, ndf.conf.eeg_channels, 1.00);
    
    % Buffer for trigger channel
    buffer.tri = ndf_ringbuffer(ndf.conf.sf, ndf.conf.tri_channels, 1.00);
    
    
    % Optional Parameters
    evidence_acc = 0.95;
    adaptClassifier = false;
    nUSBamp = 1;
    
    % Initialize variables
    output = 0.5;
    event = 0;
    Zf_s = [];Zf_t = []; initFilter_s = true; initFilter_t = true;
    tf = [];sf = [];features_temporal = [];features_spectral = [];
    pr = []; o = [];buff_trig = [];
    buff_dataAll= struct('SCP',[],'ERD',[],'EEG',[]);
    outputAll = {}; probAll=  {};bufferData = {};bufTrig = {};parameters = {};features = {};  
   
    
    disp('[ndf_miw] Receiving NDF frames...');
 
    while(true)
        
        [ndf.frame, ndf.size] = ndf_read(ndf.sink, ndf.conf, ndf.frame);
        % Acquisition is down, exit
        if(ndf.size == 0)
            disp('[ndf_miw] Broken pipe');
            break;
        end
        
      
        if sum(sum((isnan(ndf.frame.eeg)))) > 0
            disp('WARNING!')
           ndf.frame.eeg(isnan(ndf.frame.eeg)) = 0;
        end
        
        % Preprocessing
        [data_s,Zf_s,initFilter_s] = filtering_ERD(user.parameters,ndf.frame.eeg,initFilter_s,Zf_s);
        [data_t,Zf_t,initFilter_t] = filtering_SCP(user.parameters,ndf.frame.eeg,initFilter_t,Zf_t);
     
        % ringbuffer for filtered signal
        buffer.scp = ndf_add2buffer(buffer.scp, data_t');
        buffer.erd = ndf_add2buffer(buffer.erd, data_s');
       
        % ringbuffer for trigger button
        buffer.tri = ndf_add2buffer(buffer.tri, ndf.frame.tri);
        
     
        if(event == 300)
            
            % Features extraction
            sf = extract_spectral_features_new(user.parameters,buffer.erd');
            tf = extract_temporal_features_new(user.parameters,buffer.scp');
            
            % Classification
            new_prob = online_classify(tf,sf,user.parameters);
            
            % Feedback integration using exponential evidence accumulation
            output = f_integration(new_prob,output,evidence_acc);
            
               
            if isnan(output)
                fprintf ('buffering trouble \n');
                output = 0.5;
            end
            
            
            %% Save all the data
	    
            % Button
            if nUSBamp == 1
                buff_trig = [buff_trig ;ndf.frame.tri];
            elseif nUSBamp == 2   % with two USBamps two triggers ! otherwise no colum specification
                buff_trig = [buff_trig ;buffer.tri(:,2)];
            end
            
            % Raw Signal
            buff_dataAll.EEG = [buff_dataAll.EEG ndf.frame.eeg'];
            % SCP
            buff_dataAll.SCP = [buff_dataAll.SCP data_t];
            % ERD
            buff_dataAll.ERD = [buff_dataAll.ERD data_s];

            % Probability and Evidence accumulation recorded
            o = [o output];
            pr = [pr new_prob];
            
            % Save features
            features_temporal = [features_temporal; tf];
            features_spectral = [features_spectral; sf];
        else
		output = 0.5;
        end
        
        % Handle sync TOBI iC communication
        if(tic_isattached(IC) == true)
            icmessage_setvalue(icm, 'alpha', '1', output);
            tic_setmessage(IC, ics, ndf.frame.index);
            
        else
            tic_attach(IC, ica);
        end
        
        % Handle sync TOBI iD communication
        if(tid_isattached(ID) == true)
            
            % Here read any incoming messages from the feedback
            while(tid_getmessage(ID, ids) == true)
                msg = idmessage_getevent(idm);
                if(msg == 300)
                    output = 0.5;
                    o =[];pr = [];buff_trig = [];
                    buff_dataAll= struct('SCP',[],'ERD',[],'EEG',[]);
                    features_temporal = [];
                    features_spectral = [];
                elseif(msg == 9999)
                    % Update the classifier with the last trial
                    if adaptClassifier
                        user.parameters = adaptative_classifier(buff_dataAll,user.parameters,ndf,buff_trig);
                    end
                    
                    % Saving Information for each trial
                    outputAll{end+1} = o;
                    probAll{end+1} = pr;
                    bufTrig{end+1} = buff_trig;
                    features{end+1} = struct('temporal', features_temporal,'spectral',features_spectral);
                    parameters{end+1} = user.parameters;
                    bufferData{end+1} = buff_dataAll;
                  
                    save([folder filesep 'bufferData.mat'],'bufferData')
                    save([folder filesep 'features.mat'],'features')
                    save([folder filesep 'outputAll.mat'],'outputAll')
                    save([folder filesep 'parameters.mat'],'parameters')
                    save([folder filesep 'probAll.mat'],'probAll')
                    save([folder filesep 'bufTrig.mat'],'bufTrig')
                  
                    
                end
                
                if(msg < 990 || msg > 999)
                    event = msg;
                end
                
                if (msg == 001)
                    evidence_acc = evidence_acc + 0.01;
                    fprintf('alpha is equal to %d \n',evidence_acc)
                elseif (msg == 002)
                    evidence_acc = evidence_acc - 0.01;
                    fprintf('alpha is equal to %d \n',evidence_acc)
                end
                
            end
        else
            tid_attach(ID);
        end
        
    end
    
catch exception
    ndf_printexception(exception);
    
    % Tear down loop structure
    ndf_close(ndf.sink);
    icserializerrapid_delete(ics);
    idserializerrapid_delete(ids);
    icmessage_delete(icm);
    idmessage_delete(idm);
    tic_detach(IC);
    tic_delete(IC);
    tid_detach(ID);
    tid_delete(ID);
    
    
    fclose('all');
    disp('[ndf_miw] Going down');
end
