function prob = online_classify(tf,sf,params)

if strcmp(params.classifier.type,'temporal')
    
    % normalize features
    features = tf;
    features = normalize_feature(features,params);
     
    % apply PCA
    if params.classifier.applyPCA
        features = features * params.classifier.coeff;
    end
    
    % regularization sLDA
    if params.classifier.regularization
        DC = features * params.classifier.Beta;
    else
        DC = features(params.classifier.Beta);
        
    end

elseif strcmp(params.classifier.type,'spectral')
    
    % normalize features
    features = sf;
    features = normalize_feature(features,params);
     
    % apply PCA
    if params.classifier.applyPCA
        features = features * params.classifier.coeff;
    end
    
    % regularization sLDA
    if params.classifier.regularization
        DC = features * params.classifier.Beta;
    else
        DC = features(params.classifier.Beta);
        
    end
    
elseif strcmp(params.classifier.type,'fusion')
    
    % normalize features
    [tf,sf] = normalize_feature_fusion(tf,sf,params);
    
     % apply PCA
     if params.classifier.applyPCA
        tf = tf * params.classifier.coeff.temporal;
        sf = sf * params.classifier.coeff.spectral;
    end
    
    % regularization sLDA
    if params.classifier.regularization
        DC_scp = tf* params.classifier.Beta.temporal;
        DC_erd = sf * params.classifier.Beta.spectral;
        
    else
        DC_scp = tf(params.classifier.Beta.temporal);
        DC_erd = sf(params.classifier.Beta.spectral);
    end
    DC = [DC_scp DC_erd];
end



%% Classification
[~,~,POSTERIOR] = classify(DC,params.classifier.DC,params.classifier.Y, params.classifier.classifierType);
 prob = POSTERIOR(:,2);   

end

function features = normalize_feature(features,params)

features = (features - params.classifier.paramZscore.mu) ./(params.classifier.paramZscore.sigma);

end

function [tf,sf] = normalize_feature_fusion(tf,sf,params)

    tf = (tf - params.classifier.paramZscore.mu.temporal) ./(params.classifier.paramZscore.sigma.temporal);
    sf = (sf - params.classifier.paramZscore.mu.spectral) ./(params.classifier.paramZscore.sigma.spectral);


end