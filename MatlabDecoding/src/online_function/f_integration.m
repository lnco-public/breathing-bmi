function output = f_integration(probArray,params)

if strcmp(params.params_online.detectionMethod,'evidenceAccumulation')
    output = probArray(end-1)* (1-params.alpha) + probArray(end)* params.alpha;
elseif strcmp(params.params_online.detectionMethod,'movingAverage')
    output  =  movingmean(Posterior_prob,params.nPoint2DetectOrSmooth,2);
    
elseif strcmp(params.params_online.detectionMethod,'accumulateDecision')
    output = accumulate_decision(probArray,params);
end
end

function output = accumulate_decision(probArray,params)
ProbUpToThreshold = probArray(end-params.nPoint2DetectOrSmooth:end) > params.threshold;
sum_Detection = sum(ProbUpToThreshold);

output = sum_Detection * 1/params.nPoint2DetectOrSmooth;

end