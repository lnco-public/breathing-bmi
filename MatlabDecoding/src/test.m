prob = prob_offset;
proSmoothed=  prob_offset;


lead = 1;
lag = 400;

prob_values = cell2mat(prob.value);
proSmoothed.value = movingmean(prob_values,400,2);

for i = 1:size(prob_values,1)
    [~,p] = movavg(prob_values(i,:),lead,lag,1);
    proSmoothed2(i,:) = p;
end



figure()
plot_CR_over_time(prob.value,prob.time,'color',[1 0 0],'ColorPatch',[1    0.8    0.8],'showDeviation',{'none'});
hold on;
plot_CR_over_time(prob.smoothed,prob.time,'color',[0 0 1],'ColorPatch',[0.8    0.8    1],'showDeviation',{'none'});
hold on;
plot_CR_over_time(proSmoothed2,prob.time,'color',[0 1 0],'ColorPatch',[0.8    1    0],'showDeviation',{'none'});
