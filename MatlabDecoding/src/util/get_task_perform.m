function task2Perform = get_task_perform(folderName)
%GET_TASK_PERFORM Summary of this function goes here
%   Detailed explanation goes here

script = dir([folderName filesep '*.m']);
script = {script.name};
indx = listdlg('ListString','Select an analysis to do:','ListString',script,'ListSize',[300,300]);
task2Perform = {script{indx}};

end

