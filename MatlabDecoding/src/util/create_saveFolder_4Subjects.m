function fileSave = create_saveFolder_4Subjects(OutputPath,subSubjectFolders,statusAnalysis)
j = 0;

if nargin <3
    
    statusAnalysis = struct('LoadedData',false,...
        'Preprocessed',false,...
        'Analyzed',[],...
        'Classified',[]);
end

params.params_loading = [];
params.params_preprocessing = [];


warning('off')
for iFile = 3:numel(subSubjectFolders)
    j = j + 1;
    lastwarn('')
    fileSave{j} = fullfile(OutputPath,subSubjectFolders(iFile).name);
    mkdir(fileSave{j})
    [warnMsg, warnId] = lastwarn;
    if ~isempty(warnMsg)
        continue
    end
    save([fileSave{j} filesep 'statusAnalysis.mat'],'statusAnalysis');
    save([fileSave{j} filesep 'params.mat'],'params');
end

end

