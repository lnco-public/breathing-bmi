function [subSubjectFolders,nFolder,GAexist] = find_subject_folder(folderName)

subSubjectFolders = dir(folderName);
subSubjectFolders = subSubjectFolders ([subSubjectFolders(:).isdir]);
if any(strcmp({subSubjectFolders(:).name},'GA'))
    
    subSubjectFolders(strcmp({subSubjectFolders(:).name},'GA')) = [];
    GAexist = true;
else
    GAexist = false;
end
nFolder = numel(subSubjectFolders);


end

