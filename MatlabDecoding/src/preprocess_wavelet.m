function [tf_mean_epoch,tf_epochs]  = preprocess_wavelet(theseEpochs,varargin)

inputs = parse_my_inputs(theseEpochs,varargin{:});


nFreq = length(inputs.freq);
[nEpoch,nPointsEpoch] = size(theseEpochs);
time  = -inputs.win:1/inputs.fs:inputs.win;
half_wave = (length(time)-1)/2;

% other wavelet parameters
cycRange = [4 15];
nCycles  = logspace(log10(cycRange(1)),log10(cycRange(end)),nFreq);

% FFT parameters Epochs
nWave = length(time);
nData = nPointsEpoch*nEpoch;
nConv = nWave+nData-1;

% initialize output time-frequency data
tf_mean_epoch = zeros(nFreq,nPointsEpoch);


% spectra of data
data_epoch = fft( reshape(theseEpochs',1,[]) ,nConv);
tf_epochs = [];

for fi=1:nFreq
    
    % create wavelet and get its FFT
    s = nCycles(fi)/(2*pi*inputs.freq(fi));
    cmw = exp(2*1i*pi*inputs.freq(fi).*time) .* exp(-time.^2./(2*s^2)); 
    tempX = fft(cmw,nConv);
    cmwX = tempX ./ max(tempX);
    
    % run convolution for epochs
    asE = ifft(cmwX.*data_epoch,nConv);
    asE = asE(half_wave+1:end-half_wave);
    asE = reshape(asE,nPointsEpoch,nEpoch);
    
    % put power data into big matrix
    tf_mean_epoch(fi,:) = mean(abs(asE).^2,2);
    tf_epochs = [tf_epochs transpose(abs(asE).^2)];
end

end


%% Parser
function funcInputs = parse_my_inputs(epochs,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'theseEpochs',@isnumeric);
addParameter(funcInputs,'freq',4:1:40,@isnumeric);

addParameter(funcInputs,'fs',512,@isnumeric);
addParameter(funcInputs,'win',2,@isnumeric);

parse(funcInputs, epochs, varargin{:});

funcInputs = funcInputs.Results;

end


