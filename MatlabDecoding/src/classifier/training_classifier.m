function [output,classifier] = training_classifier(features,labels,varargin)

%TRAINING_CLASSIFIER extract features that will be used later by the
%classifier
%   YPREDICTED = TRAINING_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TRAINING_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none')
%                              (default 'fisherScore')
%
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(features,labels,varargin{:});

% choose the type of cross validation we want
CVO = find_partition(inputs);
coeff = [];
%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    fprintf('Fold #%d\n', iCV);
    
    % distribution of samples for training and testing set (temporal and spectral)
    if strcmp(inputs.cvType{1},'LeaveOneRun')
        iTrain = CVO.training{iCV};
        iTest = CVO.test{iCV};
    else
        iTrain = CVO.training(iCV);
        iTest = CVO.test(iCV);
    end
    
    % label for training and testing
    yTrain{iCV} =labels(iTrain);
    yTest{iCV} =labels(iTest);
    
    
    % features for training and testing
    xTrain = features(iTrain,:);
    xTest= features(iTest,:);
    
    
    %% Normalization
    % normalize features together - Training Set
    [xTrain,mu,sigma] = zscore(xTrain);
    
    % normalize features together - Testing Set
    xTest = normalize_feature(xTest,mu,sigma);
    
    
    %% PCA
    if inputs.applyPCA
        
        % PCA - Training Set
        [coeffa,xTrain,latent]  = pca(xTrain);
        nPC = find(cumsum(latent)./sum(latent) >= 0.95,1,'first'); % select 95% variance
        xTrain = xTrain(:,1:nPC);
        coeff{iCV} = coeffa(:,1:nPC);
        
        % project on PC - Testing Set
        xTest = xTest * coeff{iCV};
        
    end
    
    %% Feature selection
    if ~strcmp(inputs.featureSelection,'none')
        
        % features selection - Training Set
        [xTrain,Beta{iCV},P{iCV}] = features_selection(xTrain,yTrain{iCV},...
            'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
        
        % features selection - Testing Set
        xTest = xTest(:,Beta{iCV});
        
    end
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%% Performance assesment of the classifier %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % training
    [classTrain{iCV},~,posterior_training{iCV}] = classify(xTrain, xTrain, yTrain{iCV},inputs.classifierType{1});
    
    % testing
    [classTest{iCV},~,posterior_testing{iCV} ] = classify(xTest, xTrain, yTrain{iCV},inputs.classifierType{1});
    
    % random classification
    randIndex = randperm(length(yTrain{iCV}));
    yy{iCV} = yTrain{iCV}(randIndex);
    [classRandom{iCV},~,posterior_random{iCV} ] = classify(xTest, xTrain, yy{iCV},inputs.classifierType{1});
    
    
end


paramZscore.mu = mu;
paramZscore.sigma = sigma;


%% Output of the functions

% performance results for classifier
trainStruct = struct('class',classTrain,'posterior',posterior_training,'target',yTrain);
testingStruct = struct('class',classTest,'posterior',posterior_testing,'target',yTest);
randomStruct = struct('class',classRandom,'posterior',posterior_random,'target',yTest);
output = struct('training',trainStruct,'testing',testingStruct,'random',randomStruct);

% build structure for classifier
classifier = struct('Beta',{Beta},'Pfeature',{P},'paramZscore',paramZscore,'coeff',{coeff},...
    'applyPCA',inputs.applyPCA,'nbFeature2Select',inputs.features2Select,...
    'featureSelection',inputs.featureSelection,'features',features,'labels',labels);



end

function feat = normalize_feature(feat,mu,sigma)

n = size(feat,1);
feat = (feat-ones(n,1)*mu)./(ones(n,1)*sigma);

end


function CVO = find_partition(inputs)

nSample = size(inputs.labels,1);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(features,labels,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'features',@isnumeric);
addRequired(funcInputs, 'labels',@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'features2Select',[],@isnumeric);
expectedType = {'fisherScore','rankfeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',@isnumeric)
expectedType = {'diaglinear','diagquadratic','linear','quadratic'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,features,labels, varargin{:});
funcInputs = funcInputs.Results;

if strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankfeatures'};
    
end

end

