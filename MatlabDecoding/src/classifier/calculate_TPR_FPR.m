function [TPR,FPR] = calculate_TPR_FPR(target,output_classifier)
%CALCULATE_TPR_FPR Summary of this function goes here
%   Detailed explanation goes here

C = confusionmat(target',output_classifier');
nClass = size(C,1);
iDiag = eye(nClass,nClass) == 1;
nSample = sum(sum(C,1));
C = C/nSample;
TPR = sum(C(iDiag));
FPR = sum(C(~iDiag));

end

