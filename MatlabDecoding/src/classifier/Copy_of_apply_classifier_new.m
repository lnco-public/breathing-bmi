function [prob,output,classifier] = apply_classifier_new(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier
%   YPREDICTED = TEST_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TEST_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type
%                              (default 1/10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none')
%                              (default 'fisherScore')
%
%    'regularization'          boolean to use or not use regularization(default false)
%    'features2Select'         number of features to select (default 10)
%    'delta'                   regularization parameter for L1-regularization (default 1e-6)
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(training,varargin{:});

% choose the type of cross validation we want
CVO = find_partition(inputs);


warning('off','all')
global verbose

if verbose
    fprintf('\n*************************')
    fprintf('\nClassification Process...\n')
end

% Initialize variable 
Power_feature = [];
coeff_PCA = [];
xT ={}; xt = {};
prob = [];

%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    if verbose
        fprintf('Fold #%d\n', iCV);
    end
    
    % Prepare labels
    [yTrain{iCV},yTest{iCV},Ytrain,Ytest] = prepare_labels_for_classification(training,CVO,inputs,iCV);

    % Prepare features 
    [xTrain,xTest,xTest_period] = prepare_features_for_classification(training,CVO,inputs,iCV);
    
    % Normalize features
    [xTrain,paramZscore.mu,paramZscore.sigma] = zscore(xTrain);
    xTest =  normalize_feature(xTest,paramZscore);
 
    %% PCA
    if inputs.applyPCA
            % PCA - Training Set
%             [coeffa,xTrain,latent]  = pca(xTrain);
%             [coeffa,ddd,latent]  = pca(xTrain);
%             nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
%             coeff_PCA{iCV} = coeffa(:,1:nPC);
            
            [coeff_PCA{iCV},inputs] = apply_PCA(xTrain,xTest,inputs);
            % project on PC
%             xTrain = xTrain(:,1:nPC);
            xTrain = xTrain * coeff_PCA{iCV};    
            xTest = xTest * coeff_PCA{iCV};  
            
            
            
            if nPC < inputs.nbFeature2Select && ~strcmp(inputs.featureSelection,'none')
                inputs.nbFeature2Select = nPC;
                fprintf('not enough principal component, decrease features to select to %d \n',inputs.nbFeature2Select )
                
            end
            
    end
    
    %% Feature selection
    if ~strcmp(inputs.featureSelection,'none')
%         % features selection - Training Set
%         [xTrain,Beta{iCV},Power_feature{iCV}] = features_selection(xTrain,yTrain{iCV},...
%             'nfeature2Select',inputs.nbFeature2Select,...
%             'method',inputs.featureSelection);
%         
%         % features selection - Testing Set
%         xTest = xTest(:,Beta{iCV});  
%         
        
        [xTrain,xTest,Beta{iCV},Power_feature{iCV}] = apply_feature_selection(xTrain,xTest,yTrain{iCV},inputs);

        
    else
        Beta{iCV} = ones(size((xTrain),2),1);
    end
    
    %% Regularization
    if inputs.regularization
        [xTrain,xTest,Beta] = apply_regularization(xTrain,Ytrain,xTest);
%         % train SLDA with all features
%         Beta{iCV} = slda(xTrain,  Ytrain, inputs.delta,-1*(inputs.nbFeature2Select),1);
%         
%         % project data
%         xTrain = xTrain*Beta{iCV};
%         xTest = xTest*Beta{iCV};
    end
    
    %% Classification
    
    % Build model
    %     if strcmp(inputs.classifierType,'SVM')
    %         Model = fitcsvm(xTrain,yTrain{iCV},'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    %         Model = fitPosterior(Model);
    %     else
    %         Model = fitcdiscr(xTrain,yTrain{iCV},'DiscrimType',inputs.classifierType{1});
    %     end
    Model = build_model(xTrain,yTrain{iCV},inputs);
    [classTrain{iCV},posterior_training{iCV}] = predict(Model,xTrain);
    [classTest{iCV},posterior_testing{iCV}] = predict(Model,xTest);
    
    
    %     % Build model ramdom
    %     if strcmp(inputs.classifierType,'SVM')
    %         Model = fitcsvm(xTrain,yTrain{iCV},'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    %         Model = fitPosterior(Model);
    %     else
    %         Model = fitcdiscr(xTrain,yTrain{iCV},'DiscrimType',inputs.classifierType{1});
    %     end
    
    % random chance level permutation-based
    classRandom{iCV} = [];
    posterior_random{iCV} = [];
    yRandom{iCV} = [];
    
    randIndex = randperm(length(yTest{iCV}));
    xRandom = xTest(randIndex);
    yRandom{iCV} = yTest{iCV}(randIndex);
    [cr,pr] = predict(Model,xTest);
    classRandom{iCV} =  cr;
    posterior_random{iCV} =  pr;
    
    
    
%     for i = 1:10
%         randIndex = randperm(length(yTest{iCV}));
%         xRandom = xTest(randIndex);
%         yRandom{iCV} = [yRandom{iCV}  ;yTest{iCV}(randIndex)];
%         [cr,pr] = predict(Model,xTest);
%         classRandom{iCV} = [classRandom{iCV}; cr];
%         posterior_random{iCV} = [posterior_random{iCV}; pr];
%     end
    
    
    xT{iCV} = xTrain;
    xt{iCV} = xTest;
    %% Pseudo-online classification
    
    if ~isempty(inputs.pseudoOnline)
        nPeriod = size(inputs.pseudoOnline.label,1);
        for iTime = 1:nPeriod
            
            % normalization - Testing Set over time
            feat_test = normalize_feature_over_period(xTest_period,iTime,paramZscore);
            
            if inputs.applyPCA
                feat_test = feat_test * coeff_PCA{iCV};
            end
            
            if inputs.regularization
                feat_test = feat_test*Beta{iCV};
            else
                if ~strcmp(inputs.featureSelection,'none')
                    feat_test = feat_test(:,Beta{iCV});
                end
            end
            
            % Classifier ouput
            [~,POSTERIOR ] = predict(Model,feat_test);
            yPredicted{iCV,iTime}  = POSTERIOR(:,2);
            
        end
    end
end

   
%% Output of the functions
% 
% paramZscore.mu = struct('mu',mu,'sigma',sigma);

% performance results for classifier
trainStruct = struct('class',classTrain,'posterior',posterior_training,'target',yTrain);
testingStruct = struct('class',classTest,'posterior',posterior_testing,'target',yTest);
randomStruct = struct('class',classRandom,'posterior',posterior_random,'target',yRandom);
output = struct('training',trainStruct,'testing',testingStruct,'random',randomStruct);

% build structure for classifier
classifier = struct('Beta',{Beta},'Pfeature',{Power_feature},'paramZscore',paramZscore,'coeff',{coeff_PCA},...
    'applyPCA',inputs.applyPCA,'nbFeature2Select',inputs.nbFeature2Select,...
    'regularization',inputs.regularization,...
    'featureSelection',inputs.featureSelection,'training',training,'pseudoOnline',inputs.pseudoOnline,...
    'classifierType',inputs.classifierType,'DC',{xT},'Y',{yTrain},'CVO',CVO);

% pseudo-online probabilities
if ~isempty(inputs.pseudoOnline)
    prob.value = yPredicted;
    prob.time = inputs.pseudoOnline.time;
end
end



function [coeff_PCA,inputs] = apply_PCA(xTrain,xTest,inputs)

% PCA - Training Set
[coeff,~,latent]  = pca(xTrain);
nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
coeff_PCA = coeff(:,1:nPC);

% project on PC
xTrain = xTrain * coeff_PCA;
xTest = xTest * coeff_PCA;

if nPC < inputs.nbFeature2Select && ~strcmp(inputs.featureSelection,'none')
    inputs.nbFeature2Select = nPC;
    fprintf('not enough principal component, decrease features to select to %d \n',inputs.nbFeature2Select )
    
end
end


function [xTrain,xTest,Beta] = apply_regularization(xTrain,Ytrain,xTest)

% train SLDA with all features
Beta = slda(xTrain,  Ytrain, inputs.delta,-1*(inputs.nbFeature2Select),1);

% project data
xTrain = xTrain*Beta;
xTest = xTest*Beta;

end

function [xTrain,xTest,Beta,Power_feature] = apply_feature_selection(xTrain,xTest,yTrain,inputs)

% features selection - Training Set
[xTrain,Beta,Power_feature] = features_selection(xTrain,yTrain,...
    'nfeature2Select',inputs.nbFeature2Select,...
    'method',inputs.featureSelection);

% features selection - Testing Set
xTest = xTest(:,Beta);

end

function Model = build_model(xTrain,yTrain,inputs)


if strcmp(inputs.classifierType,'SVM')
    Model = fitcsvm(xTrain,yTrain,'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    Model = fitPosterior(Model);
else
    Model = fitcdiscr(xTrain,yTrain,'DiscrimType',inputs.classifierType{1});
end


end

function [yTrain,yTest,Ytrain,Ytest] = prepare_labels_for_classification(training,CVO,inputs,iCV)
% Distribution of samples for training and testing set (temporal and spectral)
Ytrain = [];
Ytest=  [];
if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end

% Labeling - training and testing
yT= training.label(:,iTrain);
yTrain = reshape(yT',[],1);
yt = training.label(:,iTest);
yTest = reshape(yt',[],1);

% prepare label for sLDA use
if inputs.regularization
    Ytrain = prepare_label_sLDA(yTrain);
    Ytest = prepare_label_sLDA(yTest);
end

end


function [xTrain,xTest,xTest_period] = prepare_features_for_classification(training,CVO,inputs,iCV)

if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end

if ~isempty(training.features.spectral)
    xTrain = training.features.spectral(:,iTrain,:);
    xTrain = change_structure_period(xTrain);
    xTest = training.features.spectral(:,iTest,:);
    xTest = change_structure_period(xTest);
    
    if ~isempty(inputs.pseudoOnline)
        xTest_period = inputs.pseudoOnline.features.spectral(:,iTest,:);
    end
else
    xTrain = training.features.temporal(:,iTrain,:);
    xTrain = change_structure_period(xTrain);
    xTest = squeeze(training.features.temporal(:,iTest,:));
    xTest = change_structure_period(xTest);
    
    if ~isempty(inputs.pseudoOnline)
        xTest_period = inputs.pseudoOnline.features.temporal(:,iTest,:);
    end
end

 
end

function feat = normalize_feature_over_period(xPeriod,iTime,paramZscore)

feat = squeeze(xPeriod(iTime,:,:));
if iscolumn(feat)
   feat = feat'; 
end
feat = normalize_feature(feat,paramZscore);

end

function feat = normalize_feature(feat,paramZscore)

paramZscore.sigma(paramZscore.sigma==0) = 1;
feat = bsxfun(@minus,feat, paramZscore.mu);
feat = bsxfun(@rdivide, feat, paramZscore.sigma);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function CVO = find_partition(inputs)

nSample = size(inputs.training.label,2);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs, 'pseudoOnline',[],@isstruct);
addParameter(funcInputs, 'regularization',false,@islogical);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'percent2Hold',5/10,@(x) isnumeric(x))
expectedType = {'rankfeatures','corr','fisher','relief','infgain','none'};
addParameter(funcInputs, 'featureSelection',{'fisher'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',[],@isnumeric)
addParameter(funcInputs,'nbFeature2Select',[],@isnumeric);
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
expectedType = {'linear','RBF','gaussian','polynomial'};
addParameter(funcInputs,'kernelType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));


parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

if funcInputs.regularization
    funcInputs.featureSelection = {'none'};
end

end
