 function [yPredicted,yTrainPredicted,output,classifier] = apply_classifier_gau(training,testing,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = TEST_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TEST_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type    
%                              (default 1/10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none') 
%                              (default 'fisherScore')
% 
%    'regularization'          boolean to use or not use regularization(default false)
%    'features2Select'         number of features to select (default 10)
%    'delta'                   regularization parameter for L1-regularization (default 1e-6)
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(training,testing,varargin{:});
nPeriod = size(testing.label,1);
% choose the type of cross validation we want
CVO = find_partition(inputs);

P = [];

warning('off','all')
if inputs.verbose
    fprintf('\n*************************')
    fprintf('\nClassification Process...\n')
end

orderTest = [];
coeff = [];coeff_t = [];coeff_s = [];
BetaT = [];BetaS = [];
BetaChT = [];BetaChS = [];BetaCh = [];
xT ={}; xt = {};





settings.modules.smr.gau.somunits 	= [1 1]; % QDA-style
settings.modules.smr.gau.sharedcov 	= 'f'; % No difference anyway
% settings.modules.smr.gau.epochs 	= 20;
% settings.modules.smr.gau.mimean		= 0.0001;
% settings.modules.smr.gau.micov		= 0.00001;
% settings.modules.smr.gau.th		= 0.70;
% settings.modules.smr.gau.terminate	= true;



%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    if inputs.verbose 
        fprintf('Fold #%d\n', iCV);
    end
    % distribution of samples for training and testing set (temporal and spectral)
    if strcmp(inputs.cvType{1},'LeaveOneRun')
        iTrain = CVO.training{iCV};
        iTest = CVO.test{iCV};
    else
        iTrain = CVO.training(iCV);
        iTest = CVO.test(iCV);
    end
    
    orderTest = [orderTest; find(iTest == 1)];
    % label for training
    yT= training.label(:,iTrain);
    yTrain{iCV} = reshape(yT',[],1);
    
    % label for testing
    yt = training.label(:,iTest);
    yTest{iCV} = reshape(yt',[],1);
    

    %%
    
    if  ~strcmp(inputs.feature2Use,'spectral')
        xt_Train = training.features.temporal(:,iTrain,:);
        xt_Train = change_structure_period(xt_Train);
        xt_Test = training.features.temporal(:,iTest,:);
        xt_Test  = change_structure_period(xt_Test);
        nFeaturePerChannel = training.features.Dtemp;
        xt_Train_period = testing.features.temporal(:,iTrain,:);
        xt_Test_period = testing.features.temporal(:,iTest,:);
        
    else
        xt_Train = [];
        xt_Test = [];
        xt_Train_period = [];
        xt_Test_period = [];
    end
    
    if ~strcmp(inputs.feature2Use,'temporal')
        xs_Train = training.features.spectral(:,iTrain,:);
        xs_Train = change_structure_period(xs_Train);
        xs_Test = training.features.spectral(:,iTest,:);
        xs_Test = change_structure_period(xs_Test);
        nFeaturePerChannel = training.features.Dspec;
       
        xs_Train_period = testing.features.spectral(:,iTrain,:);
        xs_Test_period = testing.features.spectral(:,iTest,:);
        
    else
        xs_Train = [];
        xs_Test = [];
        xs_Train_period = [];
        xs_Test_period = [];
    end
    
       %% Normalization
    if strcmp(inputs.feature2Use,'fusion')
        
        %normalize features independently - Training Set
        [xt_Train,mu_t,sigma_t] = zscore(xt_Train);
        [xs_Train,mu_s,sigma_s] = zscore(xs_Train);
        
        % normalize features independently - Testing Set
        xt_Test =  normalize_feature(xt_Test,mu_t,sigma_t);
        xs_Test =  normalize_feature(xs_Test,mu_s,sigma_s);     
    else
        % normalize features together - Training Set
        xTrain = [xt_Train xs_Train];
        [xTrain,mu,sigma] = zscore(xTrain);
        
        % normalize features together - Testing Set
        xTest = [xt_Test xs_Test];
        xTest =  normalize_feature(xTest,mu,sigma);           
    end
    
   
    %% PCA 
    if inputs.applyPCA
        if strcmp(inputs.feature2Use,'fusion')
            
            % PCA - Training Set
            [coeff_tt,xt_Train,latent_t] = pca(xt_Train);
            [coeff_ss,xs_Train,latent_s] = pca(xs_Train);
           
            nPC_t = find(cumsum(latent_t)./sum(latent_t) <= inputs.variance2KeepPC,1,'last');  % select x% variance
            coeff_t{iCV} = coeff_tt(:,1:nPC_t);
            
            nPC_s = find(cumsum(latent_s)./sum(latent_s) <= inputs.variance2KeepPC,1,'last'); % select x% variance
            coeff_s{iCV} = coeff_ss(:,1:nPC_s);
            
            % project on PC - Training Set
            xt_Train = xt_Train(:,1:nPC_t);
            xs_Train = xs_Train(:,1:nPC_s);
           
            % project on PC - Testing Set
            xt_Test = xt_Test * coeff_t{iCV};
            xs_Test = xs_Test * coeff_s{iCV};
            
        else
            
            % PCA - Training Set
            [coeffa,xTrain,latent]  = pca(xTrain);
            nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
            coeff{iCV} = coeffa(:,1:nPC);
            
            % project on PC - Training Set
            xTrain = xTrain(:,1:nPC);
             
            % project on PC - Testing Set
            xTest = xTest * coeff{iCV};
            
        end
    end
    %% Feature selection
    if ~strcmp(inputs.featureSelection,'none')
        
        if strcmp(inputs.feature2Use,'fusion')
            
            % features selection - Training Set
            [xt_Train,BetaT{iCV},P{iCV}] = features_selection(xt_Train,yTrain{iCV},...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
            
            [xs_Train,BetaS{iCV},P{iCV}] = features_selection(xs_Train,yTrain{iCV},...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
            
            xTrain = [xt_Train xs_Train];
            
            % features selection - Testing Set
            xt_Test = xt_Test(:,BetaT{iCV});
            xs_Test = xs_Test(:,BetaS{iCV});
            
            % put features selected together
            xTest = [xt_Test xs_Test];
        else
            
            % features selection - Training Set
            [xTrain,Beta{iCV},P{iCV}] = features_selection(xTrain,yTrain{iCV},...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
            
            % features selection - Testing Set
            xTest = xTest(:,Beta{iCV});
            
        end
        
    end
    
    
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%% Performance assesment of the classifier %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % training
    map = settings.modules.smr.gau.somunits;
    shared = settings.modules.smr.gau.sharedcov ;
    [centers,covariances] = gauInitialization([xTrain  yTrain{iCV}],map,shared,0);
     
    % training
    for i = 1:size(xTrain, 1)
        [classTrain{i,iCV},posterior_training{i,iCV}] = gauClassifier(centers,covariances,xTrain(i,:));
    end
    % testing
    for i = 1:size(xTest, 1)
        [classTest{i,iCV},posterior_testing{i,iCV} ] = gauClassifier(centers,covariances,xTest(i,:));
    end
    % random classification
    randIndex = randperm(length(yTrain{iCV}));
    yy{iCV} = yTrain{iCV}(randIndex);
    [classRandom{iCV},posterior_random{iCV} ] = gauClassifier(centers,covariances,xTrain(i,:));
    xT{iCV} = xTrain;
    xt{iCV} = xTest;
    %%
    %%%%%%%%%%%%%%%%%%%%%%% Classification on testing dataset over time %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Dataset Preparation - Testing and Training
    xTest_period = cat(3,xt_Test_period, xs_Test_period);
    xTrain_period = cat(3,xt_Train_period, xs_Train_period);
    
    for iTime = 1:nPeriod
        
        if strcmp(inputs.feature2Use,'fusion')
            
            % normalization - Training Set
            feat_ttrain = normalize_feature_over_period(xt_Train_period,iTime,mu_t,sigma_t);
            feat_strain = normalize_feature_over_period(xs_Train_period,iTime,mu_s,sigma_s);
            
            
            % normalization - Testing Set
            feat_ttest = normalize_feature_over_period(xt_Test_period,iTime,mu_t,sigma_t);
            feat_stest = normalize_feature_over_period(xs_Test_period,iTime,mu_s,sigma_s);
            
           
            if inputs.applyPCA
                % apply PCA - Training Set
                feat_ttrain = feat_ttrain * coeff_t{iCV};
                feat_strain = feat_strain * coeff_s{iCV};
                
                % apply PCA - Testing Set
                feat_ttest = feat_ttest * coeff_t{iCV};
                feat_stest = feat_stest * coeff_s{iCV};
            end
             
            % apply feature selection - Training Set
            feat_ttrain = feat_ttrain(:,BetaT{iCV});
            feat_strain = feat_strain(:,BetaS{iCV});
            
            % apply feature selection - Testing Set
            feat_ttest = feat_ttest(:,BetaT{iCV});
            feat_stest = feat_stest(:,BetaS{iCV});
            
            
            % put features together for each period
            feat_test = [feat_ttest feat_stest];
            feat_train = [feat_ttrain feat_strain];
        else
            % normalization - Training Set
            feat_test = normalize_feature_over_period(xTest_period,iTime,mu,sigma);
            
            % normalization - Testing Set
            feat_train = normalize_feature_over_period(xTrain_period,iTime,mu,sigma);
            
            if inputs.applyPCA
                % apply PCA - Training Set
                feat_train = feat_train * coeff{iCV};
                
                % apply PCA - Testing Set
                feat_test = feat_test * coeff{iCV};
            end
              
            % apply feature selection - Training Set
            feat_train = feat_train(:,Beta{iCV});
            
            % apply feature selection - Testing Set
            feat_test = feat_test(:,Beta{iCV});
            
            
        end
        
        %% Classifier ouput  
        % training
      
        %testing
        for i = 1:size(feat_test,1)
            [classTest{i,iCV},POSTERIOR(i,:) ] = gauClassifier(centers,covariances,feat_test(i,:));
        end
        yPredicted{iCV,iTime}  = POSTERIOR(:,2);
        
        for i = 1:size(feat_test,1)
            [classTest{i,iCV},POSTERIOR(i,:)] = gauClassifier(centers,covariances,feat_train(i,:));
        end
        yTrainPredicted{iCV,iTime}  = POSTERIOR(:,2);
    end
end

if strcmp(inputs.feature2Use,'fusion')
    
    paramZscore.mu.temporal = mu_t;
    paramZscore.sigma.temporal = sigma_t;
    
    paramZscore.mu.spectral = mu_s;
    paramZscore.sigma.spectral = sigma_s;
    
    Beta.temporal = BetaT;
    Beta.spectral = BetaS;
    coeff.temporal = coeff_t;
    coeff.spectral = coeff_s;
    BetaCh.temporal = BetaCh;
    BetaCh.spectral = BetaCh;
    
else
    paramZscore.mu = mu;
    paramZscore.sigma = sigma;
   
    
end

%% Output of the functions

% performance results for classifier

% trainStruct = struct('class',classTrain,'posterior',posterior_training,'target',yTrain);
% testingStruct = struct('class',classTest,'posterior',posterior_testing,'target',yTest);
% randomStruct = struct('class',classRandom,'posterior',posterior_random,'target',yTest);
% output = struct('training',trainStruct,'testing',testingStruct,'random',randomStruct);
output = []
% build structure for classifier
classifier = struct('Beta',{Beta},'BetaChannel',{BetaCh},'Pfeature',{P},'paramZscore',paramZscore,'coeff',{coeff},...
    'applyPCA',inputs.applyPCA,'nbFeature2Select',inputs.features2Select,...
    'feature2Use',inputs.feature2Use,...
    'featureSelection',inputs.featureSelection,'training',training,'testing',testing,...
    'classifierType',inputs.classifierType,'DC',{xT},'Y',{yTrain});

end

function feat = normalize_feature_over_period(xPeriod,iTime,mu,sigma)

feat = squeeze(xPeriod(iTime,:,:));
feat = normalize_feature(feat,mu,sigma);

end

function feat = normalize_feature(feat,mu,sigma)

% feat = (feat-ones(size(feat,1),1)*mu)./(ones(size(feat,1),1)*sigma);

sigma(sigma==0) = 1;
feat = bsxfun(@minus,feat, mu);
feat = bsxfun(@rdivide, feat, sigma);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function CVO = find_partition(inputs)

nSample = size(inputs.training.label,2);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(training,testing,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addRequired(funcInputs, 'testing',@isstruct);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'percent2Hold',5/10,@(x) isnumeric(x))
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',[],@isnumeric)
addParameter(funcInputs,'features2Select',[],@isnumeric);
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
addParameter(funcInputs,'channel2Select',[],@isnumeric);
addParameter(funcInputs,'verbose',false,@isnumeric);
expectedType = {'diaglinear','diagquadratic','linear','quadratic'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'spectral','temporal','both','fusion'};
addParameter(funcInputs,'feature2Use',{'both'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs,training,testing, varargin{:});
funcInputs = funcInputs.Results;

if strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankFeatures'};

end

end

