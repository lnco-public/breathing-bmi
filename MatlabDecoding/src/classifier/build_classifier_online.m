function classifier_struct = build_classifier_online(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = BUILD_CLASSIFIER_ONLINE(TRAINING). training is structure
%   where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = APPLY_CLASSIFIER_OFFLINE(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value

%    'featuresSelection'       method use for features selection ('fisherScore','rankFeatures','none')
%                             (default 'none')
%    'features2Select'        number of features you want to keep
%    'feature2Use'            type of features you want to use ('spectral','temporal','both','fusion')
%                            (default 'both')
%    'regularization'         logical, use of sparse LDA if true (default true)
%    'delta'                  L1 regularization parameters for sLDA


%% Parameters

inputs = parse_my_inputs(training,varargin{:});

warning('off','all')
fprintf('\n*************************')
fprintf('\nClassification Process...\n')

% label for training and testing
yTrain = training.label;
yTrain = reshape(yTrain',[],1);

if isempty(training.features.spectral)
    xTrain = training.features.temporal;
else
    xTrain = training.features.spectral;
end
xTrain = change_structure_period(xTrain);

% initialization
coeff_PCA = [];
paramZscore = struct('mu',[],'sigma',[]);
SelectedFeatures = [];

%% Normalization
[xTrain,paramZscore.mu,paramZscore.sigma] = zscore(xTrain);


%% PCA or Feature selection
% PCA
if inputs.applyPCA
    % PCA - Training Set
    [coeffa,xTrain,latent]  = pca(xTrain);
    nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
    coeff_PCA = coeffa(:,1:nPC);
    
    % project on PC
    xTrain = xTrain(:,1:nPC);    
    fprintf('PCA: %d PC kept using %d%% of variance explained\n',nPC,inputs.variance2KeepPC*100)

% Feature selection
else
    if isempty(inputs.features_params)
        if ~strcmp(inputs.featureSelection,'none')
            [xTrain,SelectedFeatures,P] = features_selection(xTrain,yTrain,...
                'nfeature2Select',inputs.nbFeatures2Select,'method',inputs.featureSelection);
        end
        
    else
    % Feature selection
    xTrain = xTrain(:,inputs.features_params);
    disp([num2str(sum(inputs.features_params)) ' selected features for training classifier']);
    SelectedFeatures = inputs.features_params;
    end
end

%% Build model
if strcmp(inputs.classifierType,'SVM')
    Model = fitcsvm(xTrain,yTrain,'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    Model = fitPosterior(Model);
else
    Model = fitcdiscr(xTrain,yTrain,'DiscrimType',inputs.classifierType{1});
end
disp(['Classifier Used : ' inputs.classifierType{1}]);

%% Output
classifier_struct = struct('model',Model,...
    'Beta',SelectedFeatures,...
    'paramZscore',paramZscore,...
    'applyPCA',inputs.applyPCA,...
    'variance2KeepPC',inputs.variance2KeepPC,...
    'coeffPCA',coeff_PCA,...
    'training',training,...
    'DC',xTrain,...
    'Y',yTrain,...
    'classifierType',inputs.classifierType,....
    'kernelType',inputs.kernelType);


end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;
addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs, 'features_params',[],@islogical);
addParameter(funcInputs, 'applyPCA',false,@islogical);
addParameter(funcInputs, 'variance2KeepPC',0.95,@isnumeric);
addParameter(funcInputs,'nbFeatures2Select',[],@isnumeric);
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'featureSelection',{'fisherScore'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'linear','RBF','gaussian','polynomial'};
addParameter(funcInputs,'kernelType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));


parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

end

