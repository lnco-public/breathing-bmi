function [ errorTrain,errorTest] = training_classifier_hyperparameters(features,labels,varargin)

%TRAINING_CLASSIFIER extract features that will be used later by the
%classifier
%   YPREDICTED = TRAINING_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TRAINING_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none')
%                              (default 'fisherScore')
%
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(features,labels,varargin{:});

% choose the type of cross validation we want
CVO = find_partition(inputs);

%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    fprintf('Fold #%d\n', iCV);
    
    % distribution of samples for training and testing set (temporal and spectral)
    if strcmp(inputs.cvType{1},'LeaveOneRun')
        iTrain = CVO.training{iCV};
        iTest = CVO.test{iCV};
    else
        iTrain = CVO.training(iCV);
        iTest = CVO.test(iCV);
    end
    
    % label for training and testing
    yTrain =labels(iTrain);
    yTest =labels(iTest);
    
    
    % features for training and testing
    xTrain = features(iTrain,:);
    xTest= features(iTest,:);
    
    
    %% Normalization
    % normalize features together - Training Set
    [xTrain,mu,sigma] = zscore(xTrain);
    
    % normalize features together - Testing Set
    xTest = normalize_feature(xTest,mu,sigma);
    
    
    
    coeff  = pca(xTrain);
    
    
    % Optimization hyperparameters
    for iFeature = 1:size(coeff,2)
        
        %% PCA
        if inputs.applyPCA && strcmp(inputs.featureSelection,'none')
            % PCA - Training Set
            xT = xTrain * coeff(:,1:iFeature);
            % project on PC - Testing Set
            xt = xTest * coeff(:,1:iFeature);
        elseif inputs.applyPCA && ~strcmp(inputs.featureSelection,'none')
            xT = xTrain*coeff;
            xt = xTest*coeff;  
        else
            xT = xTrain;
            xt = xTest;
        end
        
        
        %% Feature selection
        if ~strcmp(inputs.featureSelection,'none')
            % features selection - Training Set
            [xT,Beta] = features_selection(xT,yTrain,...
                'nfeature2Select',iFeature,'method',inputs.featureSelection);
            % features selection - Testing Set
            xt = xt(:,Beta);
        end
        
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%% Performance assesment of the classifier %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % training
        classTrain = classify(xT, xT, yTrain, 'diaglinear','empirical');
        errorTrain(iCV,iFeature) = sum(classTrain ~= yTrain)/length(yTrain);
        
        % testing
        classTest = classify(xt, xT, yTrain,'diaglinear','empirical');
        errorTest(iCV,iFeature) = sum(classTest ~= yTest)/length(yTest);
        
    end
    
    
    
end


end

function feat = normalize_feature(feat,mu,sigma)

n = size(feat,1);
feat = (feat-ones(n,1)*mu)./(ones(n,1)*sigma);

end


function CVO = find_partition(inputs)

nSample = size(inputs.labels,1);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(training,labels,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isnumeric);
addRequired(funcInputs, 'labels',@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',@isnumeric)
parse(funcInputs,training,labels, varargin{:});
funcInputs = funcInputs.Results;

if strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankFeatures'};
    
end

end

