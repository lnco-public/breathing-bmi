 function [yPredicted,yTrainPredicted,classifier] = apply_classifier_ICA(training,testing,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = TEST_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TEST_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type    
%                              (default 1/10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none') 
%                              (default 'fisherScore')
% 
%    'regularization'          boolean to use or not use regularization(default false)
%    'features2Select'         number of features to select (default 10)
%    'delta'                   regularization parameter for L1-regularization (default 1e-6)
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(training,testing,varargin{:});
nPeriod = size(testing.label,1);
% choose the type of cross validation we want
CVO = find_partition(inputs);

% sLDA parameters
delta = inputs.delta;%1e-3; % l2-norm constraint
stop  = -1*(inputs.features2Select); % request non-zero variables


warning('off','all')
fprintf('\n*************************')
fprintf('\nClassification Process...\n')
orderTest = [];
coeff = [];coeff_t = [];coeff_s = [];
BetaT = [];
BetaS = [];
%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    
    
    
    
    
    
    
    
    
    
    
    fprintf('Fold #%d\n', iCV);
    
    % distribution of samples for training and testing set (temporal and spectral)
    if strcmp(inputs.cvType{1},'LeaveOneRun')
        iTrain = CVO.training{iCV};
        iTest = CVO.test{iCV};
    else
        iTrain = CVO.training(iCV);
        iTest = CVO.test(iCV);
    end
    
    ii = find(iTest == 1);
    orderTest = [orderTest; ii];
    
    % label for training
    Y = training.label(:,iTrain);
    Y = reshape(Y',[],1);
    
    % prepare label for sLDA use
    yTrain = prepare_label_sLDA(Y);
    

    %%
    
    if  ~strcmp(inputs.feature2Use,'spectral')
        xt_Train = training.features.temporal(:,iTrain,:);
        xt_Train = change_structure_period(xt_Train);
        xt_Test = testing.features.temporal(:,iTest,:);
        xt_Train_as_Test = testing.features.temporal(:,iTrain,:);
        
    else
        xt_Train = [];
        xt_Test = [];
        xt_Train_as_Test = [];
    end
    
    if ~strcmp(inputs.feature2Use,'temporal')
        xs_Train = training.features.spectral(:,iTrain,:);
        xs_Train = change_structure_period(xs_Train);
        xs_Test = testing.features.spectral(:,iTest,:);
        xs_Train_as_Test = testing.features.spectral(:,iTrain,:);
    else
        xs_Train = [];
        xs_Test = [];
        xs_Train_as_Test = [];
    end
    
    %% Normalization
    if strcmp(inputs.feature2Use,'fusion')
        
        %normalize features independently
        [xt_Train,mu_t,sigma_t] = zscore(xt_Train);
        [xs_Train,mu_s,sigma_s] = zscore(xs_Train);
        
    else
        % normalize features together
        xTrain = [xt_Train xs_Train];
        [xTrain,mu,sigma] = zscore(xTrain);
        
    end
    
    %% IC component selection // channel selection
    if ~strcmp(inputs.channelSelection,'none')
         if strcmp(inputs.feature2Use,'fusion')
            [xt_Train,BetaT{iCV}] = channel_selection(xt_Train,Y,...
                'nchan2Select',inputs.channel2Select,'method',inputs.featureSelection);
            
            
            [xs_Train,BetaS{iCV}] = channel_selection(xs_Train,Y,...
                'nchan2Select',inputs.channel2Select,'method',inputs.featureSelection);     
            
            xTrain = [xt_Train xs_Train];
        else
            [xTrain,Beta{iCV}] = channel_selection(xTrain,Y,...
                'nchan2Select',inputs.channel2Select,'method',inputs.featureSelection);
            
         end
    end
    
    
    
    %% PCA 
    if inputs.applyPCA
        if strcmp(inputs.feature2Use,'fusion')
            [coeff_tt,xt_Train,latent_t] = pca(xt_Train);
            [coeff_ss,xs_Train,latent_s] = pca(xs_Train);
            % select 95% variance
            nPC_t = find(cumsum(latent_t)./sum(latent_t) >= 0.95,1,'first');
            xt_Train = xt_Train(:,1:nPC_t);
            coeff_t{iCV} = coeff_tt(:,1:nPC_t);
            
            nPC_s = find(cumsum(latent_s)./sum(latent_s) >= 0.95,1,'first');
            xs_Train = xs_Train(:,1:nPC_s);
            coeff_s{iCV} = coeff_ss(:,1:nPC_s);
            
        else
            [coeffa,xTrain,latent]  = pca(xTrain);
            nPC = find(cumsum(latent)./sum(latent) >= 0.95,1,'first');
            xTrain = xTrain(:,1:nPC);
            coeff{iCV} = coeffa(:,1:nPC);   
        end
    end
    %% Feature selection
    if ~strcmp(inputs.featureSelection,'none')
        
        if strcmp(inputs.feature2Use,'fusion')
            [xt_Train,BetaT{iCV}] = features_selection(xt_Train,Y,...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
            
            
            [xs_Train,BetaS{iCV}] = features_selection(xs_Train,Y,...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);     
            
            xTrain = [xt_Train xs_Train];
        else
            [xTrain,Beta{iCV}] = features_selection(xTrain,Y,...
                'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
            
        end
        
    end
    
    
    %% Regularization
    if inputs.regularization
        if strcmp(inputs.feature2Use,'fusion')
            % train sLDA with temporal features
            [BetaT{iCV}, ~] = slda(xt_Train, yTrain,delta,stop,1);
            
            % train sLDA with spectral features
            [BetaS{iCV}, ~] = slda(xs_Train, yTrain,delta,stop,1);
            
            % project data
            DC_ttrain = xt_Train*BetaT{iCV};
            DC_strain = xs_Train*BetaS{iCV};
            xTrain = [DC_ttrain DC_strain];
        else
            
            % train SLDA
            Beta{iCV} = slda(xTrain, yTrain, delta, stop,1);
            xTrain = xTrain*Beta{iCV};
            
        end
        
    end
   
    %% Testing dataset preparation
    xTest = cat(3,xt_Test, xs_Test);
    xTrain_as_Test = cat(3,xt_Train_as_Test, xs_Train_as_Test);
    
     
    %% Classification on testing dataset 
    for iTime = 1:nPeriod
        
        if strcmp(inputs.feature2Use,'fusion')
            
            % testing
            feat_ttest = normalize_feature(xt_Test,iTime,mu_t,sigma_t);
            feat_stest = normalize_feature(xs_Test,iTime,mu_s,sigma_s);
           
            % training
            feat_ttrain = normalize_feature(xt_Train_as_Test,iTime,mu_t,sigma_t);
            feat_strain = normalize_feature(xs_Train_as_Test,iTime,mu_s,sigma_s);
           
            
            if inputs.applyPCA
                % testing
                feat_ttest = feat_ttest * coeff_t{iCV};
                feat_stest = feat_stest * coeff_s{iCV};
 
                % training
                feat_ttrain = feat_ttrain * coeff_t{iCV};
                feat_strain = feat_strain * coeff_s{iCV};
                
            end
            
            if inputs.regularization
                feat_ttest = feat_ttest*BetaT{iCV};
                feat_stest = feat_stest*BetaS{iCV};
                feat_ttrain = feat_ttrain*BetaT{iCV};
                feat_strain = feat_strain*BetaS{iCV};
            else
                feat_ttest = feat_ttest(:,BetaT{iCV});
                feat_stest = feat_stest(:,BetaS{iCV});
                feat_ttrain = feat_ttrain(:,BetaT{iCV});
                feat_strain = feat_strain(:,BetaS{iCV});    
            end
            
            % put features togehter for each period
            feat_test = [feat_ttest feat_stest];
            feat_train = [feat_ttrain feat_strain];
        else
            
            feat_test = normalize_feature(xTest,iTime,mu,sigma);
            feat_train = normalize_feature(xTrain_as_Test,iTime,mu,sigma);
            
            
            if inputs.applyPCA 
                % training     
                feat_train = feat_train * coeff{iCV};
           
                % testing
                feat_test = feat_test * coeff{iCV}; 
            end
            
 
            if inputs.regularization
                feat_test = feat_test*Beta{iCV};
                feat_train = feat_train*Beta{iCV};
            else
                feat_test = feat_test(:,Beta{iCV});
                feat_train = feat_train(:,Beta{iCV});
            end
        end
        
        %% Classifier ouput
        
        
        % training
        [~,~,POSTERIOR] = classify(feat_train, xTrain, Y, 'linear','empirical');
        yTrainPredicted{iCV,iTime} = POSTERIOR(:,2);
        
        %testing
        [~,~,POSTERIOR] = classify(feat_test, xTrain, Y,'linear','empirical');
        yPredicted{iCV,iTime}  = POSTERIOR(:,2);
        
        
    end
end

if strcmp(inputs.feature2Use,'fusion')
    
    paramZscore.mu.temporal = mu_t;
    paramZscore.sigma.temporal = sigma_t;
    
    paramZscore.mu.spectral = mu_s;
    paramZscore.sigma.spectral = sigma_s;
    
    Beta.temporal = BetaT;
    Beta.spectral = BetaS;
    coeff.temporal = coeff_t;
    coeff.spectral = coeff_s;
    
else
    paramZscore.mu = mu;
    paramZscore.sigma = sigma;
    
end
 
% build structure for classifier
classifier = struct('Beta',Beta,'paramZscore',paramZscore,'coeff',coeff,...
    'applyPCA',inputs.applyPCA,'nbFeature2Select',inputs.features2Select,...
    'regularization',inputs.regularization,'feature2Use',inputs.feature2Use,...
    'featureSelection',inputs.featureSelection,'training',training,'testing',testing);

end

function feat = normalize_feature(xPeriod,iTime,mu,sigma)

feat = squeeze(xPeriod(iTime,:,:));
n = size(feat,1);
feat = (feat-ones(n,1)*mu)./(ones(n,1)*sigma);

end



function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function CVO = find_partition(inputs)

nSample = size(inputs.training.label,2);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(training,testing,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addRequired(funcInputs, 'testing',@isstruct);
addParameter(funcInputs, 'regularization',false,@islogical);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'percent2Hold',5/10,@(x) isnumeric(x))
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'channelSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',@isnumeric)
addParameter(funcInputs,'features2Select',[],@isnumeric);
addParameter(funcInputs,'channel2Select',[],@isnumeric);
expectedType = {'spectral','temporal','both','fusion'};
addParameter(funcInputs,'feature2Use',{'both'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs,training,testing, varargin{:});
funcInputs = funcInputs.Results;

if ~funcInputs.regularization && strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankFeatures'};

end

end
