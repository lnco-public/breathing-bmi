function output = testing_classifier(features,labels,classifier)

%% only the period for training

%% Normalization

% normalize features together
features = normalize_feature(features,classifier.paramZscore.mu,classifier.paramZscore.sigma);


%% PCA
if classifier.applyPCA
features  = features * classifier.coeff;
end

%% Feature selection
% if ~isempty(classifier.nbFeatures2Select)

% if ~isempty(classifier.nbFeatures2Select)
features  = features (:,classifier.Beta);
% end

%%
%%%%%%%%%%%%%%%%%%%%%%% Performance assesment of the classifier %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% testing
[class_output,~,posterior] = classify(features, classifier.DC,classifier.Y,classifier.classifierType,'empirical');


%% Output of the functions

% performance results for classifier
output= struct('class',class_output,'posterior',posterior,'target',labels);

end

function feat = normalize_feature(feat,mu,sigma)

n = size(feat,1);
feat = (feat-ones(n,1)*mu)./(ones(n,1)*sigma);

end


