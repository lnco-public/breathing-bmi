function latency = get_latency_from_prob(prob,time,threshold)


if nargin == 3
    threshold = 0.5;
end
    
if iscell(prob)
    prob = cell2mat(prob);
end

nTrial = size(prob,1);


latency = [];
for iTrial = 1:nTrial
    disp(iTrial)
    prob4thisTrial = prob(iTrial,:);
    
    index_decoding  = find(prob4thisTrial > threshold);
    
         
    if ~isempty(index_decoding)
        index_decoding = index_decoding(1);
        thisLatency = time(index_decoding);
    end
        
    
         
    latency = [latency; thisLatency];
    
end

end

