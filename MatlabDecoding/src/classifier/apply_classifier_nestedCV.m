function [prob,output,classifier,smoothed_probabilities] = apply_classifier_nestedCV(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier
%   YPREDICTED = TEST_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TEST_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type
%                              (default 1/10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none')
%                              (default 'fisherScore')
%
%    'regularization'          boolean to use or not use regularization(default false)
%    'features2Select'         number of features to select (default 10)
%    'delta'                   regularization parameter for L1-regularization (default 1e-6)
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(training,varargin{:});

% choose the type of cross validation we want
CVO = find_partition(inputs);


warning('off','all')
global verbose

if verbose
    fprintf('\n*************************')
    fprintf('\nClassification Process...\n')
end

% Initialize variable 
Power_feature = [];
coeff_PCA = [];
xTrainModel ={}; xTestModel = {};
smoothed_probabilities = {};
prob = [];
bestHyperParameter = [];

%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    if verbose
        fprintf('Fold #%d\n', iCV);
    end
    
    %% Prepare features and labels
    [yTrain{iCV},yTest{iCV},Ytrain,Ytest,yTrain_Trials,yTest_Trials] = prepare_labels_for_classification(training,CVO,inputs,iCV);
    [xTrain,xTest,xTrain_period,xTest_period,xTrain_trials] = prepare_features_for_classification(training,CVO,inputs,iCV);
    
    %% Normalize features
    [xTrain,paramZscore.mu,paramZscore.sigma] = zscore(xTrain);
    xTest =  normalize_feature(xTest,paramZscore);
 
    %% PCA
    if inputs.applyPCA
            [coeff_PCA{iCV},inputs] = apply_PCA(xTrain,xTest,inputs);
    else
        coeff_PCA{iCV} = [];
    end
    
    %% Regularization
    if inputs.regularization
        [xTrain,xTest,Beta{iCV}] = apply_regularization(xTrain,Ytrain,xTest);     
    end
    
     %% Feature selection
    if ~strcmp(inputs.featureSelection,'none')
       [xTrain,xTest,Beta{iCV},Power_feature{iCV}] = apply_feature_selection(xTrain,xTest,yTrain{iCV},inputs);
    else
        Beta{iCV} = ones(size((xTrain),2),1);
    end
    
    %% Classification
    Model = build_model(xTrain,yTrain{iCV},inputs);
    [classTrain{iCV},posterior_training{iCV}] = predict(Model,xTrain);
    [classTest{iCV},posterior_testing{iCV}] = predict(Model,xTest);
    
    % random chance level permutation-based
    randIndex = randperm(length(yTrain{iCV}));
    yTrainRandom{iCV} = yTrain{iCV}(randIndex);
    Model = build_model(xTrain,yTrainRandom{iCV},inputs);
    
    classRandom{iCV} = [];
    posterior_random{iCV} = [];
    yRandom{iCV} = [];
    
    randIndex = randperm(length(yTest{iCV}));
    yRandom{iCV} = yTest{iCV}(randIndex);
    [cr,pr] = predict(Model,xTest);
    classRandom{iCV} =  cr;
    posterior_random{iCV} =  pr;
    
    
    xTrainModel{iCV} = xTrain;
    xTestModel{iCV} = xTest;
    
    
      
    %% Pseudo-online classification
    if ~isempty(inputs.pseudoOnline)
          [bestHyperParameter{iCV},error] = perform_nested_innerFold(xTrain_trials,yTrain_Trials,xTrain_period,inputs);
        nPeriod = size(inputs.pseudoOnline.label,1);
        for iTime = 1:nPeriod
            yPredicted{iCV,iTime} = apply_pseudoOnline(xTest_period,iTime,paramZscore,coeff_PCA{iCV},Beta{iCV},Model,inputs);    
        end
    end
    
    
    
    %% Smoothin probabilities
%      smoothed_probabilities{iCV}=  movingmean(cell2mat(yPredicted),bestHyperParameter{iCV},2);
    
end

   
%% Output of the functions

% performance results for classifier
trainStruct = struct('class',classTrain,'posterior',posterior_training,'target',yTrain);
testingStruct = struct('class',classTest,'posterior',posterior_testing,'target',yTest);
randomStruct = struct('class',classRandom,'posterior',posterior_random,'target',yRandom);
output = struct('training',trainStruct,'testing',testingStruct,'random',randomStruct);

% build structure for classifier
classifier = struct('Beta',{Beta},...
    'Pfeature',{Power_feature},...
    'paramZscore',paramZscore,...
    'coeff',{coeff_PCA},...
    'applyPCA',inputs.applyPCA,...
    'nbFeature2Select',inputs.nbFeature2Select,...
    'regularization',inputs.regularization,...
    'featureSelection',inputs.featureSelection,...
    'training',training,...
    'pseudoOnline',inputs.pseudoOnline,...
    'classifierType',inputs.classifierType,...
    'DC',{xTrainModel},...
    'Y',{yTrain},...
    'CVO',CVO,...
    'hyper',bestHyperParameter);

% pseudo-online probabilities
if ~isempty(inputs.pseudoOnline)
    prob.value = yPredicted;
    prob.time = inputs.pseudoOnline.time;
end
end


function [bestHyperparameter,error_mean] = perform_nested_innerFold(xTrain_trials,yTrain_Trials,xTrain_period,inputs)


% Inner cross validation
nTrial = size(xTrain_period,2);
CVO_inner = cvpartition(nTrial,'KFold',10);

nPeriod = size(inputs.pseudoOnline.label,1);

smoothing_parameters = 2:1:25;
for iiCV = 1:CVO_inner.NumTestSets

    %% Prepare features and labels
    xTrain_inner_period = xTrain_period(:,CVO_inner.training(iiCV),:);
    xTest_inner_period = xTrain_period(:,CVO_inner.test(iiCV),:);
    timeTraining = cell2mat(inputs.training.time);
    xTrain_inner = xTrain_trials(:,CVO_inner.training(iiCV),:);
    xTrain_inner = change_structure_period(xTrain_inner);
    yTrain_inner = reshape(yTrain_Trials(:,CVO_inner.training(iiCV))',[],1);
    xTest_inner  = xTrain_trials(:,CVO_inner.training(iiCV),:);
    xTest_inner = change_structure_period(xTest_inner);
    yTest_inner = reshape(yTrain_Trials(:,CVO_inner.test(iiCV))',[],1);
    yPredicted = [];    
    timePseudo_online = inputs.pseudoOnline.time;
    
    timeOfInterest4Optimization = [];
    for iTime = 1:length(timeTraining)
        timeOfInterest4Optimization =  [timeOfInterest4Optimization find(timePseudo_online > timeTraining(iTime),1,'first')];
        
    end
    
    %% Normalize features
    [xTrain_inner,paramZscore.mu,paramZscore.sigma] = zscore(xTrain_inner);
    
    %% PCA
    if inputs.applyPCA
        [coeff_PCA,inputs] = apply_PCA(xTrain_inner,xTest_inner,inputs);
    else
        coeff_PCA = [];
    end
    
    %% Regularization
    %     if inputs.regularization
    %         [xTrain,xTest,Beta{iCV}] = apply_regularization(xTrain_inner,Ytrain,xTest);
    %     end
    
    % Feature selection
    if ~strcmp(inputs.featureSelection,'none')
        [xTrain_inner,xTest_inner,Beta,Power_feature] = apply_feature_selection(xTrain_inner,xTest_inner,yTrain_inner,inputs);
    else
        Beta = ones(size((xTrain_inner),2),1);
    end
    
    % train model
    Model = build_model(xTrain_inner,yTrain_inner,inputs);
    
    
    % pseudo Online
    for iTime = 1:nPeriod
        yPredicted(:,iTime) = apply_pseudoOnline(xTest_inner_period,iTime,paramZscore,coeff_PCA,Beta,Model,inputs);
    end
    
    
    % smoothing
    for iHyper = 1:length(smoothing_parameters)
        smoothed_probabilities =  movingmean(yPredicted,smoothing_parameters(iHyper),2);
        label_output = smoothed_probabilities > 0.5;
        label_output = reshape(label_output(:,timeOfInterest4Optimization)',[],1);
        error(iiCV,iHyper) = (sum(label_output ~=  yTest_inner))/length(yTest_inner);
    end
end
    
    %% Take decision average over inner fold on test 
    error_mean = mean(error,1);
    bestHyperparameter = find(error_mean == min(error_mean),1,'first');
    bestHyperparameter = smoothing_parameters(bestHyperparameter);
       
end



function Model = build_model(xTrain,yTrain,inputs)


if strcmp(inputs.classifierType,'SVM')
    Model = fitcsvm(xTrain,yTrain,'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    Model = fitPosterior(Model);
else
    Model = fitcdiscr(xTrain,yTrain,'DiscrimType',inputs.classifierType{1});
end


end

function yhat = apply_pseudoOnline(xTest_period,iTime,paramZscore,coeff_PCA,Beta,Model,inputs)

    % normalization - Testing Set over time
    feat_test = normalize_feature_over_period(xTest_period,iTime,paramZscore);
    
    if inputs.applyPCA
        feat_test = feat_test * coeff_PCA;
    end
    
    if inputs.regularization
        feat_test = feat_test*Beta;
    else
        if ~strcmp(inputs.featureSelection,'none')
            feat_test = feat_test(:,Beta);
        end
    end
    
    % Classifier ouput
    [~,POSTERIOR ] = predict(Model,feat_test);
    yhat  = POSTERIOR(:,2);
    
end





function [coeff_PCA,inputs] = apply_PCA(xTrain,xTest,inputs)

% PCA - Training Set
[coeff,~,latent]  = pca(xTrain);
nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
coeff_PCA = coeff(:,1:nPC);

% project on PC
xTrain = xTrain * coeff_PCA;
xTest = xTest * coeff_PCA;

if nPC < inputs.nbFeature2Select && ~strcmp(inputs.featureSelection,'none')
    inputs.nbFeature2Select = nPC;
    fprintf('not enough principal component, decrease features to select to %d \n',inputs.nbFeature2Select )
    
end
end


function [xTrain,xTest,Beta] = apply_regularization(xTrain,Ytrain,xTest)

% train SLDA with all features
Beta = slda(xTrain,  Ytrain, inputs.delta,-1*(inputs.nbFeature2Select),1);

% project data
xTrain = xTrain*Beta;
xTest = xTest*Beta;

end

function [xTrain,xTest,Beta,Power_feature] = apply_feature_selection(xTrain,xTest,yTrain,inputs)

% features selection - Training Set
[xTrain,Beta,Power_feature] = features_selection(xTrain,yTrain,...
    'nfeature2Select',inputs.nbFeature2Select,...
    'method',inputs.featureSelection);

% features selection - Testing Set
xTest = xTest(:,Beta);

end


function [yTrain,yTest,Ytrain,Ytest,yTrain_Trials,yTest_Trials] = prepare_labels_for_classification(training,CVO,inputs,iCV)
% Distribution of samples for training and testing set (temporal and spectral)
Ytrain = [];
Ytest=  [];
if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end

% Labeling - training and testing
yTrain_Trials= training.label(:,iTrain);
yTrain = reshape(yTrain_Trials',[],1);
yTest_Trials = training.label(:,iTest);
yTest = reshape(yTest_Trials',[],1);

% prepare label for sLDA use
if inputs.regularization
    Ytrain = prepare_label_sLDA(yTrain);
    Ytest = prepare_label_sLDA(yTest);
end

end


function [xTrain,xTest,xTrain_period,xTest_period,xTrain_trials] = prepare_features_for_classification(training,CVO,inputs,iCV)

if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end

if ~isempty(training.features.spectral)
    xTrain_trials = training.features.spectral(:,iTrain,:);
    xTrain = change_structure_period(xTrain_trials);
    xTest = training.features.spectral(:,iTest,:);
    xTest = change_structure_period(xTest);
    
    if ~isempty(inputs.pseudoOnline)
        xTest_period = inputs.pseudoOnline.features.spectral(:,iTest,:);
        xTrain_period = inputs.pseudoOnline.features.spectral(:,iTrain,:);
    end
else
    xTrain_trials = training.features.temporal(:,iTrain,:);
    xTrain = change_structure_period(xTrain_trials);
    xTest = squeeze(training.features.temporal(:,iTest,:));
    xTest = change_structure_period(xTest);
    
    if ~isempty(inputs.pseudoOnline)
        xTest_period = inputs.pseudoOnline.features.temporal(:,iTest,:);
        xTrain_period = inputs.pseudoOnline.features.temporal(:,iTrain,:);
    end
end

end

function feat = normalize_feature_over_period(xPeriod,iTime,paramZscore)

feat = squeeze(xPeriod(iTime,:,:));
if iscolumn(feat)
   feat = feat'; 
end
feat = normalize_feature(feat,paramZscore);

end

function feat = normalize_feature(feat,paramZscore)

paramZscore.sigma(paramZscore.sigma==0) = 1;
feat = bsxfun(@minus,feat, paramZscore.mu);
feat = bsxfun(@rdivide, feat, paramZscore.sigma);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function CVO = find_partition(inputs)

nSample = size(inputs.training.label,2);

if strcmp(inputs.cvType{1},'LeaveOneOut')
    CVO = cvpartition(nSample,'LeaveOut',1);
elseif strcmp(inputs.cvType{1},'KFold')
    CVO = cvpartition(nSample,'KFold',inputs.nFold);
elseif strcmp(inputs.cvType{1},'HoldOut')
    CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    
elseif strcmp(inputs.cvType{1},'LeaveOneRun')
    CVO.NumTestSets = max(inputs.index4LeaveOneRun);
    CVO.training = {};
    CVO.test = {};
    for ii = 1:CVO.NumTestSets
        CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
        CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
    end
end

end




function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs, 'pseudoOnline',[],@isstruct);
addParameter(funcInputs, 'regularization',false,@islogical);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'percent2Hold',5/10,@(x) isnumeric(x))
expectedType = {'rankfeatures','corr','fisher','relief','infgain','none'};
addParameter(funcInputs, 'featureSelection',{'fisher'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'index4LeaveOneRun',[],@isnumeric)
addParameter(funcInputs,'nbFeature2Select',[],@isnumeric);
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
expectedType = {'linear','RBF','gaussian','polynomial'};
addParameter(funcInputs,'kernelType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));


parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

if funcInputs.regularization
    funcInputs.featureSelection = {'none'};
end

end
