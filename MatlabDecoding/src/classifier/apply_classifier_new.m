function [prob,output,classifier] = apply_classifier_new(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier
%   YPREDICTED = TEST_CLASSIFIER(TRAINING,TESTING). training and testing
%   are structures where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = TEST_CLASSIFIER(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%    'condClassification'      asynchronous/synchronous analysis (default synchronous)
%    'cvType'                  type of crossvalidation used in the
%                              classification('KFold','LeaveOneOut','HoldOut',LeaveOneRun)
%                              (default KFold)
%    'nFold'                   number of fold used in Kfold CV type (default 10)
%    'percent2Hold'            percentage to keep for testing in Hold out CV type
%                              (default 1/10)
%    'featureSelection'        method use for feature selection ('fisherScore','rankFeatures','none')
%                              (default 'fisherScore')
%
%    'regularization'          boolean to use or not use regularization(default false)
%    'features2Select'         number of features to select (default 10)
%    'delta'                   regularization parameter for L1-regularization (default 1e-6)
%    'feature2Use'             type of features used for classification ('spectral','temporal','both','fusion')
%                                 (default 'both')
%    'index4LeaveOneRun'       for LeaveOneRun cvType gives specific index
%                              for training and testing run-dependent



inputs = parse_my_inputs(training,varargin{:});

% choose the type of cross validation we want
CVO = find_partition(inputs);

Ctrain = {};
warning('off','all')
global verbose

if verbose
    fprintf('\n*************************')
    fprintf('\nClassification Process...\n')
end

% Initialize variable
Power_feature = [];
coeff_PCA = {};
xTrainModel ={}; xTestModel = {};
prob = [];
bestHyperparameter = {};
error_mean = [];
smoothed_probabilities = {};

%% Cross validation
for iCV = 1:CVO.NumTestSets
    
    if verbose
        fprintf('Fold #%d\n', iCV);
    end
    %% Prepare features and labels
    [yTrain{iCV},yTest{iCV},Ytrain,Ytest,yTrain_Trials,yTest_Trials] = prepare_labels_for_classification(training,CVO,inputs,iCV);
    [xTrain,xTest,xTrain_period,xTest_period,xTrain_trials,Ctrain{iCV}] = prepare_features_for_classification(training,CVO,inputs,iCV,yTrain{iCV});
    
    if isempty(inputs.model2Use)
        
        
        if inputs.nestedCV
            [bestHyperparameter{iCV},error_mean] = perform_nested_innerFold(xTrain_trials,yTrain_Trials,inputs);
            inputs.nbFeature2Select = bestHyperparameter{iCV};
        end
        
        
        % Normalize features
        if inputs.normalizeFeature
            [xTrain,paramZscore{iCV}.mu,paramZscore{iCV}.sigma] = zscore(xTrain);
            xTest =  normalize_feature(xTest,paramZscore{iCV});
        end
        
        %% PCA
        if inputs.applyPCA
            [coeff_PCA{iCV},xTrain,xTest,inputs] = apply_PCA(xTrain,xTest,inputs,iCV);
        else
            coeff_PCA{iCV} = [];
        end
        
        %% Feature selection
        if ~strcmp(inputs.featureSelection,'none') && ~strcmp(inputs.classifierType,'TreeBagger')
            [xTrain,xTest,Beta{iCV},Power_feature{iCV}] = apply_feature_selection(xTrain,xTest,yTrain{iCV},inputs);
        else
            Beta{iCV} = ones(size((xTrain),2),1);
        end
        
        %% Regularization
        if inputs.regularization
            [xTrain,xTest,Beta{iCV}] = apply_regularization(xTrain,Ytrain,xTest,inputs);
        end
        
        %% Build Model
        Model{iCV} = build_model(xTrain,yTrain{iCV},inputs);
        
    else
        [yTrain{iCV},~,Ytrain,~,yTrain_Trials,~] = prepare_labels_for_classification(inputs.model2Use.training,CVO,inputs,iCV);
        [xTrain,~,xTrain_period,~,xTrain_trials] = prepare_features_for_classification(inputs.model2Use.training,CVO,inputs,iCV,yTrain_Trials);
        %% Check with already built model
        paramZscore{iCV} =  inputs.model2Use.paramZscore{iCV};
        xTrain =  normalize_feature(xTrain,paramZscore{iCV});
        xTest =  normalize_feature(xTest,paramZscore{iCV});
        %% PCA
        if inputs.model2Use.applyPCA
            coeff_PCA{iCV} = inputs.model2Use.coeff{iCV};
            % project on PC
            xTrain = xTrain * coeff_PCA{iCV};
            xTest = xTest * coeff_PCA{iCV};
        else
            coeff_PCA{iCV} = [];
        end
        
        %% Feature selection
        if ~strcmp(inputs.model2Use.featureSelection,'none') && ~strcmp(inputs.model2Use.classifierType,'TreeBagger')
            Beta{iCV}  =  inputs.model2Use.Beta{iCV};
            Power_feature{iCV}  =  inputs.model2Use.Pfeature{iCV};
            % features selection - Testing Set
            xTrain = xTrain(:,Beta{iCV});
            xTest = xTest(:,Beta{iCV});
        else
            Beta{iCV} = ones(size((xTrain),2),1);
        end
        
        %% Regularization
        if inputs.model2Use.regularization
            % project data
            Beta{iCV}  =  inputs.model2Use.Beta{iCV};
            xTrain = xTrain*Beta{iCV};
            xTest = xTest*Beta{iCV};
        end
        
        %% Classification
        Model{iCV} =  inputs.model2Use.Model{iCV};
    end
    
    
    [classTrain{iCV},posterior_training{iCV}] = predict(Model{iCV},xTrain);
    [classTest{iCV},posterior_testing{iCV}] = predict(Model{iCV},xTest);
    
    
    xTrainModel{iCV} = xTrain;
    xTestModel{iCV} = xTest;
    
    % Build random model
    randIndex = randperm(length(yTrain{iCV}));
    yTrainRandomized{iCV} = yTrain{iCV}(randIndex);
    ModelRandom = build_model(xTrain,yTrainRandomized{iCV},inputs);
    [classRandom{iCV},posterior_random{iCV}] = predict(ModelRandom,xTest);
    
    % Pseudo-online classification
    if ~isempty(inputs.pseudoOnline)
        % Hyperparameter estimation
        
        nPeriod = size(inputs.pseudoOnline.label,1);
        for iTime = 1:nPeriod
            yPredicted{iCV,iTime} = apply_pseudoOnline(xTest_period,iTime,paramZscore{iCV},coeff_PCA{iCV},Beta{iCV},Model{iCV},xTrain,inputs);
        end
    end
end


%% Output of the functions

% performance results for classifier
trainStruct = struct('class',classTrain,'posterior',posterior_training,'target',yTrain);
testingStruct = struct('class',classTest,'posterior',posterior_testing,'target',yTest);
randomStruct = struct('class',classRandom,'posterior',posterior_random,'target',yTest);
output = struct('training',trainStruct,'testing',testingStruct,'random',randomStruct);


% build structure for classifier
classifier = struct('Beta',{Beta},...
    'Pfeature',{Power_feature},...
    'paramZscore',{paramZscore},...
    'coeff',{coeff_PCA},...
    'applyPCA',inputs.applyPCA,...
    'nbFeature2Select',inputs.nbFeature2Select,...
    'regularization',inputs.regularization,...
    'featureSelection',inputs.featureSelection,...
    'training',training,...
    'pseudoOnline',inputs.pseudoOnline,...
    'classifierType',inputs.classifierType,...
    'DC',{xTrainModel},...
    'Y',{yTrain},...
    'CVO',CVO,...
    'bestHyperparameter',{bestHyperparameter},...
    'Model',{Model},...
    'Ctrain',{Ctrain},...
    'error_nested',error_mean);

% pseudo-online probabilities
if ~isempty(inputs.pseudoOnline)
    prob.value = yPredicted;
    prob.time = inputs.pseudoOnline.time;
    prob.smoothed = smoothed_probabilities';
end
end

%% NESTED CV
function [bestHyperparameter,error_mean] = perform_nested_innerFold(xTrain_trials,yTrain_Trials,inputs)

% Inner cross validation
nTrial = size(xTrain_trials,2);
CVO_inner = cvpartition(nTrial,'KFold',10);
error = [];
for iiCV = 1:CVO_inner.NumTestSets
    
    %% Prepare features and labels
    xTrain_inner = xTrain_trials(:,CVO_inner.training(iiCV),:);
    xTrain_inner = change_structure_period(xTrain_inner,CVO_inner.training(iiCV));
    yTrain_inner = reshape(yTrain_Trials(:,CVO_inner.training(iiCV))',[],1);
    
    xTest_inner  = xTrain_trials(:,CVO_inner.test(iiCV),:);
    
    
    xTest_inner = change_structure_period(xTest_inner,CVO_inner.test(iiCV));
    yTest_inner = reshape(yTrain_Trials(:,CVO_inner.test(iiCV))',[],1);
    
    
    %% Normalize features
    if inputs.normalizeFeature
        [xTrain_in,paramZscore.mu,paramZscore.sigma] = zscore(xTrain_inner);
        xTest_in=  normalize_feature(xTest_inner,paramZscore);
    end
    
    %% PCA
    if inputs.applyPCA
        [coeff_PCA,xTrain_in,xTest_inner,inputs] = apply_PCA(xTrain_in,xTest_in,inputs,iCV);
    else
        coeff_PCA = [];
    end
    
    %% Feature Selection
    featuresNumberRange = 1:1:50;
    for iHyper =  1:length(featuresNumberRange)
        inputs.nbFeature2Select = featuresNumberRange(iHyper);
        
        if ~strcmp(inputs.featureSelection,'none')
            [xTrain,xTest] = apply_feature_selection(xTrain_in,xTest_in,yTrain_inner,inputs);
        else
            Beta = ones(size((xTrain_in),2),1);
        end
        
        % Train model
        Model = build_model(xTrain,yTrain_inner,inputs);
        
        % Evaluate parameters to tune
        output_test = predict(Model,xTest);
        
        error(iiCV,iHyper) = (sum(output_test ~=  yTest_inner))/length(yTest_inner);
    end
end
%% Take decision average over inner fold on test
error_mean = mean(error,1);
bestHyperparameter = find(error_mean == min(error_mean),1,'first');
bestHyperparameter = featuresNumberRange(bestHyperparameter);

end


function proSmoothed = calculate_moving_average(prob_values,smooth_parameter)

nTrial = size(prob_values,1);
prob_values_temp = [ prob_values(:,1)*ones(1,smooth_parameter) prob_values];
for i = 1:size(prob_values,1)
    [~,p] = movavg(prob_values_temp(i,:),1,smooth_parameter,1);
    proSmoothed(i,:) = p(smooth_parameter+1:end);
end


end


function [coeff_PCA,xTrain,xTest,inputs] = apply_PCA(xTrain,xTest,inputs, iCV)

% PCA - Training Set
[coeff,~,latent]  = pca(xTrain);
nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
coeff_PCA = coeff(:,1:nPC);

% project on PC
xTrain = xTrain * coeff_PCA;
xTest = xTest * coeff_PCA;
if  iCV == 1
    fprintf('PCA: %d PC kept using %d%% of variance explained\n',nPC,inputs.variance2KeepPC*100)
end

if nPC < inputs.nbFeature2Select && ~strcmp(inputs.featureSelection,'none')
    inputs.nbFeature2Select = nPC;
    fprintf('not enough principal component, decrease features to select to %d \n',inputs.nbFeature2Select )
    
end
end


function yhat = apply_pseudoOnline(xTest_period,iTime,paramZscore,coeff_PCA,Beta,Model,xTrain,inputs)

% normalization - Testing Set over time
if inputs.normalizeFeature
    feat_test = normalize_feature_over_period(xTest_period,iTime,paramZscore);
else
    feat_test = xTest_period(iTime,:,:);
end

if inputs.applyPCA
    feat_test = feat_test * coeff_PCA;
end

if inputs.regularization
    feat_test = feat_test*Beta;
else
    if ~strcmp(inputs.featureSelection,'none')
        feat_test = feat_test(:,Beta);
    end
end

% Classifier ouput
if inputs.trueProb && ~(strcmp(inputs.classifierType,{'SVM'}) || strcmp(inputs.classifierType,{'Bagger'}))
    POSTERIOR = calculate_probabilities_from_LDA(feat_test,Model,xTrain);
else
    [~,POSTERIOR ] = predict(Model,feat_test);
end

yhat  = POSTERIOR(:,2);

yhat  = POSTERIOR;

end

function [xTrain,xTest,Beta] = apply_regularization(xTrain,Ytrain,xTest,inputs)

% train SLDA with all features
Beta = slda(xTrain,  Ytrain, inputs.delta,-1*(inputs.nbFeature2Select),1);

% project data
xTrain = xTrain*Beta;
xTest = xTest*Beta;

end

function [xTrain,xTest,Beta,Power_feature] = apply_feature_selection(xTrain,xTest,yTrain,inputs)

if strcmp(inputs.featureSelection{1},'manual')
    Beta = ismember(1:size(xTrain,2),inputs.selectedFeatures)';
    xTrain = xTrain(:,inputs.selectedFeatures);
    xTest = xTest(:,inputs.selectedFeatures);
    Power_feature = [];
else
    % features selection - Training Set
    [xTrain,Beta,Power_feature] = features_selection(xTrain,yTrain,...
        'nfeature2Select',inputs.nbFeature2Select,...
        'method',inputs.featureSelection);
    
    % features selection - Testing Set
    xTest = xTest(:,Beta);
end
end

function Model = build_model(xTrain,yTrain,inputs)

if strcmp(inputs.classifierType,'SVM')
    Model = fitcsvm(xTrain,yTrain,'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    Model = fitPosterior(Model);
elseif strcmp(inputs.classifierType,'TreeBagger')
    
    maxNumSplits = 5; %Maximal number of decision splits, positive integer
    numTrees = 5000; %Scalar value equal to the number of desicion trees
    method = 'classification';
    
    if inputs.featuresImportanceTreebagger
        features2Save  = 'on';
    else
        features2Save  = 'off';
    end
    
    Model = TreeBagger(numTrees,xTrain,yTrain,...
        'Method',method,...
        'OOBPredictorImportance',features2Save);
    
    
else
    Model = fitcdiscr(xTrain,yTrain,'DiscrimType',inputs.classifierType{1},'Prior','empirical');
    
end
end

function [yTrain,yTest,Ytrain,Ytest,yTrain_Trials,yTest_Trials] = prepare_labels_for_classification(training,CVO,inputs,iCV)
% Distribution of samples for training and testing set (temporal and spectral)
Ytrain = [];
Ytest=  [];
if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end

% Labeling - training and testing
yTrain_Trials= training.label(:,iTrain);
yTrain = reshape(yTrain_Trials',[],1);
yTest_Trials = training.label(:,iTest);
yTest = reshape(yTest_Trials',[],1);

% prepare label for sLDA use
if inputs.regularization
    Ytrain = prepare_label_sLDA(yTrain);
    Ytest = prepare_label_sLDA(yTest);
end

end

function [xTrain,xTest,xTrain_period,xTest_period,xTrain_trials,Ctrain] = prepare_features_for_classification(training,CVO,inputs,iCV,yTrain)
Ctrain = [];

xTrain_period_temporal = [];
xTrain_period_spectral = [];

xTest_period_temporal = [];
xTest_period_spectral = [];

if strcmp(inputs.cvType{1},'LeaveOneRun')
    iTrain = CVO.training{iCV};
    iTest = CVO.test{iCV};
else
    iTrain = CVO.training(iCV);
    iTest = CVO.test(iCV);
end


if ~isempty(training.features.spectral)
    xTrain_trials = training.features.spectral(:,iTrain,:);
    xTrain_spectral = change_structure_period(xTrain_trials);
    
    xTest_spectral= training.features.spectral(:,iTest,:);
    if sum(iTest) == 1
        xTest_spectral = squeeze(xTest_spectral);
    else
        xTest_spectral = change_structure_period(xTest_spectral);
    end
    if ~isempty(inputs.pseudoOnline)
        xTest_period_spectral = inputs.pseudoOnline.features.spectral(:,iTest,:);
        xTrain_period_spectral = inputs.pseudoOnline.features.spectral(:,iTrain,:);
    else
        xTest_period_spectral = [];
        xTrain_period_spectral = [];
    end
    
else
    xTrain_spectral = [];
    xTest_spectral = [];
    xTest_period_spectral = [];
    xTrain_period_spectral = [];
end


if ~isempty(training.features.temporal)
    
    if ndims(training.features.temporal) == 3
        xTrain_trials = training.features.temporal(:,iTrain,:);
        xTest_temporal = squeeze(training.features.temporal(:,iTest,:));
    elseif ndims(training.features.temporal) == 4
        xTrain_trials = training.features.temporal(:,iTrain,:,:);
        xTest_temporal = squeeze(training.features.temporal(:,iTest,:,:));
    end
    
    xTrain_temporal = change_structure_period(xTrain_trials);
    xTest_temporal = change_structure_period(xTest_temporal);
    
    %% Riemann
    if inputs.riemann
        xTrain_temporal = permute(xTrain_temporal,[3 2 1]);
        xTest_temporal = permute(xTest_temporal,[3 2 1]);
        
        [xTrain_temporal,xTest_temporal,Ctrain] = compute_riemann(xTrain_temporal,yTrain,xTest_temporal);
        xTrain_temporal = xTrain_temporal';
        xTest_temporal = xTest_temporal';
        
    end
    
    if ~isempty(inputs.pseudoOnline)
        
        if ndims(training.features.temporal) == 3
            xTest_period_temporal = inputs.pseudoOnline.features.temporal(:,iTest,:);
            xTrain_period_temporal = inputs.pseudoOnline.features.temporal(:,iTrain,:);
        elseif ndims(training.features.temporal) == 4
            xTest_period_temp = inputs.pseudoOnline.features.temporal(:,iTest,:,:);
            xTrain_period_temp = inputs.pseudoOnline.features.temporal(:,iTrain,:,:);
            
            if inputs.riemann
                xTrain_period_temporal = [];
                xTest_period_temporal = [];
                
                nTime = size(xTest_period_temp,1);
                for iTime =1:nTime
                    xTrainPeriod_temporal = squeeze(xTrain_period_temp(iTime,:,:,:));
                    xTrainPeriod_temporal = permute(xTrainPeriod_temporal,[3 2 1]);
                    xTrainPeriod_temporal = Tangent_space(xTrainPeriod_temporal,Ctrain);
                    xTrain_period_temporal(iTime,:,:) = permute(xTrainPeriod_temporal,[3 2 1]);
                    
                    xTestPeriod_temporal= squeeze(xTest_period_temp(iTime,:,:,:));
                    xTestPeriod_temporal = permute(xTestPeriod_temporal,[3 2 1]);
                    xTestPeriod_temporal = Tangent_space(xTestPeriod_temporal,Ctrain);
                    xTest_period_temporal(iTime,:,:) = permute(xTestPeriod_temporal,[3 2 1]);
                end
            else
                xTest_period_temporal = xTest_period_temp;
                xTrain_period_temporal = xTrain_period_temp;
            end
            
            
        end
        
        
    else
        xTest_period = [];
        xTrain_period = [];
    end
    
else
    
    xTrain_temporal = [];
    xTest_temporal = [];
    xTest_period_temporal = [];
    xTrain_period_temporal = [];
    
    
    
end



xTrain = [xTrain_temporal xTrain_spectral];
xTest = [xTest_temporal xTest_spectral];
xTrain_period = cat(3,xTrain_period_temporal,xTrain_period_spectral);
xTest_period = cat(3,xTest_period_temporal,xTest_period_spectral);

end



function [xTrain,xTest,Ctrain] = compute_riemann(xTrain,yTrain,xTest)


% estimation of center
features = xTrain(:,:,yTrain==1);
Ctrain = mean_covariances(features,'riemann');
xTrain = Tangent_space(xTrain,Ctrain);
xTest = Tangent_space(xTest,Ctrain);

end


function feat = normalize_feature_over_period(xPeriod,iTime,paramZscore)

feat = squeeze(xPeriod(iTime,:,:));
if iscolumn(feat)
    feat = feat';
end
feat = normalize_feature(feat,paramZscore);

end

function feat = normalize_feature(feat,paramZscore)

paramZscore.sigma(paramZscore.sigma==0) = 1;
feat = bsxfun(@minus,feat, paramZscore.mu);
feat = bsxfun(@rdivide, feat, paramZscore.sigma);

end


function xAfter = change_structure_period(xBefore,CVO)


doChange = true;

if nargin == 1
    CVO = [];
else
    
    if sum(CVO) == 1
        xAfter = squeeze(xBefore);
        doChange = false;
    end
end

if doChange
    xAfter = [];
    nPeriod = size(xBefore,1);
    for iPeriod = 1:nPeriod
        if ndims(xBefore) == 3
            t = squeeze(xBefore(iPeriod,:,:));            
            xAfter= [xAfter ; t];
        elseif ndims(xBefore) == 4
            t = squeeze(xBefore(iPeriod,:,:,:));
            xAfter= [xAfter ; t];
        end
    end
end
end



function xAfter = change_structure_for_channel_3d(xBefore,training)

% nChannel = length(training.channelOfInterestERD);
nChannel = 16;
nFeature = size(xBefore,2);
nEpoch = size(xBefore,1);
nFeaturePerChannel = nFeature/nChannel;
xAfter = zeros(nEpoch,nFeaturePerChannel,nChannel);

iChannel = 1;
for iFeature = nFeaturePerChannel:nFeaturePerChannel:nFeature
    xAfter(:,:,iChannel) = xBefore(:,iFeature - nFeaturePerChannel+1:iFeature);
    iChannel = iChannel+1;
end

end



function CVO = find_partition(inputs)

nSample = size(inputs.training.label,2);
if ~isempty(inputs.model2Use)
    CVO = inputs.model2Use.CVO;
    %      CVO = cvpartition(nSample,'KFold',inputs.nFold);
else
    if strcmp(inputs.cvType{1},'LeaveOneOut')
        CVO = cvpartition(nSample,'LeaveOut',1);
    elseif strcmp(inputs.cvType{1},'KFold')
        CVO = cvpartition(nSample,'KFold',inputs.nFold);
    elseif strcmp(inputs.cvType{1},'HoldOut')
        CVO = cvpartition(nSample,'HoldOut',inputs.percent2Hold);
    elseif strcmp(inputs.cvType{1},'LeaveOneRun')
        CVO.NumTestSets = max(inputs.index4LeaveOneRun);
        CVO.training = {};
        CVO.test = {};
        for ii = 1:CVO.NumTestSets
            CVO.training{ii} = ~ismember(inputs.index4LeaveOneRun,ii);
            CVO.test{ii} = ismember(inputs.index4LeaveOneRun,ii)';
        end
    end
end

end


function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs, 'pseudoOnline',[],@isstruct);
addParameter(funcInputs, 'regularization',false,@islogical);
addParameter(funcInputs, 'normalizeFeature',true,@islogical);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'KFold','LeaveOneOut','HoldOut','LeaveOneRun'};
addParameter(funcInputs, 'cvType', {'KFold'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'nFold',10,@(x) isnumeric(x) & x>1)
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'percent2Hold',0.2,@(x) isnumeric(x))
expectedType = {'rankfeatures','corr','fisher','relief','infgain','none','manual'};
addParameter(funcInputs, 'featureSelection',{'fisher'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'selectedFeatures',1,@isnumeric);
addParameter(funcInputs,'index4LeaveOneRun',[],@isnumeric)
addParameter(funcInputs,'nbFeature2Select',[],@isnumeric);
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
addParameter(funcInputs,'nestedCV',false,@islogical);
addParameter(funcInputs,'model2Use',[],@isstruct);
addParameter(funcInputs,'trueProb',false,@islogical);
expectedType = {'linear','RBF','gaussian','polynomial'};
addParameter(funcInputs,'kernelType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM','TreeBagger'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'riemann',false,@islogical);
addParameter(funcInputs,'featuresImportanceTreebagger',false,@islogical);

parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

if funcInputs.regularization || strcmp(funcInputs.classifierType,'TreeBagger')
    funcInputs.featureSelection = {'none'};
end

if ~isempty(funcInputs.model2Use)
    funcInputs.featureSelection = funcInputs.model2Use.featureSelection;
    funcInputs.applyPCA = funcInputs.model2Use.applyPCA;
    funcInputs.regularization = funcInputs.model2Use.regularization;
    funcInputs.nbFeature2Select = funcInputs.model2Use.nbFeature2Select;
end

end
