function classifier_struct = apply_classifier_for_online_simple(features,labels,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = APPLY_CLASSIFIER_OFFLINE(TRAINING). training is structure
%   where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = APPLY_CLASSIFIER_OFFLINE(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value

%    'featuresSelection'       method use for features selection ('fisherScore','rankFeatures','none')
%                             (default 'none')
%    'features2Select'        number of features you want to keep
%    'feature2Use'            type of features you want to use ('spectral','temporal','both','fusion')
%                            (default 'both')
%    'regularization'         logical, use of sparse LDA if true (default true)
%    'delta'                  L1 regularization parameters for sLDA


%% Parameters

inputs = parse_my_inputs(features,labels,varargin{:});
coeff = [];

%sLDA parameters
delta = inputs.delta; % l2-norm constraint
stop  = -1*(inputs.features2Select); % request non-zero variables


warning('off','all')
fprintf('\n*************************')
fprintf('\nClassification Process...\n')


%% Normalization
[xTrain,mu,sigma] = zscore(features);

%% PCA
if inputs.applyPCA
    
    % PCA - Training Set
    [coeffa,xTrain,latent]  = pca(xTrain);
    nPC = find(cumsum(latent)./sum(latent) >= 0.95,1,'first'); % select 95% variance
    xTrain = xTrain(:,1:nPC);
    coeff = coeffa(:,1:nPC);
    
    % project on PC - Testing Set
    xTest = xTest * coeff;
    
end
%% Feature selection
if ~strcmp(inputs.featureSelection,'none')
    
    % features selection - Training Set
    [xTrain,Beta,P] = features_selection(xTrain,labels,...
        'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
 
end

%% Prepare output

paramZscore.mu = mu;
paramZscore.sigma = sigma;

%% Create classifier structure

classifier_struct = struct('Beta',Beta,'DC',xTrain,'Y',labels,...
    'paramZscore',paramZscore,'regularization',...
    inputs.regularization,'applyPCA',inputs.applyPCA,'coeff',coeff,...
    'nbFeature2Select', inputs.features2Select,'training',features,...
    'classifierType',inputs.classifierType);

end


function funcInputs = parse_my_inputs(features,labels,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'features',@isnumeric);
addRequired(funcInputs, 'labels',@isnumeric);
addParameter(funcInputs,'regularization',false,@islogical);
addParameter(funcInputs,'features2Select',[],@isnumeric);
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'fisherScore','rankfeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,features,labels, varargin{:});
funcInputs = funcInputs.Results;

if ~funcInputs.regularization && strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankFeatures'};
    
end

end
