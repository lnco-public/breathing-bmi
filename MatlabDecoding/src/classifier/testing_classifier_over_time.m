function yPredicted = testing_classifier_over_time(testing,classifier,class)


if  ~strcmp(classifier.type,'spectral')
   
    xt_Test_period = testing.features.temporal;
    
else
    xt_Test_period = [];
end

if ~strcmp(classifier.type,'temporal')
    xs_Test_period = testing.features.spectral;    
else

    xs_Test_period = [];
end



%%%%%%%%%%%%%%%%%%%%%%% Classification on testing dataset over time %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Dataset Preparation - Testing and Training
xTest_period = cat(3,xt_Test_period, xs_Test_period);


nPeriod = size(testing.label,1);
for iTime = 1:nPeriod
    
    % normalization - Training Set
    feat_test = normalize_feature_over_period(xTest_period,iTime,classifier.paramZscore.mu,classifier.paramZscore.sigma);
    
    
     if classifier.applyPCA
        % apply PCA - Testing Set
        feat_test = feat_test * classifier.coeff;
    end
    
%     if classifier.regularization
        % apply regularization - Testing Set
%         feat_test = feat_test*classifier.Beta;
%     else
        
        % apply feature selection - Testing Set
        feat_test = feat_test(:,classifier.Beta);
        
%     end
    
    
    %% Classifier ouput
    %testing
    [~,~,POSTERIOR] = classify(feat_test,classifier.DC,classifier.Y,classifier.classifierType,'empirical');
    yPredicted.value{iTime}  = POSTERIOR(:,class+1);
    
    
end
yPredicted.time = testing.time;
end

function feat = normalize_feature_over_period(xPeriod,iTime,mu,sigma)

feat = squeeze(xPeriod(iTime,:,:));
try
    feat = normalize_feature(feat,mu,sigma);
catch
    feat = normalize_feature(feat',mu,sigma);
end
end

function feat = normalize_feature(feat,mu,sigma)

feat = (feat-ones(size(feat,1),1)*mu)./(ones(size(feat,1),1)*sigma);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end
