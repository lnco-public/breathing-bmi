function [epochs,baselineMeanEpoch] = extract_epochs(eeg,varargin)

%   EXTRACT_EPOCHS extract epochs for selected channels within a defined interval.
%   EPOCHS = EXTRACT_EPOCHS(EEG). EEG is a structure whith dataset, sampling rate and
%   event.
%   [...] = EXTRACT_EPOCHS(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%     'alignEvent'              the event to be aligned to 
%     'timeBeforeEvent'         time before the event for epoching (default 1 seconds)
%     'timeAfterEvent'          time after the event for epoching (default 1 seconds)
%     'channelID'               specify the channel we want for epoching (default all channels) 
%    


inputs = parse_my_inputs(eeg,varargin{:});

[triggers_position,triggers_pos_stop] = find_event_of_interest(eeg,inputs);

eventStruct = struct('name',{},'position',{},'time',{});


% find position
if isempty(inputs.alignEvent_End{1})
    
    posLeft = triggers_position - floor(eeg.rate*inputs.timeBeforeEvent);
    posRight = triggers_position + floor(eeg.rate*inputs.timeAfterEvent)-1;
    nTime = floor(eeg.rate*inputs.timeAfterEvent)+ floor(eeg.rate*inputs.timeBeforeEvent);
    time = -inputs.timeBeforeEvent:1/eeg.rate:inputs.timeAfterEvent-1/eeg.rate;

else
    
    posLeft = triggers_position - floor(eeg.rate*inputs.timeBeforeEvent);
    posRight = triggers_pos_stop + floor(eeg.rate*inputs.timeAfterEvent)-1;
    
    if posLeft(1) > posRight(1)
        posRight(1) = [];
        posLeft(end) = [];
        triggers_position(end) = [];
    end
    %     nTime = max(posRight-posLeft);
    nTime = floor(median(posRight-posLeft))+1;
    %
    %      time = -inputs.timeBeforeEvent:1/eeg.rate:inputs.timeAfterEvent-1/eeg.rate;
    time = 1/nTime:1/nTime:1;
    %      nTime = max((posRight-posLeft))+1;
    
end

% initialization variables
nEpoch = length(triggers_position);
nChannel = length(inputs.channelID);
data = nan(nEpoch,nChannel,nTime);
baseline = zeros(nEpoch,nChannel);
baselineMeanEpoch = zeros(1,nChannel);
removeEpoch = [];
%% Compute baseline
if inputs.removeBaseline
    for iEpoch = 1: nEpoch    
        baseline(iEpoch,1:nChannel) = compute_meanBaseline(eeg,inputs.channelID,triggers.position(iEpoch),inputs.intervalBaseline);
    end
    baselineMeanEpoch = mean(baseline);
end

%% Extract epochs
for iEpoch = 1: nEpoch
    posLeft4ThisEpoch = posLeft(iEpoch);
    posRight4ThisEpoch = posRight(iEpoch);
    nPoint = posRight4ThisEpoch-posLeft4ThisEpoch +1;
    
    % check that the epoch is long enough otherwise doesnt take it and put warning
    if posRight4ThisEpoch > size(eeg.data,2) || posLeft4ThisEpoch < 0
        fprintf('cannot take epoch %d \n',iEpoch)
        removeEpoch = [removeEpoch iEpoch];
        
    else
        
        if inputs.removeBaseline
            data(iEpoch,:,:) = eeg.data(inputs.channelID,posLeft4ThisEpoch:posRight4ThisEpoch) - baselineMeanEpoch'*ones(1,nTime) ;
        else
            % data(iEpoch,:,1:nPoint) = eeg.data(inputs.channelID,posLeft4ThisEpoch:posRight4ThisEpoch);
            thisData = eeg.data(inputs.channelID,posLeft4ThisEpoch:posRight4ThisEpoch);
            thisData = resample(thisData',nTime,nPoint)';
            data(iEpoch,:,:) = thisData;
            
        end
        
       eventStruct =  prepare_event_for_trial(eeg,posLeft4ThisEpoch,posRight4ThisEpoch,eventStruct,iEpoch,triggers_position,inputs);
    end
end

data(removeEpoch,:,:) =[];

%% Create structure of epochs

map = containers.Map(eeg.label(inputs.channelID), 1:length(eeg.label(inputs.channelID)));

[nTrial,nChannel,nTime] = size(data); 

epochs = struct('data',data,...
    'time',time,...
    'rate',eeg.rate,...
    'label',{eeg.label(inputs.channelID)},...
    'event',eventStruct,...
    'state',eeg.state,...
    'alignEvent',inputs.alignEvent{1},...
    'alignEvent_End',inputs.alignEvent_End{1},...
    'map_ch',map,...  
    'nTrial',nTrial,...
    'nChannel',nChannel,...
    'nTimePoint',nTime,...
    'eloc',eeg.eloc);

end



function [triggers_pos,triggers_pos_stop] = find_event_of_interest(eeg,inputs)

triggers_pos_stop = [];

% start event
listEvent2AlignEpoch = [];
for iEvent= 1:length(inputs.alignEvent{1})
    listEvent2AlignEpoch = [listEvent2AlignEpoch find(eeg.event.name == inputs.alignEvent{1}(iEvent))];
end

triggers_pos = eeg.event.position(listEvent2AlignEpoch);

% stop event
if ~isempty(inputs.alignEvent_End{1})
    listEvent2AlignEpoch = [];
    for iEvent= 1:length(inputs.alignEvent{1})
        listEvent2AlignEpoch = [listEvent2AlignEpoch; find(eeg.event.name == inputs.alignEvent_End{1}(iEvent))];
    end
    
    triggers_pos_stop = eeg.event.position(listEvent2AlignEpoch);
end

end

function  eventStruct =  prepare_event_for_trial(eeg,posLeft,posRight,eventStruct,iEpoch,triggers_position,inputs)

index = find(eeg.event.position >= posLeft & eeg.event.position <= posRight);
eventStruct(iEpoch).name = eeg.event.name(index);

% event realignement with the beginning of the epoch
eventStruct(iEpoch).position = eeg.event.position(index)-posLeft + 1;
eventStruct(iEpoch).time = inputs.timeBeforeEvent + round(eventStruct(iEpoch).position/eeg.rate,1);
eventStruct(iEpoch).raw_position = triggers_position(iEpoch);
end

function meanBase = compute_meanBaseline(eeg,chanID,trig,intervalBaseline)

posLeft = trig + floor(eeg.rate*intervalBaseline(1));
posRight = trig + floor(eeg.rate*intervalBaseline(2));

if posRight > size(eeg.data,2) || posLeft < 0
    if posRight > size(eeg.data,2)
        warning('cannot take the last epoch because not long enough')
    else
        warning('cannot take the first epoch because not long enough')
    end
else
    
    theseEpochs = eeg.data(chanID,posLeft:posRight);
    meanBase = mean(theseEpochs,2);
end
end


%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);
addParameter(funcInputs,'alignEvent',{[]}, @iscell);
addParameter(funcInputs,'alignEvent_End',{[]}, @iscell);
addParameter(funcInputs, 'timeBeforeEvent', 1, @(x) isnumeric(x));
addParameter(funcInputs, 'timeAfterEvent', 1, @(x) isnumeric(x));
addParameter(funcInputs,'channelID',[],@(x) isnumeric(x));
addParameter(funcInputs,'removeBaseline',false,@islogical)
addParameter(funcInputs,'intervalBaseline',[],@isnumeric)

parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1);
end

end
