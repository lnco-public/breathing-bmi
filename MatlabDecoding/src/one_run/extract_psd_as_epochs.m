function epochs = extract_psd_as_epochs(eeg,varargin)

%   EXTRACT_EPOCHS extract epochs for selected channels within a defined interval.
%   EPOCHS = EXTRACT_EPOCHS(EEG). EEG is a structure whith dataset, sampling rate and
%   event.
%   [...] = EXTRACT_EPOCHS(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%     'alignEvent'              the event to be aligned to
%     'timeBeforeEvent'         time before the event for epoching (default 1 seconds)
%     'timeAfterEvent'          time after the event for epoching (default 1 seconds)
%     'channelID'               specify the channel we want for epoching (default all channels)
%


inputs = parse_my_inputs(eeg,varargin{:});

[triggers_position,triggers_pos_stop] = find_event_of_interest(eeg,inputs);
eventStruct = struct('name',{},'position',{},'time',{});

% find position
if isempty(inputs.alignEvent_End{1})
    posLeft = triggers_position - floor(eeg.rate*inputs.timeBeforeEvent);
    posRight = triggers_position + floor(eeg.rate*inputs.timeAfterEvent)-1;
else
    posLeft = triggers_position - floor(eeg.rate*inputs.timeBeforeEvent);
    posRight = triggers_pos_stop + floor(eeg.rate*inputs.timeAfterEvent);
end

% initialization variables
nTime = median(posRight-posLeft+1);
nEpoch = length(triggers_position);
nChannel = length(inputs.channelID);
nFreq = size(eeg.data,2);
data = zeros(nEpoch,nChannel,nFreq,nTime);
removeEpoch = [];
% extract epochs
for iEpoch = 1: nEpoch
    
    % find position of the limit of the interested interval
    posLeft4ThisEpoch = posLeft(iEpoch);
    posRight4ThisEpoch = posRight(iEpoch);
    nPoint = posRight4ThisEpoch-posLeft4ThisEpoch +1;
    
    % check that the epoch is long enough otherwise doesnt take it and put
    % warning
    if posRight4ThisEpoch > size(eeg.data,3) || posLeft4ThisEpoch < 0
        fprintf('cannot take epoch %d \n',iEpoch)
        removeEpoch = [removeEpoch iEpoch];
    else
        if isempty(inputs.alignEvent_End{1})
            data(iEpoch,:,:,1:nPoint) = eeg.data(inputs.channelID,:,posLeft4ThisEpoch:posRight4ThisEpoch);
        else
            thisEpoch = eeg.data(inputs.channelID,:,posLeft4ThisEpoch:posRight4ThisEpoch);
            for iChannel = 1:16
                data(iEpoch,iChannel,:,:) = resample(squeeze(thisEpoch(iChannel,:,:))',nTime,nPoint)';
            end
        end
        eventStruct =  prepare_event_for_trial(eeg,posLeft4ThisEpoch,posRight4ThisEpoch,eventStruct,iEpoch,triggers_position,inputs);
    end
end

data(removeEpoch,:,:,:) =[];

% create structure of epochs
map = containers.Map(eeg.label(inputs.channelID), 1:length(eeg.label(inputs.channelID)));


[nTrial,nChannel,nFreq,nTime] = size(data); 

if isempty(inputs.alignEvent_End{1})
    time = -inputs.timeBeforeEvent:1/eeg.rate:-inputs.timeBeforeEvent+nTime/eeg.rate-1/eeg.rate;
else
    time = -inputs.timeBeforeEvent:1/eeg.rate:-inputs.timeBeforeEvent+nTime/eeg.rate-1/eeg.rate;
end



epochs = struct('data',data,...
    'time',time,...
    'rate',eeg.rate,...
    'label',{eeg.label(inputs.channelID)},...
    'freq',eeg.freq,...
    'event',eventStruct,...
    'state',eeg.state,...
    'alignEvent',inputs.alignEvent{1},...
    'map_ch',map,...
    'nTrial',nTrial,...
    'nChannel',nChannel,...
    'nFreq',nFreq,...
    'nTimePoint',nTime);
end

function [triggers_pos,triggers_pos_stop] = find_event_of_interest(eeg,inputs)

triggers_pos_stop = [];

% start event
listEvent2AlignEpoch = [];
for iEvent= 1:length(inputs.alignEvent{1})
    listEvent2AlignEpoch = [listEvent2AlignEpoch; find(eeg.event.name == inputs.alignEvent{1}(iEvent))];
end

triggers_pos = eeg.event.position(listEvent2AlignEpoch);

% stop event
if ~isempty(inputs.alignEvent_End{1})
    listEvent2AlignEpoch = [];
    for iEvent= 1:length(inputs.alignEvent{1})
        listEvent2AlignEpoch = [listEvent2AlignEpoch; find(eeg.event.name == inputs.alignEvent_End{1}(iEvent))];
    end
    
    triggers_pos_stop = eeg.event.position(listEvent2AlignEpoch);
end
end

function  eventStruct =  prepare_event_for_trial(eeg,posLeft,posRight,eventStruct,iEpoch,triggers_position,inputs)

index = find(eeg.event.position >= posLeft & eeg.event.position <= posRight);
eventStruct(iEpoch).name = eeg.event.name(index);

% event realignement with the beginning of the epoch
eventStruct(iEpoch).position = eeg.event.position(index)-posLeft + 1;
eventStruct(iEpoch).time = inputs.timeBeforeEvent + round(eventStruct(iEpoch).position/eeg.rate,1);
eventStruct(iEpoch).raw_position = triggers_position(iEpoch);
end

%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);
addParameter(funcInputs,'alignEvent',[], @iscell);
addParameter(funcInputs,'alignEvent_End',[], @iscell);
addParameter(funcInputs, 'timeBeforeEvent', 1, @(x) isnumeric(x));
addParameter(funcInputs, 'timeAfterEvent', 1, @(x) isnumeric(x));
addParameter(funcInputs,'channelID',[],@(x) isnumeric(x));


parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1);
end

end

