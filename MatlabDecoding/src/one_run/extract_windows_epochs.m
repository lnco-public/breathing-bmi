function [training,testing] = extract_windows_epochs(varargin)

inputs = parse_my_inputs(varargin{:});

%% TRAINING DATASET
% Onset
try
 
    timeInterest = inputs.timeInfo.onset.timeInterval(1)+inputs.ws:inputs.timeInfo.onset.noverlap:inputs.timeInfo.onset.timeInterval(2);
    [ft_onset,fs_onset] = get_features_from_timeInfo(inputs,timeInterest);
    
    if inputs.timeInfo.onset.meanOverWindow && size(ft_onset,1) > 1
        ft_onset = mean(ft_onset,1);
        fs_onset = mean(fs_onset,1);
    end   
catch
    ft_onset = [];fs_onset = [];
end

% Rest
try

    timeInterest = inputs.timeInfo.rest.timeInterval(1)+inputs.ws:inputs.timeInfo.rest.noverlap:inputs.timeInfo.rest.timeInterval(2);
    [ft_rest,fs_rest] = get_features_from_timeInfo(inputs,timeInterest);
    if inputs.timeInfo.rest.meanOverWindow && size(ft_rest,1) > 1
        ft_rest = mean(ft_rest,1);
        fs_rest = mean(fs_rest,1);
    end
catch
    ft_rest = [];fs_rest = [];
end


% create training dataset
training.features.temporal = cat(1,ft_onset,ft_rest);
training.features.spectral = cat(1,fs_onset,fs_rest);
training.channelOfInterestERD = inputs.channelERD;

% create training labels
if ~isempty(inputs.temporal)
     [nTime,nTrial,~]  = size(training.features.temporal);
    training.label = zeros(nTime,nTrial);
    nOnset = size(ft_onset,1);nRest = size(ft_rest,1);
    training.label = [ones(nOnset,nTrial);zeros(nRest,nTrial)];
else
    
    [nTime,nTrial,~] = size(training.features.spectral);
    training.label = zeros(nTime,nTrial);
    nOnset = size(fs_onset,1);nRest = size(fs_rest,1);
    training.label =  [ones(nOnset,nTrial);zeros(nRest,nTrial)];
end



%% TESTING DATASET

if nargout > 1
    try
        timeInterest = inputs.timeInfo.test.timeInterval(1)+inputs.ws:inputs.timeInfo.test.noverlap:inputs.timeInfo.test.timeInterval(2);
        nTime = length(timeInterest);
        index = get_label_for_testing_set(inputs);
        [ft_test,fs_test] = get_features_from_timeInfo(inputs,timeInterest);
        if inputs.timeInfo.test.meanOverWindow && size(ft_test,1) > 1
            ft_test = mean(ft_test,1);
            fs_test = mean(fs_test,1);
        end
    catch
        ft_test = [];fs_test = [];
    end
    
    % create testing dataset
    testing.features.temporal = ft_test;
    testing.features.spectral = fs_test;
    
    if ~isempty(inputs.temporal)
        [nTime,nTrial,~] = size(testing.features.temporal);
        testing.label = zeros(nTime,nTrial);
    else
        [nTime,nTrial,~] = size(testing.features.spectral);
        testing.label = zeros(nTime,nTrial);
        
    end
    
    testing.label(index,:) = 1;
    
end

% info on features
if ~isempty(training.features.temporal)
training.features.Dtemp = size(training.features.temporal,3)/length(inputs.channelSCP);
end
if ~isempty(training.features.spectral)
training.features.Dspec = size(training.features.spectral,3)/length(inputs.channelERD);
end
delete(gcp('nocreate'))
end


function index = get_label_for_testing_set(inputs)

timeTest = inputs.timeInfo.test.timeInterval(1)+inputs.ws:inputs.timeInfo.test.noverlap:inputs.timeInfo.test.timeInterval(2);
timeOnset = inputs.timeInfo.onset.timeInterval(1)+inputs.ws:inputs.timeInfo.onset.noverlap:inputs.timeInfo.onset.timeInterval(2);


index = ismember(timeTest,timeOnset);

end

function [t,s] = get_features_from_timeInfo(inputs,timeInterest)

nTime = length(timeInterest);
t = [];s = [];

for iTime = 1:nTime
    iInterest = [timeInterest(iTime)-inputs.ws timeInterest(iTime)];
    
    % spectral features extraction
    if ~isempty(inputs.spectral)
            s(iTime,:,:) = extract_features_from_epoch(inputs.spectral,...
                'typefeatures',{'spectral'},...
                'rate',inputs.spectral.rate,...
                'timeOfInterest',iInterest,...
                'winsize',inputs.ws,...
                'freqRange',inputs.Dspec,...
                'channelID',inputs.channelERD,...
                'psdMethod',inputs.psdMethod);      
    end
      % temporal features extraction
    if ~isempty(inputs.temporal)
        t(iTime,:,:) = extract_features_from_epoch(inputs.temporal,...
            'typefeatures',{'amplitude'},...
            'rate',inputs.temporal.rate,...
            'timeOfInterest',iInterest,...
            'winsize',inputs.ws,...
            'Dtemp',inputs.Dtemp,...
            'channelID',inputs.channelSCP);
    end
end

end





function funcInputs = parse_my_inputs(varargin)

funcInputs = inputParser;

addParameter(funcInputs, 'temporal',[],@isstruct);
addParameter(funcInputs,'spectral',[],@isstruct);
addParameter(funcInputs,'Dtemp',64,@isnumeric);
addParameter(funcInputs,'Dspec',7:2:25,@isnumeric);
addParameter(funcInputs,'channelSCP',[],@isnumeric);
addParameter(funcInputs,'channelERD',[],@isnumeric);
addParameter(funcInputs,'ws',1,@isnumeric);
addParameter(funcInputs,'timeInfo',[],@isstruct);
expectedType = {'pwelch','fastPSDUsed','multitaper'};
addParameter(funcInputs,'psdMethod',{'pwelch'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs, varargin{:});
funcInputs = funcInputs.Results;


if isempty(funcInputs.channelSCP) && ~isempty(funcInputs.temporal)
    funcInputs.channelSCP = 1:length(funcInputs.temporal.label);
    
end


if isempty(funcInputs.channelERD) && ~isempty(funcInputs.spectral) 
    funcInputs.channelERD = 1:length(funcInputs.spectral.label);
end




end

