function x_feat = extract_features_from_epoch(epochs,varargin)
%EXTRACT_FEATURES extract features that will be used later by the
%classifier
%   EPOCHS = EXTRACT_FEATURES(EPOCHS). EPOCHS is a structure where DATA is
%   a matrix nEpoch x nChannel x nTimePoint and TIME is the timeseries.
%   [...] = EXTRACT_FEATURES(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%     'typefeatures'            what kind of features the user want to
%                               extract(amplitude [SCP],phase
%                               [SCP],spectral[PSD]) (default amplitude)
%     'freqRange'               frequency range used in the computation of PSD (default none)
%     'rate'                    sampling frequency of the epochs (default 512)
%     'winsize'                 window of study (default 1)
%     'timeOfInterest'          the interval the user is interested to
%                               extract the features (default none)
%     'noverlap'                use noverlap samples from section to
%                               section (default 0)
%     'averageWindow'           average of all the window (default false)
%     'channelID'               channel where we want to extract the
%                               features

inputs = parse_my_inputs(epochs,varargin{:});
a_feat = []; s_feat = [];p_feat = [];c_feat = [];

% extract features
if strcmp(inputs.typefeatures{1},'spectral')
    s_feat = extract_spectral_features(epochs,inputs);
elseif strcmp(inputs.typefeatures{1},'amplitude')
    a_feat = extract_amplitude_features(epochs,inputs);
elseif strcmp(inputs.typefeatures{1},'phase')
    p_feat = extract_phase_features(epochs,inputs);
elseif strcmp(inputs.typefeatures{1},'cov')
    c_feat = extract_cov_features(epochs,inputs);
elseif strcmp(inputs.typefeatures{1},'psd')
    s_feat = extract_psd_features(epochs,inputs);
    
    
end
% combine features
x_feat = [a_feat s_feat p_feat c_feat];

end

function features = extract_amplitude_features(epochs,inputs)

if ~isempty(inputs.Dtemp)
    stepD = inputs.rate*inputs.winsize/inputs.Dtemp;
else
    stepD = 1;
end

nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);
features = [];

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');

for iChannel = 1:nChannel
    thisChannel = inputs.channelID(iChannel);
%     theseEpochs = squeeze(epochs.data(:,thisChannel,tLeft:stepD:tRight));
    theseFeatures = squeeze(epochs.data(:,thisChannel,tLeft:stepD:tRight));
%     theseFeatures = theseEpochs - theseEpochs(:,1)*ones(1,size(theseEpochs,2));
    features  = [features theseFeatures];
end

end

function feature = extract_phase_features(epochs,inputs)

%% extract phase features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');

feature = [];

for iEpoch = 1: nEpoch
    f = [];
    for iChannel = 1: nChannel
        thisChannel = inputs.channelID(iChannel);
        thisEpoch = squeeze(epochs.data(iEpoch,thisChannel,tLeft:tRight));
        theseEpochsAngle = angle(hilbert(thisEpoch));
        f = [f theseEpochsAngle'];
    end
    feature = [feature; f];
end

end



function feature = extract_cov_features(epochs,inputs)

%% extract phase features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');

feature = zeros(nEpoch,nChannel,nChannel);

for iEpoch = 1: nEpoch
    thisEpoch = squeeze(epochs.data(iEpoch,:,tLeft:tRight));
    feature(iEpoch,:,:) = shcov(thisEpoch');
end
  
end

function features = extract_spectral_features(epochs,inputs)

if strcmp(inputs.psdMethod,'pwelch')
    features = extract_pwelch_features(epochs,inputs);
elseif strcmp(inputs.psdMethod,'fastPSDUsed')
    features = extract_psd_features(epochs,inputs);
elseif strcmp(inputs.psdMethod,'multitaper')
    features = extract_multitaper_features(epochs,inputs);
elseif strcmp(inputs.psdMethod,'wavelet')
   features = extract_wavelet_features(epochs,inputs);
end


end


function features = extract_psd_features(epochs,inputs)

%% extract spectral features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tRight = find(epochs.time <= inputs.timeOfInterest(2),1,'last');
features = [];

for iEpoch = 1: nEpoch
    f = [];
    for iChannel = 1: nChannel
        thisChannel = inputs.channelID(iChannel);
         % Selecting desired frequencies
        [freqs, idfreqs] = intersect(epochs.freq,inputs.freqRange);
        
        thisEpoch = squeeze(epochs.data(iEpoch,thisChannel,idfreqs,tRight));
        f = [f thisEpoch'];
    end
    features = [features; f];
end

end


function features = extract_multitaper_features(epochs,inputs)
%% extract spectral features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');

features = [];
for iChannel = 1: nChannel
    thisChannel = inputs.channelID(iChannel);
    theseEpoch = squeeze(epochs.data(:,thisChannel,tLeft:tRight));
    
    [psd, freqgrid] = pmtm(theseEpoch',3,[],inputs.rate);
    
    [freqs, idfreqs] = intersect(freqgrid,inputs.freqRange);
     psd = psd(idfreqs,:)';
     index0 = find(psd == 0);
     
     if ~isempty(index0)
         psd_temp = zeros(size(psd));
         features = [features psd_temp];
     else
         features = [features log(psd)];
     end
         
end





end


function features = extract_wavelet_features(epochs,inputs)
%% extract spectral features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');

features = [];
for iChannel = 1: nChannel
    thisChannel = inputs.channelID(iChannel);
    theseEpochs = squeeze(epochs.data(:,thisChannel,tLeft:tRight));
    [~,tf] =  preprocess_wavelet(theseEpochs,'freq',inputs.freqRange,'win',2,'fs',512);
    features = [features tf];
end


end


function feature = extract_pwelch_features(epochs,inputs)
%% extract spectral features
nChannel = length(inputs.channelID);
nEpoch = size(epochs.data,1);

tLeft = find(epochs.time >= inputs.timeOfInterest(1),1,'first');
tRight = find(epochs.time < inputs.timeOfInterest(2),1,'last');
win = inputs.rate*inputs.win*inputs.winsize;
noverlap = inputs.winsize*inputs.rate*inputs.win*inputs.noverlap;
feature = [];
for iEpoch = 1:nEpoch
    f = [];
    
    for iChannel = 1: nChannel
        thisChannel = inputs.channelID(iChannel);
        thisEpoch = squeeze(epochs.data(iEpoch,thisChannel,tLeft:tRight));
        
        [psd, freqgrid] = pwelch(thisEpoch,win,noverlap,[],inputs.rate);
        
        % Selecting desired frequencies
        [freqs, idfreqs] = intersect(freqgrid,inputs.freqRange);
        psd = psd(idfreqs)';
       
        f = [f log(psd)];
      
    end
 
    feature = [feature ;f];
end


end


function funcInputs = parse_my_inputs(epochs,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'epochs',@(x) isstruct(x));
expectedType = {'amplitude','spectral','phase','psd','cov'};
addParameter(funcInputs, 'typefeatures', {'amplitude'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'freqRange',4:2:40, @(x) isnumeric(x));
addParameter(funcInputs, 'rate',512, @(x) isnumeric(x));
addParameter(funcInputs,'winsize',1,@(x) isnumeric(x) & x >= 0);
addParameter(funcInputs,'timeEpoch',[],@isnumeric)
addParameter(funcInputs,'timeOfInterest',[],@(x) isnumeric(x));
addParameter(funcInputs,'win',0.5,@(x) isnumeric(x) & x >= 0);
addParameter(funcInputs,'noverlap',0.5,@(x) isnumeric(x));
addParameter(funcInputs,'channelID',[],@isnumeric)
expectedType = {'pwelch','fastPSDUsed','multitaper','wavelet'};
addParameter(funcInputs,'psdMethod',{'pwelch'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'Dtemp',64,@isnumeric)
parse(funcInputs, epochs, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(epochs.data,2);
end


end




