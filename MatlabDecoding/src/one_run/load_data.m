function eeg = load_data(filename,varargin)

%   LOAD_DATA create a EEG structure containing:
%       - signal recorded from all electrodes(EEG.data) 
%       - sampling rate (EEG.rate)
%       - event structure (EEG.event.name EEG.event.position)
%       - label of electrode (EEG.label) 
%       - map container (EEG.map_ch)
%       - amplifier used (EEG.amplifierUsed)
%       - channel location (EEG.eloc)
%   EEG = LOAD_DATA(FILENAME). FILENAME is a string indicating the location
%   of the recording file.
%   [...] = LOAD_DATA(..., 'PARAM1',VAL1) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%     'amplifierType'           the amplifier used during the recording of
%                               the experiment (default hiamp)
%     'processFolder'           folder containing location of ectrodes 


inputs = parse_my_inputs(filename,varargin{:});

% build eeg struture
[signal,header] = sload(filename);
eeg.data = signal';
eeg.rate = header.SampleRate;
eeg.event = struct('name',header.EVENT.TYP,'position',header.EVENT.POS);
eeg.amplifierUsed = inputs.amplifierType{1};

% if strcmp(inputs.amplifierType{1},'hiamp')
% %     for i = 1:64
% %         ll = strsplit(header.Label{i},'.');
% %         header.Label{i} = ll{2};
% %     end
% end

%% Check that event and length coincide.
position = header.EVENT.POS;
nTimePts = size(eeg.data,2);
iEvent2Remove = find(position > nTimePts);

if ~isempty(iEvent2Remove)
  
    trial = find(eeg.event.name == inputs.eventStartTrial);
    if iEvent2Remove(1) ~= inputs.eventStartTrial
        iTrial = find(trial <= iEvent2Remove(1),1,'last');
        iEvent2Remove = [trial(iTrial):1:iEvent2Remove(1) iEvent2Remove'];
        
        trial2Remove = find(eeg.event.name(iEvent2Remove) == inputs.eventStartTrial);
        
        eeg.event.name(iEvent2Remove) = [];
        eeg.event.position(iEvent2Remove) = [];
    end
    
     fprintf('Discarded trials: %d \n',length(trial2Remove))
    
end



eeg.label = regexprep(header.Label,'[^\w'']','');

if ~isempty(inputs.processFolder)
    eeg.eloc = importdata([inputs.processFolder filesep 'chanlocs.mat']);
end

if strcmp(inputs.amplifierType{1},'gtec')
    try
    eeg.label(1:16) = {eeg.eloc.labels};   
    catch
    eeg.label(1:16) = eeg.eloc.labels;   
    end
         eeg.label{end} = 'TRIG';
end

eeg.map_ch = containers.Map(eeg.label, 1:length(eeg.label));

% check for NaN values
for iChannel = 1:size(eeg.data,1)
    iNan = isnan(eeg.data(iChannel,:))==1;
    eeg.data(iChannel,iNan) = 0;
end

end


%% Parser
function funcInputs = parse_my_inputs(filename,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'filename',@ischar);
expectedType = {'hiamp','gtec'};
addParameter(funcInputs, 'amplifierType',{'hiamp'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'processFolder',@ischar);
addParameter(funcInputs,'eventStartTrial',100,@isnumeric);

parse(funcInputs, filename, varargin{:});
funcInputs = funcInputs.Results;

end