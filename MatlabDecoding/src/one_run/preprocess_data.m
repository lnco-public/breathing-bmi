function [eeg,parameters] = preprocess_data(eeg,varargin)

%   PREPROCESS_DATA process the data using different techniques(CAR,rereferencing,correction EOG,BSS)
%   EEG = PREPROCESS_DATA(EEG). EEG is a structure.
%   [...] = PREPROCESS_DATA(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value
%     'channelID'               specify the channel the user wants to process
%                               (default all channels)
%     'badChannel'              specify the channel not took into account
%                               for spatial filtering procedure (default none)
%     'refChannel'              specify the channel used as references (default 63 64)
%     'correctionArtefact'      specify the technique the user want to use for artefact correction
%                               ('EOG','ICA','addEOG','none') (default none)
%     'channelEOG'             specify the channel used as EOG (default none)
%     'nICs'                    number of source assumed when performing ICA (default 10)
%     'choiceBased'             specifiy the way we use ICA ('manual','auto')
%                               where 'manual' show plots to decide and
%                               'auto' use the variance explained of each
%                               component to select the component to remove
%                               (default 'manual')
%    'threshRejection'          variance explained tolerated
%                               to keep individual source component (default 4)
%    'channel2Interpolate'      specify which channel we want to keep by
%                               performing a spatial interpolation

inputs = parse_my_inputs(eeg,varargin{:});
global verbose

if verbose
    fprintf('\n***************************')
    fprintf('\nProcessing of the data ...')
end
filterParam = [];

% bad channels + channel to interpolate to remove for spatial filtering
bad_chan = [inputs.badChannel];
bad_chan = convert_string_channel_to_numeric(eeg.map_ch,bad_chan);


%% Temporal Filtering
if verbose
    fprintf('\nFiltering the data temporally ...(step 1/3)')
end
[eeg,filterParam] = temporal_filtering(eeg,'channelID',inputs.channelID,'cutoff',inputs.cutoff,'order',inputs.order,'type',inputs.type,'backward',inputs.backward);

%% Artefacts Correction
if verbose
    fprintf('\nArtefact correction using %s ...(step 2/3)',inputs.correctionArtefact{1})
end
[eeg,bss_struct] = artefact_correction(eeg,inputs);

%% Spatial Filtering
if verbose
    fprintf('\nFiltering the data spatially using %s ...(step 2/3)',inputs.spatialFiltering{1})
end
sf_matrix = build_spatial_filtering_matrix(inputs,bad_chan);

%% Rereferencing
linked_ear_matrix = build_reference_matrix(eeg,inputs);

%% Spatial interpolation or put zero to bad channels
if inputs.doSpatialInterpolation || inputs.setChannelToNull
    sf_matrix = spatial_interpolation_matrix(sf_matrix,eeg,inputs);
end

%% Apply Final Matrix
spatial_matrix = sf_matrix*linked_ear_matrix;
eeg.data(inputs.channelID,:) = spatial_matrix*eeg.data(inputs.channelID,:);

%% Remove channels
if ~isempty(inputs.badChannel) && inputs.removeBadChannel
    eeg = remove_channels(eeg,inputs.badChannel);
end

%% Save the parameters
parameters.temporal = filterParam;
parameters.spatial = struct('matrix',spatial_matrix ,'bss',bss_struct);

end

function sf_matrix = build_spatial_filtering_matrix(inputs,bad_chan)

nChannelEEG = length(inputs.channelID);

if strcmp(inputs.spatialFiltering{1},'CAR')
    sf_matrix = eye(nChannelEEG,nChannelEEG);
    nChan = nChannelEEG - length(bad_chan);
    nChan = nChannelEEG;
    sf_matrix = sf_matrix - 1/(nChan);
    
elseif strcmp(inputs.spatialFiltering{1},'LSF')
    sf_matrix = importdata([inputs.processFolder filesep 'laplacian.mat']);
    
elseif strcmp(inputs.spatialFiltering{1},'none')
    sf_matrix = eye(nChannelEEG,nChannelEEG);
end

% Dont take into account bad electrodes for spatial filtering
sf_matrix(bad_chan,:) = 0;
sf_matrix(:,bad_chan) = 0;



end


function sf_matrix = spatial_interpolation_matrix(sf_matrix,eeg,inputs)

interp_chan = convert_string_channel_to_numeric(eeg.map_ch,inputs.channel2Interpolate);

if ~isempty(inputs.badChannel) && inputs.setChannelToNull
    interp_chan = convert_string_channel_to_numeric(eeg.map_ch,inputs.badChannel);
end

if isempty(interp_chan)
    return
end

if strcmp(eeg.amplifierUsed,'gtec')
    sf_matrix = importdata([inputs.processFolder filesep 'laplacian.mat']);
    for iChan = 1:length(interp_chan)
        ch2Interpolate = interp_chan(iChan);
        if ~isempty(inputs.badChannel) && inputs.setChannelToNull
            sf_matrix(ch2Interpolate,:) = 0;
        else
            sf_matrix(ch2Interpolate,:) = abs(sf_matrix(ch2Interpolate,:));
        end
        sf_matrix(iChan,iChan) = 0;
    end
elseif strcmp(eeg.amplifierUsed,'hiamp')
    [~,sf_matrix] = spatial_interpolation(eeg,inputs.channel2Interpolate,[inputs.badChannel inputs.channel2Interpolate],spatial_matrix);
end


end

function linked_ear_matrix = build_reference_matrix(eeg,inputs)

nChannelEEG = length(inputs.channelID);
ref_chan = convert_string_channel_to_numeric(eeg.map_ch,inputs.refChannel);

if strcmp(eeg.amplifierUsed,'hiamp')
    linked_ear_matrix = eye(nChannelEEG,nChannelEEG);
    linked_ear_matrix(:,ref_chan) = -1/2;
else
    linked_ear_matrix = 1;
end

end


function [eeg,bss_struct] = artefact_correction(eeg,inputs)

bss_struct = [];

if strcmp(inputs.correctionArtefact,'addEOG')
    
    eeg = add_channel_EOG(eeg,inputs);
    
elseif strcmp(inputs.correctionArtefact,'EOG')
    
    [eeg,bss_struct] = correction_EOG(eeg,inputs);
    
elseif strcmp(inputs.correctionArtefact,'ICA')
    [eeg,bss_struct] = perform_ICA(eeg,inputs);
    
end
end

function [eeg,chanID] = add_channel_EOG(eeg,inputs)

if ~isnumeric(inputs.channelEOG)
    chanID = convert_string_channel_to_numeric(eeg.map_ch,inputs.channelEOG);
else
    chanID = inputs.channelEOG;
end
eog_chan = eeg.data(chanID,:);

% create EOG component
hEOG = eog_chan(1,:)- eog_chan(2,:);
vEOG = mean(eog_chan(1:2,:)) - eog_chan(3,:);
data = [hEOG;vEOG];

eeg = add_channel(eeg,data,{'hEOG','vEOG'});

end


function [eeg,eog] = correction_EOG(eeg,inputs)

[eeg,chanID] = add_channel_EOG(eeg,inputs);


Y = eeg.data(inputs.channelID,:);
U = eeg.data(inputs.channelEOG,:);

if isempty(inputs.eogStruct)
    covariance = cov([U ;Y]');
    autoCovariance_EOG = covariance(1:size(U,1), 1:size(U,1));
    crossCovariance_EOG_EEG = covariance(1:size(U,1), size(U,1)+1:end);
    coeff = autoCovariance_EOG \ crossCovariance_EOG_EEG;
    
else
    coeff = inputs.eogStruct.coeff;
end

% remove EOG component from EEG signal
eeg.data(inputs.channelID,:)=  Y - coeff'*U ;

% create the structure
eog = struct('coeff',coeff,'chan_EOG',chanID);

end
%% Parser
function funcInputs = parse_my_inputs(eeg,varargin)

funcInputs = inputParser;

addRequired(funcInputs,'eeg',@isstruct);
addParameter(funcInputs,'channelID',[],@(x) isnumeric(x));
addParameter(funcInputs,'badChannel',[],@iscellstr);
addParameter(funcInputs,'refChannel',{'REF1' 'REF2'},@iscellstr);
expectedType = {'EOG','ICA','none','addEOG'};
addParameter(funcInputs,'correctionArtefact',{'none'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'channelEOG',[],@(x) isnumeric(x) & length(x) == 3);
addParameter(funcInputs,'showEOG',false,@islogical);
expectedType = {'CAR','LSF','none'};
addParameter(funcInputs,'spatialFiltering',{'CAR'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs,'periChannels',true,@islogical);
addParameter(funcInputs,'nICs',10,@isnumeric);
expectedType = {'auto','manual'};
addParameter(funcInputs,'choiceBased',{'manual'},@(x) any(validatestring(x{:}, expectedType)))
addParameter(funcInputs,'threshRejection',4,@isnumeric)
addParameter(funcInputs,'channel2Interpolate',[],@iscellstr)
addParameter(funcInputs,'cutoff',[],@isnumeric);
addParameter(funcInputs,'order',2,@isnumeric);
addParameter(funcInputs,'backward',true,@islogical);
addParameter(funcInputs,'removeBadChannel',false,@islogical);
addParameter(funcInputs,'eogStruct',[]);
addParameter(funcInputs,'processFolder',[],@ischar);
addParameter(funcInputs,'doSpatialInterpolation',false,@islogical)
addParameter(funcInputs,'setChannelToNull',false,@islogical)

expectedType = {'low','high','bandpass'};
addParameter(funcInputs,'type',{'low'},@(x) any(validatestring(x{:}, expectedType)))
parse(funcInputs, eeg, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1) ;
end

if strcmp(funcInputs.correctionArtefact,'EOG')
    if isempty(funcInputs.channelEOG)
        error('missing EOG component')
    end
end


end
