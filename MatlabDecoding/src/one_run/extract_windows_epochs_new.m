function [training,testing] = extract_windows_epochs_new(timeInfo,varargin)

inputs = parse_my_inputs(timeInfo,varargin{:});

%% TRAINING DATASET
nClass = numel(timeInfo.training);

% create training features
for iClass = 1:nClass
    try
        timeInterest = timeInfo.training{iClass}.timeInterval(1)+inputs.ws:timeInfo.training{iClass}.noverlap:timeInfo.training{iClass}.timeInterval(2);
        [ft_class{iClass},fs_class{iClass}] = get_features_from_timeInfo(inputs,iClass,timeInterest);
        
        if timeInfo.training{iClass}.meanOverWindow && size(ft_class,1) > 1
            ft_class{iClass} = mean(ft_class{iClass},1);
            fs_class{iClass} = mean(fs_class{iClass},1);
        end
    catch
        ft_class{iClass} = [];
        fs_class{iClass} = [];
    end
    
    training.time{iClass} = timeInterest;
    
end

training.features.temporal = cat(1,ft_class{:});
training.features.spectral = cat(1,fs_class{:});
training.channelOfInterestERD = inputs.channelSpectral;


% Create training labels
for iClass = 1:nClass
    
    if ~isempty(inputs.temporal)
        [nTime,nTrial,~]  = size(ft_class{iClass});
    else
        [nTime,nTrial,~] = size(fs_class{iClass});
    end
    
    label_class{iClass}  = repmat(iClass-1,nTime,nTrial);
    
end

training.label = cat(1,label_class{:});


%% TESTING DATASET

if nargout > 1
    try
        timeInterest = timeInfo.testing.timeInterval(1)+inputs.ws:timeInfo.testing.noverlap:timeInfo.testing.timeInterval(2);
        nTime = length(timeInterest);
        [ft_test,fs_test] = get_features_from_timeInfo(inputs,nClass+1,timeInterest);
 
    catch
        ft_test = [];fs_test = [];
    end
    
    % create testing dataset
    testing.features.temporal = ft_test;
    testing.features.spectral = fs_test;
    testing.label= get_label_for_testing_set(testing,inputs);
    tresting.time = timeInterest;
    
end

% info on features
if ~isempty(training.features.temporal)
training.features.Dtemp = size(training.features.temporal,3)/length(inputs.channelTemporal);
end
if ~isempty(training.features.spectral)
training.features.Dspec = size(training.features.spectral,3)/length(inputs.channelSpectral);
end

testing.time = inputs.timeInfo.testing.timeInterval(1) + inputs.ws:...
inputs.timeInfo.testing.noverlap:inputs.timeInfo.testing.timeInterval(2);

if inputs.parallelComputing
    delete(gcp('nocreate'))
end

end


function label_test = get_label_for_testing_set(testing,inputs)

timeTest = inputs.timeInfo.testing.timeInterval(1)+inputs.ws:inputs.timeInfo.testing.noverlap:inputs.timeInfo.testing.timeInterval(2);

if ~isempty(inputs.temporal)
    [nTime,nTrial,~]  = size(testing.features.temporal);
else
    [nTime,nTrial,~] = size(testing.features.spectral);
end

label_test  = zeros(nTime,nTrial);

for iClass = 1: numel(inputs.timeInfo.training);
    timeInfo_class = inputs.timeInfo.training{iClass}.timeInterval(1)+inputs.ws:inputs.timeInfo.training{iClass}.noverlap:inputs.timeInfo.training{iClass}.timeInterval(2);
    index = ismember(timeTest,timeInfo_class);
    label_test(index,:) = iClass-1;
end

end

function [t,s] = get_features_from_timeInfo(inputs,iClass,timeInterest)

nTime = length(timeInterest);


if inputs.parallelComputing
    if strcmp(inputs.tempMethod{1},'cov')
        
        parfor iTime = 1:nTime
            
            [s(iTime,:,:),t(iTime,:,:,:)] = extract_features_from_epoch_spectral_temporal_together(iTime,iClass,inputs,timeInterest);
        end
        
    else
        
        parfor iTime = 1:nTime
            
            [s(iTime,:,:),t(iTime,:,:)] = extract_features_from_epoch_spectral_temporal_together(iTime,iClass,inputs,timeInterest);
        end
        
    end
    
else
    
    for iTime = 1:nTime
        if strcmp(inputs.tempMethod{1},'cov')
            
            [s(iTime,:,:),t(iTime,:,:,:)] = extract_features_from_epoch_spectral_temporal_together(iTime,iClass,inputs,timeInterest);
            
        else
            [s(iTime,:,:),t(iTime,:,:)] = extract_features_from_epoch_spectral_temporal_together(iTime,iClass,inputs,timeInterest);
        end
    end
end

end

function [s,t] = extract_features_from_epoch_spectral_temporal_together(iTime,iClass,inputs,timeInterest)

iInterest = [timeInterest(iTime)-inputs.ws timeInterest(iTime)];
t = [];s = [];
% spectral features extraction
if ~isempty(inputs.spectral)
    s = extract_features_from_epoch(inputs.spectral{iClass},...
        'typefeatures',{'spectral'},...
        'rate',inputs.spectral{iClass}.rate,...
        'timeOfInterest',iInterest,...
        'winsize',inputs.ws,...
        'freqRange',inputs.Dspec,...
        'channelID',inputs.channelSpectral,...
        'psdMethod',inputs.psdMethod);
end
% temporal features extraction
if ~isempty(inputs.temporal)
    t = extract_features_from_epoch(inputs.temporal{iClass},...
        'typefeatures',inputs.tempMethod,...
        'rate',inputs.temporal{iClass}.rate,...
        'timeOfInterest',iInterest,...
        'winsize',inputs.ws,...
        'Dtemp',inputs.Dtemp,...
        'channelID',inputs.channelTemporal);
end

end



function funcInputs = parse_my_inputs(timeInfo,varargin)

funcInputs = inputParser;
addRequired(funcInputs,'timeInfo',@isstruct);
addParameter(funcInputs, 'temporal',[]);
addParameter(funcInputs,'spectral',[]);
addParameter(funcInputs,'Dtemp',[],@isnumeric);
addParameter(funcInputs,'Dspec',[],@isnumeric);
addParameter(funcInputs,'channelTemporal',[],@isnumeric);
addParameter(funcInputs,'channelSpectral',[],@isnumeric);
addParameter(funcInputs,'parallelComputing',true,@islogical);
addParameter(funcInputs,'ws',1,@isnumeric);
expectedType = {'amplitude','phase','cov'};
addParameter(funcInputs,'tempMethod',{'amplitude'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'pwelch','fastPSDUsed','multitaper','wavelet'};
addParameter(funcInputs,'psdMethod',{'pwelch'},@(x) any(validatestring(x{:}, expectedType)));
parse(funcInputs,timeInfo, varargin{:});

funcInputs = funcInputs.Results;

if isempty(funcInputs.channelTemporal) && ~isempty(funcInputs.temporal)
    funcInputs.channelTemporal = 1:length(funcInputs.temporal.label);
    
end


if isempty(funcInputs.channelSpectral) && ~isempty(funcInputs.spectral) 
    funcInputs.channelSpectral = 1:length(funcInputs.spectral.label);
end


end

