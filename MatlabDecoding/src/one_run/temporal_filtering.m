function [eeg,filterParam] = temporal_filtering(eeg,varargin)
%FILTERING_PROCESSING filter the data from eeg with different options


%   EEG = FILTERING_PROCESSING(EEG). EEG is a structure containing the
%   signal recorded (channel x time matrix)
%   [...] = FILTERING_PROCESSING(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
%
%     Parameters                Value

%     'channelID'               specify the channel the user wants to filter (default all channels)
%     'cutoff'                  specifiy the cutoff frequency or frequency
%                               band. (default is 25)
%     'order'                   filter order (default is 2)
%     'type'                    fiter type ('low','high','bandpass','stop') (default low)
%     'backward'                zero_phase filter (backward = TRUE) (default FALSE)

inputs = parse_my_inputs(eeg,varargin{:});
filterParam = [];

if isempty(inputs.cutoff)
    return
end

% find coefficient for filtering using butterworth filter
[b,a] = butter(inputs.order,inputs.cutoff/(eeg.rate/2),inputs.type{1});

nChannel = length(inputs.channelID);

for iChannel = 1:nChannel
    
    thisChannel = inputs.channelID(iChannel);
    chan = eeg.data(thisChannel,:);
    
    % apply filter
    if inputs.backward
        eeg.data(thisChannel,:) = filtfilt(b,a,chan);
    else
        eeg.data(thisChannel,:) = filter(b,a,chan);
    end
    
end

% save parameters in a structure
filterParam = struct('a',a,'b',b,'type',inputs.type,'non_causal',...
    inputs.backward,'cutoff',inputs.cutoff,'order',inputs.order);



end

%% Parser

function funcInputs = parse_my_inputs(data,rate, varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'eeg',@isstruct);
addParameter(funcInputs,'channelID',[],@(x) isnumeric(x));
addParameter(funcInputs, 'cutoff', [], @(x) length(x)<=2);
addParameter(funcInputs, 'order',2, @(x) isnumeric(x) & x > 0);
expectedType = {'low', 'high','bandpass','stop'};
addParameter(funcInputs, 'type', {'low'},@(x) any(validatestring(x{:}, expectedType)));
addParameter(funcInputs, 'backward', false, @islogical);
parse(funcInputs, data,rate, varargin{:});
funcInputs = funcInputs.Results;

if isempty(funcInputs.channelID)
    funcInputs.channelID = 1:size(eeg.data,1) ;
end

end