  function classifier_struct = build_classifier_online(training,varargin)


%% Parameters

inputs = parse_my_inputs(training,varargin{:});

warning('off','all')
fprintf('\n*************************')
fprintf('\nClassification Process...\n')

    
% label for training and testing
yTrain = training.label;
yTrain = reshape(yTrain',[],1);

xTrain = training.features.spectral;
xTrain = change_structure_period(xTrain);

coeff_PCA = [];
paramZscore = struct('mu',[],'sigma',[]);
% Zscore normalization
[xTrain,paramZscore.mu,paramZscore.sigma] = zscore(xTrain);

%% PCA
if inputs.applyPCA
    % PCA - Training Set
    [coeffa,xTrain,latent]  = pca(xTrain);
    nPC = find(cumsum(latent)./sum(latent)<= inputs.variance2KeepPC,1,'last'); % select x% variance
    coeff_PCA = coeffa(:,1:nPC);
    
    % project on PC
    xTrain = xTrain(:,1:nPC);    
    disp([num2str(nPC) ' PC component for training classifier']);
else
    % Feature selection
    xTrain = xTrain(:,inputs.features_params);
    disp([num2str(sum(inputs.features_params)) ' selected features for training classifier']);
    
end

%% Build model
if strcmp(inputs.classifierType,'SVM')
    Model = fitcsvm(xTrain,yTrain,'KernelFunction',inputs.kernelType{1},'KernelScale','auto');
    Model = fitPosterior(Model);
else
    Model = fitcdiscr(xTrain,yTrain,'DiscrimType',inputs.classifierType{1});
end


    %% Output
    classifier_struct = struct('model',Model,...
        'Beta',inputs.features_params,...
        'paramZscore',paramZscore,...
        'applyPCA',inputs.applyPCA,...
        'variance2KeepPC',inputs.variance2KeepPC,...
        'coeffPCA',coeff_PCA,...
        'training',training,...
        'classifierType',inputs.classifierType,....
        'kernelType',inputs.kernelType);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;
addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs, 'features_params',[],@islogical);
addParameter(funcInputs,'nbFeatures2Select',[],@isnumeric);
addParameter(funcInputs, 'applyPCA',false,@islogical);
addParameter(funcInputs, 'variance2KeepPC',0.95,@isnumeric);
expectedType = {'linear','RBF','gaussian','polynomial'};
addParameter(funcInputs,'kernelType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

end
