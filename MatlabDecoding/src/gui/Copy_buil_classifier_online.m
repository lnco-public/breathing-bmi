  function classifier_struct = feature_selection_online(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = BUILD_CLASSIFIER_ONLINE(TRAINING). training is structure
%   where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = APPLY_CLASSIFIER_OFFLINE(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value

%    'featuresSelection'       method use for features selection ('fisherScore','rankFeatures','none')
%                             (default 'none')
%    'features2Select'        number of features you want to keep
%    'feature2Use'            type of features you want to use ('spectral','temporal','both','fusion')
%                            (default 'both')
%    'regularization'         logical, use of sparse LDA if true (default true)
%    'delta'                  L1 regularization parameters for sLDA


%% Parameters

inputs = parse_my_inputs(training,varargin{:});
coeff_PCA = [];

%sLDA parameters
delta = inputs.delta; % l2-norm constraint
stop  = -1*(inputs.features2Select); % request non-zero variables


warning('off','all')
fprintf('\n*************************')
fprintf('\nClassification Process...\n')

    
% label for training and testing
Y = training.label;
Y = reshape(Y',[],1);
% prepare label for sLDA use
yTrain = prepare_label_sLDA(Y);


xTrain_ini = training.features.spectral;
xTrain_ini = change_structure_period(xTrain_ini);

% normalize features together
[xTrain,mu,sigma] = zscore(xTrain_ini);

if inputs.applyPCA
    [coeff_PCA,xTrain,latent]  = pca(xTrain);
    nPC = find(cumsum(latent)./sum(latent) >= inputs.variance2KeepPC,1,'first');
    xTrain = xTrain(:,1:nPC);
    coeff_PCA = coeff_PCA(:,1:nPC);
end

%% Feature selection
if ~strcmp(inputs.featureSelection,'none')
    [xTrain,SelectedFeatures,P] = features_selection(xTrain,Y,'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
end

%% Regularization
if inputs.regularization
    % train sLDA
    Beta = slda(xTrain, yTrain, delta, stop,1);
    xTrain = xTrain*Beta;
end

paramZscore = struct('mu',mu,'sigma',sigma);
classifier_struct = struct('Beta',SelectedFeatures,'DC',xTrain,'Y',Y,'Pfeature',{P},...
    'paramZscore',paramZscore,'regularization',...
    inputs.regularization,'applyPCA',inputs.applyPCA,'coeff',coeff_PCA,'nbFeature2Select',...
    inputs.features2Select,'training',training,'classifierType',inputs.classifierType);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs,'regularization',false,@islogical);
addParameter(funcInputs,'features2Select',[],@isnumeric);
expectedType = {'spectral','temporal','both','fusion'};
addParameter(funcInputs,'applyPCA',false,@islogical)
addParameter(funcInputs,'variance2KeepPC',0.95,@isnumeric);
addParameter(funcInputs,'delta',1e-6,@isnumeric);
expectedType = {'fisherScore','rankFeatures','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'automatic','manual'};
addParameter(funcInputs, 'selection',{'none'},@(x) any(validatestring(x{:}, expectedType)));
expectedType = {'diaglinear','diagquadratic','linear','quadratic'};
addParameter(funcInputs,'classifierType',{'linear'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

if ~funcInputs.regularization && strcmp(funcInputs.featureSelection,{'none'})
    funcInputs.featureSelection = {'rankFeatures'};
    
end

end
