function varargout = GUI_FeatureSelection(varargin)
% GUI_FEATURESELECTION MATLAB code for GUI_FeatureSelection.fig
%      GUI_FEATURESELECTION, by itself, creates a new GUI_FEATURESELECTION or raises the existing
%      singleton*.
%
%      H = GUI_FEATURESELECTION returns the handle to a new GUI_FEATURESELECTION or the handle to
%      the existing singleton*.
%
%      GUI_FEATURESELECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_FEATURESELECTION.M with the given input arguments.
%
%      GUI_FEATURESELECTION('Property','Value',...) creates a new GUI_FEATURESELECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_FeatureSelection_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_FeatureSelection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_FeatureSelection

% Last Modified by GUIDE v2.5 30-Jan-2019 11:45:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_FeatureSelection_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_FeatureSelection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_FeatureSelection is made visible.
function GUI_FeatureSelection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_FeatureSelection (see VARARGIN)

% Choose default command line output for GUI_FeatureSelection
handles.output = hObject;
set(0,'DefaultFigureWindowStyle','normal');
set(0,'defaultAxesFontSize',10)
handles.training = varargin{1};
handles.params = varargin{2};
handles.testing = varargin{3};

global SelectedFeatures_workspace;
global locFile;
global freqUsed;
freqUsed  = handles.params.params_featExtraction.freq; 
global axes17; 
axes17 = handles.axes17;
global axes18;
axes18 = handles.axes18;

fileLoc = handles.params.params_loading.processFolder;
handles.locFile = importdata([fileLoc filesep 'chanlocs.mat']);
locFile = handles.locFile;


set(0,'defaultAxesFontSize',12)
list =  {'fisher','corr','relief','rankfeatures','infgain'};
set(handles.listbox_features_algo,'String',list,'Value',1,'Enable','off');
list =  {'movingAverage','evidenceAccumulation','accumulateDecision'};
set(handles.popupmenu_detectionMethod,'String',list,'Value',1);
list = {'linear','RBF','gaussian','polynomial'};
set(handles.popupmenu_kernel,'String',list,'Value',1);
list = {'diaglinear','diagquadratic','linear','quadratic','pseudoLinear','pseudoQuadratic','SVM','TreeBagger'};
set(handles.popupmenu_classifier,'String',list,'Value',1);

handles.features_struct = feature_selection_online(handles.training,...
    'features2Select',handles.params.params_classifier.nbFeature2Select,....
    'featureSelection',handles.params.params_classifier.featureSelection);
handles.powerFeatures = handles.features_struct.Pfeature;
handles.nFeature = length(handles.params.params_featExtraction.freq);
handles.nChannel = length(handles.params.params_featExtraction.channelOfInterestERD);

handles.powerFeatures = create_features_selection_matrix(handles.features_struct.Pfeature,handles.nFeature,handles.nChannel);   %% HARD CODED

% Set value for Edit Parameters
set(handles.slider_smoothing,'Value',handles.params.params_online.smoothing,'Max',1,'Min',0.5, 'SliderStep',[1/10 1/10],'Enable','off');
set(handles.slider_point2DetectOrSmooth,'Value',handles.params.params_online.nPoint2DetectOrSmooth,'Min',5,'Max',30,'SliderStep',[1/25 1/25],'Enable','on');
set(handles.slider_threshold,'Value',handles.params.params_online.threshold,'Min',0.5,'Max',1,'SliderStep',[1/20 1/20],'Enable','on');
set(handles.slider8_nFeature2Select,'Value',handles.params.params_classifier.nbFeature2Select,'Max',300,'Min',0, 'SliderStep',[1/300 1/300],'Enable','on');


% Find threshold and extremum
handles.maximumThreshold = max(max(handles.powerFeatures));
handles.minimumThreshold = min(min(handles.powerFeatures));
handles.threshold = 0.5*(handles.maximumThreshold+handles.minimumThreshold) ;
handles.selectedFeatures  = handles.powerFeatures > handles.threshold;


[~,orderedInd] = sort( handles.features_struct.Pfeature,'descend');
posFeature = 1:length(handles.features_struct.Pfeature);
selected_features = ismember(posFeature,orderedInd(1:handles.params.params_classifier.nbFeature2Select));
handles.selectedFeatures = create_features_selection_matrix(selected_features',handles.nFeature,handles.nChannel);   %% HARD CODED


SelectedFeatures_workspace = handles.selectedFeatures;
plot_GUI_features_mapping(handles)
plot_topoplot(axes17,axes18,handles.selectedFeatures,locFile,freqUsed);
handles.classifier = [];
handles.opti_threshold = handles.params.params_online.threshold;

% Set up threshold slider
set(handles.slider1,'Value',handles.threshold ,'Enable','off');
set(handles.slider1,'Max',handles.maximumThreshold);
set(handles.slider1,'Min',handles.minimumThreshold);

% Set Checkbox
set(handles.checkbox_PCA,'Value' ,handles.params.params_classifier.applyPCA);
if handles.params.params_classifier.applyPCA 
handles.params.params_classifier.regularization = false;
set(handles.checkbox_Regularization,'Value',handles.params.params_classifier.regularization,'Enable','off');
else
   set(handles.checkbox_Regularization,'Value',handles.params.params_classifier.regularization,'Enable','on'); 
end


if handles.params.params_classifier.applyPCA
    str = 'on';
else
    str = 'off';
end
set(handles.slider_varianceExplained,'Value',handles.params.params_classifier.variance2KeepPC,'Min',0.7,'Max',1,'SliderStep',[1/30 1/30],'Enable',str);
set(handles.edit_varianceExplained,'String',num2str(handles.params.params_classifier.variance2KeepPC),'Enable',str);


set(handles.edit_smoothing,'String',num2str(handles.params.params_online.smoothing),'Enable','off');
set(handles.edit_point2DetectOrSmooth,'String',num2str(handles.params.params_online.nPoint2DetectOrSmooth),'Enable','on');
set(handles.edit_threshold,'String',num2str(handles.params.params_online.threshold),'Enable','on');
set(handles.edit_nFeature2Select,'String',handles.params.params_classifier.nbFeature2Select,'Enable','on');


% Set correct detection frame
set(handles.edit_detectionFrame1,'String',num2str(handles.params.params_online.detectionFrame(1)));
set(handles.edit_detectionFrame2,'String',num2str(handles.params.params_online.detectionFrame(2)));

train_classifier(hObject,handles)

% Set up data cursor
handles.dcm = datacursormode(hObject);
set(handles.dcm,'UpdateFcn',@featureHandler,'Enable','on');

% Set Up Kernel popmenu
set(handles.popupmenu_kernel,'Enable','off');

% UIWAIT makes GUI_FeatureSelection wait for user response (see UIRESUME)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_FeatureSelection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} =  handles.classifier;
delete(hObject);

% --- Executes on button press in pushbutton1.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure

% SelPlotImHandle = findobj(findobj(GUIHandle,'Tag','SelectionPlot'),'Type','image');
% SelectedData = get(SelPlotImHandle,'CData');
% 
% disp(SelectedData)
% handles.features_struct = SelectedData;
%     'features_params',handles.features_struct,...
global SelectedFeatures_workspace;
handles.features_struct = [];

features_selected = reshape(logical(SelectedFeatures_workspace)',[],1);
handles.classifier = build_classifier_online(handles.training,...
    'features_params',features_selected,...
    'classifierType',handles.params.params_classifier.classifierType,....
    'kernelType',handles.params.params_classifier.kernelType,...
    'applyPCA',handles.params.params_classifier.applyPCA,...
    'variance2KeepPC',handles.params.params_classifier.variance2KeepPC);

handles.classifier.params_online = handles.params.params_online;
handles.classifier.features_ready = SelectedFeatures_workspace;

% Update handles structure
guidata(hObject, handles);
delete(hObject)
figure1_DeleteFcn(hObject)

% Callback to handle the data cursor clicks
function txt = featureHandler(obj,event_obj)

global SelectedFeatures_workspace;
global axes17; 
global axes18;
global locFile;
global freqUsed;

ImageHandle = event_obj.Target;

Pos = get(event_obj,'Position');
txt = {['Frequency bin: ' num2str(Pos(1))], ['Electrode: ' num2str(Pos(2))]};
if(strcmp(get(get(ImageHandle,'Parent'),'Tag'),'DPPlot')) % Means it is the Selection Plot
    
    GUIHandle = get(get(ImageHandle,'Parent'),'Parent');
    GUIdata = guidata(GUIHandle);
    SelPlotImHandle = findobj(findobj(GUIHandle,'Tag','SelectionPlot'),'Type','image');
    SelectedData = get(SelPlotImHandle,'CData');
    % Remove selection
    SelectedData(Pos(2),Pos(1)) = 1;
    SelectedFeatures_workspace = SelectedData;
    set(SelPlotImHandle,'CData',SelectedData);
    plot_topoplot(axes17,axes18,SelectedData,locFile,freqUsed);
    guidata(GUIHandle,GUIdata);
    return;
elseif(strcmp(get(get(ImageHandle,'Parent'),'Tag'),'SelectionPlot')) % Means it is the DPPlot
    GUIHandle = get(get(ImageHandle,'Parent'),'Parent');
    GUIdata = guidata(GUIHandle);
    SelPlotImHandle = findobj(findobj(GUIHandle,'Tag','SelectionPlot'),'Type','image');
    SelectedData = get(SelPlotImHandle,'CData');
    % Perform selection
    SelectedData(Pos(2),Pos(1)) = 0;
    SelectedFeatures_workspace = SelectedData;
    set(SelPlotImHandle,'CData',SelectedData);
     plot_topoplot(axes17,axes18,SelectedData,locFile,freqUsed);
    guidata(GUIHandle,GUIdata);
    return;
end

% 
% % --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global SelectedFeatures_workspace;
global axes17; 
global axes18;
global locFile;
global freqUsed;

handles.threshold = get(hObject,'Value');
handles.selectedFeatures  = handles.powerFeatures > handles.threshold;
axes(handles.axes2);
imagesc(handles.selectedFeatures)
plotPresent(handles)
add_separation_features_map(handles.selectedFeatures)
plot_topoplot(axes17,axes18,handles.selectedFeatures,locFile,freqUsed);
SelectedFeatures_workspace = handles.selectedFeatures;
% Update handles structure
guidata(hObject, handles);

% 
% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function plotPresent(handles)
    set(gca, 'YTick',      1:length(handles.locFile));
    set(gca, 'YTickLabel', {handles.locFile.labels});
    set(gca, 'XTick',      [1:length(handles.params.params_featExtraction.freq)]);
    set(gca, 'XTickLabel', handles.params.params_featExtraction.freq);


function train_classifier(hObject,handles)

fprintf('New Classifier Training \n')
[handles.prob,handles.output,handles.classifier] = apply_classifier_new(handles.training,...
    'cvType',handles.params.params_classifier.cvType,...
    'nFold',handles.params.params_classifier.nFold,....
    'featureSelection',handles.params.params_classifier.featureSelection,...
    'nbFeature2Select',handles.params.params_classifier.nbFeature2Select,...
    'classifierType',handles.params.params_classifier.classifierType,...
    'regularization',handles.params.params_classifier.regularization,...
    'featureSelection',handles.params.params_classifier.featureSelection,...
    'applyPCA',handles.params.params_classifier.applyPCA,...
    'variance2KeepPC',handles.params.params_classifier.variance2KeepPC,...
    'kernelType',handles.params.params_classifier.kernelType,...
    'pseudoOnline',handles.testing,...
    'trueProb',false);

plot_GUI_performance_classifier(handles);
plot_GUI_singleTrial_classification(handles);
plot_GUI_CR_over_time(handles);
plot_GUI_single_trial_detection(handles)
% prob = handles.prob;
% saveProcess = false;
% classification_smoothing_probabiliities;
fprintf('Done \n\n')
% Update handles structure
guidata(hObject, handles);

function plot_GUI_features_mapping(handles)

colormap('jet')
axes(handles.axes1);
imagesc(handles.powerFeatures)
add_separation_features_map(handles.powerFeatures)
plotPresent(handles)
set(handles.axes1,'Tag','DPPlot');
axes(handles.axes2);
imagesc(handles.selectedFeatures)
add_separation_features_map(handles.selectedFeatures)
plotPresent(handles)
set(handles.axes2,'Tag','SelectionPlot');

function plot_topoplot(axes17,axes18,selectedFeatures,locFile,freq)

axes(axes17);
cla;
plot_topo_features(selectedFeatures,{locFile.labels},{locFile.labels},...
    locFile,freq,'mu')
axes(axes18);
caxis([-1 1])
cla;
plot_topo_features(selectedFeatures,{locFile.labels},{locFile.labels},...
    locFile,freq,'beta')
caxis([-1 1])


function add_separation_features_map(selectedFeatures)

hold on;
[nChannel,nFreq] = size(selectedFeatures);
for i = 2:nFreq;
    line([i-0.5 i-0.5],[0.5 nChannel + 0.5],'Color',[0.8 0.8 0.8],'LineWidth',1)
end

for i = 2:nChannel;
    line([0.5 nFreq + 0.5],[i-0.5 i-0.5],'Color',[0.8 0.8 0.8],'LineWidth',1)
end



function opti_threshold = plot_GUI_performance_classifier(handles)
% Axes 
axes(handles.axes4);
cla;
[perf,threshold] = plot_performance_classifier(handles.output,...
    'showLegend',true,...
    'showTraining',true,...
    'showTesting',true,...
    'showRandom',false,...
    'showThreshold',true);
set(gca,'title',[])
set(gca,'xlabel',[])
set(gca,'ylabel',[])

axes(handles.axes13);
cla;
width = 0.5;
Y = [perf.TPR.train.mean*100,perf.TPR.test.mean*100];
for i = 1:2
    h=barh(i,Y(i),width);
    if i ==1
        set(h,'FaceColor',[ 0    0.4471    0.7412]);
    elseif i==2
        set(h,'FaceColor',[0.8510    0.3255    0.0980]);
    end
    hold on;
end
hold off
xlim([0 100])
ylim([0 3])
set(gca,'ytick',[]);


function plot_GUI_singleTrial_classification(handles)
axes(handles.axes15);
% cla;
% plot_singleTrial_classification(handles.prob.value,handles.prob.time,3);
% caxis([0 1])
% set(gca,'title',[])
% set(gca,'xlabel',[])
% set(gca,'ylabel',[])

cla reset
prob_values = cell2mat(handles.prob.value);
if strcmp(handles.params.params_online.detectionMethod{1},'evidenceAccumulation')
    prob_smoothed = evidence_accumulation(prob_values(:,2:2:end),handles.params.params_online.smoothing);
    plot_singleTrial_classification(prob_smoothed,handles.prob.time,2,3);
elseif strcmp(handles.params.params_online.detectionMethod{1},'movingAverage')
    prob_smoothed = moving_average_from_probabilities(prob_values,handles.params.params_online.nPoint2DetectOrSmooth);
    plot_singleTrial_classification(prob_smoothed,handles.prob.time,2,3);
else
    plot_singleTrial_classification(handles.prob.value,handles.prob.time,2,3);
end
caxis([0 1])
set(gca,'title',[])
set(gca,'xlabel',[])
set(gca,'ylabel',[])





function plot_GUI_CR_over_time(handles)
axes(handles.axes16);
cla reset
plot_CR_over_time(handles.prob.value,handles.prob.time,'Color',[0 0 0],'ColorPatch',[0.8 0.8 0.8],'showDeviation',{'stdError'},'threshold',[]);
hold on; 
prob_values = cell2mat(handles.prob.value);
if strcmp(handles.params.params_online.detectionMethod{1},'evidenceAccumulation')
    prob_smoothed = evidence_accumulation(prob_values(:,2:2:end),handles.params.params_online.smoothing);
    plot_CR_over_time(prob_smoothed,handles.prob.time,'Color',[1 0 0],'ColorPatch',[1 0.8 0.8],'showDeviation',{'stdError'},'threshold',handles.params.params_online.threshold);
elseif strcmp(handles.params.params_online.detectionMethod{1},'movingAverage')
    prob_smoothed = moving_average_from_probabilities(prob_values(:,2:2:end),handles.params.params_online.nPoint2DetectOrSmooth);
    plot_CR_over_time(prob_smoothed,handles.prob.time,'Color',[1 0 0],'ColorPatch',[1 0.8 0.8],'showDeviation',{'stdError'},'threshold',handles.params.params_online.threshold);
end
set(gca,'title',[])
set(gca,'xlabel',[])
set(gca,'ylabel',[])



function plot_GUI_single_trial_detection(handles)
axes(handles.axes14);
cla;
plot_single_trial_detection(handles.prob,...
    'detectionMethod',handles.params.params_online.detectionMethod,...
    'windowForCorrectTrial',handles.params.params_online.detectionFrame,...
    'parameter',handles.params.params_online.nPoint2DetectOrSmooth,...
    'smoothing_parameter',handles.params.params_online.smoothing,...
    'threshold',handles.params.params_online.threshold,...
    'showCorrectTrials',false);
set(gca,'title',[])
set(gca,'xlabel',[])
set(gca,'ylabel',[])


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uiresume(hObject);  


% --- Executes on selection change in listbox_features_algo.
function listbox_features_algo_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_features_algo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_features_algo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_features_algo

global axes17
global axes18
global locFile
global freqUsed

contents = cellstr(get(hObject,'String'));
handles.params.params_classifier.featuresSelection{1} = contents{get(hObject,'Value')};
handles.features_struct = feature_selection_online(handles.training,...
    'features2Select',handles.params.params_classifier.nbFeature2Select,....
    'featureSelection',handles.params.params_classifier.featuresSelection);

handles.powerFeatures = create_features_selection_matrix(handles.features_struct.Pfeature,handles.nFeature,handles.nChannel); %% HARD CODED
handles.maximumThreshold = max(max(handles.powerFeatures));
handles.minimumThreshold = min(min(handles.powerFeatures));
handles.threshold = 0.5*(handles.maximumThreshold+handles.minimumThreshold);
handles.selectedFeatures  = handles.powerFeatures > handles.threshold;
colormap('jet')
axes(handles.axes1);
imagesc(handles.powerFeatures)
add_separation_features_map(handles.powerFeatures)
plotPresent(handles)
set(handles.axes1,'Tag','DPPlot');
axes(handles.axes2);
imagesc(handles.selectedFeatures)
add_separation_features_map(handles.selectedFeatures)
plotPresent(handles)
plot_topoplot(axes17,axes18,handles.selectedFeatures,locFile,freqUsed)
set(handles.axes2,'Tag','SelectionPlot');
% Set up threshold slider
set(handles.slider1,'Value',handles.threshold ,'Enable','on');
set(handles.slider1,'Max',handles.maximumThreshold);
set(handles.slider1,'Min',handles.minimumThreshold);


% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function listbox_features_algo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_features_algo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_classifier.
function popupmenu_classifier_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_classifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_classifier contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_classifier
contents = cellstr(get(hObject,'String'));
handles.params.params_classifier.classifierType{1} = contents{get(hObject,'Value')};
disp(handles.params.params_classifier.classifierType{1})

if strcmp(handles.params.params_classifier.classifierType{1},'SVM')
    set(handles.popupmenu_kernel,'Enable','on');
else
    set(handles.popupmenu_kernel,'Enable','off');
end

train_classifier(hObject,handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_classifier_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_classifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_kernel.
function popupmenu_kernel_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_kernel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_kernel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_kernel
contents = cellstr(get(hObject,'String'));
handles.params.params_classifier.kernelType{1} = contents{get(hObject,'Value')};
disp(handles.params.params_classifier.kernelType{1})
train_classifier(hObject,handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_kernel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_kernel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_PCA.
function checkbox_PCA_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_PCA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_PCA
handles.params.params_classifier.applyPCA = logical(get(hObject,'Value'));
handles.params.params_classifier.regularization = false;

if handles.params.params_classifier.applyPCA
    str = 'on';
    set(handles.checkbox_Regularization,'Enable','off');
else
    str = 'off';
    set(handles.checkbox_Regularization,'Enable','on');
end
set(handles.slider_varianceExplained,'Enable',str);
set(handles.edit_varianceExplained,'Enable',str);

train_classifier(hObject,handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in checkbox_Regularization.
function checkbox_Regularization_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_Regularization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_Regularization
handles.params.params_classifier.regularization = logical(get(hObject,'Value'));
handles.params.params_classifier.applyPCA = false;

if handles.params.params_classifier.regularization
    set(handles.checkbox_PCA,'Enable','off');
else
   set(handles.checkbox_PCA,'Enable','on');
end

train_classifier(hObject,handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in popupmenu_detectionMethod.
function popupmenu_detectionMethod_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_detectionMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_detectionMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_detectionMethod
contents = cellstr(get(hObject,'String'));
handles.params.params_online.detectionMethod{1} = contents{get(hObject,'Value')};
disp(handles.params.params_online.detectionMethod{1})
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
plot_GUI_singleTrial_classification(handles)

if strcmp(handles.params.params_online.detectionMethod{1},'movingAverage')
    
    set(handles.slider_smoothing,'Enable','off');
    set(handles.edit_smoothing,'Enable','off');
    
    set(handles.slider_point2DetectOrSmooth,'Enable','on');
    set(handles.edit_point2DetectOrSmooth,'Enable','on');
    
   elseif strcmp(handles.params.params_online.detectionMethod{1},'evidenceAccumulation')
    
    set(handles.slider_smoothing,'Enable','on');
    set(handles.edit_smoothing,'Enable','on');
    
    set(handles.slider_point2DetectOrSmooth,'Enable','off');
    set(handles.edit_point2DetectOrSmooth,'Enable','off');
        
elseif strcmp(handles.params.params_online.detectionMethod{1},'accumulateDecision')
    set(handles.slider_smoothing,'Enable','off');
    set(handles.edit_smoothing,'Enable','off');
    
    set(handles.slider_point2DetectOrSmooth,'Enable','on');
    set(handles.edit_point2DetectOrSmooth,'Enable','on');
    
end


% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_detectionMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_detectionMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_smoothing_Callback(hObject, eventdata, handles)
% hObject    handle to edit_smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_smoothing as text
%        str2double(get(hObject,'String')) returns contents of edit_smoothing as a double

handles.params.params_online.smoothing = str2double(get(hObject,'String'));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
plot_GUI_singleTrial_classification(handles)
% Update handles structure
guidata(hObject, handles);




% --- Executes during object creation, after setting all properties.
function edit_smoothing_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_point2DetectOrSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to edit_point2DetectOrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_point2DetectOrSmooth as text
%        str2double(get(hObject,'String')) returns contents of edit_point2DetectOrSmooth as a double

handles.params.params_online.nPoint2DetectOrSmooth = str2double(get(hObject,'String'));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
plot_GUI_singleTrial_classification(handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_point2DetectOrSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_point2DetectOrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_threshold_Callback(hObject, eventdata, handles)
% hObject    handle to edit_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_threshold as text
%        str2double(get(hObject,'String')) returns contents of edit_threshold as a double
handles.params.params_online.threshold = str2double(get(hObject,'String'));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_threshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_smoothing_Callback(hObject, eventdata, handles)
% hObject    handle to slider_smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.params_online.smoothing = get(hObject,'Value');
set(handles.edit_smoothing,'String', num2str(handles.params.params_online.smoothing));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
plot_GUI_singleTrial_classification(handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_smoothing_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_threshold_Callback(hObject, eventdata, handles)
% hObject    handle to slider_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.params_online.threshold  = get(hObject,'Value');
set(handles.edit_threshold,'String', num2str(handles.params.params_online.threshold));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_threshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_point2DetectOrSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to slider_point2DetectOrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.params_online.nPoint2DetectOrSmooth = get(hObject,'Value');
set(handles.edit_point2DetectOrSmooth,'String',  num2str(handles.params.params_online.nPoint2DetectOrSmooth));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)
plot_GUI_singleTrial_classification(handles)
 % Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_point2DetectOrSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_point2DetectOrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit_detectionFrame1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_detectionFrame1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_detectionFrame1 as text
%        str2double(get(hObject,'String')) returns contents of edit_detectionFrame1 as a double
handles.params.params_online.detectionFrame(1) = str2double(get(hObject,'String'));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_detectionFrame1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_detectionFrame1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_detectionFrame2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_detectionFrame2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_detectionFrame2 as text
%        str2double(get(hObject,'String')) returns contents of edit_detectionFrame2 as a double
handles.params.params_online.detectionFrame(2) = str2double(get(hObject,'String'));
plot_GUI_single_trial_detection(handles)
plot_GUI_CR_over_time(handles)

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_detectionFrame2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_detectionFrame2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_opti_threshold.
function pushbutton_opti_threshold_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_opti_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.params.params_online.threshold = handles.opti_threshold;
set(handles.slider_threshold,'Value',handles.params.params_online.threshold);
set(handles.edit_threshold,'String',num2str(handles.params.params_online.threshold));
plot_GUI_CR_over_time(handles)
plot_GUI_single_trial_detection(handles)
% Update handles structure
guidata(hObject, handles);

function edit_varianceExplained_Callback(hObject, eventdata, handles)
% hObject    handle to edit_varianceExplained (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_varianceExplained as text
%        str2double(get(hObject,'String')) returns contents of edit_varianceExplained as a double
handles.params.params_classifier.variance2KeepPC = str2double(get(hObject,'String'));
set(handles.slider_varianceExplained,'Value',handles.params.params_classifier.variance2KeepPC);
train_classifier(hObject,handles)
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_varianceExplained_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_varianceExplained (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_varianceExplained_Callback(hObject, eventdata, handles)
% hObject    handle to slider_varianceExplained (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.params_classifier.variance2KeepPC = get(hObject,'Value');
set(handles.edit_varianceExplained,'String',  num2str(handles.params.params_classifier.variance2KeepPC));
train_classifier(hObject,handles)
% Update handles structure
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function slider_varianceExplained_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_varianceExplained (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function edit_nFeature2Select_Callback(hObject, eventdata, handles)
% hObject    handle to edit_nFeature2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_nFeature2Select as text
%        str2double(get(hObject,'String')) returns contents of edit_nFeature2Select as a double

global SelectedFeatures_workspace;
global axes17; 
global axes18;
global locFile;
global freqUsed;

handles.params.params_classifier.nbFeature2Select = str2double(get(hObject,'String'));
train_classifier(hObject,handles)
set(handles.slider8_nFeature2Select,'Value',handles.params.params_classifier.nbFeature2Select);

[~,orderedInd] = sort( handles.features_struct.Pfeature,'descend');
posFeature = 1:length(handles.features_struct.Pfeature);
selected_features = ismember(posFeature,orderedInd(1:handles.params.params_classifier.nbFeature2Select));
handles.selectedFeatures = create_features_selection_matrix(selected_features',handles.nFeature,handles.nChannel);   %% HARD CODED
SelectedFeatures_workspace = handles.selectedFeatures;
plot_GUI_features_mapping(handles)
plot_topoplot(axes17,axes18,handles.selectedFeatures,locFile,freqUsed);

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function edit_nFeature2Select_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_nFeature2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider8_nFeature2Select_Callback(hObject, eventdata, handles)
% hObject    handle to slider8_nFeature2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global SelectedFeatures_workspace;
global axes17; 
global axes18;
global locFile;
global freqUsed;

handles.params.params_classifier.nbFeature2Select = get(hObject,'Value');
set(handles.edit_nFeature2Select,'String',  num2str(handles.params.params_classifier.nbFeature2Select));
train_classifier(hObject,handles)

[~,orderedInd] = sort( handles.features_struct.Pfeature,'descend');
posFeature = 1:length(handles.features_struct.Pfeature);
selected_features = ismember(posFeature,orderedInd(1:handles.params.params_classifier.nbFeature2Select));
handles.selectedFeatures = create_features_selection_matrix(selected_features',handles.nFeature,handles.nChannel);   %% HARD CODED
SelectedFeatures_workspace = handles.selectedFeatures;
plot_GUI_features_mapping(handles)
plot_topoplot(axes17,axes18,handles.selectedFeatures,locFile,freqUsed);

% Update handles structure
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function slider8_nFeature2Select_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider8_nFeature2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
