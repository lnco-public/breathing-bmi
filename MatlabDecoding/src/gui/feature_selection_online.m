  function features_struct = feature_selection_online(training,varargin)
%TEST_CLASSIFIER extract features that will be used later by the
%classifier 
%   YPREDICTED = BUILD_CLASSIFIER_ONLINE(TRAINING). training is structure
%   where the temporal features(training.features.temporal) and
%   spectral features(training.features.spectral) are kept as well as the
%   label (training.label).
%   [...] = APPLY_CLASSIFIER_OFFLINE(..., 'PARAM1',VAL1,'PARAM2',VAL2,...) specifies
%   additional parameters and their values.  Valid parameters are the
%   following:
% 
%     Parameters                Value

%    'featuresSelection'       method use for features selection ('fisherScore','rankFeatures','none')
%                             (default 'none')
%    'features2Select'        number of features you want to keep
%    'feature2Use'            type of features you want to use ('spectral','temporal','both','fusion')
%                            (default 'both')
%    'regularization'         logical, use of sparse LDA if true (default true)
%    'delta'                  L1 regularization parameters for sLDA


%% Parameters

inputs = parse_my_inputs(training,varargin{:});

warning('off','all')

% label for training and testing
Y = training.label;
Y = reshape(Y',[],1);

if ~isempty(training.features.spectral)
xTrain_ini = training.features.spectral;
else
xTrain_ini = training.features.temporal;
end
xTrain_ini = change_structure_period(xTrain_ini);

% normalize features together
[xTrain,mu,sigma] = zscore(xTrain_ini);

%% Feature selection
if ~strcmp(inputs.featureSelection,'none')
    [xTrain_sub,SelectedFeatures,P] = features_selection(xTrain,Y,'nfeature2Select',inputs.features2Select,'method',inputs.featureSelection);
end

paramZscore = struct('mu',mu,'sigma',sigma);
features_struct = struct('SelectedFeatures',SelectedFeatures,'DC',xTrain,'Y',Y,'Pfeature',{P},...
    'paramZscore',paramZscore,'nbFeature2Select',...
    inputs.features2Select,'training',training);

end


function xAfter = change_structure_period(xBefore)

xAfter = [];
for iPeriod = 1:size(xBefore,1)
    t = squeeze(xBefore(iPeriod,:,:));
    xAfter= [xAfter ; t];
end

end


function funcInputs = parse_my_inputs(training,varargin)

funcInputs = inputParser;

addRequired(funcInputs, 'training',@isstruct);
addParameter(funcInputs,'features2Select',[],@isnumeric);
expectedType =  {'rankfeatures','corr','fisher','relief','infgain','none'};
addParameter(funcInputs, 'featureSelection',{'none'},@(x) any(validatestring(x{:}, expectedType)));

parse(funcInputs,training, varargin{:});
funcInputs = funcInputs.Results;

end
