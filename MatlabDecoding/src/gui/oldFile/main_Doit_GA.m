
% DESK
clear;
% close all
set(0,'DefaultFigureWindowStyle','docked'); % makes figures dock into the same window
set(0,'DefaultFigureWindowStyle','docked'); % makes figures dock into the same window
set(0,'defaultAxesFontSize',16)
set(0,'defaultLineLineWidth',2)

folderName ='C:\Users\bastien\Documents\internship\results\multitaper_ws025';
% folderName2 = 'C:\Users\bastien\Documents\internship\results\decoding_shortMI_day2';
% folderName2 = 'C:\Users\bastien\Documents\internship\results\decoding_shortMI_day3';
% folderName3 = 'C:\Users\bastien\Documents\internship\results\decodingOnAnticipationSession';


% File Processing
[subSubjectFolders,nFolder] = find_subject_folder(folderName);
strSubjectID = {subSubjectFolders(3:end).name};
fileSaveGA = fullfile(folderName,'GA');
mkdir(fileSaveGA)

global verbose saveProcess saveVariable
verbose = false;
saveProcess = true;
saveVariable = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Select GA to perform
folder_GA = 'C:\Users\bastien\Documents\internship\Miw\MOD_loop\projects\BCIcourse\GA';
analysisGA2Perform = get_task_perform(folder_GA);

%%
fprintf('*******************************\n');
fprintf('Starting Grand Average Script \n');

j = 1;

for iFolder = 3:nFolder
    
    fprintf('*******************************\n');
    fprintf(['Subject %d - ' subSubjectFolders(iFolder).name '\n'],j);
    fileSave = fullfile(folderName,subSubjectFolders(iFolder).name);
%       fileSave2 = fullfile(folderName2,subSubjectFolders(iFolder).name);
%       fileSave3 = fullfile(folderName3,subSubjectFolders(iFolder).name);
        % Analysis GA script
        for iAnalysis = 1:numel(analysisGA2Perform)
            
            thisAnalysis = analysisGA2Perform{iAnalysis};
            [~,thisAnalysis,~] = fileparts(thisAnalysis);
            feval(thisAnalysis)
        end
        
        j = j + 1;
end
fprintf('*******************************\n');
fprintf('Finished script \n');
