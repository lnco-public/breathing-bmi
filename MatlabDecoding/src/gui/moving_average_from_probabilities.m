function proSmoothed = moving_average_from_probabilities(prob_values,smooth_parameter)


nTrial = size(prob_values,1);
prob_values_temp = [ prob_values(:,1)*ones(1,smooth_parameter) prob_values];

for i = 1:size(prob_values,1)
    [~,p] = movavg(prob_values_temp(i,:),1,smooth_parameter,1);
    proSmoothed(i,:) = p(smooth_parameter+1:end);
end

end

