function spectrogramData = preprocess_runs_spectrogram(runProcessedData,params_spectrogram)


spectrogramData = runProcessedData;
global verbose
nRun = numel(runProcessedData);

fs = runProcessedData{1}.rate;
win = params_spectrogram.mlength*fs;
noverlap = win - params_spectrogram.wshift*fs;

for iRun = 1:nRun
    if verbose
        fprintf('\n*****************************************')
        fprintf('\n Spectrogram for run %d/%d \n',iRun,nRun)
    end
    thisRun = runProcessedData{iRun};
    
    PSD_channels = [];
    ii = 1;
    % Compute spectrogram
    for iChannel = params_spectrogram.selchans
       
        [~,freqgrid,~,psd] = spectrogram(thisRun.data(iChannel,:),win,noverlap,params_spectrogram.freq,fs);
            
        psd = log10(psd);
        % Selecting desired frequencies
        [freqs, idfreqs] = intersect(freqgrid,params_spectrogram.freq);
        PSD_channels(ii,:,:) = psd(idfreqs, :);
        ii = ii+1;
    end
    
    % Event alignement and subsampling
    cevents     = thisRun.event;
    events.name = cevents.name;
    events.position = proc_pos2win(cevents.position, params_spectrogram.wshift*thisRun.rate, 'backward', params_spectrogram.mlength*thisRun.rate);
    
    % Save structure for each run
    spectrogramData{iRun}.event = events;
    spectrogramData{iRun}.freq = freqs;
    spectrogramData{iRun}.data = PSD_channels;
    spectrogramData{iRun}.rate = 1/params_spectrogram.wshift;
    
    nChannel = size(thisRun.data,1);
    rmChannel = setdiff(1:nChannel,params_spectrogram.selchans);
    spectrogramData{iRun}.label(rmChannel) = [];
    spectrogramData{iRun}.map_ch = containers.Map(spectrogramData{iRun}.label,...
        1:length(spectrogramData{iRun}.label));
    
%     try
%     spectrogramData{iRun}.eloc(rmChannel) = [];
%     catch
% %         disp('issue with eloc')
%     end
    
    
    
    
end

end

