function runData = extract_signal_runs(folderName,loadStateCodeStr,params_loading,Data_inspection)
%EXTRACT_RUNS
global verbose
% File Processing
subSessionFolders = [dir([folderName filesep '*.gdf']);dir([folderName filesep '*.bdf'])];
nFile = numel(subSessionFolders);


if nargin  == 3
    Data_inspection = false;
end

%% Extract signal from runs
j = 1;
for iFile = 1:nFile
    if verbose
        disp('****************************')
        fprintf('******* Run %d/%d ******* \n',iFile,nFile)
    end
    fileName = fullfile(folderName,subSessionFolders(iFile).name);
    
    try
        % Extract data
        runData{j} = load_data(fileName,...
            'amplifierType',params_loading.amplifierType,...
            'processFolder',params_loading.processFolder);
        runData{j}.nameFile = subSessionFolders(iFile).name;
        runData{j}.state = feval(loadStateCodeStr);
        
        % Data inspection
        if Data_inspection
            figure()
            badChannels = plot_channel_inspection(runData{j},...
                'alignEvent',params_loading.alignEvent,...
                'nchannel',params_loading.chan_tot,...
                'interval',500,...
                'threshCorrelation',2,...
                'threshDeviation',2,....
                'verbose',{'on'});
        else
            badChannels  = {};
        end
        
        bad_channels = unique([badChannels params_loading.manualSeleCh]);
        bad_channels_struct = struct('name',bad_channels,'id',[]);
        for iBadCh = 1:length(bad_channels)
            chStr = bad_channels_struct(iBadCh).name;
            bad_channels_struct(iBadCh).id = runData{1}.map_ch(chStr);
            
        end
        runData{j}.bad_channels = bad_channels_struct;
        
        j = j+1;
    catch
        disp('cannot load data for this run')
    end
end


end

