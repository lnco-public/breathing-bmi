function spectrogramData = preprocess_runs_fastpwelch(runProcessedData,params_spectrogram)

spectrogramData = runProcessedData;
global verbose
nRun = numel(runProcessedData);
for iRun = 1:nRun
    if verbose 
        fprintf('\n*****************************************')
        fprintf('\nFast pwelch for run %d/%d \n',iRun,nRun)
    end
    thisRun = runProcessedData{iRun};
    % Compute fast spectrogram
    
    [psd, freqgrid] = proc_spectrogram(thisRun.data', params_spectrogram.wlength, params_spectrogram.wshift, params_spectrogram.pshift, thisRun.rate, params_spectrogram.mlength);
    psd = log(psd);
    
    bad_channel = find(psd(1,1,:) == -Inf);

    % Selecting desired frequencies
    [freqs, idfreqs] = intersect(freqgrid,params_spectrogram.freq);
    psd = psd(:, idfreqs, :);
    
    % Event alignement and subsampling
    cevents     = thisRun.event;
    events.name = cevents.name;
    events.position = proc_pos2win(cevents.position, params_spectrogram.wshift*thisRun.rate, 'backward', params_spectrogram.mlength*thisRun.rate);
    events.old_position = cevents.position;
    % Save structure for each run
    spectrogramData{iRun}.event = events;
    spectrogramData{iRun}.freq = freqs;
    spectrogramData{iRun}.data = permute(psd,[3 2 1]);
    spectrogramData{iRun}.data(bad_channel,:,:) = 0; 
    spectrogramData{iRun}.rate = 1/params_spectrogram.wshift;
    
    
end

end

