function [epochs,indexRun] = extract_epochs_runs(runProcessedData,alignEvent,timeBeforeEvent,timeAfterEvent,alignEvent_End,chan)

if nargin == 4
    alignEvent_End = [];
     chan = 1:16;
end

if nargin == 5 
      chan = 1:16;
end

%% Epoching
nRun = numel(runProcessedData);
indexRun = [];
for iRun = 1:nRun
    
    if ndims(runProcessedData{1}.data) == 3
        epochs{iRun}  = extract_psd_as_epochs(runProcessedData{iRun},...
            'alignEvent',{alignEvent},...
            'alignEvent_End',{alignEvent_End},...
            'timeBeforeEvent',timeBeforeEvent,...
            'timeAfterEvent',timeAfterEvent,...
            'channelID',chan);
    else
        epochs{iRun} = extract_epochs(runProcessedData{iRun},...
            'alignEvent',{alignEvent},...
            'alignEvent_End',{alignEvent_End},...
            'timeBeforeEvent',timeBeforeEvent,...
            'timeAfterEvent',timeAfterEvent,...
            'channelID',chan);
    end
    
    try
        if iRun == 3;
            epochs{iRun} = remove_trials(epochs{iRun},14);
        end
    catch
    end
    
    
    nTrialPerRun = epochs{iRun}.nTrial;
    indexRun = [indexRun iRun*ones(1,nTrialPerRun)];
end
epochs = concatenate_epochs(epochs);
epochs.indexRun = indexRun;

