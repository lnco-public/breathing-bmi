function set_step_to_perform(subjectFolder,fileSave,params_str,loadStateCodeStr,analysis2Perform,classification2Perform)
%SET_STEP_TO_PERFORM Summary of this function goes here
%   Detailed explanation goes here

%% Initiate variables;
runData = [];
runProcessedData = [];
feval(params_str)

%% Check if it is the same parameters 
statusAnalysis = check_parameters(fileSave,params);

%% Steps to perform based on statusAnalysis variable
if ~statusAnalysis.LoadedData 
    [statusAnalysis,runData] = loadingData_step(statusAnalysis,subjectFolder,fileSave,loadStateCodeStr,params);
end

if ~statusAnalysis.Preprocessed
    [statusAnalysis,runProcessedData] = preprocessing_step(statusAnalysis,fileSave,runData,params);
end

if ~isempty(analysis2Perform)
    analysis_step(subjectFolder,fileSave,analysis2Perform,runProcessedData,runData,params)
end
 
    
if ~isempty(classification2Perform)
    classification_step(fileSave,classification2Perform,runProcessedData,params)
end

end

function statusAnalysis = check_parameters(fileSave,params)

params_current = importdata([fileSave filesep 'params.mat']);
statusAnalysis = importdata([fileSave filesep 'statusAnalysis.mat']);

cond1 = ~isequal(params_current.params_loading,params.params_loading);
cond2 = ~isequal(params_current.params_preprocessing,params.params_preprocessing) || cond1;

if cond1
    statusAnalysis.LoadedData = false;
    fprintf('parameters modified for loading data\n')
end

if cond2
    statusAnalysis.Preprocessed = false;
    fprintf('parameters modified for preprocessing of the data\n')
end

end


function [statusAnalysis,runData] = loadingData_step(statusAnalysis,subjectFolder,fileSave,loadStateCodeStr,params)

global saveVariable
% Extract signals from all the runs in folderName
runData = extract_signal_runs(subjectFolder,loadStateCodeStr,params.params_loading);
statusAnalysis.LoadedData = true;

if saveVariable
    save([fileSave filesep 'statusAnalysis.mat'],'statusAnalysis');
    save([fileSave filesep 'runData.mat'],'runData')
end
end

function [statusAnalysis,runProcessedData] = preprocessing_step(statusAnalysis,fileSave,runData,params)

global saveVariable
if isempty(runData)
    runData = importdata([fileSave filesep 'runData.mat']);
end

% Preprocessing
if strcmp(params.params_preprocessing.correctionArtefact{1},'EOG')
    try
        params_EOG = importdata('params_EOG.mat');
        
        if isempty(params_EOG)
            disp('no EOG correction applied')
        else
            disp('EOG correction applied')
        end
        
    catch
        params_EOG = [];
        disp('no EOG correction applied')
    end
    [runProcessedData,params_online_filter] = preprocess_signal_runs(runData,params.params_preprocessing,params_EOG);
end


[runProcessedData,params_online_filter] = preprocess_signal_runs(runData,params.params_preprocessing);
statusAnalysis.Preprocessed = true;

parameters = params;
parameters.params_online_filter = params_online_filter;

if saveVariable
    save([fileSave filesep 'statusAnalysis.mat'],'statusAnalysis');
    save([fileSave filesep 'runProcessedData.mat'],'runProcessedData');
    save([fileSave filesep 'params.mat'],'parameters');
end
end

function analysis_step(subjectFolder,fileSave,analysis2Perform,runProcessedData,runData,params)

global saveProcess


if isempty(runProcessedData)
    runProcessedData = importdata([fileSave filesep 'runProcessedData.mat']);
end

if isempty(runData)
    runData = importdata([fileSave filesep 'runData.mat']);
end


for iAnalysis = 1:numel(analysis2Perform)
    thisAnalysis = analysis2Perform{iAnalysis};
    [~,thisAnalysis,~] = fileparts(thisAnalysis);
        feval(thisAnalysis)
end


end


function classification_step(fileSave,classification2Perform,runProcessedData,params)

global saveProcess


if isempty(runProcessedData)
    runProcessedData = importdata([fileSave filesep 'runProcessedData.mat']);
end


for iClassification = 1:numel(classification2Perform)
    thisClassification = classification2Perform{iClassification};
     [~,thisClassification,~] = fileparts(thisClassification);
        feval(thisClassification)
end

end
