


laplacian64(1:64,1:64) = laplacian64(1:64,1:64)./(-1*(sum(laplacian64(1:64,1:64),2)-1)*ones(1,64));
laplacian64(61:64,:) = 0;
for i = 1:64
laplacian64(i,i) = 1;
end