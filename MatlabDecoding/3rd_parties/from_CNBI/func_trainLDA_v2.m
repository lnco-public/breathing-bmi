function [ w,b, d, accuracy, predicted, distance ] = func_trainLDA_v2( train, test, regularizeCovariance )


        d = [];
        if (nargin == 2)
            regularizeCovariance = 0;
        end

        labelsTrain = train.labels;
        dataTrain = train.features;

        labelsTest = test.labels;
        dataTest = test.features;

        indexOnset = (labelsTrain==1);
        indexRest = (labelsTrain==0);


        cov1 = cov(dataTrain(indexRest,:))';
        cov2 = cov(dataTrain(indexOnset,:))';

        covariance = cov(dataTrain);
%         covariance = (cov1+cov2)/2;

        % Check why this is happening
        mu1 = mean(dataTrain(indexRest,:))';
        mu2 = mean(dataTrain(indexOnset,:))';
        mu = (mu1 + mu2) ./ 2;
        
        
        numFeatures = size(dataTrain,2);
        
        if (regularizeCovariance) && (numFeatures > 1) % We cannot regularize with 1 feature!!!
            
%             % Regularization (see page 218 of the book "Toward brain-computer
%             % interfacing" Dornhege and Millan, for more information).
%             % We compute the optimal lambda (lambdaStar) for the shrinkage following 
%             % Single-trial analysis and classification of ERP components � A tutorial        
%             n = size(dataTrain,1);
%             d = size(dataTrain,2);
%             z = zeros(d,d,n);
% 
%             for k=1:n
%                 z(:,:,k) = (dataTrain(k,:)' - mu) * (dataTrain(k,:)' - mu)';
%             end
%             
%             v = trace(covariance) / size(covariance,1);
%             z = sum(sum(var(z,0,3)));
%             
% 
%             denominador = sum(sum(covariance-diag(diag(covariance)))) + sum((diag(covariance)-v).^2);            
%             lambdaStar = (n/(n-1)^2) * z / denominador;
            
            if (regularizeCovariance < 1)
%                 disp(['Regularizing by a factor of ' num2str(regularizeCovariance)]);
                lambdaStar = regularizeCovariance;
            end
            
            covariance = (1-lambdaStar)*covariance + (lambdaStar/size(dataTrain,2)) * trace(covariance) * eye(size(covariance));
        end
        
        w = pinv(covariance) * (mu2 - mu1);
        b = -(w') * mu;
                   
        [accuracy, predicted, distance] = calculate_accuracy(labelsTest, dataTest, w, b);
        
        distance = 1 ./ (1 + exp(-distance));

end


function [accuracy, predicted, distance] = calculate_accuracy(labelsTest, dataTest, w, b)

    predicted = w'*dataTest'+b';

    distance = predicted;

    predicted = predicted';
    predicted(predicted>=0) = 1;
    predicted(predicted<0) = 0;
    accuracy= [];
%     indexC = find(labelsTest == 1);
%     indexE = find(labelsTest == -1);
%     accuracy(1) = sum(predicted(indexC) == labelsTest(indexC)) ./ length(indexC);
%     accuracy(2) = sum(predicted(indexE) == labelsTest(indexE)) ./ length(indexE);
%     accuracy(3) = sum(predicted == labelsTest) ./ length(predicted);

    
    
    
end

