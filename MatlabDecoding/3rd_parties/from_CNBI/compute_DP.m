function DP = compute_DP(s, tSelect, winsize)

% This function is used of calculating the discriminant power of each
% channel between movement intention and idle class.

nChannel =  s.nchan;
rate= s.rate;
epoch_data = s.epochs;
baseline_data = s.baselines;

time = s.intervalEpoch(1):1/rate:s.intervalEpoch(2);

basewsize= tod_check_getWinFs(rate,winsize);
baseoffset = basewsize;
wsize = basewsize/2;
nBaseline = size(baseline_data,1);

% idle features 
for iBaseline = 1: nBaseline
       thisBaseline = squeeze(baseline_data(iBaseline,:,:));
       f_base(iBaseline,:,:) = thisBaseline(1:nChannel,baseoffset+1:2*baseoffset);
end

f_base = reshape(f_base,nBaseline,nChannel*size(f_base,3)); %idle features
d_base = ones(size(f_base,1),1); %idle label 

% active features
tInterest = find(time >= tSelect,1,'first');
f_onset = epoch_data(:,:,tInterest-wsize+1:tInterest+wsize); % active features
f_onset = reshape(f_onset,nBaseline,nChannel*size(f_onset,3));
d_onset = ones(size(f_onset,1),1)*2;% active label

DP = cva_tun([[d_base f_base];[d_onset f_onset]]);

iChannel = 1;
for i=1:16:size(DP,1)
    DP_ch(iChannel) = mean(DP(i:i+baseoffset-1));
    iChannel = iChannel+1;
end

end

