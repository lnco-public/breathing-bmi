import socket
import mne
import os
import time
import numpy as np
from pycnbi import logger
import pycnbi.utils.pycnbi_utils as pu
from pycnbi.utils import q_common as qc
from pycnbi.stream_receiver.stream_receiver import StreamReceiver

#----------------------------------------------------------------------
# Parameters to define
#----------------------------------------------------------------------
CH_INDEX = [65]                 # channels' index to monitor

streamBuffer = 10                    # Stream buffer [sec]

windowSize = 10                    # window length of acquired data when calling get_window [sec]

n_jobs = 1                          # For multicore PSD processing

os.environ['OMP_NUM_THREADS'] = '1' # actually improves performance for multitaper
mne.set_log_level('ERROR')          # DEBUG, INFO, WARNING, ERROR, or CRITICAL


#----------------------------------------------------------------------
# UDP Communication
#----------------------------------------------------------------------

source_IP = "127.0.0.1"
source_port = 1011

receiver_IP = "127.0.0.1"
receiver_PORT =1010
MESSAGE = "start".encode()

print ("UDP target IP:", receiver_IP)
print ("UDP target port:", receiver_PORT)
print ("message:", MESSAGE)

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind((source_IP,source_port))
sock.sendto(MESSAGE, (receiver_IP, receiver_PORT))


#----------------------------------------------------------------------
# Find LSL stream to acquire the EEG data
#----------------------------------------------------------------------
logger.info('Looking for lsl streams')
amp_name, amp_serial = pu.search_lsl()

# Open the stream
sr = StreamReceiver(window_size=windowSize, buffer_size=streamBuffer, amp_serial=amp_serial, eeg_only=False, amp_name=amp_name)

sfreq = sr.get_sample_rate()        # Sampling rate
trg_ch = sr.get_trigger_channel()   # Trigger channel

#----------------------------------------------------------------------
# Main
#----------------------------------------------------------------------
watchdog = qc.Timer()
tm = qc.Timer(autoreset=True)
last_ts = 0
expirationPhaseDetected = True
inspirationPhaseDetected = True
thresold = 0.25

try:
    while True:
        sr.acquire()
        window, tslist = sr.get_window()    # window = [samples x channels]
        window = window.T                   # chanel x samples
        tsnew = np.where(np.array(tslist) > last_ts)[0]
        if len(tsnew) == 0:
            logger.warning('There seems to be delay in receiving data.')
            time.sleep(1)
            continue
        # Detection for activate arm
        data = window[CH_INDEX,:];
        mu = np.mean(data)  
        min_data = np.min(data)
        max_data = np.max(data)
        new_mu = max_data + min_data

       signData = np.sum(np.sign(data))


        if window[CH_INDEX,-1] > thresold*max_data  and inspirationPhaseDetected:
            print("expiration phase")
            MESSAGE = "99".encode()
            print ("message:", MESSAGE)
            expirationPhaseDetected = True
            inspirationPhaseDetected = False
            sock.sendto(MESSAGE,(receiver_IP, receiver_PORT))
            expirationPhaseDetected = True
        elif window[CH_INDEX,-1] < thresold*min_data and expirationPhaseDetected:
            print("inspiration phase") 
            inspirationPhaseDetected = True
            expirationPhaseDetected = False
            MESSAGE = "88".encode()
            print ("message:", MESSAGE)
            sock.sendto(MESSAGE, (receiver_IP, receiver_PORT))
        last_ts = tslist[-1]
        tm.sleep_atleast(0.5)
except KeyboardInterrupt:
    print('Done.')