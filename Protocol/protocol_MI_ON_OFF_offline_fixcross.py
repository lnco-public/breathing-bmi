
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import configparser
import sys, time, math, string
import sys
from PyQt5 import QtGui,QtWidgets,QtCore
from PyQt5.QtCore import QThread, pyqtSignal,Qt,pyqtSlot
import pycnbi.triggers.pyLptControl as pyLptControl
from time import *
import numpy as np
import math
from datetime import datetime
import os
import random
import socket


class Thread(QThread):
    
    msg_Threading = 0
    UDP_IP = "127.0.0.1"
    UDP_PORT = 1010
    CUE_ON_EXHALE = False
    CUE_ON_INHALE = False
    
    def __init__(self,phase):
        super(Thread,self).__init__()
        self.phase = phase

    def udpate_value(self,value):
       self.phase = value
       print(self.phase)
       print('mdofied')

    def run(self):
        self.sock = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
        self.sock.bind((self.UDP_IP,self.UDP_PORT))

        while True:

            if self.phase == 'ST_END':
                print ("end of loop")
                break;

            data, addr = self.sock.recvfrom(1024) # buffer size is 1024 bytes
            print ("received message:", data.decode())
            msg_received = data
            if msg_received == "99".encode():
                print('expiration phase detection received')
                self.CUE_ON_EXHALE = True
                self.CUE_ON_INHALE = False
            elif msg_received == "88".encode():
                print('inspiration phase detection received')
                self.CUE_ON_INHALE = True
                self.CUE_ON_EXHALE = False
            else: 
                self.CUE_ON_EXHALE = False
                self.CUE_ON_INHALE = False

class TrainingMI(QtWidgets.QMainWindow):
    
    def __init__(self):
        super(TrainingMI,self).__init__()        
        self.initUI()
                  
    def initUI(self): 
        # [LOADING PARAMS] PROTOCOL CONFIG FILE     
        if (len(sys.argv) == 1):
            self.config_file = "protocol_configuration_MI.ini"
        else:
            if (string.find(sys.argv[1], '.ini') == -1): 
                self.config_file = str(sys.argv[1]) + ".ini"
            else:
                self.config_file = str(sys.argv[1])

        print("[Loading ConfigFile] Using configuration file: ", self.config_file)
        config = configparser.RawConfigParser(allow_no_value=True)
        config.read("./" + self.config_file)  
        
        # [LOADING PARAMS] EXPERIMENT
        print('[Loading Params]  Read Configuration File')
        self.maxnTrials = config.getint("offline", "nTrial");
        self.nExhaleTrials = config.getint("offline", "nExhale");
        self.nTarget = config.getint("experiment", "nTarget")
        self.refresh_time = config.getfloat("experiment", "refreshCycle");

        # [LOADING PARAMS] INTERFACE
        self.full_screen = config.getboolean("interface", "FullScreen")   
        self.connect_to_loop = config.getboolean("interface", "connectLoop")
        
        # [LOADING PARAMS] OPTIONS
        listTask = ['onset','offset']
        self.choiceTask = config.getint("options", "choiceTask")
        self.choiceTask = listTask[self.choiceTask-1]
        print("[SELECTED TASK] " + self.choiceTask + " task condition")

        # [LOADING PARAMS] STATE
        self.nsec_relax = config.getint("state", "relax")
        self.nsec_fixcross = config.getint("state", "fixationCross")
        self.nsec_start = config.getint("state", "start")
        self.nsec_mi = config.getint("state", "mi")
        self.nsec_stop = config.getint("state", "stop")
        self.nsec_end = config.getint("state", "end")

        # PARAMETERS INITIALIZATION
        self.ready = False
        self.phase = 'ST_WAIT'
        self.iTrial = 1
        self.msg2send = 123
        self.phaseJustChanged = False
        self.counter = 0;   
        self.buttonPress = False
        self.buttonEnterPress = False          
        self.l1 = 50 
        self.l2 = 10
        self.entry = 0 # selfpaced paradigm decision
        self.target = 0 # cue based randomly decided
        self.stopDone = False
        self.previous_counter = 0
        self.counterStartJittering = random.random()
        self.nsec_wait = 1000
        self.sendTarget = None
        
        # [THREADING INITIALIZATION]
        self.th = Thread(self.phase)
        self.th.start()


        self.prepare_order_trials_exhale()
        
        # CREATING WINDOW
        if(self.full_screen):
            self.screen_width = QtWidgets.QDesktopWidget().screenGeometry().width()-200
            self.screen_height = QtWidgets.QDesktopWidget().screenGeometry().height()-200
         
        else:
            self.screen_width = 300
            self.screen_height = 300

        self.setWindowTitle('Onset Offset Experiment')
        self.setStyleSheet("background-color: 'black'")
        self.statusBar().setStyleSheet("color: 'white';font-weight:bold")
        self.setGeometry(10,10, self.screen_width, self.screen_height)
             
        if(~self.full_screen):        
            screen = QtWidgets.QDesktopWidget().screenGeometry()
            size = self.geometry()
            self.move((screen.width()-size.width())/2,(screen.height()-size.height())/2)
        self.pos_x = self.screen_width/2
        self.pos_y  = self.screen_height/2      
        self.show()
        
        # CREATING TIMER
        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.loop)
        self.timer.start(self.refresh_time)

        # connect to the loop 
        if(self.connect_to_loop):
             self.trigger =  pyLptControl.Trigger('SOFTWARE')
    
    def loop(self):
        t = time()
       
        self.phaseJustChanged = False;
        self.counter +=1
        
        if self.iTrial > self.maxnTrials:
            self.phase = 'ST_END'

        if self.phase == 'ST_END' and self.counter == round(self.nsec_end/self.refresh_time):
            self.th.udpate_value(self.phase)
            sleep(5)
            print('[END EXPERIMENT] Send end event')
            self.send_event_if_connected(999)  
            print('[END EXPERIMENT] Close interface')
            self.connect_to_loop = 0
            self.close()
            print('[END EXPERIMENT] Stop connection loop')

        if self.ready:
            if self.phase == 'ST_START' and self.counter == round(self.nsec_start/self.refresh_time):
                    self.counter = 0
                    print ("*************************** Trial  " + str(self.iTrial) +  " ***************************")
                    
                    if self.trialsexhale[:,self.iTrial-1]:
                        print ("[TRIALS CONDITION] Exhale")
                    else:
                    	print ("[TRIALS CONDITION] Inhale")
                    self.phase = 'ST_HOLD'
                    self.phaseJustChanged = True
                    self.stopDone  = False
                    self.startMI = 0
                    self.counterStartJittering = random.random()
            
            if self.phase == 'ST_HOLD' and self.counter == round((1 + self.counterStartJittering)*(self.nsec_fixcross/self.refresh_time)):
                    print('Time elapsed before task: ' + str(round((1 + self.counterStartJittering)*self.nsec_fixcross)))
                    self.phase = 'ST_MI_TASK';
                    self.counter= 0;
                    self.th.CUE_ON_INHALE = False
                    self.th.CUE_ON_EXHALE = False
                    self.phaseJustChanged = True

            if self.phase == 'ST_MI_TASK':
                    
                    if self.choiceTask == 'onset': 
                        if (self.th.CUE_ON_EXHALE and self.trialsexhale[:,self.iTrial-1]) or \
                        (self.th.CUE_ON_INHALE and self.trialsexhale[:,self.iTrial-1] == False):
                            print('Time elapsed before start: ' + str(self.counter*self.refresh_time))
                            self.counter = 0
                            self.phaseJustChanged = True
                            self.phase = 'ST_MI_START'
                    
                    elif self.choiceTask == 'offset':
                            print('Time elapsed before start: ' + str(self.counter*self.refresh_time))
                            self.counter = 0
                            self.phaseJustChanged = True
                            self.th.CUE_ON_INHALE = False
                            self.th.CUE_ON_EXHALE = False
                            self.phase = 'ST_MI_START'
                    
            if self.phase == 'ST_MI_START':
                    
                    if self.choiceTask == 'onset' and self.counter >= round(self.nsec_mi/self.refresh_time):
                            print('Time elapsed before stop: ' + str(self.counter*self.refresh_time))
                            self.counter = 0
                            self.phaseJustChanged = True
                            self.phase = 'ST_MI_STOP'

                    elif self.choiceTask == 'offset': 
                            if (self.th.CUE_ON_EXHALE and self.trialsexhale[:,self.iTrial-1] and self.counter >= round(self.nsec_mi/self.refresh_time)) or \
                            (self.th.CUE_ON_INHALE and self.trialsexhale[:,self.iTrial-1] == False and self.counter >= round(self.nsec_mi/self.refresh_time)):
                                print('Time elapsed before stop: ' + str(self.counter*self.refresh_time))
                                self.counter = 0
                                self.phaseJustChanged = True
                                self.phase = 'ST_MI_STOP'

            if self.phase == 'ST_MI_STOP' and self.counter >= round(self.nsec_stop/self.refresh_time):
                    print('Time elapsed after stop: ' + str(self.counter*self.refresh_time))
                    self.counter = 0
                    self.phaseJustChanged = True
                    self.phase = 'ST_RELAX'

            if self.phase == 'ST_RELAX' and self.counter == round(self.nsec_relax/self.refresh_time):
                    self.phase = 'ST_START'  
                    self.counter= 0
                    self.sendTarget = None
                    self.phaseJustChanged = True    
        self.handle_tobiid_output()

        if self.phase ==  'ST_START' and self.counter == 0:
                self.iTrial += 1

        self.repaint()
        elapse = time() -t;
        
    def handle_tobiid_output(self):
        if self.phase == 'ST_START':
            self.msg2send = 10
        elif self.phase == 'ST_HOLD':
            self.msg2send = 20
        elif self.phase == 'ST_MI_TASK':
            self.msg2send = 30
        elif self.phase == 'ST_MI_START':
            if self.choiceTask == 'onset':
                self.msg2send = 43
            elif self.choiceTask == 'offset':
                self.msg2send = 40
        elif self.phase == 'ST_MI_STOP':
            if self.choiceTask == 'onset':
                self.msg2send = 55
            elif self.choiceTask == 'offset':
                self.msg2send = 50
        elif self.phase == 'ST_RELAX':
            self.msg2send = 70
        elif self.phase == 'ST_END':
            self.msg2send = 99
            
        if self.phase != 'ST_END': 
            if self.trialsexhale[:,self.iTrial-1] == True and self.phase =='ST_MI_START' and self.choiceTask == 'onset':
                self.msg2send +=1        
            elif self.trialsexhale[:,self.iTrial-1] == True and self.phase == 'ST_MI_STOP' and self.choiceTask == 'offset':
                self.msg2send +=1        

        if self.phaseJustChanged:
            self.send_event_if_connected(self.msg2send);    
    
    def paintEvent(self, e):
        
        qp = QtGui.QPainter()
        qp.begin(self)
        self.paintInterface(qp)
        qp.end()
       
    def paintInterface(self, qp):
        
        metrics = qp.fontMetrics()
        col = QtGui.QColor(200, 200, 200)
        col.setNamedColor('#d4d4d4')
        qp.setPen(col)
        qp.setBrush(QtGui.QColor(200, 200, 200))
         
        if self.phase == 'ST_WAIT' or self.phase == 'ST_END':
            pen = QtGui.QPen(QtCore.Qt.white, 5, QtCore.Qt.SolidLine)
            qp.setPen(pen)
            qp.setFont(QtGui.QFont('Decorative', 50))
           
            if self.phase == 'ST_WAIT': 
                qp.drawText(self.rect(),QtCore.Qt.AlignCenter, "Hello! \n Press 'Enter' when you are ready") 
            else:    
                qp.drawText(self.rect(),QtCore.Qt.AlignCenter, "End run")      

        else:
        	# Show trials 
            self.center = QtCore.QPoint(self.pos_x,self.pos_y)
            pen = QtGui.QPen(QtCore.Qt.gray, 5, QtCore.Qt.SolidLine)
            qp.setPen(pen)
            qp.setFont(QtGui.QFont('Decorative', 30))
            text = "Trial " +  str(self.iTrial) + "/" + str(self.maxnTrials)
            textWidth = metrics.width(text)    
            qp.drawText( self.center.x()-2*textWidth,100,text) 
            qp.translate(self.center.x(),self.center.y())

            if self.phase != 'ST_RELAX' or self.phase !='ST_REPORT':
                self.draw_fixation_cross(qp)
                    
            if self.phase == 'ST_RELAX':
                self.write_text(qp,metrics,"RELAX")
               
            if self.phase == 'ST_MI_START':
                col_start = QtGui.QColor(60, 200, 60)
                self.draw_fixation_cross(qp,col_start)


            if self.phase == 'ST_MI_STOP':
                col_stop = QtGui.QColor(200, 60, 60)
                self.draw_fixation_cross(qp,col_stop)           		
                
                
    def draw_fixation_cross(self,qp,color=QtCore.Qt.gray):
        pen = QtGui.QPen(color,5, QtCore.Qt.SolidLine)
        qp.setBrush(color)
        qp.setPen(pen)
        qp.drawRect( - self.l1/2, -self.l2/2, self.l1, self.l2);
        qp.drawRect( - self.l2/2, -self.l1/2, self.l2, self.l1);
          
    def write_text(self,qp,metrics,text):
        pen = QtGui.QPen(QtCore.Qt.gray, 5, QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setFont(QtGui.QFont('Decorative', 30))
        textWidth = metrics.width(text)
        qp.drawText(-textWidth-20,-40,text) 

    def find_last_txt_file_recorded(self, PathData):
    	listFile = os.listdir(PathData)
    	try:
    		timingFile = listFile[-1]
    	except: 
    		timingFile = 'None'
    	return timingFile 

    def read_file_for_finding_timing(self,file):
        f= open(file,"r+")
        lines=f.readlines()
        start=[]
        stop=[]
        for x in lines:
            start.append(int(x.split(' ')[1]))
            stop.append(int(x.split(' ')[2]))
        f.close()

        stop = np.array(stop)
        start = np.array(start)

        index = np.arange(len(lines))
        index = np.random.permutation(index)
        stop = stop[index]
        start = start[index]
        return start,stop

    def prepare_order_trials_exhale(self):
        self.trialsexhale = np.full((1,self.maxnTrials), False, dtype=bool)
        array = np.random.permutation(self.maxnTrials)
        self.trialsexhale[:,array[:self.nExhaleTrials]] = True
        print(self.trialsexhale)

    def send_event_if_connected(self,msg):
        
         if(self.connect_to_loop):
            print("[TRIGGERS]  " + self.phase + " " + str(msg) + " sent to loop")
            self.trigger.signal(int(msg))
         else:
            print ("[TRIGGERS]  " + self.phase + " " + str(msg) + " NOT SENT")             

    def keyPressEvent(self, e):
            
        if e.key() == QtCore.Qt.Key_Escape:
            self.ready = True
            if self.phase == 'ST_WAIT':
                print('[Start Experiment] Total Trials : ' + str(self.maxnTrials))
                self.phase = 'ST_START'
                self.counter = 0
                
        if e.key() == QtCore.Qt.Key_Backspace:
                self.phase = 'ST_END'
                print('[Keyboard Entry] Esc')
                self.counter = 0

        if e.key() == QtCore.Qt.Key_Backspace:
            self.counter = 0
            self.buttonPress = False
            self.entry = 0
            print("[ENTRY]  return")

        if e.key() == QtCore.Qt.Key_Enter:
            self.buttonEnterPress = True
            self.counter = 0
            print ("[ENTRY]  you selected number " + str(self.entry))
            
        if ((e.key()>=QtCore.Qt.Key_0) & (e.key()<=QtCore.Qt.Key_9)):
            self.buttonPress = True
            self.entry = self.entry*10+e.key()-QtCore.Qt.Key_0
            self.buttonEnterPress = False
        
        if e.key()==QtCore.Qt.Key_A and self.connect_to_loop:
                self.trigger.signal(int(1))    
                print ("[KeyBoard Entry]  A")              
        if e.key()==QtCore.Qt.Key_S and self.connect_to_loop:
                self.trigger.signal(int(2))
                print ("[KeyBoard Entry]  B")
                    
        if e.key()==QtCore.Qt.Key_P:
            if self.ready:
                self.ready = False
                self.counter = 0
            else:
                self.ready = True;
                self.counter = 0
            print ("[KeyBoard Entry]  PAUSE")
            msg = '88'
            self.send_event_if_connected(msg)
                           
if __name__ == '__main__':
    
    app = QtWidgets.QApplication(sys.argv)
    ex = TrainingMI()
    sys.exit(app.exec_())
