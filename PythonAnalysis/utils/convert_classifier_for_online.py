# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 10:16:11 2019

@author: bastien
"""
import pickle 
import os 
import sys
sys.path.append("..")
from utils.saving_functions import saving_figure,saving_var

def convert_classifier_for_online(classifierFile,save_cls=True):

    ''' Open template classifier '''
    template_classifier = r'C:\Users\bastien\Documents\Git\python_analysis\template_classifier\classifier-64bit.pkl'
    classifier_struct = pickle.load(open(template_classifier,'rb'))
    
    ''' Open classifier '''
    cls = pickle.load(open(classifierFile,'rb'))
    folder2Save = os.path.split(classifierFile)[0]
    
    classifier_struct['cls'] = cls
    print('classifier changed inside struct')
    
    if save_cls:
        saving_var('cl_online',folder2Save,classifier_struct)
    
    return classifier_struct

if __name__=='__main__':
    
    classifierFile = r'C:\Users\bastien\Documents\internship\results\test_python_3class_new\a1\cl_online'
    classifier = convert_classifier_for_online(classifierFile)