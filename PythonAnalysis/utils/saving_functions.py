
import pickle
from matplotlib import pyplot as plt
import os

def saving_figure(filename,path,fig):
    try:
        os.stat(path)
    except:
        os.mkdir(path)
    filename = os.path.join(path,filename)    
    fig.savefig( filename + '.png', bbox_inches='tight')
    fig.savefig( filename + '.svg', bbox_inches='tight')
    fig.savefig(filename + '.pdf', bbox_inches='tight')
    
def saving_var(filename,path,var):
    filename = os.path.join(path,filename)    
    pickle.dump(var, open(filename,'wb'))
