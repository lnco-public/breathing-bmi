# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 17:46:30 2019

@author: basti
"""

import configparser

def read_config_files(fileName):
    p = configparser.ConfigParser()
    p.read(fileName)    
    dict_config = {s:dict(p.items(s)) for s in p.sections()}
#    dict_config = p.__dict__['_sections'].copy()

    return dict_config