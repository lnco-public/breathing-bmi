# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 17:25:34 2019

@author: basti
"""

import pickle
import os
from pycnbi.utils.convert2fif import pcl2fif

def find_all_recordings(folderName):
    fnames_raws = []
    fnames_events = []
    for f in os.listdir(folderName):
        if f.endswith('raw.pcl'):
            fnames_raws.append(os.path.join(folderName,f))
        if f.endswith('eve.txt'):
            fnames_events.append(os.path.join(folderName,f))
    return fnames_raws,fnames_events 
    
def pcl2fif_offset(folderName):
    """ Convert pcl to fif file with offset"""
    fnames_raws,fnames_events  = find_all_recordings(folderName)
    
    for filename,eventFile in zip(fnames_raws, fnames_events):
        v =pickle.load(open(filename,'rb'))
        lsl_time_offset = v['lsl_time_offset']
        pcl2fif(filename, interactive=False, outdir=None, external_event=eventFile, offset=lsl_time_offset, overwrite=True, precision='single')

def add_events_relative_to_others(event_file):
    event = np.loadtxt(event_file)[:, -1]
    mid = np.loadtxt(event_file)[:, 1]
    time = np.loadtxt(event_file)[:, 0]
    index_event = np.where(event==555)
    
    new_event= 44*np.ones(np.size(index_event))
    new_time = time[index_event] -2
    
    time = np.concatenate((time,new_time),axis=0)
    event = np.concatenate((event,new_event),axis=0)
    
    index_sorting = np.argsort(time)
    time = np.sort(time)
    event = event[index_sorting]
    mid = np.zeros(np.size(event))
    
    mat = np.matrix([time,mid,event])
    mat = mat.transpose()
    a = os.path.split(event_file)[0]
    
    file = os.path.join(a,"testfile.txt")
    
    with open(event_file,"w+") as f:
        for line in mat:
            np.savetxt(f, line, fmt='%.6f %.f	%.f')
    f.close()
    
#if __name__ == "__main__":
##    folder = r'G:\My Drive\DataSet\data_3class\bastien\together'
##    pcl2fif_offset(folder)

    
