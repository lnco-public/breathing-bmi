import numpy as np
from mne.utils.check import * 

def zscore_over_trials(data):
    array_data = np.reshape(data,-1)
    mu = np.nanmean(array_data)
    sigma = np.nanstd(array_data)
    data = (data - mu)/sigma
    return data


def bootstrap_confidence_interval(arr, ci=.95, n_bootstraps=2000,
                                  stat_fun='mean', random_state=None):
    """Get confidence intervals from non-parametric bootstrap.
    Parameters
    ----------
    arr : ndarray, shape (n_samples, ...)
        The input data on which to calculate the confidence interval.
    ci : float
        Level of the confidence interval between 0 and 1.
    n_bootstraps : int
        Number of bootstraps.
    stat_fun : str | callable
        Can be "mean", "median", or a callable operating along `axis=0`.
    random_state : int | float | array_like | None
        The seed at which to initialize the bootstrap.
    Returns
    -------
    cis : ndarray, shape (2, ...)
        Containing the lower boundary of the CI at `cis[0, ...]` and the upper
        boundary of the CI at `cis[1, ...]`.
    """
    if stat_fun == "mean":
        def stat_fun(x):
            return x.mean(axis=0)
    elif stat_fun == 'median':
        def stat_fun(x):
            return np.median(x, axis=0)
    elif not callable(stat_fun):
        raise ValueError("stat_fun must be 'mean', 'median' or callable.")
    n_trials = arr.shape[0]
    indices = np.arange(n_trials, dtype=int)  # BCA would be cool to have too
    rng = check_random_state(random_state)
    boot_indices = rng.choice(indices, replace=True,
                              size=(n_bootstraps, len(indices)))
    stat = np.array([stat_fun(arr[inds]) for inds in boot_indices])
    ci = (((1 - ci) / 2) * 100, ((1 - ((1 - ci) / 2))) * 100)
    ci_low, ci_up = np.percentile(stat, ci, axis=0)
    return np.array([ci_low, ci_up])