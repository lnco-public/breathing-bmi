
import os 
import pickle

def loading_from_listfolder(folder_selected,str2define):
    data_GA_loaded = []
    listFolders = os.listdir(folder_selected)
    for folder in listFolders: 
        if folder == 'GA':
           continue
        subjectFolder = os.path.join(folder_selected,folder)
        print(subjectFolder)

        try:
            var2Load = os.path.join(subjectFolder,str2define)
            data_subject = pickle.load(open(var2Load,'rb'))
            data_GA_loaded.append(data_subject)
        except: 
            print("cannot be loaded for the subject")  
    return data_GA_loaded


def loading_from_subfolder(folder_selected,subfolder,str2define):
    data_GA_loaded = []
    listFolders = os.listdir(folder_selected)
    for folder in listFolders: 
        if folder == 'GA':
           continue
        subjectFolder = os.path.join(folder_selected,folder,subfolder)
        print(subjectFolder)
        try:
            var2Load = os.path.join(subjectFolder,str2define)
            data_subject = pickle.load(open(var2Load,'rb'))
            data_GA_loaded.append(data_subject)
        except: 
            print("cannot be loaded for the subject")  
    return data_GA_loaded