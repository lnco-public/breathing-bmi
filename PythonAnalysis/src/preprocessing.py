# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 17:22:57 2019

@author: bastien
"""

import scipy.io as sio
import numpy as np

mat_fname = r'C:\Users\bastien\Documents\internship\Miw\gtec16\laplacian'

def apply_spatial_filter(raws,spatial_filer='CAR'):

    """ Apply spatial filtering on each raw: CAR or Laplacian"""
    if spatial_filer == 'CAR':
        raws._data -= np.mean(raws._data, axis=0)
    elif spatial_filer == 'LSF':   
        sp_matrix = sio.loadmat(mat_fname)  
        sp_matrix = sp_matrix['lap']
        raws._data = np.matmul(sp_matrix,raws._data)
    
    return raws

def temporal_filtering(raws,l_freq=1,hfreq=50):
    
    """ Apply temporal filtering, specify low and high freq"""
    raws.filter(l_freq = l_freq,h_freq = hfreq)
    return raws


def preprocessing_data(raws,cfg,picks_type=None):
    
    raws.apply_function(apply_spatial_filter,picks=picks_type)
    """ Perform preprocessing on raw data """
    raws = apply_spatial_filter(raws,cfg['spatial_filter'])
    raws = temporal_filtering(raws,int(cfg['low_freq']),int(cfg['high_freq']))
    return raws 