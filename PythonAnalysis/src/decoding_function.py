# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 00:07:45 2019

@author: basti
"""
import numpy as np
import mne
# from src.prepare_data import epoch_over_runs
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix,roc_auc_score
from sklearn.model_selection import GridSearchCV
from pprint import pprint

def get_data_from_epoch(epoch, tmin, tmax,picks=None):
  
    """ Extract data from epoch"""
    epochs_data = epoch.copy().crop(tmin=tmin, tmax=tmax)
    epochs_data = epochs_data.get_data(picks=picks)
    return epochs_data

def sliding_win(epochs_data, w_start, w_length, psde=None,channels_concat=True):

    """
    Extract samples using a sliding window

    Params 
    =====
    epochs_data: data 
    w_start: time for predifined samples
    w_length: buffer length
    psde: PSD structure from mne
    channels_concat: if True features from each channels are concatenated together

    Returns
    =====
    features_all 
	
    """
    features_all = None
    nTrial = int(epochs_data.shape[0])
    if len(w_start) == 0:
   	    w_start = [0]

    for n in w_start:
        winOfinterest = np.array(epochs_data[:, :, int(n):(int(n+w_length))]);
        if psde is not None:
            features = psde.transform(winOfinterest);
        else:
        	features = winOfinterest;
        if channels_concat:
            features = features.reshape((features.shape[0], features.shape[1] * features.shape[2]))
            features = features.reshape(nTrial,1, features.shape[1])
        else:
            features = features.reshape(nTrial,1, features.shape[1],features.shape[2]);
            
        if features_all is None:
            features_all = features
        else:
            features_all = np.concatenate((features_all,features),axis=1)
    return features_all

def reshape_features_trials(features,idx=None):

    """ Reshape features inside trial-based CV """
    if idx is None:
        X= features
    else:
        X = features[idx,:,:]
    X = X.reshape((X.shape[0]*X.shape[1], X.shape[2]));
    return X

def pipeline_pseudo_online(raws,events,psde,cfg,channels_concat=True):
    
    """ Extract features for training and pseudo-online"""
    win = find_index_for_sliding_windows(cfg)
    training = prepare_features_for_classification(raws,events,cfg['eventTraining'],\
                                                   cfg['timeTraining'],\
                                                   psde,win['training'],cfg['wlength'])
    
    testing = prepare_features_for_classification(raws,events,[cfg['eventTesting'][0]],\
                                                   cfg['timeTraining'],\
                                                   psde,win['testing'],cfg['wlength'])
    testing.update(times=win['times'])
    return training,testing
                   
def prepare_features_for_classification(raws,events,eventOfInterest,timeIntervalClass,w_start,w_length,picks=None,psde=None,channels_concat=True):

    """
    Extract samples using a sliding window

    Params 
    =====
    raws:
    events:
    eventOfInterest:
    timeIntervalClass:
    psde:
    w_start:
    w_length:
    channels_concat:
    picks:

    Returns
    =====
    training 
	
    """
    features = []
    labels = []
    nClasses = len(timeIntervalClass)
    
    if nClasses ==2 and np.size(eventOfInterest) ==1:
        eventOfInterest = [eventOfInterest]*nClasses
        
    for iClass in range(0,nClasses):
        print('Class ' + str(iClass+1) + '/ ' + str(nClasses) + ' extracting')
        epochs  = mne.Epochs(raws, events, event_id=eventOfInterest[iClass],tmin=timeIntervalClass[iClass][0]-2,
        	tmax=timeIntervalClass[iClass][1]+2,baseline=None, preload = True)
        epochs_data_train_class = get_data_from_epoch(epochs,timeIntervalClass[iClass][0],timeIntervalClass[iClass][1],picks=picks)
        features_class = sliding_win(epochs_data_train_class, w_start, w_length, psde,channels_concat=channels_concat)
        labels_class = np.ones((features_class.shape[0],features_class.shape[1]))*iClass
        labels.append(labels_class)
        features.append(features_class)
    labels,features = balance_training_for_classes(features,labels)
    labels_array = np.concatenate((labels[:]), axis=1)
    features_array = np.concatenate((features[:]), axis=1)
    training = dict(features=features_array,labels=labels_array);
    return training

def balance_training_for_classes(features,labels):
    trials_classes = []
    print('Balance classes in training dataset')
    for l in labels:
        trials_classes.append(np.size(l,0))
    # Find minimum classes
    min_trials = np.min(trials_classes)
    
    labels_temp = []
    features_temp = []
    
    for i,(f,l) in enumerate(zip(features, labels)):
        number2remove = np.shape(l)[0]-min_trials
        if number2remove > 0:
            labels_temp.append(l[:-number2remove][:]) 
            features_temp.append(f[:-number2remove,:,:])
        else:
            labels_temp.append(l) 
            features_temp.append(f)
            
    return labels_temp,features_temp

def find_index_for_sliding_windows(cfg):
    w_start_pseudo = np.arange(0, 2*cfg['timeTesting'][0][1]*cfg['sfreq']\
                               - 1*cfg['wlength'], cfg['wstep'])
    w_start = np.arange(0, 2*cfg['sfreq'] -1*cfg['wlength'], cfg['wstep'])
    w_times = (w_start_pseudo + 32+ cfg['wlength']) / cfg['sfreq'] + cfg['timeTesting'][0][0]
    win = dict(testing=w_start_pseudo,training=w_start,testing_times=w_times)
    return win 

def perform_CV_on_trial(training,testing,clf,cv,featureSelection=False,n_feat=10,verbose=True):
    
    """
    Evaluation of classifier via CV on trial-based level 

    Params 
    =====
    training: training samples for decoding
    testing: pseudo-online decoding (buffer of specific window)
    clf: classifier type
    cv: predefined cross validation object 
    verbose: print message

    Returns
    =====
    features_importance,results,yvalues 
	
    """
    trialBasedCV = range(0,training['labels'].shape[0])
    cv_split = cv.split(trialBasedCV)
    pred_windows = []
    acc_scores = []
    prob_train=[]
    ytrue = None
    ypred = None
    iFold = 1
    features_importance = []
    auc_score = []

    for train_idx, test_idx in cv_split:
        if verbose:
            print('Fold ' + str(iFold) +'/' + str(cv.n_splits) )

        """ Processing Training Dataset for trial-based CV """
        y_train, y_test = training['labels'][train_idx], training['labels'][test_idx]
        y_train = y_train.flatten()
        y_test = y_test.flatten()
        ytrueFold = y_test
            
        X_train = reshape_features_trials(training['features'],train_idx)
        X_test = reshape_features_trials(training['features'],test_idx)

        if featureSelection:
           X_train,index =  select_Xfeature(X,n_feat)
           X_test = X_test[:,index]

        clf.fit(X_train, y_train)
        acc_scores.append(clf.score(X_test,y_test))
        prob_train.append(clf.predict_proba(X_test));  
        
        """ Select Best Model based on Grid Search CV """
        if type(clf) is GridSearchCV:
            pprint(clf.best_params_)
            best_grid = clf.best_estimator_
        else:
            best_grid = clf
        
        """ Feature Importances """   
        features_importance.append(best_grid.feature_importances_)
 
        """ Confusion Matrix """
        if ytrue is None:
            ytrue = ytrueFold
            ypred = best_grid.predict(X_test)
            if training['features'].ndim == 4:
                ypred = ypred.argmax(axis=-1)
        else:
            ytrue = np.concatenate((ytrue,ytrueFold))
            yy = best_grid.predict(X_test)
            if training['features'].ndim == 4:
                yy = yy.argmax(axis=-1)
            ypred = np.concatenate((ypred,yy))
        
        """ AUC Evaluation """
        auc_score.append(roc_auc_score(ytrue, ypred))

        """ Pseudo-Online """
        if testing is not None:
            pred_this_window = []

            for iTime in range(0,testing['features'].shape[1]):
                if  training['features'].ndim == 3:
                    X_test = testing['features'][test_idx, iTime,:]     
                
                pred = best_grid.predict_proba(X_test)
                pred_this_window.append(pred)
            pred_windows.append(pred_this_window)
        
        iFold = iFold + 1;
    
    """ Saving Data """
    results = dict(accuracy=acc_scores,post_prob=pred_windows,auc=auc_score)
    yvalues = dict(true=ytrue,pred=ypred,prob=prob_train)

    """ Compute Final Confusion Matrix and Accuracy """
    if verbose:
        print('Cross Validation Accuracy ' + str(np.mean(results['accuracy'])) +  ' +/- ' + str(np.std(results['accuracy'])))
        cm = confusion_matrix(yvalues['true'],yvalues['pred'])
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print(cm)
    return features_importance,results,yvalues

def Fisher_Score(X,y):
    col_tot = np.shape(X)[1]
    class0 = np.where(y == 0)[0]
    class1 = np.where(y == 1)[0]
    X_class0,X_class1 = X[class0,:],X[class1,:]
    print(np.shape(X_class0),np.shape(X_class1))
    fs = []
    for c in range(col_tot):
        mu1,mu2 = np.mean(X_class0[:,c]),np.mean(X_class1[:,c])
        sigma1,sigma2 = np.std(X_class0[:,c]),np.std(X_class1[:,c])
        fs.append((mu1 - mu2)/(sigma1-sigma2))   
    return np.array(fs)

def select_topn_features(fs,n_feat):
    index = np.argsort(fs)
    return index[-n_feat:]

def select_Xfeature(X,n_feat):
    index = select_topn_features(fs,n_feat)
    return X[:,index],index

def training_real_time_decoding(training,clf):

    """ Training of decoder with all dataset """
    X_train,y_train = training['features'],training['labels'] 
    y_train = y_train.flatten()
    X_train =  reshape_features_trials(X_train)
    clf.fit(X_train, y_train)
    return clf

def evaluate(model, xTest, yTest):
    predictions = model.predict(xTest)
    accuracy = 100 * model.score(xTest,yTest)
    print('Model Performance')
    print('Accuracy = {:0.2f}%.'.format(accuracy))
    return accuracy


def choose_classifier(str,input_shape=None,nclass=2):

    """Selection of predefined classifier for classification"""
    if str == 'diagLDA':
        clf = LinearDiscriminantAnalysis(n_components=None, priors=None, shrinkage='auto', solver='lsqr',\
                                        store_covariance=False, tol=0.0001)
    elif str == 'RF':
        RF = dict(trees=1000, max_depth=5, seed=666);
        clf = RandomForestClassifier(n_estimators= RF['trees'], max_features='auto',\
                                max_depth=RF['max_depth'], n_jobs=-1, random_state=RF['seed'],\
                                oob_score=False, class_weight='balanced_subsample')
    return clf


def calculate_conf_matrix(ytrue,ypred,normalize=True):
    
    """Compute Confusion matrix with normalization option"""
    cm = confusion_matrix(ytrue, ypred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    return cm