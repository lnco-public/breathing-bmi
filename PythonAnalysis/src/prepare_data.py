# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 18:22:43 2019

@author: basti
"""
import os
import mne
import numpy as np


def load_fif_from_folder(folderName,concatFile=True,preload=True):
    """ Load data directly from fif file """
    raw_fnames = []
    file = []
    for f in os.listdir(folderName):
        if f.endswith('.fif'):
            raw_fnames.append(os.path.join(folderName,f))
            file.append(f)
            # print(f)
    
    raw_list = list()
    for f in raw_fnames:
        raw = mne.io.read_raw_fif(f,preload=preload)
        raw_list.append(raw)
    
    nFile = len(raw_fnames);
    print("[Load Data] Found " + str(nFile) + " files")
    raw_data = []
    events = []
    if concatFile:
        raw_data = mne.concatenate_raws(raw_list)
        raw_data.load_data()
        events = mne.find_events(raw_data, stim_channel='TRIGGER', shortest_event = 1)
    else:
        raw_data = raw_list
        for ii,iFile in enumerate(raw_list):
            raw_data[ii].load_data()
            events.append(mne.find_events(iFile, stim_channel='TRIGGER', shortest_event = 1))
    return raw_data,events

def set_montage_from_file(raw_data,cfg,kind='antneuro_64'):
    
    mne.channels.read_layout(cfg['layout_file'])
    
    channel_set = set()
    with open(cfg['montage_file']) as f:
        for l in f:
            ch = l.strip().split('\t')[3]
            channel_set.add(ch)
        channels = list(channel_set)
        f.close()
  
    chanloc = {}
    for l in open(cfg['montage_file']):
        token = l.strip().split('\t')
        ch = token[3]
        y = float(token[0])
        x = float(token[1])
        z = float(token[2])
        chanloc[ch] = [x, y, z]
    pos = np.zeros((len(channels),3))
    for i, ch in enumerate(channels):
        pos[i] = chanloc[ch]
    
    montage = mne.channels.Montage(
            pos= pos,
            ch_names=channels,
            kind=kind,
            selection=range(len(channels)))
    raw_data.set_montage(montage)
    
    return raw_data

def set_montage_from_file_mne_v23(raw_data,cfg,kind='antneuro_64'):
    
    mne.channels.read_layout(cfg['layout_file'])
    
    channel_set = set()
    with open(cfg['montage_file']) as f:
        for l in f:
            ch = l.strip().split('\t')[3]
            channel_set.add(ch)
        channels = list(channel_set)
        f.close()
  
    chanloc = {}
    for l in open(cfg['montage_file']):
        token = l.strip().split('\t')
        ch = token[3]
        y = float(token[0])
        x = float(token[1])
        z = float(token[2])
        chanloc[ch] = [x, y, z]
    pos = np.zeros((len(channels),3))
    for i, ch in enumerate(channels):
        pos[i] = chanloc[ch]
    
    chan_dict = dict()
    for cha,this_pos in zip(channels,pos):
    	chan_dict[cha] = this_pos 

    montage = mne.channels.make_dig_montage(chan_dict)
    raw_data.set_montage(montage)
    return raw_data

def prepare_data(folderName,cfg):
    raw_data,events = load_fif_from_folder(folderName)
    raw_data = set_montage_from_file(raw_data,cfg)
    return raw_data,events 