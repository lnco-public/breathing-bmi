
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import os
import sys
import mne
import pandas as pd
import math



import warnings
warnings.filterwarnings('ignore')


folderAnalysis =  r"C:\Users\bastien\Documents\Git\python_analysis_2"
sys.path.insert(0, folderAnalysis)

from scipy import stats
from sklearn import preprocessing

from src.prepare_data import load_fif_from_folder,set_montage_from_file
from src.preprocessing import preprocessing_data,apply_spatial_filter

from utils.saving_functions import *
from utils.utils_plotting import reverse_colourmap

from visualization.plotting import *

from scipy.io import loadmat 
from utils.utils_loading_data import loading_from_listfolder

folder_PYCNBI = r'C:\Users\bastien\Documents\Git\pycnbi\pycnbi'
sys.path.insert(0,folder_PYCNBI)