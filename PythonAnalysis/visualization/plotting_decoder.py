# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 16:20:06 2019

@author: bastien
"""


import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import confusion_matrix


def find_cm_classes(ytrue,ypred):
    """ Compute confusion matrix and find classes"""               
    classes = unique_labels(ytrue,ypred)
    cm = confusion_matrix(ytrue,ypred)
    return cm,classes
    
def plot_confusion_matrix(cm,classes,
                          normalize=False,
                          alreadyNormalize = False,
                          title=None,
                          cmap=plt.cm.Blues,ax=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'


    if normalize and not alreadyNormalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    if ax is None:
        fig,ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    # fig.tight_layout()
    
    return cm,ax

def plot_pseudo_online(mu_cv,w_times,label):
    
    """ Classification over time - simulation of online """
    mu = np.mean(mu_cv,0);
    sigma = np.std(mu_cv,0);
    if label is None:
        label = {0:'onset',1:'offset',2:'rest'}
    colorM = {0:(0,0,1),1:(1,0,0),2:(0,1,0)}
    faceColor = {0:'blue',1:'red',2:'green'}

    for iClass in range(0,mu.shape[1]):
        plt.plot(w_times,mu[:,iClass], label=label[iClass],color = colorM[iClass])
        plt.fill_between(w_times, mu[:,iClass]+sigma[:,iClass], mu[:,iClass]-sigma[:,iClass], facecolor=faceColor[iClass], alpha=0.3)
    plt.axvline(0, linestyle='--', color='k', label='Offset')
    plt.xlabel('Time (s)')
    plt.ylabel('Likelihood')
    plt.title('Likelihood over time')
    plt.legend(label.values(),bbox_to_anchor=(1.1, 1.05))
  
    
    
def plot_single_trial_prob(results,w_times):
    
    """Heatmap of Posterior probabilities for every trials over time """
    dim = np.shape(results['post_prob'])
    w = np.array(results['post_prob'])
    pred_windows_accross_cv = np.reshape(w[:,:,:,1],(dim[1],dim[0]*dim[2])) 
    pred_windows_accross_cv = np.array(results['post_prob'])[0,:,:,1];
    for i in range (1,10):
    	pred_windows_accross_cv = np.concatenate((pred_windows_accross_cv,np.array(results['post_prob'])[i,:,:,1]),axis=1)

    pred_windows_accross_cv = pred_windows_accross_cv.transpose()
    plt.imshow(pred_windows_accross_cv, interpolation='lanczos')
    plt.xlabel('Testing Time (s)')
    plt.ylabel('Trials')
    locs, lab = plt.xticks()
    plt.title('Posterior probabilities over time')
    plt.clim(0,1)
    plt.colorbar()
    plt.show()


def plot_features_selected(features,extent=None,interpolation=None):
    
    """Heatmap of features importance for classification"""
    plt.imshow(features,extent=extent,interpolation=interpolation)
    plt.colorbar()
    plt.xlabel('Frequencies')
    plt.ylabel('Channels')

def plot_subjects_metrics(results_cv,metrics2plot='accuracy'):
    
    """Barplot for classification accuracy over subject"""
    means_accuracy  = []
    std_accuracy = []
    for iSubject,res_subject in enumerate(results_cv):
        if metrics2plot == 'accuracy':
            accuracies = res_subject['accuracy']
        elif metrics2plot == 'auc':
            accuracies = res_subject['auc']

        means_accuracy.append(np.mean(accuracies)) 
        std_accuracy.append(np.std(accuracies))
        
    std_accuracy.append(np.std(means_accuracy,0))    
    means_accuracy.append(np.mean(means_accuracy,0))

    nSubject = np.shape(means_accuracy)[0]

    ind = np.arange(nSubject)  # the x locations for the groups
    width = 0.5    # the width of the bars
    mu = np.round(np.nanmean(means_accuracy),2)*100
    sigma = np.round(np.nanstd(means_accuracy),2)*100
    print('Cross Validation Accuracy over subjects: mu = ' + str(mu) +  ' +/- std = ' + str(sigma))
    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, means_accuracy, width, color='b', yerr=std_accuracy)
    ax.set_ylabel('Accuracy')
    ax.set_xticks(ind,)
    plt.xticks(rotation=45)
    return means_accuracy,std_accuracy

def plot_subjects_cm(yvalues,classes=None,axes=[None, None]):
    cm_GA = []
    for i,yy in enumerate(yvalues):
        cm = confusion_matrix(yy['true'],yy['pred'])
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm_GA.append(cm)
    mu = np.nanmean(cm_GA,axis=0)
    sigma = np.nanstd(cm_GA,axis=0)
    if classes is None:
        classes = unique_labels(yy['true'],yy['pred'])
    print(mu)
    print(sigma)

    plot_confusion_matrix(mu,classes,
                          normalize=True,alreadyNormalize=True,
                          title='Mean',
                          cmap=plt.cm.Blues,ax= axes[0])
    plot_confusion_matrix(sigma,classes,
                          normalize=True,alreadyNormalize=True,
                          title='Standard deviation',
                          cmap=plt.cm.Blues,ax=axes[1])
    return mu,sigma


    