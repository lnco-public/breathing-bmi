# -*- coding: utf-8 -*-
"""
Created on Thu May 23 17:02:23 2019

@author: basti
"""
from matplotlib import pyplot as plt
from visualization.plotting_decoder import *


results = results_off
yvalues = yvalues_off
""" Pseudo-online analysis""" 
fig1 = plt.figure()
w_times = (w_start_pseudo + w_step + w_length) / sfreq + cfg['feat_extract']['timeTesting'][0][0]
mu_cv = np.mean(results['post_prob'], 2);
plot_pseudo_online(mu_cv,w_times)
#plt.rc('font', size=5)
  
""" Histogram"""
fig2 = plt.figure()
prob = np.array(results['post_prob']).flatten()
plt.hist(prob,10)
#plt.rc('font', size=5)
  

""" HeatMap"""
fig3 = plt.figure()
plot_single_trial_prob(results,w_times)

""" Confusion Matrix"""
fig4 = plot_confusion_matrix(yvalues['true'], yvalues['pred'], normalize=True,
                      title='Confusion matrix, without normalization')

plt.gcf()

fileName = 'PO_analysis_average_offset'
saving_figure(fileName,folder2Save,fig1)

fileName = 'PO_analysis_histogram_offset'
saving_figure(fileName,folder2Save,fig2)

fileName = 'PO_analysis_heatmap_offset'
saving_figure(fileName,folder2Save,fig3)

fileName = 'PO_analysis_conf_mat_offset'
saving_figure(fileName,folder2Save,plt)


results = results_on
yvalues = yvalues_on
""" Pseudo-online analysis""" 
fig1 = plt.figure()
w_times = (w_start_pseudo + w_step + w_length) / sfreq + cfg['feat_extract']['timeTesting'][0][0]
plot_pseudo_online(results,w_times)

""" Histogram"""
fig2 = plt.figure()
prob = np.array(results['post_prob']).flatten()
plt.hist(prob,10)
plt.rc('font', size=5)
  
""" HeatMap"""
fig3 = plt.figure()
plot_single_trial_prob(results,w_times)
plt.rc('font', size=5)
  
""" Confusion Matrix"""
fig4 = plot_confusion_matrix(yvalues['true'], yvalues['pred'], normalize=True,
                      title='Confusion matrix, without normalization')
plt.gcf()
plt.rc('font', size=5)
  
freq = np.arange(psde.fmin,psde.fmax + 1,cfg_featExtract['wlength']/sfreq)
nFreq = len(freq)
nChannel = len(raws[0].ch_names)
ch = np.arange(nChannel,1,-1);
features_av = np.mean(fs,0);
features_av = features_av.reshape(nChannel,nFreq)
fig5 = plt.figure()
plot_features_selected(features_av,extent=[freq[0],freq[-1],1,nChannel])
plt.yticks(ch,raws[0].ch_names)
plt.rc('font', size=1)  

fileName = 'PO_analysis_average_onset'
saving_figure(fileName,folder2Save,fig1)

fileName = 'PO_analysis_histogram_onset'
saving_figure(fileName,folder2Save,fig2)

fileName = 'PO_analysis_heatmap_onset'
saving_figure(fileName,folder2Save,fig3)

fileName = 'PO_analysis_conf_mat_onset'
saving_figure(fileName,folder2Save,plt)

fileName = 'Features_importance'
saving_figure(fileName,folder2Save,fig5)


