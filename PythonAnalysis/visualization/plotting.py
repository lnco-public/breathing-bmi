  # -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 13:35:30 2019

@author: basti
"""


import matplotlib.pyplot as plt
import numpy as np
import mne

from mne.stats import permutation_cluster_1samp_test as pcluster_test
from mne.viz.utils import center_cmap
from mne.time_frequency import tfr_multitaper
from mne import pick_channels

from mne.baseline import rescale
from mpl_toolkits import axes_grid1
from mpl_toolkits.axes_grid1 import make_axes_locatable

from utils.utils_normalization import bootstrap_confidence_interval    


def compute_ERDS(epochs,baseline,freqs= np.arange(5, 30, 1),channelsOfInterest=None,mode="logratio"):
    
    if channelsOfInterest is not None:
        picks_channels = pick_channels(epochs.ch_names,channelsOfInterest)
    else:
        picks_channels = None

    # Run TF decomposition over epochs
    n_cycles = freqs  # use constant t/f resolution

    power  = tfr_multitaper(epochs, freqs=freqs, n_cycles=n_cycles,
                         use_fft=True, return_itc=False,picks = picks_channels, average=False,
                         decim=2)
    # Apply Baseline
    if mode is "logratio":
        power.apply_baseline(baseline, mode="logratio")
        power._data = 10. * power._data
    elif mode is "percent":
        power.apply_baseline(baseline, mode="percent")
        power._data = 100. * power._data
    elif  mode is "zlogratio":
        power.apply_baseline(baseline, mode="zlogratio")
        power._data = 10. * power._data

    return power

def topoplot_average_band(power,bands,interval,cmap_use=plt.cm.RdBu,scale=[-3,3]):
    
    cmap = center_cmap(cmap_use, scale[0],scale[1])
      
    fig, axes = plt.subplots(1, np.shape(bands)[0],figsize=(12, 4))
    for i,ax in enumerate(axes):
        power.plot_topomap(tmin = interval[0] , tmax=interval[1],
                           fmin=bands[i][0], fmax=bands[i][1],
                           axes=ax,
                           title= bands[i][2], 
                           cmap=(cmap, False),
                           vmin=scale[0], vmax=scale[1],
                           colorbar=False,show=False,show_names = False)
    
    fig.show()
    return fig


def plot_topoplot_over_time(power,timeOfInterest,fmin,fmax,cmap_use=plt.cm.RdBu,scale = [-1,1],title=None):
    freqs = np.arange(fmin-1, fmax+1, 1)
    n_cycles = freqs  # use constant t/f resolution
    kwargs = dict(n_permutations=100, step_down_p=0.05, seed=1,
              buffer_size=None)  # for cluster test
    cmap = center_cmap(cmap_use, scale[0], scale[1])  # zero maps to white
     
    fig, axes = plt.subplots(1, len(timeOfInterest)+1,figsize=(12, 4))
    for i,ax in enumerate(axes[:-1]):
        thisTime = timeOfInterest[i]
        power.plot_topomap(tmin = thisTime-0.25 , tmax=thisTime+0.25,
                           fmin=fmin, fmax=fmax,
                           axes=ax,
                           title=str(thisTime), 
                           cmap=(cmap, False),
                           vmin=scale[0], vmax=scale[1],
                           colorbar=False,show=False,show_names = False)
    axes[-1].axis('off')
    divider = make_axes_locatable(axes[-1])
    cax = divider.append_axes('left', size='40%')
    cb = fig.colorbar(axes[0].images[-1], cax=cax, pad=0.05)
    cb.ax.set_ylabel('ERDS[dB]')
    fig.suptitle(title,y=0.9)
    fig.show()
    return fig

def plotting_ERDS(power,channelsOfInterest,cmap_use=plt.cm.RdBu,title=None,scale = [-5,5],permutation = True):
    
    cmap = center_cmap(cmap_use, scale[0], scale[1])  # zero maps to white
    kwargs = dict(n_permutations=1000, step_down_p=0.05, seed=1,
              buffer_size=None)  # for cluster test
    
    nChannel = len(channelsOfInterest)
    ratio_plt = 10*np.ones((1,nChannel))
    # Visualization
    fig, axes = plt.subplots(1, nChannel+1, figsize=(12,2),
                                 gridspec_kw={"width_ratios": np.append(ratio_plt,1)})
    
    idx_chan = mne.pick_channels(power.ch_names,channelsOfInterest,ordered=True)
    for ch, ax in enumerate(axes[:-1]):
        if permutation:
            # positive clusters
            _, c1, p1, _ = pcluster_test(power.data[:, idx_chan[ch], ...], tail=1, **kwargs)
            # negative clusters
            _, c2, p2, _ = pcluster_test(power.data[:, idx_chan[ch], ...], tail=-1,**kwargs)
            
            #print(np.shape(c1),np.shape(c2))
            # note that we keep clusters with p <= 0.05 from the combined clusters
            # of two independent tests; in this example, we do not correct for
            # these two comparisons
            if np.shape(c1)[0] == 0:
                c1 = np.zeros(np.shape(c2))
            elif np.shape(c2)[0] == 0:
                c2 = np.zeros(np.shape(c1))

            c = np.stack(c1 + c2, axis=2)  # combined clusters
            p = np.concatenate((p1, p2))  # combined p-values
            
            mask = c[..., p <= 0.05].any(axis=-1)

            # plot TFR (ERDS map with masking)
            power.average().plot([idx_chan[ch]], cmap=(cmap, False),vmin=scale[0], vmax=scale[1],
                       axes=ax, colorbar=False, show=False, mask=mask,
                       mask_style="mask")
        else:
            power.average().plot([idx_chan[ch]], vmin=scale[0], vmax=scale[1], cmap=(cmap, False),
                       axes=ax, colorbar=False, show=False)

        ax.set_title(power.ch_names[idx_chan[ch]], fontsize=10)
        ax.axvline(0, linewidth=1, color="black", linestyle=":")  # event
        if not ax.is_first_col():
            ax.set_ylabel("")
            ax.set_yticklabels("")
    cb = fig.colorbar(axes[0].images[-1], cax=axes[-1], pad=0.05)
    cb.ax.set_ylabel('ERDS[dB]')
    fig.suptitle(title,y=1.2)
    fig.show()
    return fig


def plotting_bandpower(raws,events,eventOfInterest,iter_freqs,chan=None):
    tmin, tmax = -2., 5.
    baseline = (-2,0)
    frequency_map = list()
    channelsOfInterest = ['FC3','C3','CP3']
    raws_preprocessed = raws.copy()
    if chan is not None:
        picks_bandpower = mne.pick_channels(raws_preprocessed.ch_names,channelsOfInterest)
        raws_preprocessed = raws_preprocessed.pick(picks_bandpower)

    for band, fmin, fmax in iter_freqs:
        # bandpass filter
        raws_filterband = raws.copy()
        raws_filterband.filter(fmin, fmax, n_jobs=1,  # use more jobs to speed up.
                   l_trans_bandwidth=1,  # make sure filter params are the same
                   h_trans_bandwidth=1)  # in each band and skip "auto" option.
        # epoch
        epochs_filterband = mne.Epochs(raws_filterband, events, eventOfInterest, tmin, tmax, baseline=baseline,preload=True)
        # remove evoked response
        epochs_filterband.subtract_evoked()
        # get analytic signal (envelope)
        epochs_filterband.apply_hilbert(envelope=True)
        frequency_map.append(((band, fmin, fmax), epochs_filterband.average()))
        del epochs_filterband
    del raws_filterband

    # Plot
    nFreq = len(iter_freqs)
    fig, axes = plt.subplots(nFreq, 1, figsize=(10, 7), sharex=True, sharey=True)
    colors = plt.get_cmap('winter_r')(np.linspace(0, 1, 4))
    for ((freq_name, fmin, fmax), average), color, ax in zip(
        frequency_map, colors, axes.ravel()[::-1]):
        times = average.times * 1e3
        gfp = np.sum(average.data ** 2, axis=0)
        gfp = mne.baseline.rescale(gfp, times, baseline=(None, 0))
        ax.plot(times, gfp, label=freq_name, color=color, linewidth=2.5)
        ax.axhline(0, linestyle='--', color='grey', linewidth=2)
        ci_low, ci_up = bootstrap_confidence_interval(average.data, random_state=0,
                                                      stat_fun="mean")
        ci_low = rescale(ci_low, average.times, baseline=(None, 0))
        ci_up = rescale(ci_up, average.times, baseline=(None, 0))
        ax.fill_between(times, gfp + ci_up, gfp - ci_low, color=color, alpha=0.3)
        ax.grid(True)
        ax.set_ylabel('GFP')
        ax.annotate('%s (%d-%dHz)' % (freq_name, fmin, fmax),
                xy=(0.95, 0.8),
                horizontalalignment='right',
                xycoords='axes fraction')
        ax.set_xlim(tmin*1000, tmax*1000)

    axes.ravel()[-1].set_xlabel('Time [ms]')
    return fig

def stat_fun(x):
    """Return sum of squares."""
    return np.sum(x ** 2, axis=0)

