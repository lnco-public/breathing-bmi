# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 19:33:11 2019

@author: basti
"""

from matplotlib import pyplot as plt
import numpy as np

def plot_channels(raw,interval=100000):

    data = raw._data
    
    fig = plt.figure(tight_layout=True,figsize=(10, 10))
    posY = []
    for i in range(0,len(raw.ch_names)):
        offset = i*interval
        plt.plot(data[i,:] + offset)
        posY.append(np.mean(data[i,:]) + offset)
        
    plt.title('Sample channels')
    plt.ylabel('Channels')
    plt.xlabel('Time')
    plt.yticks(posY,raw.ch_names)


def plot_correlation_between_channels(raw):

    data = raw._data
    nChannel = len(raw.ch_names)
    
    fig = plt.figure(tight_layout=True,figsize=(10, 10))
    value_corr= np.corrcoef(data)
    plt.xticks(np.arange(nChannel),raw.ch_names)
    plt.yticks(np.arange(nChannel),raw.ch_names)
    plt.imshow(value_corr)
    plt.colorbar()
    plt.show()